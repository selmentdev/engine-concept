#include "Benchmark/Benchmark.hxx"
#include "Benchmark/Timing.hxx"
#include <Graphyte/Threading/Thread.hxx>
#include <Graphyte/Diagnostic/Stopwatch.hxx>

namespace Benchmark
{
    void Timing()
    {
        GX_LOG(LogBenchmark, Verbose, "Timing benchmark\n");
        {
            auto start = Graphyte::System::Time::GetSeconds();
            {
                Graphyte::Threading::Thread::Sleep(1000);
            }
            auto end = Graphyte::System::Time::GetSeconds();
            auto diff = end - start;

            GX_LOG(LogBenchmark, Verbose, "Native val: {}\n", diff);
        }

        {
            auto start = Graphyte::System::Time::GetTimestamp();
            {
                Graphyte::Threading::Thread::Sleep(1000);
            }
            auto end = Graphyte::System::Time::GetTimestamp();
            auto diff = end - start;

            GX_LOG(LogBenchmark, Verbose, "Native: freq {}, val: {}\n", Graphyte::System::Time::GetTimestampResolution(), diff);
        }
        {
            Graphyte::Diagnostic::Stopwatch sw{};
            sw.Start();
            {
                Graphyte::Threading::Thread::Sleep(1000);
            }
            sw.Stop();
            GX_LOG(LogBenchmark, Verbose, "Sleep(1000) took: {} s\n", sw.GetElapsedTime());
        }
    }
}
