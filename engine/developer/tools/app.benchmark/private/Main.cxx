#include <Graphyte/Base.module.hxx>
#include <Graphyte/System/ApplicationProperties.hxx>
#include <Graphyte/System/SystemProperties.hxx>
#include <Graphyte/Threading/TaskDispatcher.hxx>
#include <Graphyte/Threading/ParallelFor.hxx>
#include <Graphyte/Diagnostic/Profiler.hxx>
#include <Graphyte/Math/Scalar.hxx>
#include <Graphyte/Storage/FileManager.hxx>
#include "Benchmark/Benchmark.hxx"
#include "Benchmark/Timing.hxx"

//
//  Enable NVidia Optimus support for the current executable.
//
//  Link:
//      http://developer.download.nvidia.com/devzone/devcenter/gamegraphics/files/OptimusRenderingPolicies.pdf
//

extern "C" { GX_LIB_EXPORT uint32_t NvOptimusEnablement = 0x00000001; }
extern "C" { GX_LIB_EXPORT uint32_t AmdPowerXpressRequestHighPerformance = 0x00000001; }

#if GRAPHYTE_PLATFORM_WINDOWS
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
#else
int main(int argc, char* argv[])
#endif
{
#if GRAPHYTE_PLATFORM_WINDOWS

    (void)hInstance;
    (void)hPrevInstance;
    (void)lpCmdLine;
    (void)nShowCmd;
#else
    void* hInstance = nullptr;
    (void)argc;
    (void)argv;
#endif

    Graphyte::System::SystemProperties::Initialize();
    Graphyte::System::ApplicationProperties::SetInstanceHandle(reinterpret_cast<void*>(hInstance));
    Graphyte::System::ApplicationProperties::SetApplicationName("Graphyte Benchmark App");
    Graphyte::System::ApplicationProperties::SetApplicationId("graphyte.benchmark.demo");
    Graphyte::System::ApplicationProperties::SetApplicationVersion(Graphyte::Version{ 1, 0, 0, 0 });
    Graphyte::System::ApplicationProperties::SetCompanyName("graphyte");

    Graphyte::System::ApplicationProperties::Initialize();

    Graphyte::Threading::TaskDispatcher::Initialize();

    Graphyte::Threading::Thread::Sleep(1000);

    Graphyte::Storage::FileManager::GetEngineDirectory();

    {
        std::atomic<uint32_t> cnt{};
        GX_PROFILE_REGION("Parallel");
        Graphyte::Threading::ParallelFor(331, [&](uint32_t index)
        {
            GX_PROFILE_REGION(fmt::format("test-{}", index).c_str());
            Graphyte::Threading::Thread::Sleep(120);
            ++cnt;
        });

        GX_ASSERT(cnt == 331);

        GX_PROFILE_REGION("Parallel");
        Graphyte::Threading::ParallelFor(320, [&](uint32_t index)
        {
            GX_PROFILE_REGION(fmt::format("test-{}", index).c_str());
            Graphyte::Threading::Thread::Sleep(120);
        }, [&]()
        {
            GX_PROFILE_REGION("Sleeping with prework");
            Graphyte::Threading::Thread::Sleep(4000);
        });
    }

#if 0
    {
        GX_PROFILE_REGION("Serial");
        Graphyte::Threading::ParallelFor(3200, [&](uint32_t index)
        {
            GX_PROFILE_REGION(fmt::format("test-{}", index).c_str());

            double total = 0.0;

            for (size_t i = 0; i < 10000000; i++)
            {
                total += Graphyte::Math::Tan((float)(i + index));
            }

            gtotal += total;
        }, true);
    }

    GX_LOG(LogBenchmark, Verbose, "Double: {}\n", gtotal);


    GX_LOG(LogBenchmark, Verbose, "--> benchmarking...\n");
    {
        Benchmark::Timing();
    }
    GX_LOG(LogBenchmark, Verbose, "--< benchmarking...\n");

    Graphyte::System::ApplicationProperties::RequestExit(false);

    Graphyte::System::SystemProperties::Shutdown();
#endif
    return 0;
}
