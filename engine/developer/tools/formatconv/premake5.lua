project "formatconv"
    targetname "Tool-FormatConv"
    kind "ConsoleApp"

    graphyte_app {}

    files {
        "private/**.?xx",
        "public/**.?xx",
        "*.lua",
    }

    tags {
        --"sdks-mpack",
        --"sdks-lua",
        --"sdks-sol2",
    }

    use_com_graphyte_base()
    use_com_graphyte_launch()
    use_com_graphyte_framework()
    use_com_graphyte_graphics()
    use_com_graphyte_assets_base()
