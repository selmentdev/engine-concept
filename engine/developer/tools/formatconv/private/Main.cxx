#include <Graphyte/Launch/Main.hxx>

Graphyte::Application::ApplicationDescriptor GraphyteApp
{
    "Graphyte FormatConverter",
    "formatconv",
    "Graphyte",
    Graphyte::Application::ApplicationType::ConsoleTool,
    Graphyte::Version{ 1, 0, 0, 0 }
};
GX_DECLARE_LOG_CATEGORY(LogFormatConv, Trace, Trace);
GX_DEFINE_LOG_CATEGORY(LogFormatConv);

#if false
#include <Graphyte/Span.hxx>
#include <Graphyte/Storage/FileManager.hxx>
#include <Graphyte/Serialization/DataSerializer.hxx>


// -------------------------------------------------------------------------------------------------
// rapidjson
#include "../../sdks/RapidJSON/include/rapidjson/reader.h"
#include "../../sdks/RapidJSON/include/rapidjson/writer.h"
// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------
#include <sstream>

#include <mpack/mpack-expect.h>
#include <mpack/mpack-reader.h>
#include <mpack/mpack-writer.h>


#include <Graphyte/Diagnostics.hxx>
#include <Graphyte/Crypto/Random.hxx>


namespace Graphyte::Data
{
    // NOTE:
    //
    // - Data Reader/Writer must ensure that map key is string! mpack allows it to be anything, json
    //   requires string
    //
    // - Archive interface is so wrong at many levels
    //
    // - proposed API should work on already loaded data?
    //      - it could be faster when using notstd::span<std::byte> for reading
    //
    class DataWriter
    {
    public:
        DataWriter() noexcept = default;
        virtual ~DataWriter() noexcept = default;

    public:
        virtual void Flush() noexcept = 0;

    public:
        virtual void StartObject(uint32_t count) noexcept = 0;
        virtual void EndObject() noexcept = 0;

        virtual void StartArray(uint32_t count) noexcept = 0;
        virtual void EndArray() noexcept = 0;

        virtual void WriteKey(std::string_view value) noexcept = 0;

        virtual void WriteBoolean(bool value) noexcept = 0;
        virtual void WriteNull() noexcept = 0;
        virtual void WriteFloat(float value) noexcept = 0;
        virtual void WriteDouble(double value) noexcept = 0;

        virtual void WriteUInt8(uint8_t value) noexcept = 0;
        virtual void WriteUInt16(uint16_t value) noexcept = 0;
        virtual void WriteUInt32(uint32_t value) noexcept = 0;
        virtual void WriteUInt64(uint64_t value) noexcept = 0;

        virtual void WriteInt8(int8_t value) noexcept = 0;
        virtual void WriteInt16(int16_t value) noexcept = 0;
        virtual void WriteInt32(int32_t value) noexcept = 0;
        virtual void WriteInt64(int64_t value) noexcept = 0;

        virtual void WriteString(std::string_view value) noexcept = 0;
        virtual void WriteBinary(notstd::span<const std::byte> value) noexcept = 0;

        virtual bool Close() noexcept = 0;
    };

    class DataReader
    {
    public:
        DataReader() noexcept = default;
        virtual ~DataReader() noexcept = default;

    public:
        virtual void StartObject() noexcept = 0;
        virtual void EndObject() noexcept = 0;
        virtual void StartArray() noexcept = 0;
        virtual void EndArray() noexcept = 0;

        virtual void ReadKey(std::string& value) noexcept = 0;

        virtual void ReadUInt32(uint32_t& value) noexcept = 0;
        virtual void ReadString(std::string& value) noexcept = 0;

        virtual bool Close() noexcept = 0;
    };

    class MPackDataWriter final : public DataWriter
    {
    private:
        mpack_writer_t m_Writer;
        std::unique_ptr<std::byte[]> m_Buffer;
        Storage::Archive& m_Archive;

        static constexpr const size_t BufferSize = 0x2000;

        static void FlushCallback(mpack_writer_t* writer, const char* buffer, size_t count) noexcept
        {
            auto* self = reinterpret_cast<MPackDataWriter*>(mpack_writer_context(writer));
            self->m_Archive.Serialize(const_cast<char*>(buffer), count);
        }

    public:
        MPackDataWriter(Storage::Archive& archive) noexcept
            : m_Writer{}
            , m_Buffer{ std::make_unique<std::byte[]>(BufferSize) }
            , m_Archive{ archive }
        {
            mpack_writer_init(&m_Writer, reinterpret_cast<char*>(m_Buffer.get()), BufferSize);
            mpack_writer_set_context(&m_Writer, this);
            mpack_writer_set_flush(&m_Writer, &MPackDataWriter::FlushCallback);
        }

        virtual ~MPackDataWriter() noexcept
        {
            mpack_writer_destroy(&m_Writer);
        }

        virtual void Flush() noexcept override
        {
            mpack_writer_flush_message(&m_Writer);
            m_Archive.Flush();
        }

        virtual void StartObject(uint32_t count) noexcept override
        {
            mpack_start_map(&m_Writer, count);
        }

        virtual void EndObject() noexcept override
        {
            mpack_finish_map(&m_Writer);
        }

        virtual void StartArray(uint32_t count) noexcept override
        {
            mpack_start_array(&m_Writer, count);
        }

        virtual void EndArray() noexcept override
        {
            mpack_finish_array(&m_Writer);
        }

        virtual void WriteKey(std::string_view value) noexcept override
        {
            mpack_write_str(&m_Writer, value.data(), static_cast<uint32_t>(value.size()));
        }

        virtual void WriteBoolean(bool value) noexcept override
        {
            mpack_write_bool(&m_Writer, value);
        }

        virtual void WriteNull() noexcept override
        {
            mpack_write_nil(&m_Writer);
        }

        virtual void WriteFloat(float value) noexcept override
        {
            mpack_write_float(&m_Writer, value);
        }

        virtual void WriteDouble(double value) noexcept override
        {
            mpack_write_double(&m_Writer, value);
        }

        virtual void WriteUInt8(uint8_t value) noexcept override
        {
            mpack_write_u8(&m_Writer, value);
        }

        virtual void WriteUInt16(uint16_t value) noexcept override
        {
            mpack_write_u32(&m_Writer, value);
        }

        virtual void WriteUInt32(uint32_t value) noexcept override
        {
            mpack_write_u32(&m_Writer, value);
        }

        virtual void WriteUInt64(uint64_t value) noexcept override
        {
            mpack_write_u64(&m_Writer, value);
        }

        virtual void WriteInt8(int8_t value) noexcept override
        {
            mpack_write_i8(&m_Writer, value);
        }

        virtual void WriteInt16(int16_t value) noexcept override
        {
            mpack_write_i32(&m_Writer, value);
        }

        virtual void WriteInt32(int32_t value) noexcept override
        {
            mpack_write_i32(&m_Writer, value);
        }

        virtual void WriteInt64(int64_t value) noexcept override
        {
            mpack_write_i64(&m_Writer, value);
        }

        virtual void WriteString(std::string_view value) noexcept override
        {
            mpack_write_str(&m_Writer, value.data(), static_cast<uint32_t>(value.size()));
        }

        virtual void WriteBinary(notstd::span<const std::byte> value) noexcept override
        {
            mpack_write_bin(&m_Writer, reinterpret_cast<const char*>(value.data()), static_cast<uint32_t>(value.size()));
        }

        virtual bool Close() noexcept override
        {
            return mpack_writer_destroy(&m_Writer) == mpack_ok;
        }
    };

    class MPackDataReader final : public DataReader
    {
    private:
        mpack_reader_t m_Reader;
        std::unique_ptr<std::byte[]> m_Buffer;
        Storage::Archive& m_Archive;

        static constexpr const size_t BufferSize = 0x2000;

        static size_t ReadBuffer(mpack_reader_t* reader, char* buffer, size_t count) noexcept
        {
            auto* self = reinterpret_cast<MPackDataReader*>(mpack_reader_context(reader));
            auto position_before = self->m_Archive.GetPosition();
            auto size = std::min<size_t>(count, static_cast<size_t>(self->m_Archive.GetSize() - position_before));
            self->m_Archive.Serialize(buffer, size);
            auto position_after = self->m_Archive.GetPosition();
            return static_cast<size_t>(position_after - position_before);
        }

        static void SeekBuffer(mpack_reader_t* reader, size_t count) noexcept
        {
            auto* self = reinterpret_cast<MPackDataReader*>(mpack_reader_context(reader));
            self->m_Archive.SetPosition(self->m_Archive.GetPosition() + static_cast<ptrdiff_t>(count));
        }

    public:
        MPackDataReader(Storage::Archive& archive) noexcept
            : m_Reader{}
            , m_Buffer{ std::make_unique<std::byte[]>(BufferSize) }
            , m_Archive{ archive }
        {
            mpack_reader_init(&m_Reader, reinterpret_cast<char*>(m_Buffer.get()), BufferSize, 0);
            mpack_reader_set_context(&m_Reader, this);
            mpack_reader_set_skip(&m_Reader, MPackDataReader::SeekBuffer);
            mpack_reader_set_fill(&m_Reader, MPackDataReader::ReadBuffer);
        }

        virtual ~MPackDataReader() noexcept
        {
            mpack_reader_destroy(&m_Reader);
        }

    public:
        virtual void StartObject() noexcept override
        {
            mpack_expect_map(&m_Reader);
        }

        virtual void EndObject() noexcept override
        {
            mpack_done_map(&m_Reader);
        }

        virtual void StartArray() noexcept override
        {
            mpack_expect_array(&m_Reader);
        }

        virtual void EndArray() noexcept override
        {
            mpack_done_array(&m_Reader);
        }

        virtual void ReadKey(std::string& value) noexcept override
        {
            uint32_t length = mpack_expect_str(&m_Reader);
            value.resize(length);
            mpack_read_bytes(&m_Reader, value.data(), value.size());
            mpack_done_str(&m_Reader);
        }

        virtual void ReadUInt32(uint32_t& value) noexcept override
        {
            value = mpack_expect_u32(&m_Reader);
        }

        virtual void ReadString(std::string& value) noexcept override
        {
            uint32_t length = mpack_expect_str(&m_Reader);
            value.resize(length);
            mpack_read_bytes(&m_Reader, value.data(), value.length());
            mpack_done_str(&m_Reader);
        }

        virtual bool Close() noexcept override
        {
            return mpack_reader_destroy(&m_Reader) == mpack_ok;
        }
    };

    class JsonDataWriter final : public DataWriter
    {
    private:
        Storage::Archive& m_Archive;
        rapidjson::StringBuffer m_Buffer;
        rapidjson::Writer<rapidjson::StringBuffer> m_Writer;

    public:
        JsonDataWriter(Storage::Archive& archive) noexcept
            : m_Archive{ archive }
            , m_Buffer{}
            , m_Writer{ m_Buffer }
        {
        }

        virtual ~JsonDataWriter() noexcept = default;

        virtual void Flush() noexcept override {}

    public:
        virtual void StartObject(uint32_t count) noexcept override
        {
            (void)count;
            m_Writer.StartObject();
        }

        virtual void EndObject() noexcept override
        {
            m_Writer.EndObject();
        }

        virtual void StartArray(uint32_t count) noexcept override
        {
            (void)count;
            m_Writer.StartArray();
        }

        virtual void EndArray() noexcept override
        {
            m_Writer.EndArray();
        }

        virtual void WriteKey(std::string_view value) noexcept override
        {
            m_Writer.Key(value.data(), static_cast<rapidjson::SizeType>(value.size()));
        }

        virtual void WriteBoolean(bool value) noexcept override
        {
            m_Writer.Bool(value);
        }

        virtual void WriteNull() noexcept override
        {
            m_Writer.Null();
        }

        virtual void WriteFloat(float value) noexcept override
        {
            m_Writer.Double(value);
        }

        virtual void WriteDouble(double value) noexcept override
        {
            m_Writer.Double(value);
        }

        virtual void WriteUInt8(uint8_t value) noexcept override
        {
            m_Writer.Uint(value);
        }

        virtual void WriteUInt16(uint16_t value) noexcept override
        {
            m_Writer.Uint(value);
        }

        virtual void WriteUInt32(uint32_t value) noexcept override
        {
            m_Writer.Uint(value);
        }

        virtual void WriteUInt64(uint64_t value) noexcept override
        {
            m_Writer.Uint64(value);
        }

        virtual void WriteInt8(int8_t value) noexcept override
        {
            m_Writer.Int(value);
        }

        virtual void WriteInt16(int16_t value) noexcept override
        {
            m_Writer.Int(value);
        }

        virtual void WriteInt32(int32_t value) noexcept override
        {
            m_Writer.Int(value);
        }

        virtual void WriteInt64(int64_t value) noexcept override
        {
            m_Writer.Int64(value);
        }

        virtual void WriteString(std::string_view value) noexcept override
        {
            m_Writer.String(value.data(), static_cast<rapidjson::SizeType>(value.size()));
        }

        virtual void WriteBinary(notstd::span<const std::byte> value) noexcept override
        {
            (void)value;
        }

        virtual bool Close() noexcept override
        {
            m_Archive.Serialize(
                const_cast<char*>(m_Buffer.GetString()),
                m_Buffer.GetSize()
            );
            return !m_Archive.IsError();
        }
    };

    class RapidjsonArchiveWrapper
    {
    private:
        Graphyte::Storage::Archive& m_Archive;
        char m_Peek;
        bool m_HasPeek;

    public:
        RapidjsonArchiveWrapper(Graphyte::Storage::Archive& archive)
            : m_Archive{ archive }
            , m_Peek{ '\0' }
            , m_HasPeek{ false }
        {
        }

        using Ch = char;

        //! Read the current character from stream without moving the read cursor.
        Ch Peek()
        {
            if (!m_HasPeek)
            {
                m_Archive.Serialize(&m_Peek, sizeof(m_Peek));
                m_HasPeek = true;

                if (m_Archive.IsError())
                {
                    m_Peek = '\0';
                }
            }

            return m_Peek;
        }
        //! Read the current character from stream and moving the read cursor to next character.
        Ch Take()
        {
            if (m_HasPeek)
            {
                m_HasPeek = false;
            }
            else
            {
                m_Archive.Serialize(&m_Peek, sizeof(m_Peek));

                if (m_Archive.IsError())
                {
                    m_Peek = '\0';
                }
            }

            return m_Peek;
        }

        //! Get the current read cursor.
        //! \return Number of characters read from start.
        size_t Tell()
        {
            return static_cast<size_t>(m_Archive.GetPosition());
        }
        //! Begin writing operation at the current read pointer.
        //! \return The begin writer pointer.
        Ch* PutBegin()
        {
            GX_ASSERT(false);
            return nullptr;
        }
        //! Write a character.
        void Put(Ch c)
        {
            GX_ASSERT(false);
            (void)c;
        }

        //! Flush the buffer.
        void Flush()
        {
            GX_ASSERT(false);
        }
        //! End the writing operation.
        //! \param begin The begin write pointer returned by PutBegin().
        //! \return Number of characters written.
        size_t PutEnd(Ch* begin)
        {
            (void)begin;
            GX_ASSERT(false);
            return 0;
        }
    };


    class JsonDataReader : public DataReader
    {
    private:
        enum class DataType {
            Null,
            Bool,
            Int32,
            UInt32,
            Int64,
            UInt64,
            Double,
            String,
            StartObject,
            Key,
            EndObject,
            StartArray,
            EndArray,
        };

        struct Handler {
            DataType Type;

            bool AsBoolean;
            uint32_t AsUInt32;
            uint64_t AsUInt64;
            int32_t AsInt32;
            int64_t AsInt64;
            double AsDouble;
            std::string_view AsString;
            std::string_view AsKey;

            bool Null()
            {
                Type = DataType::Null;
                return true;
            }

            bool Bool(bool value)
            {
                Type = DataType::Bool;
                AsBoolean = value;
                return true;
            }

            bool Uint(unsigned value)
            {
                Type = DataType::UInt32;
                AsUInt32 = value;
                return true;
            }

            bool Uint64(uint64_t value)
            {
                Type = DataType::UInt64;
                AsUInt64 = value;
                return true;
            }

            bool Int(int value)
            {
                Type = DataType::Int32;
                AsInt32 = value;
                return true;
            }

            bool Int64(int64_t value)
            {
                Type = DataType::Int64;
                AsInt64 = value;
                return true;
            }

            bool Double(double value)
            {
                Type = DataType::Double;
                AsDouble = value;
                return true;
            }

            bool RawNumber(const char*, rapidjson::SizeType, bool)
            {
                GX_ASSERT(false);
                return false;
            }

            bool String(const char* str, rapidjson::SizeType size, bool)
            {
                Type = DataType::String;
                AsString = std::string_view{ str, size };
                return true;
            }

            bool StartObject()
            {
                Type = DataType::StartObject;
                return true;
            }

            bool Key(const char* str, rapidjson::SizeType size, bool)
            {
                Type = DataType::String;
                AsKey = std::string_view{ str, size };
                return true;
            }

            bool EndObject(rapidjson::SizeType)
            {
                Type = DataType::EndObject;
                return true;
            }

            bool StartArray()
            {
                Type = DataType::StartArray;
                return true;
            }

            bool EndArray(rapidjson::SizeType)
            {
                Type = DataType::EndArray;
                return true;
            }
        } m_Handler;

    private:
        rapidjson::Reader m_Reader;
        RapidjsonArchiveWrapper m_Buffer;
    public:
        JsonDataReader(Storage::Archive& archive) noexcept
            : m_Handler{}
            , m_Reader{}
            , m_Buffer{ archive }
        {
            m_Reader.IterativeParseInit();
        }

        virtual ~JsonDataReader() noexcept
        {
        }

    private:
        bool ReadNext() noexcept
        {
            if (!m_Reader.IterativeParseNext<rapidjson::kParseDefaultFlags | rapidjson::kParseCommentsFlag>(m_Buffer, m_Handler))
            {
                GX_ASSERT(false);
                return false;
            }

            return true;
        }

    public:
        virtual void StartObject() noexcept
        {
            ReadNext();
            GX_ASSERT(this->m_Handler.Type == DataType::StartObject);
        }
        virtual void EndObject() noexcept
        {
            ReadNext();
            GX_ASSERT(this->m_Handler.Type == DataType::EndObject);
        }
        virtual void StartArray() noexcept
        {
            ReadNext();
            GX_ASSERT(this->m_Handler.Type == DataType::StartArray);
        }
        virtual void EndArray() noexcept
        {
            ReadNext();
            GX_ASSERT(this->m_Handler.Type == DataType::EndArray);
        }

        virtual void ReadKey(std::string& value) noexcept
        {
            ReadNext();
            GX_ASSERT(this->m_Handler.Type == DataType::String);
            value = this->m_Handler.AsKey;
        }

        virtual void ReadUInt32(uint32_t& value) noexcept
        {
            ReadNext();
            GX_ASSERT(this->m_Handler.Type == DataType::UInt32);
            value = this->m_Handler.AsUInt32;
        }
        virtual void ReadString(std::string& value) noexcept
        {
            ReadNext();
            GX_ASSERT(this->m_Handler.Type == DataType::String);
            value = this->m_Handler.AsString;
        }

        virtual bool Close() noexcept
        {
            return m_Reader.IterativeParseComplete();
        }
    };

#if false
    namespace Impl
    {

        using namespace rapidjson;
        using namespace std;

        template <typename T> std::string stringify(T x) {
            std::stringstream ss;
            ss << x;
            return ss.str();
        }

        struct MyHandler {
            const char* type;
            std::string data;

            MyHandler() : type(), data() {}

            bool Null() { type = "Null"; data.clear(); return true; }
            bool Bool(bool b) { type = "Bool:"; data = b ? "true" : "false"; return true; }
            bool Int(int i) { type = "Int:"; data = stringify(i); return true; }
            bool Uint(unsigned u) { type = "Uint:"; data = stringify(u); return true; }
            bool Int64(int64_t i) { type = "Int64:"; data = stringify(i); return true; }
            bool Uint64(uint64_t u) { type = "Uint64:"; data = stringify(u); return true; }
            bool Double(double d) { type = "Double:"; data = stringify(d); return true; }
            bool RawNumber(const char* str, SizeType length, bool) { type = "Number:"; data = std::string(str, length); return true; }
            bool String(const char* str, SizeType length, bool) { type = "String:"; data = std::string(str, length); return true; }
            bool StartObject() { type = "StartObject"; data.clear(); return true; }
            bool Key(const char* str, SizeType length, bool) { type = "Key:"; data = std::string(str, length); return true; }
            bool EndObject(SizeType memberCount) { type = "EndObject:"; data = stringify(memberCount); return true; }
            bool StartArray() { type = "StartArray"; data.clear(); return true; }
            bool EndArray(SizeType elementCount) { type = "EndArray:"; data = stringify(elementCount); return true; }
        private:
            MyHandler(const MyHandler& noCopyConstruction);
            MyHandler& operator=(const MyHandler& noAssignment);
        };
    }

    class JsonDataReader
    {
    public:
        void Parse()
        {
            const char json[] = R"({
    "activity" : {
        "name": "test",
        "id": "2137",
        "items": [
            {
                "type": "panel",
                "halign":"left",
                "valign":"top",
                "items": [
                    {
                        "type": "button",
                        "text": "1",
                        "id": null
                    },
                    {
                        "type": "button",
                        "text": 2,
                        "id": false
                    },
                    {
                        "type": "button",
                        "text": "3",
                        "id": "3"
                    }
                ]
            }
        ]
    }
})";

            Impl::MyHandler handler;
            rapidjson::Reader reader;
            rapidjson::StringStream ss(json);
            reader.IterativeParseInit();
            while (!reader.IterativeParseComplete()) {
                reader.IterativeParseNext<rapidjson::kParseDefaultFlags>(ss, handler);
                //std::cout << handler.type << handler.data << endl;

                GX_LOG(LogFormatConv, Info, "{} = {}\n", handler.type, handler.data);
            }
        }
    };
#endif
}

#include <Graphyte/Storage/FileManager.hxx>

struct testing
{
    int a;
    int b;
    char c;
    const char* d;
};

void do_something(std::string_view value)
{
    GX_LOG(LogFormatConv, Trace, "Testing lua {}!\n", value);
}

#include <Graphyte/NewMath/Vec4.hxx>

static_assert(Graphyte::NewMath::Impl::SimdVectorType<Graphyte::NewMath::Vector4>);
static_assert(Graphyte::NewMath::Impl::SimdVectorType<Graphyte::NewMath::Vector3>);
static_assert(!Graphyte::NewMath::Impl::SimdVectorType<Graphyte::NewMath::Matrix>);

static_assert(!Graphyte::NewMath::Impl::SimdMatrixType<Graphyte::NewMath::Vector4>);
static_assert(!Graphyte::NewMath::Impl::SimdMatrixType<Graphyte::NewMath::Vector3>);
static_assert(Graphyte::NewMath::Impl::SimdMatrixType<Graphyte::NewMath::Matrix>);
#endif

#if GRAPHYTE_PLATFORM_WINDOWS
#include <wincodec.h>
#include <wrl/client.h>
#endif

#include <Graphyte/Unicode.hxx>
#include <Graphyte/CommandLine.hxx>
#include <Graphyte/Console.hxx>
#include <Graphyte/Threading/Interlocked.hxx>
#include <Graphyte/Diagnostics/Stopwatch.hxx>

Graphyte::ConsoleCommand cm_ExecuteFunction{
    Graphyte::ConsoleObjectFlags::Cheat,
    "cm_ExecuteFunction",
    [](notstd::span<std::string_view> params)
    {
        for (auto item : params)
        {
            GX_LOG(LogFormatConv, Trace, "cmd!{}\n", item);
        }
        return Graphyte::ConsoleCommandResult::Success;
    }
};

Graphyte::ConsoleVariable<float> cv_FloatVar{
    13.37F,
    Graphyte::ConsoleObjectFlags::ReadOnly,
    "cv_FloatVar",
};

Graphyte::ConsoleVariable<std::string> cv_StringVar{
    "hello",
    Graphyte::ConsoleObjectFlags::ReadOnly,
    "cv_StringVar",
};

int GraphyteMain([[maybe_unused]] int argc, [[maybe_unused]] char** argv) noexcept
{
    (void)cm_ExecuteFunction;

    using namespace std::literals;

#if GRAPHYTE_PLATFORM_WINDOWS
    {
        Microsoft::WRL::ComPtr<IWICImagingFactory2> wicFactory{};

        HRESULT hr = CoCreateInstance(
            CLSID_WICImagingFactory2,
            nullptr,
            CLSCTX_INPROC_SERVER,
            IID_PPV_ARGS(wicFactory.GetAddressOf())
        );

        std::array params{
            "1"sv,
            "2"sv,
            "3"sv,
            "4"sv,
        };

        if (auto const status = Graphyte::Console::Execute("cm_ExecuteFunction", params);
            status == Graphyte::ConsoleCommandResult::Success)
        {
            GX_LOG(LogFormatConv, Trace, "Success\n");
        }
        else
        {
            GX_LOG(LogFormatConv, Error, "Failed with {}\n", status);
        }

        {
            std::vector<std::string_view> items{};
            {
                Graphyte::Console::EnumerateCommands(items);
                GX_LOG(LogFormatConv, Trace, "Commands:\n");

                for (auto const& item : items)
                {
                    GX_LOG(LogFormatConv, Trace, "- {}\n", item);
                }
            }
            {
                Graphyte::Console::EnumerateVariables(items);
                GX_LOG(LogFormatConv, Trace, "Variables:\n");

                for (auto const& item : items)
                {
                    std::string value{};
                    Graphyte::Console::GetValue(value, item);
                    GX_LOG(LogFormatConv, Trace, "- {} = {}\n", item, value);
                }
            }

            cv_StringVar.Value = "world!";
            cv_FloatVar.Value = 21.37F;

            {
                Graphyte::Console::EnumerateVariables(items);
                GX_LOG(LogFormatConv, Trace, "Variables:\n");

                for (auto const& item : items)
                {
                    std::string value{};
                    Graphyte::Console::GetValue(value, item);
                    GX_LOG(LogFormatConv, Trace, "- {} = {}\n", item, value);
                }
            }
            Graphyte::Console::SetValue("cv_FloatVar", "11.23");
            Graphyte::Console::SetValue("cv_StringVar", "hello world!");
            {
                Graphyte::Console::EnumerateVariables(items);
                GX_LOG(LogFormatConv, Trace, "Variables:\n");

                for (auto const& item : items)
                {
                    std::string value{};
                    Graphyte::Console::GetValue(value, item);
                    GX_LOG(LogFormatConv, Trace, "- {} = {}\n", item, value);
                }
            }
        }

        GX_LOG(LogFormatConv, Trace, "WIC Create: {}\n", Graphyte::Diagnostics::GetMessageFromHRESULT(hr));

        std::string_view path{};
        auto const succ = Graphyte::CommandLine::Get("--input", path);
        GX_LOG(LogFormatConv, Trace, "{}: {}\n", succ, path);

        if (succ)
        {
            Microsoft::WRL::ComPtr<IWICBitmapDecoder> decoder{};
            std::wstring wpath{};
            if (Graphyte::Text::ConvertString(path, wpath, Graphyte::Text::ConversionType::Strict) == Graphyte::Text::ConversionResult::Success)
            {
                if (hr = wicFactory->CreateDecoderFromFilename(
                    wpath.c_str(),
                    nullptr,
                    GENERIC_READ,
                    WICDecodeMetadataCacheOnLoad,
                    decoder.GetAddressOf()
                ); SUCCEEDED(hr))
                {
                    GX_LOG(LogFormatConv, Trace, "WIC Encoder for {} succeeded\n", path);
                    UINT framesCount{};

                    hr = decoder->GetFrameCount(&framesCount);
                    GX_LOG(LogFormatConv, Trace, "Frames: {} {}\n", framesCount, Graphyte::Diagnostics::GetMessageFromHRESULT(hr));

                    IWICBitmapFrameDecode* frame{};
                    hr = decoder->GetFrame(0, &frame);
                    GX_LOG(LogFormatConv, Trace, "GetFrame: {}\n", Graphyte::Diagnostics::GetMessageFromHRESULT(hr));

                    UINT width{};
                    UINT height{};
                    frame->GetSize(&width, &height);
                    GX_LOG(LogFormatConv, Trace, "WxH: {}x{} {}\n", width, height, Graphyte::Diagnostics::GetMessageFromHRESULT(hr));
                }
                else
                {
                    GX_LOG(LogFormatConv, Trace, "Failed to create encoder: {}\n", Graphyte::Diagnostics::GetMessageFromHRESULT(hr));
                }
            }
            else
            {
                GX_LOG(LogFormatConv, Trace, "Failed to convert string\n");
            }
        }
    }
#endif

    return 0;
}
