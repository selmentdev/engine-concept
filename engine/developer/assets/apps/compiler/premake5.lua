project "dev.assets.compiler"
    targetname "dev.assets.compiler"
    kind "ConsoleApp"

    graphyte_app {}

    files {
        "private/**.?xx",
        "public/**.?xx",
        "*.lua",
    }

    tags {
        --"sdks-sol2",
        --"sdks-lua",
        "sdks-mbedtls",
        --"sdks-pugixml",
    }

    dependson {
        "module.graphics.d3d11",
        "module.graphics.d3d12",
        "module.graphics.glcore",
        "module.graphics.vulkan",
        "assets.shader",
        "assets.mesh",
        "somenonexistingproject",
    }

    use_com_graphyte_base()
    use_com_graphyte_launch()
    use_com_graphyte_graphics()
    use_com_graphyte_framework()
    use_com_graphyte_assets_base()
