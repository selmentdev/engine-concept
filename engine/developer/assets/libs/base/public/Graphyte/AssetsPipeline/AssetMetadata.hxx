#pragma once
#include <Graphyte/Assets.Base.module.hxx>
#include <Graphyte/Serialization/ISerializable.hxx>
#include <Graphyte/Platform/DateTime.hxx>
#include <Graphyte/Platform/Uuid.hxx>

namespace Graphyte::AssetsPipeline
{
    class ASSETS_BASE_API AssetMetadata final : public Serialization::ISerializable
    {
    public:
        std::string m_License{};
        Platform::DateTime m_CreationTime{};
        Platform::Uuid m_AssetId{};

    public:
        AssetMetadata() noexcept;
        virtual ~AssetMetadata() noexcept;

    public:
        bool Serialize(Serialization::Writer::Value& value) noexcept override;
        bool Deserialize(Serialization::Reader::Value& value) noexcept override;
    };
}
