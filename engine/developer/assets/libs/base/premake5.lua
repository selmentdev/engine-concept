project "com.graphyte.assets.base"
    targetname "com.graphyte.assets.base"

    language "c++"

    graphyte_module {}

    files {
        "public/**.?xx",
        "private/**.?xx",
        "*.lua",
    }

    removefiles {
        "**/xxx/*",
    }

    filter { "kind:SharedLib" }
        defines {
            "assets_base_EXPORTS=1"
        }

    use_com_graphyte_base()
