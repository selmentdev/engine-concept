#pragma once
#include <Graphyte/Platform.hxx>

#if defined(assets_font_EXPORTS)
#define ASSETS_FONT_API     GX_LIB_EXPORT
#else
#define ASSETS_FONT_API     GX_LIB_IMPORT
#endif
