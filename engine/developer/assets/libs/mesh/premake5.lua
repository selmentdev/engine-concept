project "com.graphyte.assets.mesh"
    targetname "com.graphyte.assets.mesh"

    language "c++"

    graphyte_module {}
    
    files {
        "public/**.?xx",
        "private/**.?xx",
        "*.lua",
    }

    filter { "kind:SharedLib" }
        defines {
            "assets_mesh_EXPORTS=1"
        }

    use_com_graphyte_base()
    use_com_graphyte_geometry()
    use_com_graphyte_assets_base()
