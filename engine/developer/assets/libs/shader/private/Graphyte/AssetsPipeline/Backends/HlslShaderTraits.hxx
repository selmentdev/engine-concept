#pragma once
#include <Graphyte/Assets.Shader.module.hxx>

#if GRAPHYTE_PLATFORM_WINDOWS

#include <Graphyte/AssetsPipeline/ShaderProcessor.hxx>
#include <Graphyte/Platform/Impl.Windows/Windows.Helpers.hxx>
#include <Graphyte/Platform/Impl.Windows/Windows.Types.hxx>
#include <Graphyte/Graphics/Gpu/GpuDefinitions.hxx>

namespace Graphyte::AssetsPipeline
{
    struct HlslShaderTraits final
    {
        static std::string_view GetHlslStagePrefix(Graphics::GpuShaderStage stage) noexcept
        {
            switch (stage)
            {
            case Graphics::GpuShaderStage::Pixel:
                return "ps";
            case Graphics::GpuShaderStage::Vertex:
                return "vs";
            case Graphics::GpuShaderStage::Geometry:
                return "gs";
            case Graphics::GpuShaderStage::Hull:
                return "hs";
            case Graphics::GpuShaderStage::Domain:
                return "ds";
            case Graphics::GpuShaderStage::Compute:
                return "cs";
            default:
                break;
            }

            return {};
        }

        static std::string_view GetHlslProfileSuffix(Graphics::GpuShaderProfile profile) noexcept
        {
            switch (profile)
            {
            case Graphics::GpuShaderProfile::D3DSM_5_0:
                return "5_0";
            case Graphics::GpuShaderProfile::D3DSM_5_1:
                return "5_1";
            case Graphics::GpuShaderProfile::D3DSM_6_0:
                return "6_0";
            case Graphics::GpuShaderProfile::D3DSM_6_1:
                return "6_1";
            default:
                break;
            }

            return {};
        }

        static bool GetHlslEntryPoint(std::string& entry_point, Graphics::GpuShaderStage stage) noexcept
        {
            auto const prefix = GetHlslStagePrefix(stage);
            if (!prefix.empty())
            {
                entry_point = prefix;
                entry_point += "_main";
                return true;
            }

            return false;
        }

        static bool GetHlslEntryPoint(std::wstring& entry_point, Graphics::GpuShaderStage stage) noexcept
        {
            std::string result{};
            if (GetHlslEntryPoint(result, stage))
            {
                entry_point = Platform::Impl::ConvertString(result);
                return true;
            }

            return false;
        }

        static bool GetHlslProfileName(std::string& name, Graphics::GpuShaderStage stage, Graphics::GpuShaderProfile profile) noexcept
        {
            auto prefix = GetHlslStagePrefix(stage);
            auto suffix = GetHlslProfileSuffix(profile);

            if (!prefix.empty() && !suffix.empty())
            {
                name = prefix;
                name += '_';
                name += suffix;
                return true;
            }

            return false;
        }

        static bool GetHlslProfileName(std::wstring& name, Graphics::GpuShaderStage stage, Graphics::GpuShaderProfile profile) noexcept
        {
            std::string result{};
            if (GetHlslProfileName(result, stage, profile))
            {
                name = Platform::Impl::ConvertString(result);
                return true;
            }

            return false;
        }


        static std::vector<std::pair<std::wstring, std::wstring>> SanitizeMacrosW(const ShaderCompilerInput& input) noexcept
        {
            std::vector<std::pair<std::wstring, std::wstring>> result{};

            for (auto const& item : input.Definitions)
            {
                result.push_back(
                    std::make_pair(
                        Platform::Impl::ConvertString(item.first),
                        Platform::Impl::ConvertString(item.second)
                   )
               );
            }

            return result;
        }

        static std::vector<std::pair<std::string, std::string>> SanitizeMacros(const ShaderCompilerInput& input) noexcept
        {
            std::vector<std::pair<std::string, std::string>> result{};

            for (auto const& item : input.Definitions)
            {
                result.push_back(
                    std::make_pair(
                        item.first,
                        item.second
                    )
                );
            }

            return result;
        }
    };
}

#endif
