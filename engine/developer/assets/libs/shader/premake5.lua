project "com.graphyte.assets.shader"
    targetname "com.graphyte.assets.shader"

    language "c++"

    graphyte_module {}

    includedirs {
        "%{wks.location}/engine/include",
        "./public",
    }
    
    files {
        "public/**.?xx",
        "private/**.?xx",
        "*.lua",
    }

    filter { "kind:SharedLib" }
        defines {
            "assets_shader_EXPORTS=1"
        }

    use_com_graphyte_base()
    use_com_graphyte_geometry()
    use_com_graphyte_graphics()
    use_com_graphyte_assets_base()
