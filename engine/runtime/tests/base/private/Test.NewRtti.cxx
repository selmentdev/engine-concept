#include "Test.Base.pch.hxx"
#include <Graphyte/Base.module.hxx>
#include <Graphyte/BaseObject.hxx>

namespace
{
    namespace TX
    {
        class MObject : public Graphyte::Object
        {
            GX_DECLARE_TYPEINFO(TX::MObject, 0x1000'0001'00000000);
        public:
            MObject() noexcept
            {
            }

            virtual ~MObject() noexcept
            {
            }
        };

        class FObject : public MObject
        {
            GX_DECLARE_TYPEINFO(TX::FObject, 0x1000'0001'00000001);
        public:
            FObject() noexcept
            {
            }

            virtual ~FObject() noexcept
            {
            }
        };
    }
}
GX_DEFINE_TYPEINFO(TX::MObject, Graphyte::Object);
GX_DEFINE_TYPEINFO(TX::FObject, TX::MObject);


TEST_CASE("New typeinfo concept")
{
    Graphyte::Object* o = Graphyte::TypeOf<TX::FObject>()->ActivateInstance();

    CHECK(o->RTTI_GetTypeInfo()->IsExact(Graphyte::TypeOf<Graphyte::Object>()) == false);
    CHECK(o->RTTI_GetTypeInfo()->IsExact(Graphyte::TypeOf<TX::MObject>()) == false);
    CHECK(o->RTTI_GetTypeInfo()->IsExact(Graphyte::TypeOf<TX::FObject>()) == true);

    CHECK(o->RTTI_GetTypeInfo()->IsDerived(Graphyte::TypeOf<Graphyte::Object>()) == true);
    CHECK(o->RTTI_GetTypeInfo()->IsDerived(Graphyte::TypeOf<TX::MObject>()) == true);
    CHECK(o->RTTI_GetTypeInfo()->IsDerived(Graphyte::TypeOf<TX::FObject>()) == true);

    CHECK(Graphyte::TypeCode{ 0x1000'0001'00000001 } == o->RTTI_GetTypeInfo()->GetTypeCode());

    CHECK(Graphyte::TypeCast<Graphyte::Object>(o) != nullptr);
    CHECK(Graphyte::TypeCast<TX::MObject>(o) != nullptr);
    CHECK(Graphyte::TypeCast<TX::FObject>(o) != nullptr);

    CHECK(Graphyte::TypeCode{ 0xdead'c0de'00000000 } == Graphyte::TypeCodeOf<Graphyte::Object>());
    CHECK(Graphyte::TypeCode{ 0x1000'0001'00000000 } == Graphyte::TypeCodeOf<TX::MObject>());
    CHECK(Graphyte::TypeCode{ 0x1000'0001'00000001 } == Graphyte::TypeCodeOf<TX::FObject>());

    CHECK(Graphyte::TypeCode{ 0xa00a62a942b20165 } == Graphyte::TypeCodeOf<float>());
    CHECK(Graphyte::TypeCode{ 0xa0880a9ce131dea8 } == Graphyte::TypeCodeOf<double>());

    CHECK(Graphyte::TypeCode{ 0x44bfbb392bb2b61d } == Graphyte::TypeCodeOf<std::uint8_t>());
    CHECK(Graphyte::TypeCode{ 0xe9938e6de34a7c0e } == Graphyte::TypeCodeOf<std::uint16_t>());
    CHECK(Graphyte::TypeCode{ 0xa197f65ee4adee28 } == Graphyte::TypeCodeOf<std::uint32_t>());
    CHECK(Graphyte::TypeCode{ 0x47d46876a53138eb } == Graphyte::TypeCodeOf<std::uint64_t>());
    CHECK(Graphyte::TypeCode{ 0x9ae377d680bf4106 } == Graphyte::TypeCodeOf<std::int8_t>());
    CHECK(Graphyte::TypeCode{ 0xfdee9033d2037037 } == Graphyte::TypeCodeOf<std::int16_t>());
    CHECK(Graphyte::TypeCode{ 0xaaf6c8228a70f741 } == Graphyte::TypeCodeOf<std::int32_t>());
    CHECK(Graphyte::TypeCode{ 0xef8b360a2cee43de } == Graphyte::TypeCodeOf<std::int64_t>());

    CHECK(Graphyte::TypeCode{ 0x2767bd747119cc57 } == Graphyte::TypeCodeOf<std::string>());

    delete o;
}

TEST_CASE("TypeFactory")
{
    Graphyte::TypeFactory factory{};
    
    CHECK(factory.Register<TX::MObject>() == true);
    CHECK(factory.Register<TX::FObject>() == true);
    CHECK(factory.Register<TX::MObject>() == false);
    CHECK(factory.Register<TX::FObject>() == false);

    {
        Graphyte::Object* o = factory.ActivateInstance(Graphyte::TypeCodeOf<TX::MObject>());
        CHECK(o != nullptr);
        delete o;
    }
    {
        Graphyte::Object* o = factory.ActivateInstance(Graphyte::TypeCodeOf<TX::FObject>());
        CHECK(o != nullptr);
        delete o;
    }

    CHECK(factory.Unregister<TX::MObject>() == true);
    CHECK(factory.Unregister<TX::FObject>() == true);
    CHECK(factory.Unregister<TX::MObject>() == false);
    CHECK(factory.Unregister<TX::FObject>() == false);

    {
        Graphyte::Object* o = factory.ActivateInstance(Graphyte::TypeCodeOf<TX::MObject>());
        CHECK(o == nullptr);
    }
    {
        Graphyte::Object* o = factory.ActivateInstance(Graphyte::TypeCodeOf<TX::FObject>());
        CHECK(o == nullptr);
    }
}
