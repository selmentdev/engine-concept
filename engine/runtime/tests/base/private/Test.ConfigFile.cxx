#include "Test.Base.pch.hxx"
#include <Graphyte/Base.module.hxx>
#include <Graphyte/ConfigFile.hxx>

TEST_CASE("Config file")
{
    SECTION("Creating and serializing values")
    {
        Graphyte::ConfigFile output_file{};
        output_file.SetString("windows", "path", "some/path/to/exe");
        output_file.SetInt32("windows", "cachesize", 2137);
        output_file.SetString("linux", "path", "some/other/path");

        std::string content{};
        REQUIRE(output_file.Write(content));

        Graphyte::ConfigFile input_file{};
        REQUIRE(input_file.Read(content));

        {
            std::string value{};
            REQUIRE(input_file.GetString("windows", "path", value));
            REQUIRE(value == "some/path/to/exe");
        }
        {
            int32_t value{};
            REQUIRE(input_file.GetInt32("windows", "cachesize", value));
            REQUIRE(value == 2137);
        }
        {
            std::string value{};
            REQUIRE(input_file.GetString("linux", "path", value));
            REQUIRE(value == "some/other/path");
        }
    }

    SECTION("Parsing valid file with all features")
    {
        std::string_view content{
            "\n"
            "                    ; this is some comment\n"
            "    [section]       ; comments are allowed after section designators\n"
            "name=value          ; name=values may have comments too\n"
            "\n"
            "; empty lines and comments right into source!\n"
            "\n"
            "[other]\n"
            "\t\tname1\t\t\t=\t\t\t\t\t\t\"value with some stringy\"\t\t\t;\t\t;t;t\t\t;t\t;t;\n"
            "name2 = value3\n"
            "test = 4421"
        };

        Graphyte::ConfigFile config{};
        REQUIRE(config.Read(content));

        {
            std::string value{};
            REQUIRE(config.GetString("section", "name", value));
            REQUIRE(value == "value");
        }
        {
            std::string value{};
            REQUIRE(config.GetString("other", "name1", value));
            REQUIRE(value == "\"value with some stringy\"");
        }
        {
            std::string value{};
            REQUIRE(config.GetString("other", "name2", value));
            REQUIRE(value == "value3");
        }
        {
            std::string value{};
            REQUIRE(config.GetString("other", "test", value));
            REQUIRE(value == "4421");
        }
    }

    SECTION("Parsing empty string")
    {
        std::string_view content{ "" };

        Graphyte::ConfigFile config{};
        REQUIRE(config.Read(content));

        REQUIRE(config.GetSections().size() == 0);
    }

    SECTION("Single section with no data")
    {
        std::string_view content{
            "[section]"
        };

        Graphyte::ConfigFile config{};
        REQUIRE(config.Read(content));
        REQUIRE(config.GetSections().size() == 0);
    }

    SECTION("Single value in unnamed section")
    {
        std::string_view content{
            "name=value"
        };

        Graphyte::ConfigFile config{};
        REQUIRE(config.Read(content));

        {
            std::string value{};
            REQUIRE(config.GetString("", "name", value));
            REQUIRE(value == "value");
        }
    }
}
