#include "Test.Base.pch.hxx"
#include <Graphyte/BaseObject.hxx>

namespace LocalTestNamespace
{
    using BaseClassRef = Graphyte::Reference<class BaseClass>;
    class BaseClass : public Graphyte::Object
    {
        GX_DECLARE_TYPEINFO(LocalTestNamespace::BaseClass, 0x5c8dbf6860c80a57);
    };

    using DerivedClassRef = Graphyte::Reference<class DerivedClass>;
    class DerivedClass : public BaseClass
    {
        GX_DECLARE_TYPEINFO(LocalTestNamespace::DerivedClass, 0x1000100010001001);
    };

    using EvenMoreDerivedClassRef = Graphyte::Reference<class EvenMoreDerivedClass>;
    class EvenMoreDerivedClass : public DerivedClass
    {
        GX_DECLARE_TYPEINFO(LocalTestNamespace::EvenMoreDerivedClass, 0x1000100010001337);
    };

    enum class SomeFlags
    {

    };
}
GX_DEFINE_TYPEINFO(LocalTestNamespace::BaseClass, Graphyte::Object);
GX_DEFINE_TYPEINFO(LocalTestNamespace::DerivedClass, LocalTestNamespace::BaseClass);
GX_DEFINE_TYPEINFO(LocalTestNamespace::EvenMoreDerivedClass, LocalTestNamespace::DerivedClass);

TEST_CASE("Runtime Type Info")
{
    SECTION("Type ID")
    {
        using namespace Graphyte;

        CHECK(Graphyte::TypeCodeOf<std::uint8_t>() == Graphyte::TypeCode{ 0x44bfbb392bb2b61d });
        CHECK(Graphyte::TypeCodeOf<std::uint16_t>() == Graphyte::TypeCode{ 0xe9938e6de34a7c0e });
        CHECK(Graphyte::TypeCodeOf<std::uint32_t>() == Graphyte::TypeCode{ 0xa197f65ee4adee28 });
        CHECK(Graphyte::TypeCodeOf<std::uint64_t>() == Graphyte::TypeCode{ 0x47d46876a53138eb });

        CHECK(Graphyte::TypeCodeOf<std::int8_t>() == Graphyte::TypeCode{ 0x9ae377d680bf4106 });
        CHECK(Graphyte::TypeCodeOf<std::int16_t>() == Graphyte::TypeCode{ 0xfdee9033d2037037 });
        CHECK(Graphyte::TypeCodeOf<std::int32_t>() == Graphyte::TypeCode{ 0xaaf6c8228a70f741 });
        CHECK(Graphyte::TypeCodeOf<std::int64_t>() == Graphyte::TypeCode{ 0xef8b360a2cee43de });

        CHECK(Graphyte::TypeCodeOf<float>() == Graphyte::TypeCode{ 0xa00a62a942b20165 });
        CHECK(Graphyte::TypeCodeOf<double>() == Graphyte::TypeCode{ 0xa0880a9ce131dea8 });

        CHECK(Graphyte::TypeCodeOf<std::string>() == Graphyte::TypeCode{ 0x2767bd747119cc57 });
        CHECK(Graphyte::TypeCodeOf<LocalTestNamespace::BaseClass>() == Graphyte::TypeCode{ 0x5c8dbf6860c80a57 });
    }

    SECTION("Type Name")
    {
        CHECK_THAT(Graphyte::TypeNameOf<std::uint8_t>(), Catch::Matchers::Equals("std::uint8_t"));
        CHECK_THAT(Graphyte::TypeNameOf<std::uint16_t>(), Catch::Matchers::Equals("std::uint16_t"));
        CHECK_THAT(Graphyte::TypeNameOf<std::uint32_t>(), Catch::Matchers::Equals("std::uint32_t"));
        CHECK_THAT(Graphyte::TypeNameOf<std::uint64_t>(), Catch::Matchers::Equals("std::uint64_t"));

        CHECK_THAT(Graphyte::TypeNameOf<std::int8_t>(), Catch::Matchers::Equals("std::int8_t"));
        CHECK_THAT(Graphyte::TypeNameOf<std::int16_t>(), Catch::Matchers::Equals("std::int16_t"));
        CHECK_THAT(Graphyte::TypeNameOf<std::int32_t>(), Catch::Matchers::Equals("std::int32_t"));
        CHECK_THAT(Graphyte::TypeNameOf<std::int64_t>(), Catch::Matchers::Equals("std::int64_t"));

        CHECK_THAT(Graphyte::TypeNameOf<float>(), Catch::Matchers::Equals("float"));
        CHECK_THAT(Graphyte::TypeNameOf<double>(), Catch::Matchers::Equals("double"));

        CHECK_THAT(Graphyte::TypeNameOf<std::string>(), Catch::Matchers::Equals("std::string"));
        CHECK_THAT(Graphyte::TypeNameOf<LocalTestNamespace::BaseClass>(), Catch::Matchers::Equals("LocalTestNamespace::BaseClass"));
    }

    SECTION("Casting raw pointers - base object in base pointer")
    {
        LocalTestNamespace::BaseClass* b = new LocalTestNamespace::BaseClass();
        CHECK(Graphyte::TypeCast<Graphyte::Object>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::BaseClass>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::DerivedClass>(b) == nullptr);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::EvenMoreDerivedClass>(b) == nullptr);
        CHECK_THAT(b->RTTI_GetTypeInfo()->GetTypeName(), Catch::Matchers::Equals("LocalTestNamespace::BaseClass"));
        CHECK(b->RTTI_GetTypeInfo()->GetTypeCode() == Graphyte::TypeCode{ 0x5c8dbf6860c80a57 });
        delete b;
    }

    SECTION("Casting raw pointers - derived object in base pointer")
    {
        LocalTestNamespace::BaseClass* b = new LocalTestNamespace::DerivedClass();
        CHECK(Graphyte::TypeCast<Graphyte::Object>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::BaseClass>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::DerivedClass>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::EvenMoreDerivedClass>(b) == nullptr);
        CHECK_THAT(b->RTTI_GetTypeInfo()->GetTypeName(), Catch::Matchers::Equals("LocalTestNamespace::DerivedClass"));
        CHECK(b->RTTI_GetTypeInfo()->GetTypeCode() == Graphyte::TypeCode{ 0x1000100010001001 });
        delete b;
    }

    SECTION("Casting raw pointers - even more derived object in base pointer")
    {
        LocalTestNamespace::BaseClass* b = new LocalTestNamespace::EvenMoreDerivedClass();
        CHECK(Graphyte::TypeCast<Graphyte::Object>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::BaseClass>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::DerivedClass>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::EvenMoreDerivedClass>(b) == b);
        CHECK_THAT(b->RTTI_GetTypeInfo()->GetTypeName(), Catch::Matchers::Equals("LocalTestNamespace::EvenMoreDerivedClass"));
        CHECK(b->RTTI_GetTypeInfo()->GetTypeCode() == Graphyte::TypeCode{ 0x1000100010001337 });
        delete b;
    }

    SECTION("Casting references - base object in base pointer")
    {
        LocalTestNamespace::BaseClassRef b = Graphyte::MakeRef<LocalTestNamespace::BaseClass>();
        CHECK(Graphyte::TypeCast<Graphyte::Object>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::BaseClass>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::DerivedClass>(b) == nullptr);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::EvenMoreDerivedClass>(b) == nullptr);
        CHECK_THAT(b->RTTI_GetTypeInfo()->GetTypeName(), Catch::Matchers::Equals("LocalTestNamespace::BaseClass"));
        CHECK(b->RTTI_GetTypeInfo()->GetTypeCode() == Graphyte::TypeCode{ 0x5c8dbf6860c80a57 });
    }

    SECTION("Casting references - derived object in base pointer")
    {
        LocalTestNamespace::BaseClassRef b = Graphyte::MakeRef<LocalTestNamespace::DerivedClass>();
        CHECK(Graphyte::TypeCast<Graphyte::Object>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::BaseClass>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::DerivedClass>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::EvenMoreDerivedClass>(b) == nullptr);
        CHECK_THAT(b->RTTI_GetTypeInfo()->GetTypeName(), Catch::Matchers::Equals("LocalTestNamespace::DerivedClass"));
        CHECK(b->RTTI_GetTypeInfo()->GetTypeCode() == Graphyte::TypeCode{ 0x1000100010001001 });
    }

    SECTION("Casting references - even more derived object in base pointer")
    {
        LocalTestNamespace::BaseClassRef b = Graphyte::MakeRef<LocalTestNamespace::EvenMoreDerivedClass>();
        CHECK(Graphyte::TypeCast<Graphyte::Object>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::BaseClass>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::DerivedClass>(b) == b);
        CHECK(Graphyte::TypeCast<LocalTestNamespace::EvenMoreDerivedClass>(b) == b);
        CHECK_THAT(b->RTTI_GetTypeInfo()->GetTypeName(), Catch::Matchers::Equals("LocalTestNamespace::EvenMoreDerivedClass"));
        CHECK(b->RTTI_GetTypeInfo()->GetTypeCode() == Graphyte::TypeCode{ 0x1000100010001337 });
    }
}
