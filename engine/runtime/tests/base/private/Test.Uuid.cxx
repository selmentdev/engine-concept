#include "Test.Base.pch.hxx"
#include <Graphyte/Platform/Uuid.hxx>

TEST_CASE("Uuid")
{
    using namespace Graphyte;

    SECTION("Empty")
    {
        Platform::Uuid empty{};

        REQUIRE(empty.A == 0);
        REQUIRE(empty.B == 0);
        REQUIRE(empty.C == 0);
        REQUIRE(empty.D == 0);

        REQUIRE(ToString(empty) == "00000000000000000000000000000000");
    }

    SECTION("Parsing")
    {
        Platform::Uuid parsed{};
        REQUIRE(FromString(parsed, "00000000000000000000000000000000"));
        REQUIRE(parsed.A == 0);
        REQUIRE(parsed.B == 0);
        REQUIRE(parsed.C == 0);
        REQUIRE(parsed.D == 0);

        REQUIRE(parsed == Platform::Uuid{});
    }

    SECTION("Some random uuid")
    {
        Platform::Uuid value{ 0xdeadc0de, 0xdeadbeef, 0xcafebabe, 0xbeefcace };
        REQUIRE(ToString(value) == "deadc0dedeadbeefcafebabebeefcace");
    }

    SECTION("Ordering")
    {
        Platform::Uuid value1{ 0,0,0,1 };
        Platform::Uuid value2{ 0,0,0,2 };

        REQUIRE_FALSE(value1 == value2);
        REQUIRE(value1 != value2);
        REQUIRE(value1 < value2);
        REQUIRE_FALSE(value1 > value2);
        REQUIRE(value1 <= value2);
        REQUIRE_FALSE(value1 >= value2);
    }

    SECTION("CreateUuid returns non-empty uuid")
    {
        REQUIRE(Platform::CreateUuid() != Platform::Uuid{});
    }
}
