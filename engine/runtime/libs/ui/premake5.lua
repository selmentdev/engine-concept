project "com.graphyte.ui"
    targetname "com.graphyte.ui"

    language "c++"

    graphyte_module {}
    
    files {
        "public/**.?xx",
        "private/**.?xx",
        "*.lua",
    }

    filter { "toolset:msc*" }
        pchheader "UI.pch.hxx"
        pchsource "private/UI.pch.cxx"

    filter { "kind:SharedLib" }
        defines {
            "module_ui_EXPORTS=1"
        }

    use_com_graphyte_base()
