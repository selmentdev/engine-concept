#include "UI.pch.hxx"
#include <Graphyte/UI/Controls/Visual.hxx>

namespace Graphyte::UI::Controls
{
    Visual::Visual() noexcept
    {
    }

    Visual::~Visual() noexcept
    {
    }

    void Visual::Transform(const TransformF& value) noexcept
    {
        this->m_Transform = value;

        this->OnPropertyChanged(Visual::TransformPropertyName);
    }

    void Visual::TranslationTransform(const TranslationF& value) noexcept
    {
        this->m_Transform.Translation = value;

        this->OnPropertyChanged(Visual::TransformPropertyName);
        this->OnPropertyChanged("TranslationTransform");
    }

    void Visual::RotationTransform(const RotationF& value) noexcept
    {
        this->m_Transform.Rotation = value;

        this->OnPropertyChanged(Visual::TransformPropertyName);
        this->OnPropertyChanged("RotationTransform");
    }

    void Visual::ScaleTransform(const ScaleF& value) noexcept
    {
        this->m_Transform.Scale = value;

        this->OnPropertyChanged(Visual::TransformPropertyName);
        this->OnPropertyChanged("ScaleTransform");
    }

    void Visual::SkewTransform(const SkewF& value) noexcept
    {
        this->m_Transform.Skew = value;

        this->OnPropertyChanged(Visual::TransformPropertyName);
        this->OnPropertyChanged("SkewTransform");
    }

    void Visual::Size(const SizeF& value) noexcept
    {
        this->m_Rectangle.SetSize(value);

        this->OnPropertyChanged(Visual::TransformPropertyName);
        this->OnPropertyChanged("Size");
    }

    void Visual::Location(const PointF& value) noexcept
    {
        this->m_Rectangle.SetLocation(value);

        this->OnPropertyChanged(Visual::TransformPropertyName);
        this->OnPropertyChanged("Location");
    }

    void Visual::Rectangle(const RectF& value) noexcept
    {
        this->m_Rectangle = value;

        this->OnPropertyChanged(Visual::TransformPropertyName);
        this->OnPropertyChanged("Rectangle");
    }

    void Visual::Padding(const ThicknessF& value) noexcept
    {
        this->m_Padding = value;

        this->OnPropertyChanged("Padding");
    }

    void Visual::Name(const PropertyName& value) noexcept
    {
        this->m_Name = value;

        this->OnPropertyChanged("Name");
    }

    void Visual::Opacity(float value) noexcept
    {
        this->m_Opacity = value;

        this->OnPropertyChanged("Opacity");
    }

    void Visual::Visible(const Visibility& value) noexcept
    {
        this->m_Visibility = value;

        this->OnPropertyChanged("Visible");
    }

    bool Visual::OnPropertyChanged(const PropertyName& propertyName) noexcept
    {
        if (propertyName == Visual::TransformPropertyName)
        {
            this->m_Transform.ComputeTransformMatrix(this->m_LocalTransformMatrix, this->m_Rectangle);
        }

        return true;
    }
}
