#include "UI.pch.hxx"
#include <Graphyte/UI/Controls/Control.hxx>

namespace Graphyte::UI::Controls
{
    Control::Control() noexcept
    {
    }

    Control::~Control() noexcept
    {
    }

    Control* Control::GetNextLogicalControl(NavigationDirection direction) noexcept
    {
        switch (direction)
        {
        case NavigationDirection::Forward:
            {
                if (this->m_ControlNextSibling != nullptr)
                {
                    if (this->m_ControlNextSibling->m_IsFocusable == false)
                    {
                        return this->m_ControlNextSibling->GetNextLogicalControl(direction);
                    }

                    return this->m_ControlNextSibling;
                }
                break;
            }
        case NavigationDirection::Backward:
            {
                if (this->m_ControlPrevSibling != nullptr)
                {
                    if (this->m_ControlPrevSibling->m_IsFocusable == false)
                    {
                        return this->m_ControlPrevSibling->GetNextLogicalControl(direction);
                    }

                    return this->m_ControlPrevSibling;
                }
                break;
            }
        }

        if (this->m_ControlParent != nullptr)
        {
            return this->m_ControlParent->GetNextLogicalControl(direction);
        }

        //
        // There is no next logical control.
        //
        return nullptr;
    }

    Control* Control::HitTest(const PointF& point) noexcept
    {
        PointF const transformedPoint = MatrixF::TransformPoint(this->m_TransformMatrix, point);

        bool const contains = this->m_Rectangle.Contains(transformedPoint);

        Control* result = contains ? this : nullptr;

        return result;
    }

    bool Control::IsChildControl([[maybe_unused]] Control* control) const noexcept
    {
        return false;
    }

    Control* Control::GetControlByName([[maybe_unused]] const PropertyName& name) const noexcept
    {
        return nullptr;
    }
}
