#include "UI.pch.hxx"
#include <Graphyte/UI/Matrix.hxx>
#include <Graphyte/UI/Transform.hxx>

namespace Graphyte::UI
{
    void TransformF::ComputeTransformMatrix(MatrixF& out_result, const PointF& transform_origin) noexcept
    {
        out_result.SetIdentity();
        out_result.Translate(transform_origin.X, transform_origin.Y);
        {
            out_result.Translate(this->Translation.X, this->Translation.Y);
            out_result.Rotate(this->Rotation.Angle);
            out_result.Skew(this->Skew.AngleX, this->Skew.AngleY);
            out_result.Scale(this->Scale.ScaleX, this->Scale.ScaleY);
        }
        out_result.Translate(-transform_origin.X, -transform_origin.Y);
    }
}
