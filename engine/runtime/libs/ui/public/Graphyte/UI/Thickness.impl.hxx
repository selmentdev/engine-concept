#pragma once
#include <Graphyte/UI/Matrix.hxx>
#include <Graphyte/UI/Point.hxx>
#include <Graphyte/UI/Rect.hxx>
#include <Graphyte/UI/Size.hxx>
#include <Graphyte/UI/Thickness.hxx>
#include <Graphyte/UI/Vector.hxx>
#include <Graphyte/Math/Scalar.hxx>

namespace Graphyte::UI
{
    inline ThicknessF::ThicknessF() noexcept
        : Left{}
        , Top{}
        , Right{}
        , Bottom{}
    {
    }

    inline ThicknessF::ThicknessF(const ThicknessF& other) noexcept
        : Left{ other.Left }
        , Top{ other.Top }
        , Right{ other.Right }
        , Bottom{ other.Bottom }
    {
    }

    inline ThicknessF::ThicknessF(float value) noexcept
        : Left{ value }
        , Top{ value }
        , Right{ value }
        , Bottom{ value }
    {
    }

    inline ThicknessF::ThicknessF(float horizontal, float vertical) noexcept
        : Left{ horizontal }
        , Top{ vertical }
        , Right{ horizontal }
        , Bottom{ vertical }
    {
    }

    inline ThicknessF::ThicknessF(float left, float top, float right, float bottom) noexcept
        : Left{ left }
        , Top{ top }
        , Right{ right }
        , Bottom{ bottom }
    {
    }

    inline float ThicknessF::GetLeft() const noexcept
    {
        return this->Left;
    }

    inline void ThicknessF::SetLeft(float value) noexcept
    {
        this->Left = value;
    }

    inline float ThicknessF::GetTop() const noexcept
    {
        return this->Top;
    }

    inline void ThicknessF::SetTop(float value) noexcept
    {
        this->Top = value;
    }

    inline float ThicknessF::GetRight() const noexcept
    {
        return this->Right;
    }

    inline void ThicknessF::SetRight(float value) noexcept
    {
        this->Right = value;
    }

    inline float ThicknessF::GetBottom() const noexcept
    {
        return this->Bottom;
    }

    inline void ThicknessF::SetBottom(float value) noexcept
    {
        this->Bottom = value;
    }

    inline bool ThicknessF::IsZero() const noexcept
    {
        return Math::IsZero(this->Left)
            && Math::IsZero(this->Top)
            && Math::IsZero(this->Right)
            && Math::IsZero(this->Bottom);
    }

    inline bool ThicknessF::IsUniform() const noexcept
    {
        return Math::IsNearEqual(this->Left, this->Top)
            && Math::IsNearEqual(this->Left, this->Right)
            && Math::IsNearEqual(this->Left, this->Bottom);
    }

    inline bool ThicknessF::IsClose(const ThicknessF& other) const noexcept
    {
        return Math::IsNearEqual(this->Left, other.Left)
            && Math::IsNearEqual(this->Right, other.Right)
            && Math::IsNearEqual(this->Top, other.Top)
            && Math::IsNearEqual(this->Bottom, other.Bottom);
    }

    inline ThicknessF ThicknessF::Scale(const ThicknessF& other, float scale_x, float scale_y) noexcept
    {
        ThicknessF copy = { other };
        copy.Left *= scale_x;
        copy.Top *= scale_y;
        copy.Right *= scale_x;
        copy.Bottom *= scale_y;
        return copy;
    }
}
