#pragma once
#include <Graphyte/UI/Color.hxx>

namespace Graphyte::UI
{
    inline ColorU::ColorU() noexcept
        : Value{ 0 }
    {
    }

    inline ColorU::ColorU(uint8_t r, uint8_t g, uint8_t b) noexcept
        : B{ b }
        , G{ g }
        , R{ r }
        , A{ 255 }
    {
    }

    inline ColorU::ColorU(uint8_t r, uint8_t g, uint8_t b, uint8_t a) noexcept
        : B{ b }
        , G{ g }
        , R{ r }
        , A{ a }
    {
    }

    inline ColorU::ColorU(float r, float g, float b) noexcept
        : B{ static_cast<uint8_t>(b * 255.0F) }
        , G{ static_cast<uint8_t>(g * 255.0F) }
        , R{ static_cast<uint8_t>(r * 255.0F) }
        , A{ 255 }
    {
    }

    inline ColorU::ColorU(float r, float g, float b, float a) noexcept
        : B{ static_cast<uint8_t>(b * 255.0F) }
        , G{ static_cast<uint8_t>(g * 255.0F) }
        , R{ static_cast<uint8_t>(r * 255.0F) }
        , A{ static_cast<uint8_t>(a * 255.0F) }
    {
    }

    inline ColorU::ColorU(uint32_t value) noexcept
        : Value{ value }
    {
    }

    inline ColorU::ColorU(KnownColor value) noexcept
        : Value{ static_cast<uint32_t>(value) }
    {
    }

    inline uint32_t ColorU::ToUInt32() const noexcept
    {
        return this->Value;
    }

    inline Float4A ColorU::ToFloat4() const noexcept
    {
        return Float4A
        {
            this->B / 255.0F,
            this->G / 255.0F,
            this->R / 255.0F,
            this->A / 255.0F
        };
    }
}
