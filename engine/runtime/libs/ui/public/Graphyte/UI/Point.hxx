#pragma once
#include <Graphyte/UI.module.hxx>

namespace Graphyte::UI
{
    struct VectorF;
    struct SizeF;
    struct MatrixF;

    struct PointF final
    {
        //
        // Fields.
        //
    public:
        float X;
        float Y;

        //
        // Constructors.
        //
    public:
        PointF() noexcept;
        PointF(const PointF& value) noexcept;
        PointF(float x, float y) noexcept;

        //
        // Properties.
        //
    public:
        float GetX() const noexcept;
        void SetX(float value) noexcept;

        float GetY() const noexcept;
        void SetY(float value) noexcept;

        //
        // Methods.
        //
    public:
        void Offset(float offset_x, float offset_y) noexcept;

        //
        // Static Methods.
        //
    public:
        static PointF Add(const PointF& point, const VectorF& vector) noexcept;
        static VectorF Subtract(const PointF& point1, const PointF& point2) noexcept;

        //
        // Matrix Transformation.
        //
    public:
        static PointF Transform(const PointF& point, const MatrixF& matrix) noexcept;

        //
        // Comparison operators.
        //
    public:
        bool operator == (const PointF& value) const noexcept;
        bool operator != (const PointF& value) const noexcept;

        //
        // Conversion operators.
        //
    public:
        explicit operator SizeF () const noexcept;
        explicit operator VectorF () const noexcept;
    };

    struct Point3F final
    {
        //
        // Fields.
        //
    public:
        float X;
        float Y;
        float Z;

        //
        // Constructors.
        //
    public:
        Point3F() noexcept;
        Point3F(const Point3F& value) noexcept;
        Point3F(float x, float y, float z) noexcept;

        //
        // Methods.
        //
    public:
        void Offset(float offset_x, float offset_y, float offset_z) noexcept;

        //
        // Comparison operators.
        //
    public:
        bool operator == (const Point3F& value) const noexcept;
        bool operator != (const Point3F& value) const noexcept;
    };
}

#include <Graphyte/UI/Point.impl.hxx>
