#pragma once
#include <Graphyte/UI/Matrix.hxx>
#include <Graphyte/UI/Point.hxx>
#include <Graphyte/UI/Rect.hxx>
#include <Graphyte/UI/Size.hxx>
#include <Graphyte/UI/Thickness.hxx>
#include <Graphyte/UI/Vector.hxx>
#include <Graphyte/Math/Scalar.hxx>

//
// Copyright (C) Karol Grzybowski. All rights reserved.
//
// See LICENSE.md file in the project root for full license information.
//

namespace Graphyte::UI
{
    inline PointF::PointF() noexcept
        : X{ 0.0F }
        , Y{ 0.0F }
    {
    }

    inline PointF::PointF(const PointF& value) noexcept
        : X{ value.X }
        , Y{ value.Y }
    {
    }

    inline PointF::PointF(float x, float y) noexcept
        : X{ x }
        , Y{ y }
    {
    }

    inline float PointF::GetX() const noexcept
    {
        return this->X;
    }

    inline void PointF::SetX(float value) noexcept
    {
        this->X = value;
    }

    inline float PointF::GetY() const noexcept
    {
        return this->Y;
    }

    inline void PointF::SetY(float value) noexcept
    {
        this->Y = value;
    }

    inline void PointF::Offset(float x, float y) noexcept
    {
        this->X += x;
        this->Y += y;
    }

    inline PointF PointF::Add(const PointF& point, const VectorF& vector) noexcept
    {
        return PointF
        {
            point.X + vector.X,
            point.Y + vector.Y
        };
    }

    inline VectorF PointF::Subtract(const PointF& point1, const PointF& point2) noexcept
    {
        return VectorF
        {
            point1.X - point2.X,
            point1.Y - point2.Y
        };
    }

    inline PointF PointF::Transform(const PointF& point, const MatrixF& matrix) noexcept
    {
        return PointF
        {
            (point.X * matrix.M11) + (point.Y * matrix.M21) + matrix.OffsetX,
            (point.X * matrix.M21) + (point.Y * matrix.M22) + matrix.OffsetY
        };
    }

    inline bool PointF::operator == (const PointF& value) const noexcept
    {
        return this->X == value.X
            && this->Y == value.Y;
    }

    inline bool PointF::operator != (const PointF& value) const noexcept
    {
        return this->X != value.X
            || this->Y != value.Y;
    }

    inline PointF::operator SizeF () const noexcept
    {
        return SizeF
        {
            Math::Abs(this->X),
            Math::Abs(this->Y)
        };
    }

    inline PointF::operator VectorF () const noexcept
    {
        return VectorF
        {
            this->X,
            this->Y
        };
    }
}

namespace Graphyte::UI
{
    inline Point3F::Point3F() noexcept
        : X{ 0.0F }
        , Y{ 0.0F }
        , Z{ 0.0F }
    {
    }

    inline Point3F::Point3F(const Point3F& value) noexcept
        : X{ value.X }
        , Y{ value.Y }
        , Z{ value.Z }
    {
    }

    inline Point3F::Point3F(float x, float y, float z) noexcept
        : X{ x }
        , Y{ y }
        , Z{ z }
    {
    }

    inline void Point3F::Offset(float offset_x, float offset_y, float offset_z) noexcept
    {
        this->X += offset_x;
        this->Y += offset_y;
        this->Z += offset_z;
    }

    inline bool Point3F::operator == (const Point3F& value) const noexcept
    {
        return this->X == value.X
            && this->Y == value.Y
            && this->Z == value.Z;
    }

    inline bool Point3F::operator != (const Point3F& value) const noexcept
    {
        return this->X != value.X
            || this->Y != value.Y
            || this->Z != value.Z;
    }
}
