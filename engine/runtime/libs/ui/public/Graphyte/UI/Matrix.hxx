#pragma once
#include <Graphyte/UI.module.hxx>

namespace Graphyte::UI
{
    struct PointF;
    struct SizeF;
    struct VectorF;
    struct RectF;

    struct MatrixF final
    {
        //
        // Fields.
        //
    public:
        float M11;
        float M12;
        float M21;
        float M22;
        float OffsetX;
        float OffsetY;

        //
        // Constructors.
        //
    public:
        MatrixF() noexcept;
        MatrixF(const MatrixF& other) noexcept;
        MatrixF(float m11, float m12, float m21, float m22, float offset_x, float offset_y) noexcept;

        //
        // Properties.
        //
    public:
        float GetM11() const noexcept;
        float GetM12() const noexcept;
        float GetM21() const noexcept;
        float GetM22() const noexcept;
        float GetOffsetX() const noexcept;
        float GetOffsetY() const noexcept;

        void SetM11(float value) noexcept;
        void SetM12(float value) noexcept;
        void SetM21(float value) noexcept;
        void SetM22(float value) noexcept;

        void SetOffsetX(float value) noexcept;
        void SetOffsetY(float value) noexcept;

        //
        // Methods.
        //
    public:
        bool IsIdentity() const noexcept;
        bool IsInvertible() const noexcept;
        float Determinant() const noexcept;

        void Invert() noexcept;
        void SetIdentity() noexcept;

        //
        // Static Methods.
        //
    public:
        static MatrixF Add(const MatrixF& matrix1, const MatrixF& matrix2) noexcept;
        static void Add(MatrixF& out_result, const MatrixF& matrix1, const MatrixF& matrix2) noexcept;

        static MatrixF Subtract(const MatrixF& matrix1, const MatrixF& matrix2) noexcept;
        static void Subtract(MatrixF& out_result, const MatrixF& matrix1, const MatrixF& matrix2) noexcept;

        static MatrixF Multiply(const MatrixF& matrix1, const MatrixF& matrix2) noexcept;
        static void Multiply(MatrixF& out_result, const MatrixF& matrix1, const MatrixF& matrix2) noexcept;

        static MatrixF Divide(const MatrixF& matrix1, const MatrixF& matrix2) noexcept;
        static void Divide(MatrixF& out_result, const MatrixF& matrix1, const MatrixF& matrix2) noexcept;

        static MatrixF Multiply(const MatrixF& matrix, float scalar) noexcept;
        static void Multiply(MatrixF& out_result, const MatrixF& matrix, float scalar) noexcept;

        static MatrixF Divide(const MatrixF& matrix, float scalar) noexcept;
        static void Divide(MatrixF& out_result, const MatrixF& matrix, float scalar) noexcept;

        static MatrixF Negate(const MatrixF& matrix) noexcept;
        static void Negate(MatrixF& out_result, const MatrixF& matrix) noexcept;

        //
        // Transformation Methods.
        //
    public:
        void Append(const MatrixF& matrix) noexcept;
        void Prepend(const MatrixF& matrix) noexcept;

        void Rotate(float angle) noexcept;
        void RotatePrepend(float angle) noexcept;
        void RotateAt(float angle, const PointF& point) noexcept;
        void RotateAtPrepend(float angle, const PointF& point) noexcept;

        void Scale(float scale_x, float scale_y) noexcept;
        void ScalePrepend(float scale_x, float scale_y) noexcept;
        void ScaleAt(float scale_x, float scale_y, const PointF& point) noexcept;
        void ScaleAtPrepend(float scale_x, float scale_y, const PointF& point) noexcept;

        void Skew(float shear_x, float shear_y) noexcept;
        void SkewPrepend(float shear_x, float shear_y) noexcept;
        void SkewAt(float shear_x, float shear_y, const PointF& point) noexcept;
        void SkewAtPrepend(float shear_x, float shear_y, const PointF& point) noexcept;

        void Translate(float offset_x, float offset_y) noexcept;
        void TranslatePrepend(float offset_x, float offset_y) noexcept;

        //
        // Transformation static methods.
        //
    public:
        static void Scaling(MatrixF& out_result, const VectorF& scale) noexcept;
        static MatrixF Scaling(const VectorF& scale) noexcept;
        static void Scaling(MatrixF& out_result, float scale_x, float scale_y) noexcept;
        static MatrixF Scaling(float scale_x, float scale_y) noexcept;
        static void Scaling(MatrixF& out_result, float scale) noexcept;
        static MatrixF Scaling(float scale) noexcept;
        static void Scaling(MatrixF& out_result, float scale_x, float scale_y, const VectorF& center) noexcept;
        static MatrixF Scaling(float scale_x, float scale_y, const VectorF& center) noexcept;

        static void Rotation(MatrixF& out_result, float angle) noexcept;
        static MatrixF Rotation(float angle) noexcept;
        static void Rotation(MatrixF& out_result, float angle, const VectorF& center) noexcept;
        static MatrixF Rotation(float angle, const VectorF& center) noexcept;

        static void Transformation(MatrixF& out_result, float scale_x, float scale_y, float angle, float offset_x, float offset_y) noexcept;
        static MatrixF Transformation(float scale_x, float scale_y, float angle, float offset_x, float offset_y) noexcept;

        static void Translation(MatrixF& out_result, const VectorF& vector) noexcept;
        static MatrixF Translation(const VectorF& vector) noexcept;
        static void Translation(MatrixF& out_result, float offset_x, float offset_y) noexcept;
        static MatrixF Translation(float offset_x, float offset_y) noexcept;

        static VectorF TransformVector(const MatrixF& matrix, const VectorF& vector) noexcept;
        static void TransformVector(VectorF& out_result, const MatrixF& matrix, const VectorF& vector) noexcept;

        static PointF TransformPoint(const MatrixF& matrix, const PointF& point) noexcept;
        static void TransformPoint(PointF& out_result, const MatrixF& matrix, const PointF& point) noexcept;

        static void Invert(MatrixF& out_result, const MatrixF& matrix) noexcept;

        static MatrixF Skewing(float angle_x, float angle_y) noexcept;
        static void Skewing(MatrixF& out_result, float angle_x, float angle_y) noexcept;

        //
        // Custom matrix construction.
        //
    public:
        static MatrixF FromRectanglePoly(const RectF& rect, const PointF& point1, const PointF& point2, const PointF& point3) noexcept;

        //
        // Comparison operators.
        //
    public:
        bool operator == (const MatrixF& matrix) const noexcept;
        bool operator != (const MatrixF& matrix) const noexcept;
    };
}

#include <Graphyte/UI/Matrix.impl.hxx>
