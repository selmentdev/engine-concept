#pragma once
#include <Graphyte/Hash/FNV.hxx>

namespace Graphyte::UI
{
    struct PropertyName final
    {
    private:
        uint64_t m_Value;

    public:
        constexpr PropertyName() noexcept
            : m_Value{}
        {
        }
        constexpr PropertyName(const char* name) noexcept
            : m_Value{ Hash::HashFnv1A64::FromString(name) }
        {
        }
        constexpr PropertyName(uint64_t value) noexcept
            : m_Value{ value }
        {
        }

    public:
        constexpr bool operator == (PropertyName other) const noexcept
        {
            return m_Value == other.m_Value;
        }
        constexpr bool operator != (PropertyName other) const noexcept
        {
            return m_Value != other.m_Value;
        }
        constexpr bool operator >= (PropertyName other) const noexcept
        {
            return m_Value >= other.m_Value;
        }
        constexpr bool operator >  (PropertyName other) const noexcept
        {
            return m_Value >  other.m_Value;
        }
        constexpr bool operator <= (PropertyName other) const noexcept
        {
            return m_Value <= other.m_Value;
        }
        constexpr bool operator <  (PropertyName other) const noexcept
        {
            return m_Value <  other.m_Value;
        }
    };

    inline constexpr PropertyName operator ""_property(const char* p, size_t) noexcept
    {
        return { p };
    }
}
