#pragma once
#include <Graphyte/UI/Matrix.hxx>
#include <Graphyte/UI/Point.hxx>
#include <Graphyte/UI/Rect.hxx>
#include <Graphyte/UI/Size.hxx>
#include <Graphyte/UI/Thickness.hxx>
#include <Graphyte/UI/Vector.hxx>
#include <Graphyte/Math/Scalar.hxx>

namespace Graphyte::UI
{
    inline RectF::RectF() noexcept
        : X{ 0.0F }
        , Y{ 0.0F }
        , Width{ 0.0F }
        , Height{ 0.0F }
    {
    }

    inline RectF::RectF(const RectF& rect) noexcept
        : X{ rect.X }
        , Y{ rect.Y }
        , Width{ rect.Width }
        , Height{ rect.Height }
    {
    }

    inline RectF::RectF(float x, float y, float width, float height) noexcept
        : X{ x }
        , Y{ y }
        , Width{ width }
        , Height{ height }
    {
    }

    inline RectF::RectF(const SizeF& size) noexcept
        : X{ 0.0F }
        , Y{ 0.0F }
        , Width{ size.Width }
        , Height{ size.Height }
    {
    }

    inline RectF::RectF(const PointF& location, const VectorF& vector) noexcept
        : X{ location.X }
        , Y{ location.Y }
        , Width{ vector.X }
        , Height{ vector.Y }
    {
    }

    inline RectF::RectF(const PointF& location, const SizeF& size) noexcept
        : X{ location.X }
        , Y{ location.Y }
        , Width{ size.Width }
        , Height{ size.Height }
    {
    }

    inline RectF::RectF(const PointF& point1, const PointF& point2) noexcept
    {
        this->X = Math::Min(point1.X, point2.X);
        this->Y = Math::Min(point1.Y, point2.Y);
        this->Width = Math::Max(Math::Max(point1.X, point2.X) - this->X, 0.0F);
        this->Height = Math::Max(Math::Max(point1.Y, point2.Y) - this->Y, 0.0F);
    }

    inline PointF RectF::GetLocation() const noexcept
    {
        return PointF
        {
            this->X,
            this->Y
        };
    }

    inline void RectF::SetLocation(const PointF& value) noexcept
    {
        this->X = value.X;
        this->Y = value.Y;
    }

    inline PointF RectF::GetCenter() const noexcept
    {
        return PointF
        {
            this->X + (this->Width / 2.0F),
            this->Y + (this->Height / 2.0F)
        };
    }

    inline void RectF::SetCenter(const PointF& value) noexcept
    {
        this->X = value.X - (this->Width / 2.0F);
        this->Y = value.Y - (this->Height / 2.0F);
    }

    inline SizeF RectF::GetSize() const noexcept
    {
        return SizeF
        {
            this->Width,
            this->Height
        };
    }

    inline void RectF::SetSize(const SizeF& value) noexcept
    {
        this->Width = value.Width;
        this->Height = value.Height;
    }

    inline float RectF::GetX() const noexcept
    {
        return this->X;
    }

    inline void RectF::SetX(float value) noexcept
    {
        this->X = value;
    }

    inline float RectF::GetY() const noexcept
    {
        return this->Y;
    }

    inline void RectF::SetY(float value) noexcept
    {
        this->Y = value;
    }

    inline float RectF::GetWidth() const noexcept
    {
        return this->Width;
    }

    inline void RectF::SetWidth(float value) noexcept
    {
        this->Width = value;
    }

    inline float RectF::GetHeight() const noexcept
    {
        return this->Height;
    }

    inline void RectF::SetHeight(float value) noexcept
    {
        this->Height = value;
    }

    inline float RectF::GetLeft() const noexcept
    {
        return this->X;
    }

    inline void RectF::SetLeft(float value) noexcept
    {
        this->X = value;
    }

    inline float RectF::GetTop() const noexcept
    {
        return this->Y;
    }

    inline void RectF::SetTop(float value) noexcept
    {
        this->Y = value;
    }

    inline float RectF::GetRight() const noexcept
    {
        return this->X + this->Width;
    }

    inline void RectF::SetRight(float value) noexcept
    {
        this->Width = (value - this->X);
    }

    inline float RectF::GetBottom() const noexcept
    {
        return this->Y + this->Height;
    }

    inline void RectF::SetBottom(float value) noexcept
    {
        this->Height = (value - this->Y);
    }

    inline PointF RectF::GetTopLeft() const noexcept
    {
        return PointF
        {
            this->GetTop(),
            this->GetLeft()
        };
    }

    inline void RectF::SetTopLeft(const PointF& value) noexcept
    {
        this->SetLeft(value.X);
        this->SetTop(value.Y);
    }

    inline PointF RectF::GetTopRight() const noexcept
    {
        return PointF
        {
            this->GetTop(),
            this->GetRight()
        };
    }

    inline void RectF::SetTopRight(const PointF& value) noexcept
    {
        this->SetRight(value.X);
        this->SetTop(value.Y);
    }

    inline PointF RectF::GetBottomLeft() const noexcept
    {
        return PointF
        {
            this->GetBottom(),
            this->GetLeft()
        };
    }

    inline void RectF::SetBottomLeft(const PointF& value) noexcept
    {
        this->SetLeft(value.X);
        this->SetBottom(value.Y);
    }

    inline PointF RectF::GetBottomRight() const noexcept
    {
        return PointF
        {
            this->GetBottom(),
            this->GetRight()
        };
    }

    inline void RectF::SetBottomRight(const PointF& value) noexcept
    {
        this->SetRight(value.X);
        this->SetBottom(value.Y);
    }

    inline RectF RectF::CreateFromLTRB(float left, float top, float right, float bottom) noexcept
    {
        return RectF
        {
            left,
            top,
            right - left,
            bottom - top
        };
    }

    inline RectF RectF::CreateCentered(const SizeF& size) noexcept
    {
        return RectF
        {
            size.Width / 2.0F,
            size.Height / 2.0F,
            size.Width,
            size.Height
        };
    }

    inline RectF RectF::CreateCentered(const ThicknessF& thickness) noexcept
    {
        return RectF
        {
            -thickness.Left,
            -thickness.Top,
            thickness.Left + thickness.Right,
            thickness.Top + thickness.Bottom
        };
    }

    inline RectF RectF::CreateEmpty() noexcept
    {
        return RectF
        {
            std::numeric_limits<float>::infinity(),
            std::numeric_limits<float>::infinity(),
            -std::numeric_limits<float>::infinity(),
            -std::numeric_limits<float>::infinity()
        };
    }

    inline bool RectF::Contains(const PointF& point) const noexcept
    {
        return this->Contains(point.X, point.Y);
    }

    inline bool RectF::Contains(float x, float y) const noexcept
    {
        return (x >= this->X)
            && (x <= (this->X + this->Width))
            && (y >= this->Y)
            && (y <= (this->Y + this->Height));
    }

    inline bool RectF::Contains(const RectF& rect) const noexcept
    {
        return (this->X <= rect.X)
            && (this->Y <= rect.Y)
            && ((this->X + this->Width) >= (rect.X + rect.Width))
            && ((this->Y + this->Height) >= (rect.Y + rect.Height));
    }

    inline bool RectF::IntersectsWith(const RectF& rect) const noexcept
    {
        return ((rect.X) <= (this->X + this->Width))
            && ((rect.Y) <= (this->Y + this->Height))
            && ((rect.X + rect.Width) >= (this->X))
            && ((rect.Y + rect.Height) >= (this->Y));
    }

    inline RectF RectF::Intersect(const RectF& rect1, const RectF& rect2) noexcept
    {
        float const left = Math::Max(rect1.GetLeft(), rect2.GetLeft());
        float const top = Math::Max(rect1.GetTop(), rect2.GetTop());
        float const width = Math::Max(Math::Min(rect1.GetRight(), rect2.GetRight()) - left, 0.0F);
        float const height = Math::Max(Math::Min(rect1.GetBottom(), rect2.GetBottom()) - top, 0.0F);

        return RectF
        {
            left,
            top,
            width,
            height
        };
    }

    inline RectF RectF::Union(const RectF& rect1, const RectF& rect2) noexcept
    {
        float const left = Math::Min(rect1.GetLeft(), rect2.GetLeft());
        float const top = Math::Min(rect1.GetTop(), rect2.GetTop());
        float const right = Math::Max(rect1.GetRight(), rect2.GetRight());
        float const bottom = Math::Max(rect1.GetBottom(), rect2.GetBottom());
        float const width = Math::Max(right - left, 0.0F);
        float const height = Math::Max(bottom - top, 0.0F);

        return RectF
        {
            left,
            top,
            width,
            height
        };
    }

    inline RectF RectF::Union(const RectF& rect, const PointF& point) noexcept
    {
        float const left = Math::Min(rect.GetLeft(), point.X);
        float const top = Math::Min(rect.GetTop(), point.Y);
        float const right = Math::Max(rect.GetRight(), point.X);
        float const bottom = Math::Max(rect.GetBottom(), point.Y);
        float const width = Math::Max(right - left, 0.0F);
        float const height = Math::Max(bottom - top, 0.0F);

        return RectF
        {
            left,
            top,
            width,
            height
        };
    }

    inline RectF RectF::Offset(const RectF& rect, const VectorF& vector) noexcept
    {
        return RectF::Offset(rect, vector.X, vector.Y);
    }

    inline RectF RectF::Offset(const RectF& rect, float offset_x, float offset_y) noexcept
    {
        return RectF
        {
            rect.X + offset_x,
            rect.Y + offset_y,
            rect.Width,
            rect.Height
        };
    }

    inline RectF RectF::Inflate(const RectF& rect, const SizeF& size) noexcept
    {
        return RectF::Inflate(rect, size.Width, size.Height);
    }

    inline RectF RectF::Inflate(const RectF& rect, float width, float height) noexcept
    {
        return RectF
        {
            rect.X - width,
            rect.Y - height,
            rect.Width + width + width,
            rect.Height + height + height
        };
    }

    inline RectF RectF::Scale(const RectF& rect, float scale_x, float scale_y) noexcept
    {
        float x = rect.X * scale_x;
        float y = rect.Y * scale_y;
        float width = rect.Width * scale_x;
        float height = rect.Height * scale_y;

        if (scale_x < 0.0F)
        {
            x += width;
            width *= -1.0F;
        }

        if (scale_y < 0.0F)
        {
            y += height;
            height *= -1.0F;
        }

        return RectF
        {
            x,
            y,
            width,
            height
        };
    }

    inline RectF RectF::GrowBy(const RectF& rect, float value) noexcept
    {
        return RectF::GrowBy(rect, value, value, value, value);
    }

    inline RectF RectF::GrowBy(const RectF& rect, float horizontal, float vertical) noexcept
    {
        return RectF::GrowBy(rect, horizontal, vertical, horizontal, vertical);
    }

    inline RectF RectF::GrowBy(const RectF& rect, float left, float top, float right, float bottom) noexcept
    {
        RectF copy = rect;
        copy.X -= left;
        copy.Y -= top;
        copy.Width += left + right;
        copy.Height += top + bottom;

        if (copy.Width < 0.0F)
        {
            copy.Width = 0.0F;
        }

        if (copy.Height < 0.0F)
        {
            copy.Height = 0.0F;
        }

        return copy;
    }

    inline RectF RectF::GrowBy(const RectF& rect, const ThicknessF& thickness) noexcept
    {
        return RectF::GrowBy(
            rect,
            thickness.Left,
            thickness.Top,
            thickness.Right,
            thickness.Bottom);
    }

    inline RectF RectF::Transform(const RectF& rect, const MatrixF& matrix) noexcept
    {
        //
        // Transform all corner points.
        //

        PointF const topLeft = PointF::Transform(rect.GetTopLeft(), matrix);
        PointF const topRight = PointF::Transform(rect.GetTopRight(), matrix);
        PointF const bottomLeft = PointF::Transform(rect.GetBottomLeft(), matrix);
        PointF const bottomRight = PointF::Transform(rect.GetBottomRight(), matrix);

        float const left = std::min({ topLeft.X, topRight.X, bottomLeft.X, bottomRight.X });
        float const top = std::min({ topLeft.Y, topRight.Y, bottomLeft.Y, bottomRight.Y });
        float const right = std::max({ topLeft.X, topRight.X, bottomLeft.X, bottomRight.X });
        float const bottom = std::max({ topLeft.Y, topRight.Y, bottomLeft.Y, bottomRight.Y });

        return RectF
        {
            left,
            top,
            right - left,
            bottom - top
        };
    }

    inline bool RectF::operator == (const RectF& rect) const noexcept
    {
        return this->X == rect.X
            && this->Y == rect.Y
            && this->Width == rect.Width
            && this->Height == rect.Height;
    }

    inline bool RectF::operator != (const RectF& rect) const noexcept
    {
        return this->X != rect.X
            || this->Y != rect.Y
            || this->Width != rect.Width
            || this->Height != rect.Height;
    }
}
