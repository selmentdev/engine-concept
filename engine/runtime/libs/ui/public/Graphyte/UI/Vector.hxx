#pragma once
#include <Graphyte/UI.module.hxx>

namespace Graphyte::UI
{
    struct PointF;
    struct SizeF;
    struct MatrixF;

    struct VectorF final
    {
        //
        // Friends.
        //
    private:
        friend struct Graphyte::UI::PointF;
        friend struct Graphyte::UI::SizeF;
        friend struct Graphyte::UI::MatrixF;

        //
        // Fields.
        //
    public:
        float X;
        float Y;

        //
        // Constructors.
        //
    public:
        VectorF() noexcept;
        VectorF(const VectorF& other) noexcept;
        VectorF(float x, float y) noexcept;

        //
        // Methods.
        //
    public:
        float Length() const noexcept;
        float LengthSquared() const noexcept;

        void Normalize() noexcept;
        void Negate() noexcept;

        //
        // Static Methods.
        //
    public:
        static float CrossProduct(const VectorF& vector1, const VectorF& vector2) noexcept;
        static float DotProduct(const VectorF& vector1, const VectorF& vector2) noexcept;
        static float AngleBetween(const VectorF& vector1, const VectorF& vector2) noexcept;
        static VectorF Add(const VectorF& vector1, const VectorF& vector2) noexcept;
        static VectorF Subtract(const VectorF& vector1, const VectorF& vector2) noexcept;
        static PointF Add(const VectorF& vector, const PointF& point) noexcept;
        static VectorF Multiply(const VectorF& vector, float scalar) noexcept;
        static VectorF Multiply(float scalar, const VectorF& vector) noexcept;
        static VectorF Divide(const VectorF& vector, float scalar) noexcept;
        static float Determinant(const VectorF& vector1, const VectorF& vector2) noexcept;
        static VectorF Transform(const VectorF& vector, const MatrixF& matrix) noexcept;
        static VectorF Normalize(const VectorF& vector) noexcept;
        static VectorF Negate(const VectorF& vector) noexcept;

        //
        // Operators.
        //
    public:
        bool operator == (const VectorF& vector) const noexcept;
        bool operator != (const VectorF& vector) const noexcept;

        //
        // Conversion operators.
        //
    public:
        explicit operator PointF() const noexcept;
        explicit operator SizeF() const noexcept;
    };
}

#include <Graphyte/UI/Vector.impl.hxx>
