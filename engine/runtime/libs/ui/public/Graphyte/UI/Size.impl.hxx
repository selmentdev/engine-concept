#pragma once
#include <Graphyte/UI/Matrix.hxx>
#include <Graphyte/UI/Point.hxx>
#include <Graphyte/UI/Rect.hxx>
#include <Graphyte/UI/Size.hxx>
#include <Graphyte/UI/Thickness.hxx>
#include <Graphyte/UI/Vector.hxx>
#include <Graphyte/Math/Scalar.hxx>

namespace Graphyte::UI
{
    inline SizeF::SizeF() noexcept
        : Width{ 0.0F }
        , Height{ 0.0F }
    {
    }

    inline SizeF::SizeF(const SizeF& value) noexcept
        : Width{ value.Width }
        , Height{ value.Height }
    {
    }

    inline SizeF::SizeF(float width, float height) noexcept
        : Width{ width }
        , Height{ height }
    {
    }

    inline float SizeF::GetWidth() const noexcept
    {
        return this->Width;
    }

    inline float SizeF::GetHeight() const noexcept
    {
        return this->Height;
    }

    inline void SizeF::SetWidth(float value) noexcept
    {
        this->Width = value;
    }

    inline void SizeF::SetHeight(float value) noexcept
    {
        this->Height = value;
    }

    inline bool SizeF::IsEmpty() const noexcept
    {
        float const area = this->GetArea();
        return Math::IsZero(area)
            || Math::IsInfinite(area);
    }

    inline float SizeF::GetArea() const noexcept
    {
        return this->Width * this->Height;
    }

    inline SizeF SizeF::Max(const SizeF& size, float width, float height) noexcept
    {
        return SizeF
        {
            Math::Max(size.Width, width),
            Math::Max(size.Height, height)
        };
    }

    inline SizeF SizeF::Max(const SizeF& size1, const SizeF& size2) noexcept
    {
        return SizeF
        {
            Math::Max(size1.Width, size2.Width),
            Math::Max(size1.Height, size2.Height)
        };
    }

    inline SizeF SizeF::Min(const SizeF& size, float width, float height) noexcept
    {
        return SizeF
        {
            Math::Min(size.Width, width),
            Math::Min(size.Height, height)
        };
    }

    inline SizeF SizeF::Min(const SizeF& size1, const SizeF& size2) noexcept
    {
        return SizeF
        {
            Math::Min(size1.Width, size2.Width),
            Math::Min(size1.Height, size2.Height)
        };
    }

    inline SizeF SizeF::GrowBy(const SizeF& size, float width, float height) noexcept
    {
        return SizeF
        {
            size.Width + width,
            size.Height + height
        };
    }

    inline SizeF SizeF::GrowBy(const SizeF& size, const ThicknessF& thickness) noexcept
    {
        return SizeF
        {
            size.Width + (thickness.Left + thickness.Right),
            size.Height + (thickness.Top + thickness.Bottom)
        };
    }

    inline bool SizeF::operator == (const SizeF& value) const noexcept
    {
        return this->Width == value.Width
            && this->Height == value.Height;
    }

    inline bool SizeF::operator != (const SizeF& value) const noexcept
    {
        return this->Width != value.Width
            || this->Height != value.Height;
    }

    inline SizeF::operator PointF () const noexcept
    {
        return PointF
        {
            this->Width,
            this->Height
        };
    }

    inline SizeF::operator VectorF () const noexcept
    {
        return VectorF
        {
            this->Width,
            this->Height
        };
    }
}
