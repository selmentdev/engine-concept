#pragma once
#include <Graphyte/UI.module.hxx>

namespace Graphyte::UI
{
    struct PointF;
    struct VectorF;
    struct ThicknessF;

    struct SizeF final
    {
    public:
        float Width;
        float Height;

    public:
        SizeF() noexcept;
        SizeF(const SizeF& value) noexcept;
        SizeF(float width, float height) noexcept;

    public:
        float GetWidth() const noexcept;
        void SetWidth(float value) noexcept;

        float GetHeight() const noexcept;
        void SetHeight(float value) noexcept;

    public:
        bool IsEmpty() const noexcept;
        float GetArea() const noexcept;

    public:
        static SizeF Max(const SizeF& size, float width, float height) noexcept;
        static SizeF Max(const SizeF& size1, const SizeF& size2) noexcept;
        static SizeF Min(const SizeF& size, float width, float height) noexcept;
        static SizeF Min(const SizeF& size1, const SizeF& size2) noexcept;

        static SizeF GrowBy(const SizeF& size, float width, float height) noexcept;
        static SizeF GrowBy(const SizeF& size, const ThicknessF& thickness) noexcept;

    public:
        bool operator == (const SizeF& value) const noexcept;
        bool operator != (const SizeF& value) const noexcept;

    public:
        explicit operator PointF () const noexcept;
        explicit operator VectorF () const noexcept;
    };
}

#include <Graphyte/UI/Size.impl.hxx>
