#pragma once
#include <Graphyte/UI.module.hxx>
#include <Graphyte/UI/Drawing/GeometryBuilder.hxx>

namespace Graphyte::UI::Controls
{
    struct IElement
    {
    public:
        virtual ~IElement() noexcept = default;

    public:
        virtual bool Update(float delta_time) noexcept = 0;
        virtual bool GenerateGeometry(Drawing::GeometryBuilder& builder, Drawing::GeometryType type) noexcept = 0;

    public:
        // TODO: Serialization support.
    };

    UI_API IElement* ActivateInstance() noexcept;
}
