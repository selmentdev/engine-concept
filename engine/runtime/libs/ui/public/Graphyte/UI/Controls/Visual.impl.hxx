#pragma once
#include <Graphyte/UI/Controls/Visual.hxx>

namespace Graphyte::UI::Controls
{
    inline const TransformF& Visual::Transform() const noexcept
    {
        return this->m_Transform;
    }

    inline const TranslationF& Visual::TranslationTransform() const noexcept
    {
        return this->m_Transform.Translation;
    }

    inline const RotationF& Visual::RotationTransform() const noexcept
    {
        return this->m_Transform.Rotation;
    }

    inline const ScaleF& Visual::ScaleTransform() const noexcept
    {
        return this->m_Transform.Scale;
    }

    inline const SkewF& Visual::SkewTransform() const noexcept
    {
        return this->m_Transform.Skew;
    }

    inline const MatrixF& Visual::TransformMatrix() const noexcept
    {
        return this->m_LocalTransformMatrix;
    }

    inline SizeF Visual::Size() const noexcept
    {
        return this->m_Rectangle.GetSize();
    }

    inline PointF Visual::Location() const noexcept
    {
        return this->m_Rectangle.GetLocation();
    }

    inline RectF Visual::Rectangle() const noexcept
    {
        return this->m_Rectangle;
    }

    inline ThicknessF Visual::Padding() const noexcept
    {
        return this->m_Padding;
    }

    inline const PropertyName& Visual::Name() const noexcept
    {
        return this->m_Name;
    }

    inline float Visual::Opacity() const noexcept
    {
        return this->m_Opacity;
    }

    inline Visibility Visual::Visible() const noexcept
    {
        return this->m_Visibility;
    }
}
