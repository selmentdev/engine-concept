#pragma once
#include <Graphyte/UI/Controls/Control.hxx>

namespace Graphyte::UI::Controls::Primitives
{
    enum class ButtonState
    {
        Normal,
        Disabled,
        Checked,
        Pushed,
    };

    class ButtonBase
        : public Control
    {
    private:
        std::string m_Text;
        bool m_IsPushed : 1;

    public:
        ButtonBase() noexcept;
        virtual ~ButtonBase() noexcept;

    public:
        static constexpr const auto TextProperty = "Text"_property;
        static constexpr const auto PushedProperty = "Pushed"_property;

    public:
        const std::string& GetText() const noexcept
        {
            return m_Text;
        }
        void SetText(std::string_view value) noexcept
        {
            m_Text = value;
            OnPropertyChanged(TextProperty);
        }

        bool IsPushed() const noexcept
        {
            return m_IsPushed;
        }
        void SetPushed(bool value) noexcept
        {
            m_IsPushed = value;
            OnPropertyChanged(PushedProperty);
        }

    public:
        //virtual bool Serialize(void) noexcept override;
        //virtual bool Deserialize(void) noexcept override;
    };
}

