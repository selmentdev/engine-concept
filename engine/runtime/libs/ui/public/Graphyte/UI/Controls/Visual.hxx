#pragma once
#include <Graphyte/UI.module.hxx>
#include <Graphyte/UI/Controls/Element.hxx>
#include <Graphyte/UI/Controls/PropertyChanged.hxx>
#include <Graphyte/UI/Transform.hxx>
#include <Graphyte/UI/Rect.hxx>
#include <Graphyte/UI/Matrix.hxx>
#include <Graphyte/UI/Thickness.hxx>
#include <Graphyte/UI/Enums.hxx>
#include <Graphyte/UI/PropertyName.hxx>

namespace Graphyte::UI::Controls
{
    class Visual
        : public IElement
        , public INotifyPropertyChanged
    {
        //
        // Property Names.
        //
    public:
        static constexpr const PropertyName TransformPropertyName{ "Transform" };

        //
        //
        // Fields.
        //
    protected:
        UI::TransformF m_Transform;
        float m_Opacity;
        UI::RectF m_Rectangle;
        UI::ThicknessF m_Padding;
        UI::MatrixF m_LocalTransformMatrix;
        // Style ref.
        PropertyName m_Name;
        Visibility m_Visibility;

        //
        // Constructors.
        //
    public:
        Visual() noexcept;
        virtual ~Visual() noexcept;

        //
        // Properties.
        //
    public:
        const TransformF& Transform() const noexcept;
        void Transform(const TransformF& value) noexcept;

        const TranslationF& TranslationTransform() const noexcept;
        void TranslationTransform(const TranslationF& value) noexcept;

        const RotationF& RotationTransform() const noexcept;
        void RotationTransform(const RotationF& value) noexcept;

        const ScaleF& ScaleTransform() const noexcept;
        void ScaleTransform(const ScaleF& value) noexcept;

        const SkewF& SkewTransform() const noexcept;
        void SkewTransform(const SkewF& value) noexcept;

        const MatrixF& TransformMatrix() const noexcept;

        SizeF Size() const noexcept;
        void Size(const SizeF& value) noexcept;

        PointF Location() const noexcept;
        void Location(const PointF& value) noexcept;

        RectF Rectangle() const noexcept;
        void Rectangle(const RectF& value) noexcept;

        ThicknessF Padding() const noexcept;
        void Padding(const ThicknessF& value) noexcept;

        const PropertyName& Name() const noexcept;
        void Name(const PropertyName& value) noexcept;

        float Opacity() const noexcept;
        void Opacity(float value) noexcept;

        Visibility Visible() const noexcept;
        void Visible(const Visibility& value) noexcept;

        // TODO: Style reference.

        // INotifyPropertyChanged
    public:
        virtual bool OnPropertyChanged(const PropertyName& propertyName) noexcept override;

    public:
        // TODO: Serialization
    };
}

#include <Graphyte/UI/Controls/Visual.impl.hxx>
