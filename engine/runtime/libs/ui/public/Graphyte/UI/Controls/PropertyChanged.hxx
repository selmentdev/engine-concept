#pragma once
#include <Graphyte/Base.module.hxx>
#include <Graphyte/UI.module.hxx>
#include <Graphyte/UI/PropertyName.hxx>

namespace Graphyte::UI::Controls
{
    struct INotifyPropertyChanged
    {
    public:
        virtual ~INotifyPropertyChanged() noexcept = default;

    public:
        virtual bool OnPropertyChanged(const PropertyName& propertyName) noexcept = 0;
    };
}
