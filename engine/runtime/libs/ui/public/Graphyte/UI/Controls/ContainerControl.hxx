#pragma once
#include "Graphyte.UI.Controls/Control.hxx"

namespace Graphyte::UI::Controls
{
    class ContainerControl : public Control
    {
        friend class Control;
    protected:
        Control* m_FirstChild;

    public:
        ContainerControl() noexcept;
        virtual ~ContainerControl() noexcept;

    public:
        bool AddControl(Control* control) noexcept;
        bool RemoveControl(Control* control) noexcept;

    public:
        Control* FirstChild() const noexcept;
        Control* LastChild() const noexcept;

    public:
        virtual Control* GetControlByName(const StrongName& name) const noexcept override;

    public:
        virtual bool OnControlAdded() noexcept;
        virtual bool OnControlRemoved() noexcept;

    public:
        virtual Control* GetNextLogicalControl(NavigationDirection direction) noexcept override;
        virtual Control* HitTest(const PointF& point) noexcept override;
        virtual bool IsChildControl(Control* control) const noexcept override;
    };
}
