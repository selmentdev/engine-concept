#pragma once
#include <Graphyte/UI.module.hxx>
#include <Graphyte/UI/Controls/ContainerControl.hxx>
#include <Graphyte/UI/Enums.hxx>

namespace Graphyte::UI::Controls
{
    class UI_API Window
    {
        Window(const Window&) = delete;
        Window& operator= (const Window&) = delete;

        //
        // Fields.
        //
    private:
        std::string m_Title;
        Window* m_Owner;
        WindowStartupLocation m_StartupLocation;
        WindowState m_State;
        WindowStyle m_Style;
        WindowResizeMode m_ResizeMode;
        bool m_DialogResult;
        bool m_IsTopMost;
        bool m_IsActive;

        //
        // Constructors.
        //
    public:
        Window() noexcept;
        virtual ~Window() noexcept;

        //
        // Properties.
        //
    public:
        const std::string& Title() const noexcept;
        void Title(const std::string& value) noexcept;

        WindowState State() const noexcept;
        void State(WindowState value) noexcept;

        WindowStyle Style() const noexcept;
        void Style(WindowStyle value) noexcept;

        WindowResizeMode ResizeMode() const noexcept;
        void ResizeMode(WindowResizeMode value) noexcept;

        bool DialogResult() const noexcept;
        void DialogResult(bool value) noexcept;

        bool IsTopMost() const noexcept;
        void IsTopMost(bool value) noexcept;

        bool IsActive() const noexcept;
        void IsActive(bool value) noexcept;

        //
        // Methods.
        //
    public:
        bool Show() noexcept;
        bool ShowDialog() noexcept;
        bool Hide() noexcept;
        bool Activate() noexcept;
        bool Close() noexcept;
    };
}
