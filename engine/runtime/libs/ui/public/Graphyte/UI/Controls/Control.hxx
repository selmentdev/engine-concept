#pragma once
#include <Graphyte/UI.module.hxx>
#include <Graphyte/UI/Controls/Visual.hxx>
#include <Graphyte/UI/Controls/Element.hxx>
#include <Graphyte/UI/Controls/PropertyChanged.hxx>
#include <Graphyte/UI/Matrix.hxx>
#include <Graphyte/UI/Color.hxx>
#include <Graphyte/UI/Enums.hxx>
#include <Graphyte/UI/Drawing/GeometryBuilder.hxx>

namespace Graphyte::UI::Controls
{
    class Control : public Visual
    {
        Control(const Control&) = delete;
        Control& operator= (const Control&) = delete;

        friend class ContainerControl;

        //
        // Fields.
        //
    protected:
        MatrixF m_TransformMatrix;
        MatrixF m_TransformMatrixInv;
        ColorU m_TextColor;
        ColorU m_BackgroundColor;
        float m_ZOrder;
        float m_ActualZOrder;
        Control* m_ControlParent;
        Control* m_ControlNextSibling;
        Control* m_ControlPrevSibling;
        bool m_IsEnabled;
        bool m_IsFocusable;
        bool m_IsFocused;
        bool m_IsHovered;

        //
        // Constructors.
        //
    public:
        Control() noexcept;
        virtual ~Control() noexcept;

        //
        // Properties.
        //
    public:
        const MatrixF& TransformMatrix() const noexcept;

        float ZOrder() const noexcept;
        void ZOrder(float value) noexcept;

        float ActualZOrder() const noexcept;

        Control* Parent() const noexcept;
        void Parent(Control* value) noexcept;

        Control* NextSibling() const noexcept;
        Control* PreviousSibling() const noexcept;

        bool IsEnabled() const noexcept;
        void IsEnabled(bool value) noexcept;

        bool IsFocusable() const noexcept;
        void IsFocusable(bool value) noexcept;

        bool IsHovered() const noexcept;

        bool IsFocused() const noexcept;
        void IsFocused(bool value) noexcept;

        //
        // Methods.
        //
    public:
        virtual Control* GetNextLogicalControl(NavigationDirection direction) noexcept;
        virtual Control* HitTest(const PointF& point) noexcept;
        virtual bool IsChildControl(Control* control) const noexcept;
        virtual Control* GetControlByName(const PropertyName& name) const noexcept;
        //virtual bool OnPropertyChanged(const StrongName& propertyName) noexcept override;
        //virtual bool GetSelectorRectangle(RectF& result) const noexcept;

        //virtual bool Update(float delta_time) noexcept override;
        //virtual bool GenerateGeometry(Drawing::GeometryBuilder& builder, Drawing::GeometryType type) noexcept override;
    };
}
