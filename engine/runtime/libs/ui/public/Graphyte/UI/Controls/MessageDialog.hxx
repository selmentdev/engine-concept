#pragma once
#include "Graphyte.UI.hxx"

namespace Graphyte::UI::Controls
{
    class Control;
    class Window;

    enum class MessageDialogButton
    {
        OK,
        OKCancel,
        YesNo,
        YesNoCancel,
    };

    enum class MessageDialogImage
    {
        Error,
        Information,
        Question,
        Warning,
    };

    enum class MessageDialogResult
    {
        OK,
        Yes,
        No,
        Cancel,
        None,
    };

    class MessageDialog final
    {
        MessageDialog(const MessageDialog&) = delete;
        MessageDialog& operator= (const MessageDialog&) = delete;
    public:
        static MessageDialogResult Show(const std::string& message, const std::string& caption, MessageDialogButton buttons, MessageDialogImage image, MessageDialogResult defaultResult) noexcept;
        static MessageDialogResult Show(Window* owner, const std::string& message, const std::string& caption, MessageDialogButton buttons, MessageDialogImage image, MessageDialogResult defaultResult) noexcept;
    };
}
