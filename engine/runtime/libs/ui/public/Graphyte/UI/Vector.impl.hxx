#pragma once
#include <Graphyte/UI/Matrix.hxx>
#include <Graphyte/UI/Point.hxx>
#include <Graphyte/UI/Rect.hxx>
#include <Graphyte/UI/Size.hxx>
#include <Graphyte/UI/Thickness.hxx>
#include <Graphyte/UI/Vector.hxx>
#include <Graphyte/Math/Scalar.hxx>

namespace Graphyte::UI
{
    inline VectorF::VectorF() noexcept
        : X{}
        , Y{}
    {
    }

    inline VectorF::VectorF(const VectorF& other) noexcept
        : X{ other.X }
        , Y{ other.Y }
    {
    }

    inline VectorF::VectorF(float x, float y) noexcept
        : X{ x }
        , Y{ y }
    {
    }

    inline float VectorF::Length() const noexcept
    {
        return Math::Sqrt(
            this->X * this->X +
            this->Y * this->Y
        );
    }

    inline float VectorF::LengthSquared() const noexcept
    {
        return
            this->X * this->X +
            this->Y * this->Y;
    }

    inline void VectorF::Normalize() noexcept
    {
        float const axis_normalized = Math::Max(
            Math::Abs(this->X),
            Math::Abs(this->Y)
        );

        this->X /= axis_normalized;
        this->Y /= axis_normalized;

        float const length = this->Length();

        this->X /= length;
        this->Y /= length;
    }

    inline void VectorF::Negate() noexcept
    {
        this->X = -this->X;
        this->Y = -this->Y;
    }

    inline float VectorF::CrossProduct(const VectorF& vector1, const VectorF& vector2) noexcept
    {
        return (vector1.X * vector2.Y) - (vector1.Y * vector2.X);
    }

    inline float VectorF::DotProduct(const VectorF& vector1, const VectorF& vector2) noexcept
    {
        return (vector1.X * vector2.X) + (vector1.Y * vector2.Y);
    }

    inline float VectorF::AngleBetween(const VectorF& vector1, const VectorF& vector2) noexcept
    {
        float const sin_value = (vector1.X * vector2.Y) - (vector1.Y * vector1.X);
        float const cos_value = (vector1.X * vector2.X) - (vector1.Y * vector2.Y);

        return Math::RadiansToDegrees(
            Math::Atan2(sin_value, cos_value)
        );
    }

    inline VectorF VectorF::Add(const VectorF& vector1, const VectorF& vector2) noexcept
    {
        return VectorF
        {
            vector1.X + vector2.X,
            vector1.Y + vector2.Y
        };
    }

    inline VectorF VectorF::Subtract(const VectorF& vector1, const VectorF& vector2) noexcept
    {
        return VectorF
        {
            vector1.X - vector2.X,
            vector1.Y - vector2.Y
        };
    }

    inline PointF VectorF::Add(const VectorF& vector, const PointF& point) noexcept
    {
        return PointF
        {
            point.X + vector.X,
            point.Y + vector.Y
        };
    }

    inline VectorF VectorF::Multiply(const VectorF& vector, float scalar) noexcept
    {
        return VectorF
        {
            vector.X * scalar,
            vector.Y * scalar
        };
    }

    inline VectorF VectorF::Multiply(float scalar, const VectorF& vector) noexcept
    {
        return VectorF
        {
            vector.X * scalar,
            vector.Y * scalar
        };
    }

    inline VectorF VectorF::Divide(const VectorF& vector, float scalar) noexcept
    {
        return VectorF::Multiply(vector, 1.0F / scalar);
    }

    inline float VectorF::Determinant(const VectorF& vector1, const VectorF& vector2) noexcept
    {
        return (vector1.X * vector2.Y) - (vector1.Y * vector2.X);
    }

    inline VectorF VectorF::Transform(const VectorF& vector, const MatrixF& matrix) noexcept
    {
        return VectorF
        {
            (vector.X * matrix.M11) + (vector.Y * matrix.M12),
            (vector.X * matrix.M21) + (vector.Y * matrix.M22)
        };
    }

    inline VectorF VectorF::Normalize(const VectorF& vector) noexcept
    {
        VectorF result{ vector };
        result.Normalize();
        return result;
    }

    inline VectorF VectorF::Negate(const VectorF& vector) noexcept
    {
        VectorF result{ vector };
        result.Normalize();
        return result;
    }

    inline bool VectorF::operator == (const VectorF& vector) const noexcept
    {
        return this->X == vector.X
            && this->Y == vector.Y;
    }

    inline bool VectorF::operator != (const VectorF& vector) const noexcept
    {
        return this->X != vector.X
            || this->Y != vector.Y;
    }

    inline VectorF::operator PointF() const noexcept
    {
        return PointF
        {
            this->X,
            this->Y
        };
    }

    inline VectorF::operator SizeF() const noexcept
    {
        return SizeF
        {
            Math::Abs(this->X),
            Math::Abs(this->Y)
        };
    }
}
