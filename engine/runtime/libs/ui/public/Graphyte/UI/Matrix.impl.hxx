#pragma once
#include <Graphyte/UI/Matrix.hxx>
#include <Graphyte/UI/Point.hxx>
#include <Graphyte/UI/Rect.hxx>
#include <Graphyte/UI/Size.hxx>
#include <Graphyte/UI/Thickness.hxx>
#include <Graphyte/UI/Vector.hxx>
#include <Graphyte/Math/Scalar.hxx>

namespace Graphyte::UI
{
    inline MatrixF::MatrixF() noexcept
        : M11{ 0.0F }
        , M12{ 0.0F }
        , M21{ 0.0F }
        , M22{ 0.0F }
        , OffsetX{ 0.0F }
        , OffsetY{ 0.0F }
    {
    }

    inline MatrixF::MatrixF(float m11, float m12, float m21, float m22, float offset_x, float offset_y) noexcept
        : M11{ m11 }
        , M12{ m12 }
        , M21{ m21 }
        , M22{ m22 }
        , OffsetX{ offset_x }
        , OffsetY{ offset_y }
    {
    }

    inline MatrixF::MatrixF(const MatrixF& matrix) noexcept
        : M11{ matrix.M11 }
        , M12{ matrix.M12 }
        , M21{ matrix.M21 }
        , M22{ matrix.M22 }
        , OffsetX{ matrix.OffsetX }
        , OffsetY{ matrix.OffsetY }
    {
    }

    inline float MatrixF::GetM11() const noexcept
    {
        return this->M11;
    }

    inline float MatrixF::GetM12() const noexcept
    {
        return this->M12;
    }

    inline float MatrixF::GetM21() const noexcept
    {
        return this->M21;
    }

    inline float MatrixF::GetM22() const noexcept
    {
        return this->M22;
    }

    inline float MatrixF::GetOffsetX() const noexcept
    {
        return this->OffsetX;
    }

    inline float MatrixF::GetOffsetY() const noexcept
    {
        return this->OffsetY;
    }

    inline void MatrixF::SetM11(float value) noexcept
    {
        this->M11 = value;
    }

    inline void MatrixF::SetM12(float value) noexcept
    {
        this->M12 = value;
    }

    inline void MatrixF::SetM21(float value) noexcept
    {
        this->M21 = value;
    }

    inline void MatrixF::SetM22(float value) noexcept
    {
        this->M22 = value;
    }

    inline void MatrixF::SetOffsetX(float value) noexcept
    {
        this->OffsetX = value;
    }

    inline void MatrixF::SetOffsetY(float value) noexcept
    {
        this->OffsetY = value;
    }

    inline bool MatrixF::IsIdentity() const noexcept
    {
        return this->M11 == 1.0F
            && this->M12 == 0.0F
            && this->M21 == 0.0F
            && this->M22 == 1.0F
            && this->OffsetX == 0.0F
            && this->OffsetY == 0.0F;
    }

    inline bool MatrixF::IsInvertible() const noexcept
    {
        return this->Determinant() != 0.0F;
    }

    inline float MatrixF::Determinant() const noexcept
    {
        return (this->M11 * this->M22) - (this->M12 * this->M21);
    }

    inline void MatrixF::Invert() noexcept
    {
        MatrixF::Invert(*this, *this);
    }

    inline void MatrixF::SetIdentity() noexcept
    {
        this->M11 = 1.0F;
        this->M12 = 0.0F;
        this->M21 = 0.0F;
        this->M22 = 1.0F;
        this->OffsetX = 0.0F;
        this->OffsetY = 0.0F;
    }

    inline MatrixF MatrixF::Add(const MatrixF& matrix1, const MatrixF& matrix2) noexcept
    {
        MatrixF result;
        MatrixF::Add(result, matrix1, matrix2);
        return result;
    }

    inline void MatrixF::Add(MatrixF& out_result, const MatrixF& matrix1, const MatrixF& matrix2) noexcept
    {
        out_result = MatrixF
        {
            matrix1.M11 + matrix2.M11,
            matrix1.M12 + matrix2.M12,
            matrix1.M21 + matrix2.M21,
            matrix1.M22 + matrix2.M22,
            matrix1.OffsetX + matrix2.OffsetX,
            matrix1.OffsetY + matrix2.OffsetY
        };
    }

    inline MatrixF MatrixF::Subtract(const MatrixF& matrix1, const MatrixF& matrix2) noexcept
    {
        MatrixF result;
        MatrixF::Subtract(result, matrix1, matrix2);
        return result;
    }

    inline void MatrixF::Subtract(MatrixF& out_result, const MatrixF& matrix1, const MatrixF& matrix2) noexcept
    {
        out_result = MatrixF
        {
            matrix1.M11 - matrix2.M11,
            matrix1.M12 - matrix2.M12,
            matrix1.M21 - matrix2.M21,
            matrix1.M22 - matrix2.M22,
            matrix1.OffsetX - matrix2.OffsetX,
            matrix1.OffsetY - matrix2.OffsetY
        };
    }

    inline MatrixF MatrixF::Multiply(const MatrixF& matrix1, const MatrixF& matrix2) noexcept
    {
        MatrixF result;
        MatrixF::Multiply(result, matrix1, matrix2);
        return result;
    }

    inline void MatrixF::Multiply(MatrixF& out_result, const MatrixF& matrix1, const MatrixF& matrix2) noexcept
    {
        out_result = MatrixF
        {
            (matrix1.M11 * matrix2.M11) + (matrix1.M12 * matrix2.M21),
            (matrix1.M11 * matrix2.M12) + (matrix1.M12 * matrix2.M22),
            (matrix1.M21 * matrix2.M11) + (matrix1.M22 * matrix2.M21),
            (matrix1.M21 * matrix2.M12) + (matrix1.M22 * matrix2.M22),
            (matrix1.OffsetX * matrix2.M11) + (matrix1.OffsetY * matrix2.M21) + matrix2.OffsetX,
            (matrix1.OffsetX * matrix2.M12) + (matrix1.OffsetY * matrix2.M22) + matrix2.OffsetY
        };
    }

    inline MatrixF MatrixF::Divide(const MatrixF& matrix1, const MatrixF& matrix2) noexcept
    {
        MatrixF result;
        MatrixF::Divide(result, matrix1, matrix2);
        return result;
    }

    inline void MatrixF::Divide(MatrixF& out_result, const MatrixF& matrix1, const MatrixF& matrix2) noexcept
    {
        out_result = MatrixF
        {
            matrix1.M11 / matrix2.M11,
            matrix1.M12 / matrix2.M12,
            matrix1.M21 / matrix2.M21,
            matrix1.M22 / matrix2.M22,
            matrix1.OffsetX / matrix2.OffsetX,
            matrix1.OffsetY / matrix2.OffsetY,
        };
    }

    inline MatrixF MatrixF::Multiply(const MatrixF& matrix, float scalar) noexcept
    {
        MatrixF result;
        MatrixF::Multiply(result, matrix, scalar);
        return result;
    }

    inline void MatrixF::Multiply(MatrixF& out_result, const MatrixF& matrix, float scalar) noexcept
    {
        out_result = MatrixF
        {
            matrix.M11 * scalar,
            matrix.M12 * scalar,
            matrix.M21 * scalar,
            matrix.M22 * scalar,
            matrix.OffsetX * scalar,
            matrix.OffsetY * scalar
        };
    }

    inline MatrixF MatrixF::Divide(const MatrixF& matrix, float scalar) noexcept
    {
        MatrixF result;
        MatrixF::Divide(result, matrix, scalar);
        return result;
    }

    inline void MatrixF::Divide(MatrixF& out_result, const MatrixF& matrix, float scalar) noexcept
    {
        float const inv = 1.0F / scalar;
        MatrixF::Multiply(out_result, matrix, inv);
    }

    inline MatrixF MatrixF::Negate(const MatrixF& matrix) noexcept
    {
        return MatrixF
        {
            -matrix.M11,
            -matrix.M12,
            -matrix.M21,
            -matrix.M22,
            -matrix.OffsetX,
            -matrix.OffsetY
        };
    }

    inline void MatrixF::Negate(MatrixF& out_result, const MatrixF& matrix) noexcept
    {
        out_result = MatrixF::Negate(matrix);
    }

    inline void MatrixF::Append(const MatrixF& matrix) noexcept
    {
        MatrixF::Multiply(*this, *this, matrix);
    }

    inline void MatrixF::Prepend(const MatrixF& matrix) noexcept
    {
        MatrixF::Multiply(*this, matrix, *this);
    }

    inline void MatrixF::Rotate(float angle) noexcept
    {
        float sin = 0.0F;
        float cos = 0.0F;

        Math::SinCos(sin, cos, angle);

        float const m11 = (cos * this->M11) + (sin * this->M21);
        float const m12 = (cos * this->M12) + (sin * this->M22);
        float const m21 = (cos * this->M21) - (sin * this->M11);
        float const m22 = (cos * this->M22) - (sin * this->M12);
        float const dx = (cos * this->OffsetX) - (sin * this->OffsetY);
        float const dy = (sin * this->OffsetX) + (cos * this->OffsetY);

        this->M11 = m11;
        this->M12 = m12;
        this->M21 = m21;
        this->M22 = m22;
        this->OffsetX = dx;
        this->OffsetY = dy;
    }

    inline void MatrixF::RotatePrepend(float angle) noexcept
    {
        float sin = 0.0F;
        float cos = 0.0F;

        Math::SinCos(sin, cos, angle);

        float const m11 = (cos * this->M11) + (sin * this->M21);
        float const m12 = (cos * this->M12) + (sin * this->M22);
        float const m21 = (cos * this->M21) - (sin * this->M11);
        float const m22 = (cos * this->M22) - (sin * this->M12);

        this->M11 = m11;
        this->M12 = m12;
        this->M21 = m21;
        this->M22 = m22;
    }

    inline void MatrixF::RotateAt(float angle, const PointF& point) noexcept
    {
        this->Translate(-point.X, -point.Y);
        this->Rotate(angle);
        this->Translate(point.X, point.Y);
    }

    inline void MatrixF::RotateAtPrepend(float angle, const PointF& point) noexcept
    {
        this->Translate(point.X, point.Y);
        this->RotatePrepend(angle);
        this->Translate(-point.X, -point.Y);
    }

    inline void MatrixF::Scale(float scale_x, float scale_y) noexcept
    {
        this->M11 *= scale_x;
        this->M12 *= scale_y;
        this->M21 *= scale_x;
        this->M22 *= scale_y;
        this->OffsetX *= scale_x;
        this->OffsetY *= scale_y;
    }

    inline void MatrixF::ScalePrepend(float scale_x, float scale_y) noexcept
    {
        this->M11 *= scale_x;
        this->M12 *= scale_y;
        this->M21 *= scale_x;
        this->M22 *= scale_y;
    }

    inline void MatrixF::ScaleAt(float scale_x, float scale_y, const PointF& point) noexcept
    {
        this->Translate(-point.X, -point.Y);
        this->Scale(scale_x, scale_y);
        this->Translate(point.X, point.Y);
    }

    inline void MatrixF::ScaleAtPrepend(float scale_x, float scale_y, const PointF& point) noexcept
    {
        this->Translate(point.X, point.Y);
        this->ScalePrepend(scale_x, scale_y);
        this->Translate(-point.X, -point.Y);
    }

    inline void MatrixF::Skew(float shear_x, float shear_y) noexcept
    {
        float const m11 = this->M11 + (this->M12 * shear_x);
        float const m12 = (this->M11 * shear_y) + this->M12;
        float const m21 = this->M21 + (this->M22 * shear_x);
        float const m22 = (this->M21 * shear_y) + this->M22;
        float const dx = this->OffsetX + (this->OffsetY * shear_x);
        float const dy = (this->OffsetX * shear_y) + this->OffsetY;

        this->M11 = m11;
        this->M12 = m12;
        this->M21 = m21;
        this->M22 = m22;
        this->OffsetX = dx;
        this->OffsetY = dy;
    }

    inline void MatrixF::SkewPrepend(float shear_x, float shear_y) noexcept
    {
        float const m11 = this->M11 + (this->M21 * shear_y);
        float const m12 = this->M12 + (this->M22 * shear_y);
        float const m21 = (this->M11 * shear_x) + this->M21;
        float const m22 = (this->M12 * shear_x) + this->M22;

        this->M11 = m11;
        this->M12 = m12;
        this->M21 = m21;
        this->M22 = m22;
    }

    inline void MatrixF::SkewAt(float shear_x, float shear_y, const PointF& point) noexcept
    {
        this->Translate(-point.X, -point.Y);
        this->Skew(shear_x, shear_y);
        this->Translate(point.X, point.Y);
    }

    inline void MatrixF::SkewAtPrepend(float shear_x, float shear_y, const PointF& point) noexcept
    {
        this->Translate(point.X, point.Y);
        this->SkewPrepend(shear_x, shear_y);
        this->Translate(-point.X, -point.Y);
    }

    inline void MatrixF::Translate(float offset_x, float offset_y) noexcept
    {
        this->OffsetX += offset_x;
        this->OffsetY += offset_y;
    }

    inline void MatrixF::TranslatePrepend(float offset_x, float offset_y) noexcept
    {
        this->OffsetX += (offset_x * this->M11) + (offset_y * this->M21);
        this->OffsetY += (offset_x * this->M12) + (offset_y * this->M22);
    }

    inline void MatrixF::Scaling(MatrixF& out_result, const VectorF& scale) noexcept
    {
        MatrixF::Scaling(out_result, scale.X, scale.Y);
    }

    inline MatrixF MatrixF::Scaling(const VectorF& scale) noexcept
    {
        MatrixF result;
        MatrixF::Scaling(result, scale);
        return result;
    }

    inline void MatrixF::Scaling(MatrixF& out_result, float scale_x, float scale_y) noexcept
    {
        out_result.SetIdentity();
        out_result.M11 = scale_x;
        out_result.M22 = scale_y;
    }

    inline MatrixF Scaling(float scale_x, float scale_y) noexcept
    {
        MatrixF result;
        MatrixF::Scaling(result, scale_x, scale_y);
        return result;
    }

    inline void MatrixF::Scaling(MatrixF& out_result, float scale) noexcept
    {
        MatrixF::Scaling(out_result, scale, scale);
    }

    inline MatrixF MatrixF::Scaling(float scale) noexcept
    {
        return MatrixF::Scaling(scale, scale);
    }

    inline void MatrixF::Scaling(MatrixF& out_result, float scale_x, float scale_y, const VectorF& center) noexcept
    {
        out_result = MatrixF
        {
            scale_x,
            0.0F,
            0.0F,
            scale_y,
            center.X - (scale_x * center.X),
            center.Y - (scale_y * center.Y)
        };
    }

    inline MatrixF MatrixF::Scaling(float scale_x, float scale_y, const VectorF& center) noexcept
    {
        return MatrixF
        {
            scale_x,
            0.0F,
            0.0F,
            scale_y,
            center.X - (scale_x * center.X),
            center.Y - (scale_y * center.Y)
        };
    }

    inline void MatrixF::Rotation(MatrixF& out_result, float angle) noexcept
    {
        float cos = 0.0F;
        float sin = 0.0F;
        Math::SinCos(sin, cos, angle);

        out_result = MatrixF
        {
            cos,
            sin,
            -sin,
            cos,
            0.0F,
            0.0F
        };
    }

    inline MatrixF MatrixF::Rotation(float angle) noexcept
    {
        MatrixF result;
        MatrixF::Rotation(result, angle);
        return result;
    }

    inline void MatrixF::Rotation(MatrixF& out_result, float angle, const VectorF& center) noexcept
    {
        MatrixF::Multiply(out_result, MatrixF::Translation(VectorF::Negate(center)), MatrixF::Rotation(angle));
        MatrixF::Multiply(out_result, out_result, MatrixF::Translation(center));
    }

    inline MatrixF MatrixF::Rotation(float angle, const VectorF& center) noexcept
    {
        MatrixF result;
        MatrixF::Rotation(result, angle, center);
        return result;
    }

    inline void MatrixF::Transformation(MatrixF& out_result, float scale_x, float scale_y, float angle, float offset_x, float offset_y) noexcept
    {
        MatrixF::Multiply(out_result, MatrixF::Scaling(scale_x, scale_y), MatrixF::Rotation(angle));
        MatrixF::Multiply(out_result, out_result, MatrixF::Translation(offset_x, offset_y));
    }

    inline MatrixF MatrixF::Transformation(float scale_x, float scale_y, float angle, float offset_x, float offset_y) noexcept
    {
        MatrixF result;
        MatrixF::Transformation(result, scale_x, scale_y, angle, offset_x, offset_y);
        return result;
    }

    inline void MatrixF::Translation(MatrixF& out_result, const VectorF& vector) noexcept
    {
        out_result = MatrixF
        {
            1.0F,
            0.0F,
            0.0F,
            1.0F,
            vector.X,
            vector.Y
        };
    }

    inline MatrixF MatrixF::Translation(const VectorF& vector) noexcept
    {
        MatrixF result;
        MatrixF::Translation(result, vector);
        return result;
    }

    inline void MatrixF::Translation(MatrixF& out_result, float offset_x, float offset_y) noexcept
    {
        out_result = MatrixF
        {
            1.0F,
            0.0F,
            0.0F,
            1.0F,
            offset_x,
            offset_y
        };
    }

    inline MatrixF MatrixF::Translation(float offset_x, float offset_y) noexcept
    {
        MatrixF result;
        MatrixF::Translation(result, offset_x, offset_y);
        return result;
    }

    inline VectorF MatrixF::TransformVector(const MatrixF& matrix, const VectorF& vector) noexcept
    {
        VectorF result;
        MatrixF::TransformVector(result, matrix, vector);
        return result;
    }

    inline void MatrixF::TransformVector(VectorF& out_result, const MatrixF& matrix, const VectorF& vector) noexcept
    {
        out_result = VectorF
        {
            (vector.X * matrix.M11) + (vector.Y * matrix.M21),
            (vector.X * matrix.M12) + (vector.Y * matrix.M22)
        };
    }

    inline PointF MatrixF::TransformPoint(const MatrixF& matrix, const PointF& point) noexcept
    {
        PointF result;
        MatrixF::TransformPoint(result, matrix, point);
        return result;
    }

    inline void MatrixF::TransformPoint(PointF& out_result, const MatrixF& matrix, const PointF& point) noexcept
    {
        out_result = PointF
        {
            (point.X * matrix.M11) + (point.Y * matrix.M21) + matrix.OffsetX,
            (point.X * matrix.M12) + (point.Y * matrix.M22) + matrix.OffsetY
        };
    }

    inline void MatrixF::Invert(MatrixF& out_result, const MatrixF& matrix) noexcept
    {
        float const det = matrix.Determinant();

        if (Math::IsZero(det))
        {
            out_result.SetIdentity();
        }
        else
        {
            float const inv_det = 1.0F / det;

            out_result = MatrixF{
                matrix.M22 * inv_det,
                -matrix.M12 * inv_det,
                -matrix.M21 * inv_det,
                matrix.M11 * inv_det,
                ((matrix.M21 * matrix.OffsetY) - (matrix.OffsetX * matrix.M22)) * inv_det,
                ((matrix.OffsetX * matrix.M12) - (matrix.M11 * matrix.OffsetY)) * inv_det
            };
        }
    }

    inline MatrixF MatrixF::Skewing(float angle_x, float angle_y) noexcept
    {
        MatrixF result;
        MatrixF::Skewing(result, angle_x, angle_y);
        return result;
    }

    inline void MatrixF::Skewing(MatrixF& out_result, float angle_x, float angle_y) noexcept
    {
        out_result = MatrixF
        {
            1.0F,
            Math::Tan(angle_x),
            Math::Tan(angle_y),
            1.0F,
            0.0F,
            0.0F
        };
    }

    inline MatrixF MatrixF::FromRectanglePoly(const RectF& rect, const PointF& point1, const PointF& point2, const PointF& point3) noexcept
    {
        VectorF const v1 = PointF::Subtract(point2, point1);
        VectorF const v2 = PointF::Subtract(point3, point1);
        float const scaled_x = rect.X / rect.Width;
        float const scaled_y = rect.Y / rect.Height;

        return MatrixF
        {
            v1.X / rect.Width,
            v1.Y / rect.Width,
            v2.X / rect.Height,
            v2.Y / rect.Height,
            point1.X - (scaled_x * v1.X) - (scaled_y * v2.X),
            point1.Y - (scaled_x * v1.Y) - (scaled_y * v2.Y)
        };
    }

    inline bool MatrixF::operator == (const MatrixF& matrix) const noexcept
    {
        return this->M11 == matrix.M11
            && this->M12 == matrix.M12
            && this->M21 == matrix.M21
            && this->M22 == matrix.M22
            && this->OffsetX == matrix.OffsetX
            && this->OffsetY == matrix.OffsetY;
    }

    inline bool MatrixF::operator != (const MatrixF& matrix) const noexcept
    {
        return this->M11 != matrix.M11
            || this->M12 != matrix.M12
            || this->M21 != matrix.M21
            || this->M22 != matrix.M22
            || this->OffsetX != matrix.OffsetX
            || this->OffsetY != matrix.OffsetY;
    }
}
