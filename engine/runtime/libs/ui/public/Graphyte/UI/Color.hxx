#pragma once
#include <Graphyte/UI.module.hxx>
#include <Graphyte/Types.hxx>

namespace Graphyte::UI
{
    enum class KnownColor : uint32_t;

    struct ColorU final
    {
    public:
        union
        {
            struct
            {
                uint8_t B;
                uint8_t G;
                uint8_t R;
                uint8_t A;
            };
            uint32_t Value;
        };

    public: // constructors
        ColorU() noexcept;
        ColorU(uint8_t r, uint8_t g, uint8_t b) noexcept;
        ColorU(uint8_t r, uint8_t g, uint8_t b, uint8_t a) noexcept;
        ColorU(float r, float g, float b) noexcept;
        ColorU(float r, float g, float b, float a) noexcept;
        explicit ColorU(uint32_t value) noexcept;
        explicit ColorU(KnownColor value) noexcept;

    public: // conversions
        uint32_t ToUInt32() const noexcept;
        Float4A ToFloat4() const noexcept;
    };
}

#include <Graphyte/UI/Color.impl.hxx>

