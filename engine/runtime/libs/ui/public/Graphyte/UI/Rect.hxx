#pragma once
#include <Graphyte/UI.module.hxx>

namespace Graphyte::UI
{
    struct PointF;
    struct SizeF;
    struct VectorF;
    struct ThicknessF;
    struct MatrixF;

    struct RectF final
    {
        //
        // Fields.
        //
    public:
        float X;
        float Y;
        float Width;
        float Height;

        //
        // Constructors.
        //
    public:
        RectF() noexcept;
        RectF(const RectF& rect) noexcept;
        RectF(float x, float y, float width, float height) noexcept;
        explicit RectF(const SizeF& size) noexcept;
        RectF(const PointF& location, const VectorF& vector) noexcept;
        RectF(const PointF& location, const SizeF& size) noexcept;
        RectF(const PointF& point1, const PointF& point2) noexcept;

        //
        // Mutator methods.
        //
    public:
        PointF GetLocation() const noexcept;
        void SetLocation(const PointF& value) noexcept;

        PointF GetCenter() const noexcept;
        void SetCenter(const PointF& value) noexcept;

        SizeF GetSize() const noexcept;
        void SetSize(const SizeF& value) noexcept;

        float GetX() const noexcept;
        void SetX(float value) noexcept;

        float GetY() const noexcept;
        void SetY(float value) noexcept;

        float GetWidth() const noexcept;
        void SetWidth(float value) noexcept;

        float GetHeight() const noexcept;
        void SetHeight(float value) noexcept;

        float GetLeft() const noexcept;
        void SetLeft(float value) noexcept;

        float GetTop() const noexcept;
        void SetTop(float value) noexcept;

        float GetRight() const noexcept;
        void SetRight(float value) noexcept;

        float GetBottom() const noexcept;
        void SetBottom(float value) noexcept;

        PointF GetTopLeft() const noexcept;
        void SetTopLeft(const PointF& value) noexcept;

        PointF GetTopRight() const noexcept;
        void SetTopRight(const PointF& value) noexcept;

        PointF GetBottomLeft() const noexcept;
        void SetBottomLeft(const PointF& value) noexcept;

        PointF GetBottomRight() const noexcept;
        void SetBottomRight(const PointF& value) noexcept;

        //
        // Named Constructors
        //
    public:
        static RectF CreateFromLTRB(float left, float top, float right, float bottom) noexcept;
        static RectF CreateCentered(const SizeF& size) noexcept;
        static RectF CreateCentered(const ThicknessF& thickness) noexcept;
        static RectF CreateEmpty() noexcept;

    public:
        bool Contains(const PointF& point) const noexcept;
        bool Contains(float x, float y) const noexcept;
        bool Contains(const RectF& rect) const noexcept;

    public:
        bool IntersectsWith(const RectF& rect) const noexcept;

    public:
        static RectF Intersect(const RectF& rect1, const RectF& rect2) noexcept;
        static RectF Union(const RectF& rect1, const RectF& rect2) noexcept;
        static RectF Union(const RectF& rect, const PointF& point) noexcept;
        static RectF Offset(const RectF& rect, const VectorF& vector) noexcept;
        static RectF Offset(const RectF& rect, float offset_x, float offset_y) noexcept;
        static RectF Inflate(const RectF& rect, const SizeF& size) noexcept;
        static RectF Inflate(const RectF& rect, float width, float height) noexcept;
        static RectF Scale(const RectF& rect, float scale_x, float scale_y) noexcept;

    public:
        static RectF GrowBy(const RectF& rect, float value) noexcept;
        static RectF GrowBy(const RectF& rect, float horizontal, float vertical) noexcept;
        static RectF GrowBy(const RectF& rect, float left, float top, float right, float bottom) noexcept;
        static RectF GrowBy(const RectF& rect, const ThicknessF& thickness) noexcept;

    public:
        static RectF Transform(const RectF& rect, const MatrixF& matrix) noexcept;

    public:
        bool operator == (const RectF& rect) const noexcept;
        bool operator != (const RectF& rect) const noexcept;
    };
}

#include <Graphyte/UI/Rect.impl.hxx>
