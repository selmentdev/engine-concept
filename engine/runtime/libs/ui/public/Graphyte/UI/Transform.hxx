#pragma once
#include <Graphyte/UI.module.hxx>
#include <Graphyte/UI/Rect.hxx>

namespace Graphyte::UI
{
    struct MatrixF;
    struct RectF;
    struct PointF;

    struct RotationF final
    {
    public:
        float Angle;

    public:
        RotationF() noexcept = default;
        RotationF(float angle) noexcept;
    };

    struct ScaleF final
    {
    public:
        float ScaleX;
        float ScaleY;

    public:
        ScaleF() noexcept = default;
        ScaleF(float scale_x, float scale_y) noexcept;
    };

    struct SkewF
    {
    public:
        float AngleX;
        float AngleY;

    public:
        SkewF() noexcept = default;
        SkewF(float angle_x, float angle_y) noexcept;
    };

    struct TranslationF
    {
    public:
        float X;
        float Y;
       
    public:
        TranslationF() noexcept = default;
        TranslationF(float x, float y) noexcept;
    };

    struct TransformF
    {
    public:
        TranslationF Translation;
        RotationF Rotation;
        ScaleF Scale;
        SkewF Skew;

    public:
        void ComputeTransformMatrix(MatrixF& out_result) noexcept;
        void ComputeTransformMatrix(MatrixF& out_result, const PointF& transform_origin) noexcept;
        void ComputeTransformMatrix(MatrixF& out_result, const RectF& rectangle) noexcept
        {
            return this->ComputeTransformMatrix(out_result, rectangle.GetCenter());
        }
    };
}

#include <Graphyte/UI/Transform.impl.hxx>
