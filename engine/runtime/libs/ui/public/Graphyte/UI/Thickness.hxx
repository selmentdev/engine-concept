#pragma once
#include <Graphyte/UI.module.hxx>

namespace Graphyte::UI
{
    struct PointF;
    struct SizeF;
    struct VectorF;
    struct RectF;

    struct ThicknessF final
    {
    public:
        float Left;
        float Top;
        float Right;
        float Bottom;

    public:
        ThicknessF() noexcept;
        ThicknessF(const ThicknessF& other) noexcept;
        explicit ThicknessF(float value) noexcept;
        ThicknessF(float horizontal, float vertical) noexcept;
        ThicknessF(float left, float top, float right, float bottom) noexcept;

    public:
        float GetLeft() const noexcept;
        void SetLeft(float value) noexcept;

        float GetTop() const noexcept;
        void SetTop(float value) noexcept;

        float GetRight() const noexcept;
        void SetRight(float value) noexcept;

        float GetBottom() const noexcept;
        void SetBottom(float value) noexcept;

    public:
        bool IsZero() const noexcept;
        bool IsUniform() const noexcept;
        bool IsClose(const ThicknessF& other) const noexcept;

    public:
        static ThicknessF Scale(const ThicknessF& other, float scale_x, float scale_y) noexcept;
    };
}

#include <Graphyte/UI/Thickness.impl.hxx>
