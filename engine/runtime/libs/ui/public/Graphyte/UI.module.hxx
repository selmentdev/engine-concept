#pragma once
#include <Graphyte/Platform/Impl/Detect.hxx>

#if GRAPHYTE_STATIC_BUILD
#define UI_API
#else
#if defined(module_ui_EXPORTS)
#define UI_API     GX_LIB_EXPORT
#else
#define UI_API     GX_LIB_IMPORT
#endif
#endif
