#include "Framework.pch.hxx"
#include <Graphyte/Base.module.hxx>
#include <Graphyte/Modules.hxx>

#include <Graphyte/Platform.hxx>

class FrameworkModule final : public Graphyte::IModule
{
public:
    virtual void OnInitialize() noexcept final override
    {
        GX_LOG(LogPlatform, Trace, "{}\n", __FUNCTION__);
    }

    virtual void OnFinalize() noexcept final override
    {
        GX_LOG(LogPlatform, Trace, "{}\n", __FUNCTION__);
    }
};

GX_IMPLEMENT_MODULE(FrameworkModule);
