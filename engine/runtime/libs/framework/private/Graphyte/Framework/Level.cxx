#include "Framework.pch.hxx"
#include <Graphyte/Framework/Level.hxx>
#include <Graphyte/Framework/Actor.hxx>

namespace Graphyte::Framework
{
    Level::Level() noexcept
        : Actors{}
        , ParentWorld{}
    {
    }

    Level::~Level() noexcept
    {
        for (auto& actor : Actors)
        {
            actor->Finalize();
            delete actor;
        }
    }
}
