#include "Framework.pch.hxx"
#include <Graphyte/Framework/GameMode.hxx>

namespace Graphyte::Framework
{
    GameMode::GameMode() noexcept = default;
    GameMode::~GameMode() noexcept = default;
}
