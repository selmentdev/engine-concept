#include "Framework.pch.hxx"
#include <Graphyte/Framework/Viewport.hxx>
#include <Graphyte/Graphics/Gpu/GpuDevice.hxx>

namespace Graphyte::Framework
{
    Viewport::Viewport(Application::Window* window, Framework::ViewportClient* client) noexcept
        : m_Window{ window }
        , m_Client{ client }
        , m_Viewport{ nullptr }
    {
    }

    Viewport::~Viewport() noexcept
    {
        (void)m_Client;
    }

    void Viewport::BeginRenderFrame(Graphics::GpuDevice& device) noexcept
    {
        device.BeginDrawViewport(m_Viewport);
    }

    void Viewport::EndRenderFrame(Graphics::GpuDevice& device) noexcept
    {
        device.EndDrawViewport(m_Viewport, true, 0);
    }

    void Viewport::Tick(float deltaTime) noexcept
    {
        (void)deltaTime;
    }

    void Viewport::InitializeGpuResources() noexcept
    {
        GX_ASSERT(GRenderDevice != nullptr);

        Platform::Size size = m_Window->GetViewportSize();
        m_Viewport = GRenderDevice->CreateViewport(
            m_Window->GetNativeHandle(),
            static_cast<uint32_t>(size.Width),
            static_cast<uint32_t>(size.Height),
            false,
            Graphyte::Graphics::PixelFormat::B8G8R8A8_UNORM,
            Graphyte::Graphics::PixelFormat::D32_FLOAT,
            Graphyte::Graphics::GpuMsaaQuality::Disabled
        );
    }

    void Viewport::UpdateGpuResources() noexcept
    {
    }

    void Viewport::ReleaseGpuResources() noexcept
    {
        GRenderDevice->DestroyViewport(m_Viewport);
    }

    void Viewport::SynchronizeSize(bool fullscreen) noexcept
    {
        Platform::Size size = m_Window->GetViewportSize();

        GRenderDevice->ResizeViewport(
            m_Viewport,
            static_cast<uint32_t>(size.Width),
            static_cast<uint32_t>(size.Height),
            fullscreen,
            Graphyte::Graphics::PixelFormat::B8G8R8A8_UNORM
        );
    }
}
