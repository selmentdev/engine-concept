#include "Framework.pch.hxx"
#include <Graphyte/Framework/EntityManager.hxx>
#include <Graphyte/Diagnostics.hxx>

namespace Graphyte::Framework
{
    EntityManager::EntityManager() noexcept = default;

    EntityManager::~EntityManager() noexcept = default;

    bool EntityManager::Exists(Entity entity) const noexcept
    {
        auto index = entity.Index;

        if (index < m_Entities.capacity())
        {
            auto entityData = m_Entities[index];

            return (entityData.Block != nullptr) && (entityData.Version == entity.Version);
        }
        else
        {
            GX_ASSERTF(false, "Entity does not exist or has been invalidated");
        }

        return false;
    }
}
