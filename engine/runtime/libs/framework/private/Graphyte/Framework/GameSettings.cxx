#include "Framework.pch.hxx"
#include <Graphyte/Framework/GameSettings.hxx>
#include <Graphyte/ConfigFile.hxx>

namespace Graphyte::Framework
{
    static void GetQualityLevel(ConfigFile& file, std::string_view name, QualityLevel& value) noexcept
    {
        int32_t result{};
        if (file.GetInt32("", name, result))
        {
            if (result >= 0 && result < 4)
            {
                value = static_cast<QualityLevel>(result);
            }
        }

        value = QualityLevel::Low;
    }

//    static bool GetQ

    bool GameUserSettings::Save([[maybe_unused]] Storage::Archive& archive) noexcept
    {
        return false;
    }

    bool GameUserSettings::Load(Storage::Archive& archive) noexcept
    {
        auto size_archive = archive.GetSize();
        GX_ASSERT(size_archive < std::numeric_limits<int32_t>::max());

        auto size = static_cast<size_t>(size_archive);

        std::string buffer{};
        buffer.resize(size);

        archive.Serialize(std::data(buffer), std::size(buffer));

        ConfigFile config{};

        config.GetFloat("", "ResolutionScale", this->ResolutionScale);
        config.GetBool("", "VSync", this->VSync);
        config.GetInt32("", "FrameRateLimit", this->FrameRateLimit);

        GetQualityLevel(config, "AudioQuality", this->AudioQuality);
        GetQualityLevel(config, "ViewDistanceQuality", this->ViewDistanceQuality);
        GetQualityLevel(config, "ShadowQuality", this->ShadowQuality);
        GetQualityLevel(config, "AntiAliasingQuality", this->AntiAliasingQuality);
        GetQualityLevel(config, "TextureQuality", this->TextureQuality);
        GetQualityLevel(config, "VisualEffectsQuality", this->VisualEffectsQuality);
        GetQualityLevel(config, "PostProcessQuality", this->PostProcessQuality);
        GetQualityLevel(config, "FoliageQuality", this->FoliageQuality);

        //System::Size ScreenResolution{};
        //System::WindowMode FullscreenType{};
        //int32_t FrameRateLimit{};
        //float ResolutionScale{};
        //bool VSync{};

        return true;
    }
}
