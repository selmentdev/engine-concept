#include "Framework.pch.hxx"
#include <Graphyte/Framework/GameInstance.hxx>

namespace Graphyte::Framework
{
    GameInstance::GameInstance() noexcept
        : m_World{}
        , m_CurrentMode{}
    {
    }

    GameInstance::~GameInstance() noexcept
    {
    }

    void GameInstance::Tick(float deltaTime) noexcept
    {
        if (m_CurrentMode != nullptr)
        {
            m_CurrentMode->Tick(deltaTime);
        }
    }

    void GameInstance::SetCurrentMode(std::unique_ptr<Framework::GameMode> mode) noexcept
    {
        if (m_CurrentMode != nullptr)
        {
            m_CurrentMode->Finalize();
        }

        if (mode != nullptr)
        {
            m_CurrentMode = std::move(mode);
            m_CurrentMode->Initialize();
        }
    }
}
