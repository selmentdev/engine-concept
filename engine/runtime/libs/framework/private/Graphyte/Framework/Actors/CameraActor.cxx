#include "Framework.pch.hxx"
#include <Graphyte/Framework/Actors/CameraActor.hxx>
#include <Graphyte/Diagnostics.hxx>
#include <Graphyte/Storage/Path.hxx>

#include <fmt/format.h>

namespace Graphyte::Framework
{
    CameraActor::CameraActor() noexcept
    {
    }

    CameraActor::~CameraActor() noexcept = default;
}
