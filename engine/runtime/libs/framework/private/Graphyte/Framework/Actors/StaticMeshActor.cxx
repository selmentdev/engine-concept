#include "Framework.pch.hxx"
#include <Graphyte/Framework/Actors/MeshActor.hxx>

namespace Graphyte::Framework
{
    MeshActor::MeshActor() noexcept = default;

    MeshActor::~MeshActor() noexcept = default;
}
