#include "Framework.pch.hxx"
#include <Graphyte/Framework/Actors/VehicleActor.hxx>

namespace Graphyte::Framework
{
    VehicleActor::VehicleActor() noexcept = default;
    VehicleActor::~VehicleActor() noexcept = default;
}
