#include "Framework.pch.hxx"
#include <Graphyte/Framework/Actors/StaticMeshActor.hxx>

namespace Graphyte::Framework
{
    StaticMeshActor::StaticMeshActor() noexcept = default;

    StaticMeshActor::~StaticMeshActor() noexcept = default;
}
