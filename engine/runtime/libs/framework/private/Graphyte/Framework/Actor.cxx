#include "Framework.pch.hxx"
#include <Graphyte/Framework/Actor.hxx>

namespace Graphyte::Framework
{
    Actor::Actor() noexcept
        : m_Transform{}
    {
    }

    Actor::~Actor() noexcept = default;
}
