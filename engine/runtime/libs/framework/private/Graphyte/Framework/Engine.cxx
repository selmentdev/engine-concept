#include "Framework.pch.hxx"
#include <Graphyte/Framework/Engine.hxx>

namespace Graphyte::Framework
{
    Engine::Engine() noexcept = default;

    Engine::~Engine() noexcept = default;

    FRAMEWORK_API Engine* GEngine{nullptr};
}
