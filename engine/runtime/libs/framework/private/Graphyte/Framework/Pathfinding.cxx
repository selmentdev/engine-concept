#include "Framework.pch.hxx"

#if false
#include <Graphyte/Framework/Pathfinding.hxx>

namespace Graphyte::Framework
{
    void test()
    {
        Navigation::Pathfinding::Graph<int> graph{};

        auto node1 = graph.AddNode(2);
        auto node2 = graph.AddNode(2);
        auto node3 = graph.AddNode(2);
        auto node4 = graph.AddNode(2);

        graph.AddEdge(node1, node2, 3.0F);
        graph.AddEdge(node1, node3, 2.0F);
        graph.AddEdge(node2, node4, 1.0F);
        graph.AddEdge(node3, node4, 0.5F);

        std::vector<Navigation::Pathfinding::NodeIndex> path{};
        Navigation::Pathfinding::FindPath(graph, [](auto, auto) { return 0.0F; }, node1, node4, path);
    }
}
#endif
