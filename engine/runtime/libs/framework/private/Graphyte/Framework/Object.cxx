#include "Framework.pch.hxx"
#include <Graphyte/Framework/Object.hxx>

namespace Graphyte::Framework
{
    Object::Object() noexcept = default;
    Object::~Object() noexcept = default;
}
