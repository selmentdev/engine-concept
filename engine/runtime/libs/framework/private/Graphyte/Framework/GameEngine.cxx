#include "Framework.pch.hxx"
#include <Graphyte/Framework/GameEngine.hxx>
#include <Graphyte/Application.hxx>

namespace Graphyte::Framework
{
    GameEngine::GameEngine(std::unique_ptr<GameInstance> instance) noexcept
        : m_Window{}
        , m_Viewport{ nullptr }
        , m_Client{ nullptr }
        , m_Instance{ std::move(instance) }
    {
    }

    GameEngine::~GameEngine() noexcept
    {
    }

    void GameEngine::Initialize() noexcept
    {
        CreateWindow();
        m_Client = new Framework::GameViewportClient();
        m_Viewport = new Framework::Viewport(m_Window, m_Client);
        m_Viewport->InitializeGpuResources();
        m_Instance->Initialize();

        auto size = m_Window->GetViewportSize();
        m_Renderer = std::make_unique<Rendering::DeferredShadingSceneRenderer>(
            static_cast<uint32_t>(size.Width),
            static_cast<uint32_t>(size.Height)
            );
    }

    void GameEngine::Finalize() noexcept
    {
        delete m_Client;
        m_Renderer->ReleaseGpuResources();
        m_Instance->Finalize();
        m_Viewport->ReleaseGpuResources();
        delete m_Viewport;

        Application::DestroyWindow(m_Window);
        m_Window = nullptr;
    }

    static float m_Counter = 0.0F;
    static float m_Zoom = 16.0F;
    static int FpsCount = 0;
    static float TimePassed = 0.0F;
    static bool RotateLeft{};
    static bool RotateRight{};
    static bool ZoomIn{};
    static bool ZoomOut{};

    void GameEngine::Tick(
        float deltaTime,
        [[maybe_unused]] bool idle
    ) noexcept
    {
        TimePassed += deltaTime;
        ++FpsCount;
        if (TimePassed >= 1.0F)
        {
            m_Window->SetCaption(fmt::format("FPS: {} / {}", FpsCount / TimePassed, FpsCount).c_str());
            FpsCount = 0;
            TimePassed = 0.0F;
        }

        if (RotateLeft)
        {
            m_Counter += deltaTime;
        }
        else if (RotateRight)
        {
            m_Counter -= deltaTime;
        }

        if (ZoomIn)
        {
            m_Zoom += deltaTime * 5.0F;
        }
        else if (ZoomOut)
        {
            m_Zoom -= deltaTime * 5.0F;
        }

        if (m_Counter > Math::PI2<float>)
        {
            m_Counter -= Math::PI2<float>;
        }

        float cs = Graphyte::Math::Cos(m_Counter);
        float ss = Graphyte::Math::Sin(m_Counter);

        auto view = Graphyte::Math::Matrix::LookAtLH(
            Graphyte::Math::Vector3::Make(m_Zoom * cs, m_Zoom * ss, 3.0F),
            Graphyte::Math::Vector3::Zero(),
            Graphyte::Math::Vector3::UnitZ()
        );
        auto proj = Graphyte::Math::Matrix::PerspectiveFovLH(Graphyte::Math::DegreesToRadians(55.0F), 1920.0F / 1080.0F, 0.001F, 100.0F);

        m_Renderer->SetupView(view, proj);

        m_Instance->Tick(deltaTime);
        m_Viewport->Tick(deltaTime);

        m_Viewport->BeginRenderFrame(*Graphyte::GRenderDevice);
        m_Renderer->Render(*Graphyte::GRenderDevice->GetIntermediateCommandList());
        m_Viewport->EndRenderFrame(*Graphyte::GRenderDevice);
    }

    bool GameEngine::CanHandleInput(Application::Window& window) noexcept
    {
        (void)window;
        return true;
    }

    void GameEngine::OnWindowClose(Application::Window& window) noexcept
    {
        (void)window;
        Application::RequestExit(false);
    }

    bool GameEngine::OnKeyDown(Input::KeyCode key, char32_t character, bool repeat) noexcept
    {
        if (key == Input::KeyCode::ArrowLeft)
        {
            RotateLeft = true;
        }
        else if (key == Input::KeyCode::ArrowRight)
        {
            RotateRight = true;
        }
        else if (key == Input::KeyCode::ArrowDown)
        {
            ZoomIn = true;
        }
        else if (key == Input::KeyCode::ArrowUp)
        {
            ZoomOut = true;
        }

        (void)character;
        (void)repeat;
        return Engine::OnKeyDown(key, character, repeat);
    }

    bool GameEngine::OnKeyUp(Input::KeyCode key, char32_t character, bool repeat) noexcept
    {
        if (key == Input::KeyCode::ArrowLeft)
        {
            RotateLeft = false;
        }
        else if (key == Input::KeyCode::ArrowRight)
        {
            RotateRight = false;
        }
        else if (key == Input::KeyCode::ArrowDown)
        {
            ZoomIn = false;
        }
        else if (key == Input::KeyCode::ArrowUp)
        {
            ZoomOut = false;
        }
        else if (key == Input::KeyCode::Q)
        {
            GX_ABORT("This is some expression: {}, {}, {}", static_cast<uint32_t>(key), static_cast<uint32_t>(character), repeat);
        }
        else if (key == Input::KeyCode::F1)
        {
            GX_ASSERTF(false, "raw failure reason", "some failure text: {1} {0}", 0.01F, 1.01F);
        }
        else if (key == Input::KeyCode::Escape)
        {
            static bool fs = false;
            fs = !fs;
            this->m_Viewport->SynchronizeSize(fs);
        }
        else if (key == Input::KeyCode::Enter)
        {
            m_Window->SetWindowMode(
                m_Window->GetWindowMode() == Application::WindowMode::Windowed
                ? Application::WindowMode::WindowedFullscreen
                : Application::WindowMode::Windowed
            );
        }

        return Engine::OnKeyUp(key, character, repeat);
    }

    bool GameEngine::OnControllerAnalog(
        [[maybe_unused]] Input::GamepadKey key,
        [[maybe_unused]] uint32_t controller,
        [[maybe_unused]] float value
    ) noexcept
    {
        switch (key)
        {
        case Input::GamepadKey::LeftAnalogX:
            {
                RotateLeft = Graphyte::Math::IsZero(value, 0.01F);
                break;
            }
        case Input::GamepadKey::RightAnalogX:
            {
                RotateRight = Graphyte::Math::IsZero(value, 0.01F);
                break;
            }
        case Input::GamepadKey::LeftAnalogY:
            {
                ZoomOut = Graphyte::Math::IsZero(value, 0.01F);
                break;
            }
        case Input::GamepadKey::RightAnalogY:
            {
                ZoomIn = Graphyte::Math::IsZero(value, 0.01F);
                break;
            }
        default:
            {
                break;
            }
        }

        return true;
    }

    void GameEngine::CreateWindow() noexcept
    {
        Application::WindowDescriptor descriptor{
            .Title               = "Game Window",
            .Position            = { 120, 120 },
            .Size                = { 1280, 720 },
            .Type                = Application::WindowType::GameWindow,
            .ActivationPolicy    = Application::WindowActivationPolicy::FirstShown,
            .SystemBorder        = true,
            .Taskbar             = true,
            .Topmost             = false,
            .AcceptInput         = true,
            .CloseButton         = true,
            .MinimizeButton      = true,
            .MaximizeButton      = true,
            .Resizable           = true,
            .Regular             = true,
            .PreserveAspectRatio = true,
            .DelayResize         = true,
        };

        m_Window = Application::CreateWindow(descriptor);
        m_Window->SetCaption("Graphyte Engine");
        m_Window->Show();
    }
}
