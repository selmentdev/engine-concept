#include "Framework.pch.hxx"
#include <Graphyte/Framework/World.hxx>
#include <Graphyte/Framework/Level.hxx>

namespace Graphyte::Framework
{
    World::World() noexcept
        : Levels{}
    {
    }

    World::~World() noexcept
    {
        for (auto* level : Levels)
        {
            level->Finalize();
            delete level;
        }
    }
}
