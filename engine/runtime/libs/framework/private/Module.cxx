#include "Framework.pch.hxx"
#include <Graphyte/Framework.module.hxx>
#include <Graphyte/Modules.hxx>

#include <Graphyte/Platform.hxx>

class FrameworkModule final : public Graphyte::IModule
{
public:
    virtual void OnInitialize() noexcept final override
    {
    }

    virtual void OnFinalize() noexcept final override
    {
    }
};

GX_IMPLEMENT_MODULE(FrameworkModule);
