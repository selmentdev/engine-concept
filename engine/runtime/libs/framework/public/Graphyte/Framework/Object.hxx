#pragma once
#include <Graphyte/Framework.module.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API Object
    {
    public:
        Object() noexcept;
        virtual ~Object() noexcept;

    public:
        virtual void Initialize() noexcept { }
        virtual void Finalize() noexcept { }
    };
}
