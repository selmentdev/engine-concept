#pragma once
#include <Graphyte/Framework/ViewportClient.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API GameViewportClient final
        : public ViewportClient
    {
    public:
        GameViewportClient() noexcept = default;
        virtual ~GameViewportClient() noexcept = default;
    };
}
