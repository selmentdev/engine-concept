#pragma once
#include <Graphyte/Framework.module.hxx>
#include <Graphyte/Application.hxx>
#include <Graphyte/Framework/Object.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API Engine
        : public Framework::Object
        , public Application::EventHandler
    {
    public:
        Engine() noexcept;
        virtual ~Engine() noexcept;

    public:
    	virtual void Tick(float deltaTime, bool idle) noexcept = 0;
    };

    extern FRAMEWORK_API Engine* GEngine;
}
