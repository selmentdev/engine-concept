#pragma once
#include <Graphyte/Application.hxx>
#include <Graphyte/Platform.hxx>
#include <Graphyte/Storage/FileManager.hxx>
#include <Graphyte/Storage/Archive.hxx>

namespace Graphyte::Framework
{
    enum struct QualityLevel : uint8_t
    {
        Low     = 0,
        Medium  = 1,
        High    = 2,
        Ultra   = 3,
    };

    class GameUserSettings
    {
    public:
        Platform::Size ScreenResolution{};
        Application::WindowMode FullscreenType{};
        int32_t FrameRateLimit{};
        float ResolutionScale{};
        bool VSync{};
        QualityLevel AudioQuality{};
        QualityLevel ViewDistanceQuality{};
        QualityLevel ShadowQuality{};
        QualityLevel AntiAliasingQuality{};
        QualityLevel TextureQuality{};
        QualityLevel VisualEffectsQuality{};
        QualityLevel PostProcessQuality{};
        QualityLevel FoliageQuality{};

    public:
        bool Save(Storage::Archive& archive) noexcept;
        bool Load(Storage::Archive& archive) noexcept;
    };
}
