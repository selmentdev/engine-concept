#pragma once
#include <Graphyte/Framework.module.hxx>
#include <Graphyte/Framework/Actor.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API MeshActor : public Actor
    {
    public:
        MeshActor() noexcept;
        virtual ~MeshActor() noexcept;
    };
}
