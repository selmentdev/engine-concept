#pragma once
#include <Graphyte/Framework.module.hxx>
#include <Graphyte/Framework/Actor.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API StaticMeshActor : public Actor
    {
    public:
        StaticMeshActor() noexcept;
        virtual ~StaticMeshActor() noexcept;
    };
}
