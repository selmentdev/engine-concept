#pragma once
#include <Graphyte/Framework/Actor.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API VehicleActor : public Actor
    {
    public:
        VehicleActor() noexcept;
        virtual ~VehicleActor() noexcept;
    };
}
