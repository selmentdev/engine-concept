#pragma once
#include <Graphyte/Framework/Actor.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API CameraActor : public Actor
    {
    public:
        CameraActor() noexcept;
        virtual ~CameraActor() noexcept;
    };
}
