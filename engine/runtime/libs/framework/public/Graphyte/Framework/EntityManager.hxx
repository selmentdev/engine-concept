#pragma once
#include <Graphyte/Framework.module.hxx>
#include <Graphyte/Platform.hxx>

namespace Graphyte::Framework
{
    class ChunkAllocator final
    {
    private:
        std::byte* m_FirstChunk{};
        std::byte* m_LastChunk{};
        size_t m_LastChunkUsedSize{};

        static constexpr size_t ms_ChunkSize = 64 * 1024;
        static constexpr size_t ms_ChunkAlignment = 64;

    public:
        ChunkAllocator() noexcept
        {
        }
        ~ChunkAllocator() noexcept
        {
            while (m_FirstChunk != nullptr)
            {
                auto next = reinterpret_cast<std::byte**>(m_FirstChunk)[0];
                Platform::OsFree(m_FirstChunk);
                m_FirstChunk = next;
            }

            m_LastChunk = nullptr;
        }

        std::byte* Allocate(size_t size, size_t alignment) noexcept
        {
            size_t alignedSize = (m_LastChunkUsedSize + alignment - 1) & ~(alignment - 1);

            if (m_LastChunk == nullptr || size > (ms_ChunkSize - alignedSize))
            {
                auto chunk = reinterpret_cast<std::byte*>(Platform::OsMalloc(ms_ChunkSize));
                reinterpret_cast<std::byte**>(chunk)[0] = nullptr;

                if (m_LastChunk != nullptr)
                {
                    reinterpret_cast<std::byte**>(m_LastChunk)[0] = chunk;
                }
                else
                {
                    m_FirstChunk = chunk;
                }

                m_LastChunk = chunk;
                m_LastChunkUsedSize = sizeof(std::byte*);
                alignedSize = (m_LastChunkUsedSize + alignment - 1) & ~(alignment - 1);
            }

            auto ptr = m_LastChunk + alignedSize;
            m_LastChunkUsedSize = alignedSize + size;
            return ptr;
        }

        /*public byte* Construct(int size, int alignment, void* src)
        {
            var res = Allocate(size, alignment);
            UnsafeUtility.MemCpy(res, src, size);
            return res;
        }*/
    };

    struct Entity
    {
        union
        {
            uint64_t Handle;
            struct
            {
                uint32_t Index;
                uint32_t Version;
            };
        };

        friend constexpr bool operator== (Entity lhs, Entity rhs) noexcept
        {
            return lhs.Handle == rhs.Handle;
        }
        friend constexpr bool operator!= (Entity lhs, Entity rhs) noexcept
        {
            return lhs.Handle != rhs.Handle;
        }
        friend constexpr bool operator< (Entity lhs, Entity rhs) noexcept
        {
            return lhs.Handle < rhs.Handle;
        }
        friend constexpr bool operator<= (Entity lhs, Entity rhs) noexcept
        {
            return lhs.Handle <= rhs.Handle;
        }
        friend constexpr bool operator> (Entity lhs, Entity rhs) noexcept
        {
            return lhs.Handle > rhs.Handle;
        }
        friend constexpr bool operator>= (Entity lhs, Entity rhs) noexcept
        {
            return lhs.Handle >= rhs.Handle;
        }
    };
    static_assert(sizeof(Entity) == sizeof(uint64_t));
    static_assert(alignof(Entity) == alignof(uint64_t));

    enum struct ComponentTypeCode : uint32_t
    {
        Invalid = ~uint32_t{},
    };

    struct ComponentType final
    {
        ComponentTypeCode Value;

        template <typename TComponent>
        ComponentType() noexcept
        {
            Value = TComponent::s_ComponentTypeCode;
        }
    };

    struct EntityArchetype final
    {
        std::unique_ptr<ComponentType[]> ComponentsArray;
        size_t ComponentCount;
    };

    struct EntityStoreBlock final
    {
        static constexpr size_t BufferSize = 64 * 1024;

        EntityArchetype* Archetype;
        std::unique_ptr<std::byte[]> Buffer;
    };

    class FRAMEWORK_API EntityManager final
    {
    private:
        struct EntityData final
        {
            EntityArchetype* Archetype;
            uint32_t Version;
            uint32_t BlockIndex;
            EntityStoreBlock* Block;
        };

    private:
        std::vector<EntityData> m_Entities{};

    public:
        EntityManager() noexcept;
        ~EntityManager() noexcept;

    public:
        bool Exists(Entity entity) const noexcept;
    };
}
