#pragma once
#include <Graphyte/Framework/Object.hxx>

namespace Graphyte::Framework
{
    class Level;
}

namespace Graphyte::Framework
{
    class FRAMEWORK_API World
        : public Framework::Object
    {
    public:
        World() noexcept;
        virtual ~World() noexcept;

    public:
        std::vector<Level*> Levels;
    };
}
