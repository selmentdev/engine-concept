#pragma once
#include <Graphyte/Framework/Engine.hxx>
#include <Graphyte/Application.hxx>
#include <Graphyte/Framework/Viewport.hxx>
#include <Graphyte/Framework/GameViewportClient.hxx>
#include <Graphyte/Framework/GameInstance.hxx>
#include <Graphyte/Rendering/DeferredShadingSceneRenderer.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API GameEngine : public Engine
    {
    public:
        GameEngine(std::unique_ptr<GameInstance> instance) noexcept;
        virtual ~GameEngine() noexcept;

    public:
        void Initialize() noexcept override;
        void Finalize() noexcept override;

    public:
        void Tick(float deltaTime, bool idle) noexcept override;

    public:
        bool CanHandleInput(Application::Window& window) noexcept override;
        void OnWindowClose(Application::Window& window) noexcept override;
        bool OnKeyDown(Input::KeyCode key, char32_t character, bool repeat) noexcept override;
        bool OnKeyUp(Input::KeyCode key, char32_t character, bool repeat) noexcept override;
        virtual bool OnControllerAnalog(
            [[maybe_unused]] Input::GamepadKey key,
            [[maybe_unused]] uint32_t controller,
            [[maybe_unused]] float value
        ) noexcept override;

    private:
        void CreateWindow() noexcept;

    private:
        Application::Window* m_Window;
        Framework::Viewport* m_Viewport;
        Framework::GameViewportClient* m_Client;
        std::unique_ptr<Rendering::DeferredShadingSceneRenderer> m_Renderer;
        std::unique_ptr<GameInstance> m_Instance;
    };
}
