#pragma once
#include <Graphyte/Framework.module.hxx>
#include <Graphyte/Framework/Object.hxx>

namespace Graphyte::Framework
{
    class World;
    class Actor;
}

namespace Graphyte::Framework
{
    class FRAMEWORK_API Level
        : public Framework::Object
    {
    public:
        Level() noexcept;
        virtual ~Level() noexcept;

    public:
        std::vector<Actor*> Actors;
        World* ParentWorld;
    };
}
