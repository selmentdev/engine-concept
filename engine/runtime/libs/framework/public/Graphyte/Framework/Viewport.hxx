#pragma once
#include <Graphyte/Framework.module.hxx>
#include <Graphyte/Framework/ViewportClient.hxx>
#include <Graphyte/Application.hxx>
#include <Graphyte/Graphics/Gpu/GpuCommandList.hxx>
#include <Graphyte/Graphics/Gpu/GpuDevice.hxx>
#include <Graphyte/Framework/Object.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API Viewport final
        : public Framework::Object
    {
    public:
        Viewport(Application::Window* window, Framework::ViewportClient* client) noexcept;
        virtual ~Viewport() noexcept;

    public:
        void BeginRenderFrame(Graphics::GpuDevice& device) noexcept;
        void EndRenderFrame(Graphics::GpuDevice& device) noexcept;
        void Tick(float deltaTime) noexcept;

    public:
        void InitializeGpuResources() noexcept;
        void UpdateGpuResources() noexcept;
        void ReleaseGpuResources() noexcept;
        void SynchronizeSize(bool fullscreen) noexcept;

    private:
        Application::Window* m_Window;
        Framework::ViewportClient* m_Client;
        Graphics::GpuViewportHandle m_Viewport;
    };
}
