#pragma once
#include <Graphyte/Framework.module.hxx>
#include <Graphyte/Framework/Object.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API GameMode
        : public Framework::Object
    {
    public:
        GameMode() noexcept;
        virtual ~GameMode() noexcept;

    public:
        virtual void Initialize() noexcept = 0;
        virtual void Finalize() noexcept = 0;
        virtual void Tick(float deltaTime) noexcept = 0;
    };
}
