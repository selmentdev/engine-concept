#pragma once
#include <Graphyte/Base.module.hxx>

#if false

namespace Graphyte::Framework::Navigation::Pathfinding
{
    enum struct NodeIndex : size_t
    {
        Invalid = ~size_t{},
    };

    enum struct EdgeIndex : size_t
    {
        Invalid = ~size_t{},
    };

    inline NodeIndex NextIndex(NodeIndex index) noexcept
    {
        return NodeIndex{ static_cast<size_t>(index) + 1 };
    }

    inline EdgeIndex NextIndex(EdgeIndex index) noexcept
    {
        return EdgeIndex{ static_cast<size_t>(index) + 1 };
    }

    struct NodeBase
    {
        float Cost;
        float PathCost;
        NodeIndex PathFrom;
        bool Visited;
    };

    struct Edge
    {
        NodeIndex Target;
        NodeIndex BackLink;
        float Cost;
    };

    template <typename TValue>
    struct Node : NodeBase
    {
        using ValueType = TValue;
        using NodeType = Node<TValue>;

        TValue Value;
        std::vector<EdgeIndex> Edges;
    };

    template <typename TValue>
    class Graph final
    {
    public:
        using ValueType = TValue;
        using NodeType = Node<TValue>;
        using EdgeType = Edge;
        using NodeCollection = std::vector<NodeType>;
        using EdgeCollection = std::vector<EdgeType>;

    private:
        NodeCollection m_Nodes;
        EdgeCollection m_Edges;

    public:
        NodeIndex AddNode(TValue&& value) noexcept
        {
            return AddNode(NodeType{ 0.0F, 0.0F, {}, false, {}, {}, std::move(value) });
        }

        NodeIndex AddNode(NodeType&& node) noexcept
        {
            m_Nodes.emplace_back(std::forward(node));
            return NodeIndex{ m_Nodes.size() - 1 };
        }

        EdgeIndex AddEdge(NodeIndex source, NodeIndex destination, float cost) noexcept
        {
            m_Edges.push_back(EdgeType{ destination, source, cost });

            EdgeIndex edge{ m_Edges.size() - 1 };

            m_Nodes[static_cast<size_t>(source)].Edges.emplace_back(edge);
            m_Nodes[static_cast<size_t>(destination)].Edges.emplace_back(edge);

            return edge;
        }

        EdgeIndex AddDirectedEdge(NodeIndex source, NodeIndex destination, float cost) noexcept
        {
            m_Edges.emplace_back(EdgeType{ destination, source, cost });

            EdgeIndex edge{ m_Edges.size() - 1 };

            m_Nodes[static_cast<size_t>(source)].Edges.emplace_back(edge);

            return edge;
        }

        NodeType* GetNode(NodeIndex index) noexcept
        {
            return &m_Nodes[static_cast<size_t>(index)];
        }

        EdgeType* GetEdge(EdgeIndex index) noexcept
        {
            return &m_Edges[static_cast<size_t>(index)];
        }

        NodeIndex FollowEdge(NodeIndex node, EdgeIndex edge) const noexcept
        {
            auto& lookup = m_Edges[static_cast<size_t>(edge)];

            if (lookup.BackLink == node)
            {
                return lookup.Target;
            }

            return lookup.BackLink;
        }

        NodeCollection& GetNodeCollection() noexcept
        {
            return m_Nodes;
        }

        EdgeCollection& GetEdgeCollection() noexcept
        {
            return m_Edges;
        }
    };

    template <typename TValue, typename THeurestic>
    inline bool FindPath(
        Graph<TValue>& graph,
        THeurestic&& heurestic,
        NodeIndex source,
        NodeIndex destination,
        std::vector<NodeIndex>& out_path
    ) noexcept
    {
        std::vector<NodeIndex> unvisitedNodes{};
        unvisitedNodes.reserve(graph.GetNodeCollection().size());


        //
        // Mark all nodes as visited.
        //

        NodeIndex index{};

        for (auto& item : graph.GetNodeCollection())
        {
            item.Visited = false;
            item.PathCost = std::numeric_limits<float>::max();
            item.PathFrom = NodeIndex::Invalid;

            unvisitedNodes.emplace_back(index);

            index = NextIndex(index);
        }


        //
        // Mark source node.
        //

        NodeIndex current = source;
        auto* sourceNode = graph.GetNode(current);
        sourceNode->Visited = true;
        sourceNode->PathCost = 0.0F;

        auto* destinationData = graph.GetNode(destination);


        //
        // Find path.
        //

        while (!unvisitedNodes.empty())
        {
            auto* currentData = graph.GetNode(current);

            for (auto&& currentEdge : currentData->Edges)
            {
                NodeIndex followNode = graph.FollowEdge(current, currentEdge);

                auto* followData = graph.GetNode(followNode);

                if (followData->Visited)
                {
                    continue;
                }

                const auto nodeCost = graph.GetEdge(currentEdge)->Cost + currentData->PathCost;
                const auto heuresticCost = heurestic(followData->Value, destinationData->Value);

                const auto totalCost = nodeCost + heuresticCost;

                if (totalCost < followData->PathCost)
                {
                    followData->PathCost = totalCost;
                    followData->PathFrom = current;
                }
            }
        }

        auto compare = [&](NodeIndex first, NodeIndex second) noexcept -> bool
        {
            return graph.GetNode(first)->PathCost > graph.GetNode(second)->PathCost;
        };


        //
        // Maintain heap for better performance.
        //

        std::make_heap(std::begin(unvisitedNodes), std::end(unvisitedNodes), compare);
        std::pop_heap(std::begin(unvisitedNodes), std::end(unvisitedNodes), compare);


        //
        // Get node with lowest path cost.
        //

        NodeIndex nodeLowestPath = unvisitedNodes.back();
        unvisitedNodes.pop_back();

        if (nodeLowestPath == NodeIndex::Invalid)
        {
            return false;
        }

        current = nodeLowestPath;
        graph.GetNode(current)->Visited = true;

        if (nodeLowestPath == destination)
        {
            out_path.emplace_back(current);

            while (graph.GetNode(current)->PathFrom != NodeIndex::Invalid)
            {
                current = graph.GetNode(current)->PathFrom;
                out_path.emplace_back(current);
            }

            std::reverse(std::begin(out_path), std::end(out_path));
            return true;
        }

        return false;
    }
}

#endif

