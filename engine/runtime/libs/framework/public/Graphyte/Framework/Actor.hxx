#pragma once
#include <Graphyte/Framework.module.hxx>
#include <Graphyte/Types.hxx>
#include <Graphyte/Framework/Object.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API Actor : public Framework::Object
    {
    public:
        Actor() noexcept;
        virtual ~Actor() noexcept;

    protected:
        Float4x3A m_Transform;
    };
}
