#pragma once
#include <Graphyte/Framework.module.hxx>
#include <Graphyte/Framework/GameMode.hxx>
#include <Graphyte/Framework/Object.hxx>
#include <Graphyte/Framework/World.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API GameInstance
        : public Framework::Object
    {
    public:
        GameInstance() noexcept;
        virtual ~GameInstance() noexcept;

    public:
        virtual void Initialize() noexcept = 0;
        virtual void Finalize() noexcept = 0;
        virtual void Tick(float deltaTime) noexcept;

    public:
        void SetCurrentMode(std::unique_ptr<Framework::GameMode> mode) noexcept;

    public:
        std::unique_ptr<Framework::World> m_World;

    private:
        std::unique_ptr<Framework::GameMode> m_CurrentMode;
    };
}
