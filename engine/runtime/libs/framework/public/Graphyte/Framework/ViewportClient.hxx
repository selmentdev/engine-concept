#pragma once
#include <Graphyte/Framework.module.hxx>
#include <Graphyte/Framework/Object.hxx>

namespace Graphyte::Framework
{
    class FRAMEWORK_API ViewportClient
        : public Framework::Object
    {
    public:
        ViewportClient() noexcept = default;
        virtual ~ViewportClient() noexcept = default;
    };
}
