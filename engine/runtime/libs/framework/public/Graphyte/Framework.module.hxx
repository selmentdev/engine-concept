#pragma once
#include <Graphyte/Platform/Impl/Detect.hxx>

#if GRAPHYTE_STATIC_BUILD
#define FRAMEWORK_API
#else
#if defined(module_framework_EXPORTS)
#define FRAMEWORK_API       GX_LIB_EXPORT
#else
#define FRAMEWORK_API       GX_LIB_IMPORT
#endif
#endif
