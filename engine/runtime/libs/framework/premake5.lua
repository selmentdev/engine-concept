project "com.graphyte.framework"
    targetname "com.graphyte.framework"

    language "c++"

    graphyte_module {}

    files {
        "public/**.?xx",
        "private/**.?xx",
        "*.lua",
    }

    filter { "toolset:msc*" }
        pchheader "Framework.pch.hxx"
        pchsource "private/Framework.pch.cxx"

    filter { "kind:SharedLib" }
        defines {
            "module_framework_EXPORTS=1"
        }

    use_com_graphyte_base()
    use_com_graphyte_graphics()
    use_com_graphyte_rendering()
    use_com_graphyte_geometry()
