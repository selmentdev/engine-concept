#include "Entities.pch.hxx"
#include <Graphyte/Entities/EntityArchetype.hxx>
#include <Graphyte/Entities/ComponentGroup.hxx>

#define GRAPHYTE_ENTITIES_ENABLE_CHECKS 1

namespace Graphyte::Entities
{
    EntityArchetype* EntityArchetype::Create(notstd::span<const ComponentTypeInfo> components) noexcept
    {
#if GRAPHYTE_ENTITIES_ENABLE_CHECKS
        for (auto type : components)
        {
            GX_ASSERTF(type.Type.IsValid(), "Wrong component type ID");

            if (type.Empty)
            {
                GX_ASSERTF(type.SizeOf == 1, "Empty component must have zero size");
                GX_ASSERTF(type.AlignOf == 1, "Empty component must have zero alignment");
            }
        }
#endif

        auto* result = new EntityArchetype();

        const size_t count = components.size() + 1;

        result->m_ComponentsArray = new ComponentInfo[count];
        result->m_ComponentsCount = count;

        size_t i = 0;

        result->m_ComponentsArray[i++] = { Entity::ID, 0, false };

        size_t offset{ sizeof(Entity) };
        size_t alignment{ alignof(std::max_align_t) };

        for (const auto& id : components)
        {
            if (!id.Empty)
            {
                alignment = std::max(alignment, id.AlignOf);

                offset = AlignUp<size_t>(offset, id.AlignOf);
            }

            result->m_ComponentsArray[i++] = ComponentInfo{ id.Type, offset, id.Empty };

            if (!id.Empty)
            {
                offset += id.SizeOf;
            }
        }

        result->m_EntitySize = AlignUp<size_t>(offset, alignment);

        return result;
    }

    bool EntityArchetype::IsCompatible(notstd::span<const ComponentType> components) const noexcept
    {
        for (const auto& component : components)
        {
            bool found{ false };
            for (size_t k = 0; k < this->m_ComponentsCount; ++k)
            {
                auto type = this->m_ComponentsArray[k].Type;
                if (type == component)
                {
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                return false;
            }
        }

        return true;
    }
}
