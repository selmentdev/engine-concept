#include "Entities.pch.hxx"
#include <Graphyte/Entities.module.hxx>
#include <Graphyte/Modules.hxx>

GX_DECLARE_LOG_CATEGORY(LogEntities, Trace, Trace);
GX_DEFINE_LOG_CATEGORY(LogEntities);

class EntitiesModule final : public Graphyte::IModule
{
public:
    virtual void OnInitialize() noexcept final override
    {
    }

    virtual void OnFinalize() noexcept final override
    {
    }
};

GX_IMPLEMENT_MODULE(EntitiesModule);
