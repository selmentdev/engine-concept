# Entities

## Overview

- entity contains set of unique components
- component may be placed only once within entity
- **entity** maps to **entity archetype**
- entity archetype contains list of components and store specific data
- entity store manager
- component is an POD struct with no data, memcpy movable

## Types

### Entity Manager

- responsible for managing entities
- maps temporal entity IDs to actual component data
  - index within entity manager
  - version number

### Entity Archetype Manager

- responsible for managing **entity archetypes**
- stored as linked list of archetypes
- **not** a singleton!
  - actual archetype contains list of **entity stores** allocated for these archetypes

### Entity Store Manager

- responsible for managing **entity store** memory
- **entity stores** are blocks of contigous memory

## Internal references and requirements

- EntityManager
  - requires MemoryAllocator
  - provides EntityStoreManager
  - provides EntityArchetypeManager

```cxx
class EntityManager final {

}
```