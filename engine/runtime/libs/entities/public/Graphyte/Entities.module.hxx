#pragma once
#include <Graphyte/Platform/Impl/Detect.hxx>

#if GRAPHYTE_STATIC_BUILD
#define ENTITIES_API
#else
#if defined(module_entities_EXPORTS)
#define ENTITIES_API    GX_LIB_EXPORT
#else
#define ENTITIES_API    GX_LIB_IMPORT
#endif
#endif
