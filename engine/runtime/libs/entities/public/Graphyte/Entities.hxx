#pragma once
#include <Graphyte/Entities.module.hxx>
#include <Graphyte/Hash/FNV.hxx>
#include <Graphyte/IntrusiveList.hxx>
#include <Graphyte/Span.hxx>
#include <Graphyte/Platform.hxx>

namespace Graphyte::Entities
{
    enum struct EntityIndex : uint32_t;
    enum struct EntityVersion : uint32_t;

    struct Entity final
    {
        union
        {
            struct
            {
                EntityIndex Index;
                EntityVersion Version;
            };
            uint64_t Value;
        };
    };

    static_assert(std::is_trivial_v<Entity>);
    static_assert(std::is_standard_layout_v<Entity>);
    static_assert(sizeof(Entity) == sizeof(uint64_t));
}

namespace Graphyte::Entities
{
    struct ComponentType final
    {
    public:
        uint64_t Value;
        const char* Name;

    public:
        ComponentType() = default;

        explicit constexpr ComponentType(const char* name) noexcept
            : Value{ Hash::HashFnv1A64::FromString(name) }
            , Name{ name }
        {
        }

    public:
        constexpr bool IsValid() const noexcept
        {
            return this->Value != 0 && this->Name != nullptr;
        }

        friend constexpr bool operator== (ComponentType lhs, ComponentType rhs) noexcept
        {
            return lhs.Value == rhs.Value;
        }

        friend constexpr bool operator!= (ComponentType lhs, ComponentType rhs) noexcept
        {
            return lhs.Value != rhs.Value;
        }
    };

    static_assert(sizeof(ComponentType) == (2 * sizeof(uint64_t)));
    static_assert(std::is_standard_layout_v<ComponentType>);
    static_assert(std::is_trivial_v<ComponentType>);
}

namespace Graphyte::Entities
{
    enum struct ComponentMetadataFlags : uint32_t
    {
        None = 0,
        IsStatic = 1 << 0,
        IsShared = 1 << 1,
        IsEmpty = 1 << 2,
    };

    GX_ENUM_CLASS_FLAGS(ComponentMetadataFlags);

    // note: this data is unusable outside of archetype; in order to provide better API, we may just separate component type to separate array?
    struct ComponentMetadata final
    {
        ComponentType Type;             // component type
        uint32_t SizeOf;                // size of component struct
        uint32_t AlignOf;               // align of component struct -- note: may be merged as max(sizeof, alignof) since we put components one by one.
        ComponentMetadataFlags Flags;   // additional flags
    };

    template <typename... TComponentTypes>
    std::array<ComponentType, sizeof...(TComponentTypes)> MakeComponentTypeSignature() noexcept
    {
        return { { TComponentTypes::ID... } };
    }

    template <typename... TComponentTypes>
    std::array<ComponentMetadata, sizeof...(TComponentTypes)> MakeComponentMetadataSignature() noexcept
    {
        return {
            {
                TComponentTypes::ID,
                sizeof(TComponentTypes),
                alignof(TComponentTypes),
                std::is_empty_v<TComponentTypes>
                    ? ComponentMetadataFlags::IsEmpty
                    : ComponentMetadataFlags::None
            }...
        };
    }

    class EntityArchetype final
    {
        friend class EntityManager;
    private:
        IntrusiveListNode<EntityArchetype> m_ListEntry;
        ComponentMetadata* m_ComponentMetadata;
        uint32_t m_ComponentMetadataCount;
        uint32_t m_EntitySize;

    public:
        size_t GetEntitySize() const noexcept
        {
            return m_EntitySize;
        }

        notstd::span<ComponentMetadata> GetComponentsMetadata() const noexcept
        {
            return { m_ComponentMetadata, m_ComponentMetadataCount };
        }
    };
}

namespace Graphyte::Entities
{
    struct EntityStore final
    {
        static constexpr size_t BufferSize = 64 * 1024;

        IntrusiveListNode<EntityStore> ListEntry;
        IntrusiveListNode<EntityStore> FreeListEntry;
        EntityArchetype* Archetype;
        uint32_t Count;
        uint32_t Capacity;

        std::byte* GetData() noexcept
        {
            return reinterpret_cast<std::byte*>(this + 1);
        }

        const std::byte* GetData() const noexcept
        {
            return reinterpret_cast<const std::byte*>(this + 1);
        }

        std::byte* GetData(size_t offset) noexcept
        {
            return reinterpret_cast<std::byte*>(this + 1) + offset;
        }

        const std::byte* GetData(size_t offset) const noexcept
        {
            return reinterpret_cast<const std::byte*>(this + 1) + offset;
        }
    };

    EntityStore* AllocateEntityStore() noexcept
    {
        void* buffer = Platform::OsVirtualAlloc(EntityStore::BufferSize);

        std::memset(buffer, 0, EntityStore::BufferSize);

        auto* store = reinterpret_cast<EntityStore*>(buffer);

        new(store) EntityStore();

        return store;
    }

    void DeallocateEntityStore(EntityStore* store) noexcept
    {
        GX_ASSERT(store != nullptr);

        store->~EntityStore();

        Platform::OsVirtualFree(store, EntityStore::BufferSize);
    }
}

namespace Graphyte::Entities
{
    class EntityManager final
    {
    private:
        IntrusiveList<EntityArchetype, &EntityArchetype::m_ListEntry> m_Archetypes;

        struct EntityData final
        {
            EntityVersion Version;
            uint32_t Index;             // Index of entity within store. 0 is an invalid value
            EntityStore* Store;
            EntityArchetype* Archetype;

            constexpr bool IsValid() const noexcept
            {
                return Index != 0
                    && Store != nullptr
                    && Archetype != nullptr;
            }

            constexpr bool IsValid(EntityVersion version) const noexcept
            {
                return Version == version && IsValid();
            }
        };

        std::vector<EntityData> m_Entities;

    public:
        EntityArchetype* GetOrCreateArchetype(notstd::span<ComponentType> components) noexcept;

        template <typename... TComponents>
        EntityArchetype* GetOrCreateArchetype() noexcept;

    private:
        void InternalCreateEntities(EntityArchetype* archetype, notstd::span<Entity> entities) noexcept;
        void InternalInstantiateEntities(Entity source, notstd::span<Entity> entities) noexcept;
        void InternalDestroyEntities(notstd::span<Entity> entities) noexcept;

    public:
        Entity Create(EntityArchetype* archetype) noexcept
        {
            Entity result;
            InternalCreateEntities(archetype, { &result, 1 });
            return result;
        }

        void Create(EntityArchetype* archetype, notstd::span<Entity> entities) noexcept
        {
            return InternalCreateEntities(archetype, entities);
        }

        Entity Instantiate(Entity source) noexcept
        {
            Entity result;
            InternalInstantiateEntities(source, { &result, 1 });
            return result;
        }

        void Instantiate(Entity source, notstd::span<Entity> entities) noexcept
        {
            InternalInstantiateEntities(source, entities);
        }

        void Destroy(Entity entity) noexcept
        {
            InternalDestroyEntities({ &entity, 1 });
        }

        void Destroy(notstd::span<Entity> entities) noexcept
        {
            InternalDestroyEntities(entities);
        }

        bool Exists(Entity entity) const noexcept
        {
            if (static_cast<size_t>(entity.Index) < m_Entities.size())
            {
                auto& data = m_Entities[static_cast<size_t>(entity.Index)];

                return data.IsValid(entity.Version);
            }

            return false;
        }

        bool HasComponent(Entity entity, ComponentType type) const noexcept;

        template <typename TComponent>
        bool HasComponent(Entity entity) const noexcept;

        template <typename TComponent>
        TComponent* GetComponent(Entity entity) const noexcept;
    };
}
