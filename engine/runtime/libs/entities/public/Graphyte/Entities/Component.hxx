#pragma once
#include <Graphyte/Entities.module.hxx>
#include <Graphyte/Bitwise.hxx>
#include <Graphyte/Hash/FNV.hxx>

namespace Graphyte::Entities
{
    struct ComponentType final
    {
    public:
        uint64_t Value;
        const char* Name;

    public:
        ComponentType() noexcept = default;
        constexpr ComponentType(const char* name) noexcept
            : Value{ Hash::HashFnv1A64::FromString(name) }
            , Name{ name }
        {
        }

        constexpr bool IsValid() const noexcept
        {
            return Value != 0;
        }

        friend constexpr bool operator== (ComponentType lhs, ComponentType rhs) noexcept
        {
            return lhs.Value == rhs.Value;
        }

        friend constexpr bool operator!= (ComponentType lhs, ComponentType rhs) noexcept
        {
            return lhs.Value != rhs.Value;
        }

        friend constexpr bool operator< (ComponentType lhs, ComponentType rhs) noexcept
        {
            return lhs.Value < rhs.Value;
        }

        friend constexpr bool operator<= (ComponentType lhs, ComponentType rhs) noexcept
        {
            return lhs.Value <= rhs.Value;
        }

        friend constexpr bool operator> (ComponentType lhs, ComponentType rhs) noexcept
        {
            return lhs.Value > rhs.Value;
        }

        friend constexpr bool operator>= (ComponentType lhs, ComponentType rhs) noexcept
        {
            return lhs.Value >= rhs.Value;
        }
    };
    static_assert(sizeof(ComponentType) == (2 * sizeof(uint64_t)));
    static_assert(std::is_standard_layout_v<ComponentType>);
    static_assert(std::is_trivial_v<ComponentType>);

    template <typename TComponent>
    inline constexpr bool IsValidComponent
        =  std::is_trivial_v<TComponent>
        && std::is_standard_layout_v<TComponent>
        && !std::is_polymorphic_v<TComponent>
        && TComponent::ID.IsValid();
}
