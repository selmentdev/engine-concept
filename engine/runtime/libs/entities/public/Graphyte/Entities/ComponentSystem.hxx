#pragma once
#include <Graphyte/Entities.module.hxx>
#include <Graphyte/Entities/Component.hxx>
#include <Graphyte/Entities/Entity.hxx>
#include <Graphyte/Entities/EntityArchetype.hxx>

namespace Graphyte::Entities
{
    class ENTITIES_API BaseComponentSystem
    {
    public:
        BaseComponentSystem() noexcept = default;
        virtual ~BaseComponentSystem() noexcept = default;

    public:
        virtual void OnUpdate() noexcept = 0;
    };

    template <typename... TComponentTypes>
    class ComponentSystem : public BaseComponentSystem
    {

    private: // this functions are used just as temporary replacement for show an idea.
        template <typename TComponent>
        TComponent& GetComponent([[maybe_unused]] Entity entity) noexcept
        {
            static_assert(IsValidComponent<TComponent>);
            static_assert(TComponent::ID.IsValid(), "TComponent must be valid component type");
            static TComponent fake{};
            return fake;
        }

    public:
        ComponentSystem() noexcept = default;
        virtual ~ComponentSystem() noexcept = default;

    public:
        void OnUpdate() noexcept override
        {
            // fake
            for (size_t i = 0; i < 10000; ++i)
            {
                Entity entity{};
                this->Update(
                    entity,
                    this->GetComponent<TComponentTypes>(entity)...
                );
            }
        }

        virtual void Update(Entity entity, TComponentTypes&... components) noexcept = 0;
    };
}
