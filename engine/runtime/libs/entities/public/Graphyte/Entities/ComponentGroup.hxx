#pragma once
#include <Graphyte/Entities.module.hxx>
#include <Graphyte/Entities/Component.hxx>
#include <Graphyte/Entities/EntityArchetype.hxx>

namespace Graphyte::Entities
{
    class ComponentGroup final
    {
    private:
        std::unique_ptr<ComponentType[]> m_TypesArray{ nullptr };
        size_t m_TypesCount{ 0 };

    public:
        ComponentGroup(const ComponentGroup&) noexcept = delete;
        ComponentGroup(ComponentGroup&&) noexcept = default;
        ~ComponentGroup() noexcept = default;

    public:
        ComponentGroup& operator= (const ComponentGroup&) = delete;
        ComponentGroup& operator= (ComponentGroup&&) = default;

    public:
        template <typename... TComponentTypes>
        ComponentGroup() noexcept
        {
            auto items = GetArchetypeSignature<TComponentTypes...>();
            
            m_TypesCount = std::size(items);
            m_TypesArray = std::make_unique<ComponentType[]>(m_TypesCount);

            for (size_t i = 0; i < std::size(items); ++i)
            {
                GX_ASSERT(items[i].IsValid());
                m_TypesArray[i] = items[i];
            }
        }

    public:
        notstd::span<const ComponentType> GetTypes() const noexcept
        {
            return {
                this->m_TypesArray.get(),
                this->m_TypesCount
            };
        }
    };
}
