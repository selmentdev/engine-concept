#include <Graphyte/Base.module.hxx>
#include <Graphyte/IntrusiveList.hxx>
#include <Graphyte/Span.hxx>
#include <Graphyte/Hash/FNV.hxx>

namespace Graphyte::Entities
{
    // represents entity archetype; actual description of entity and link to storage.
    class Archetype final
    {

        IntrusiveListNode<Archetype> m_ListEntry;       // link to entity archetypes list

        ComponentInfo* m_Components;
        uint32_t m_ComponentsCount;
        uint32_t m_EntitySize;

    public:
        using ArchetypeList = IntrusiveList<Archetype, &Archetype::m_ListEntry>;
    };

    // represents block of memory with entities of same archetype
    struct EntityStore final
    {
        IntrusiveListNode<EntityStore> m_ListEntry;     // link to entity stores sharing archetype
        IntrusiveListNode<EntityStore> m_FreeListEntry; // link to free entity stores
        Archetype* m_Archetype;
        uint32_t m_Count;
        uint32_t m_Capacity;
    };

    class EntityManager final
    {
        Archetype::ArchetypeList m_Archetypes;
    public:
        template <typename... TComponents>
        Archetype* GetOrCreateArchetype();
        Archetype* GetOrCreateArchetype(...);

        Entity Create(Archetype* archetype) noexcept;
        void Create(notstd::span<Entity> entities, Archetype* archetype) noexcept;

        Entity Instantiate(Entity source) noexcept;
        void Instantiate(notstd::span<Entity> entities, Entity source) noexcept;

        void Destroy(Entity entity) noexcept;
        void Destroy(notstd::span<Entity> entities) noexcept;

        bool Exists(Entity entity) const noexcept;

        template <typename TComponent>
        bool HasComponent(Entity entity) const noexcept;
        bool HasComponent(Entity entity, ComponentType type) const noexcept;

        template <typename TComponent>
        TComponent* GetComponent(Entity entity) const noexcept;
    };
}
