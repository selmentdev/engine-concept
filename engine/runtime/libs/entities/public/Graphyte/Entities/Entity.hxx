#pragma once
#include <Graphyte/Entities.module.hxx>
#include <Graphyte/Entities/Component.hxx>

namespace Graphyte::Entities
{
    struct Entity final
    {
        static constexpr ComponentType ID{ "Entity" };

        union
        {
            struct
            {
                uint32_t Index;
                uint32_t Version;
            };
            uint64_t Value;
        };
    };
    static_assert(IsValidComponent<Entity>);

    struct Parent final
    {
        static constexpr ComponentType ID{ "Parent" };

        Entity Value;
    };
    static_assert(sizeof(Parent) == sizeof(Entity));
    static_assert(alignof(Parent) == alignof(Entity));
    static_assert(IsValidComponent<Parent>);

    struct WorldTransform final
    {
        static constexpr ComponentType ID{ "WorldTransform" };

        Float4x4A Value;
    };
    static_assert(sizeof(WorldTransform) == sizeof(Float4x4A));
    static_assert(alignof(WorldTransform) == alignof(Float4x4A));
    static_assert(IsValidComponent<WorldTransform>);

    struct LocalTransform final
    {
        static constexpr ComponentType ID{ "LocalTransform" };

        Float4x4A Value;
    };
    static_assert(sizeof(LocalTransform) == sizeof(Float4x4A));
    static_assert(alignof(LocalTransform) == alignof(Float4x4A));
    static_assert(IsValidComponent<LocalTransform>);

    struct BoundingSphere final
    {
        static constexpr ComponentType ID{ "BoundingSphere" };

        Float4A Sphere;
    };
    static_assert(sizeof(BoundingSphere) == sizeof(Float4A));
    static_assert(alignof(BoundingSphere) == alignof(Float4A));
    static_assert(IsValidComponent<BoundingSphere>);
}
