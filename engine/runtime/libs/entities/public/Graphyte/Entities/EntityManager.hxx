#pragma once
#include <Graphyte/Entities.module.hxx>
#include <Graphyte/IntrusiveList.hxx>

namespace Graphyte::Entities
{
    class EntityArchetype;

    struct EntityStorage final
    {
        /*!
         * Default buffer allocation size.
         */
        static constexpr size_t BufferSize = 64 * 1024;

        /*!
         * List link to other entity storages sharing archetype.
         */
        IntrusiveListNode<EntityStorage> StorageLink;

        /*!
         * List link to partially allocated entity storages sharing archetype.
         */
        IntrusiveListNode<EntityStorage> FreeLink;

        /*!
         * Entity archetype stored within this storage block.
         */
        EntityArchetype* Archetype;

        /*!
         * Number of entities allocated within this storage block.
         */
        uint32_t Count;

        /*!
         * Number of entities which can be stored within this storage block.
         */
        uint32_t Capacity;

        //static constexpr ComputeBufferSize
    };
}

namespace Graphyte::Entities
{
    class ENTITIES_API EntityManager final
    {
    private:

        struct EntityData final
        {
            uint32_t Version;
            uint32_t StorageOffset;
            EntityArchetype* Archetype;
            EntityStorage* Storage;
        };

        static_assert(sizeof(EntityData) == (2 * sizeof(uint32_t) + 2 * sizeof(void*)));
    };
}
