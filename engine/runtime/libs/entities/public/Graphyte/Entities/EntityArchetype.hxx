#pragma once
#include <Graphyte/Entities.module.hxx>
#include <Graphyte/Entities/Entity.hxx>
#include <Graphyte/Entities/Component.hxx>
#include <Graphyte/Span.hxx>
#include <Graphyte/IntrusiveList.hxx>

namespace Graphyte::Entities
{
}

namespace Graphyte::Entities
{
    class ENTITIES_API EntityArchetype final
    {
    public:
        struct ComponentInfo final
        {
            ComponentType Type;
            size_t Offset;
            bool IsEmpty;
        };

        struct ComponentTypeInfo
        {
            ComponentType Type;
            size_t SizeOf;
            size_t AlignOf;
            bool Empty;
        };

    private:
        ComponentInfo* m_ComponentsArray{ nullptr };
        size_t m_ComponentsCount{ 0 };
        size_t m_EntitySize{ 0 };

    public:
        EntityArchetype() noexcept = default;

        EntityArchetype(const EntityArchetype&) = delete;
        EntityArchetype(EntityArchetype&&) = delete;
        EntityArchetype& operator= (const EntityArchetype&) = delete;
        EntityArchetype& operator= (EntityArchetype&&) = delete;

        ~EntityArchetype() noexcept
        {
            delete[] m_ComponentsArray;
        }

    public:
        template <typename... TComponentTypes>
        static EntityArchetype* Create() noexcept
        {
            return Create({
                { TComponentTypes::ID, sizeof(TComponentTypes), alignof(TComponentTypes), std::is_empty_v<TComponentTypes>}...
            });
        }

        static EntityArchetype* Create(std::initializer_list<const ComponentTypeInfo> components) noexcept
        {
            return EntityArchetype::Create({
                std::data(components),
                std::size(components)
            });
        }

        static EntityArchetype* Create(notstd::span<const ComponentTypeInfo> components) noexcept;

    public:
        template <size_t TCount>
        bool IsCompatible(const std::array<ComponentType, TCount>& components) const noexcept
        {
            return this->IsCompatible({
                std::data(components),
                std::size(components)
            });
        }

        bool IsCompatible(notstd::span<const ComponentType> components) const noexcept;

    public:
        notstd::span<const ComponentInfo> GetComponents() const noexcept
        {
            return {
                this->m_ComponentsArray,
                this->m_ComponentsCount
            };
        }

        size_t GetEntitySize() const noexcept
        {
            return m_EntitySize;
        }
    };

    template <typename... TComponentTypes>
    std::array<ComponentType, sizeof...(TComponentTypes)> GetArchetypeSignature() noexcept
    {
        return { { TComponentTypes::ID... } };
    }
}
