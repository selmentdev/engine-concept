project "com.graphyte.entities"
    targetname "com.graphyte.entities"

    language "c++"

    graphyte_module {}

    files {
        "public/**.?xx",
        "private/**.?xx",
        "*.lua",
        "docs/**.md",
    }

    filter { "toolset:msc*" }
        pchheader "Entities.pch.hxx"
        pchsource "private/Entities.pch.cxx"

    filter { "kind:SharedLib" }
        defines {
            "module_entities_EXPORTS=1"
        }

    tags {
        "tags:com_graphyte_base",
    }

    use_com_graphyte_base()
