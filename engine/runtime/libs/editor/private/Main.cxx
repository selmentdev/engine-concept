#include <Graphyte/Base.module.hxx>
#include <Graphyte/Diagnostics/Stopwatch.hxx>
#include <Graphyte/Base64.hxx>
#include <Graphyte/Storage/IFileSystem.hxx>
#include <Graphyte/Storage/Path.hxx>
#include <Graphyte/Threading/Thread.hxx>
#include <Graphyte/Storage/ArchiveFileWriter.hxx>
#include <Graphyte/Storage/ArchiveFileReader.hxx>
#include <Graphyte/Storage/ArchiveMemoryReader.hxx>
#include <Graphyte/Storage/FileManager.hxx>
#include <Graphyte/Network/Dns.hxx>
#include <Graphyte/Network/IpEndPoint.hxx>
#include <Graphyte/Diagnostics.hxx>
#include <Graphyte/AI/UtilityAI/Agent.hxx>
#include <Graphyte/System/ApplicationProperties.hxx>
#include <Graphyte/System/SystemProperties.hxx>
#include <Graphyte/System/PlatformProperties.hxx>
#include <Graphyte/String.hxx>
#include <Graphyte/Unicode.hxx>

#include <Graphyte/Graphics/Image.hxx>
#include <Graphyte/Graphics/ImageCodecs/ImageCodec.DDS.hxx>
#include <Graphyte/Graphics/ImageCodecs/ImageCodec.PNG.hxx>
#include <Graphyte/Graphics/ImageCodecs/ImageCodec.TGA.hxx>
#include <Graphyte/Math/Noise/SimplexNoise.hxx>
#include <Graphyte/Math/Noise/PerlinNoise.hxx>
//#include <Graphyte/Math/Vector.hxx>
#include <Graphyte/Math/Vector3.hxx>
#include <Graphyte/Math/Vector4.hxx>
#include <Graphyte/Math/Matrix.hxx>
#include <Graphyte/Math/Quaternion.hxx>

#include <Graphyte/Graphics/ImageHistogram.hxx>
#include <Graphyte/Bitwise.hxx>
#include <Graphyte/System/MessageDialog.hxx>
#include <Graphyte/Math/Scalar.hxx>

#include <Graphyte/Graphics/Gpu/GpuDevice.hxx>
#include <Graphyte/Graphics/Gpu/GpuVertex.hxx>
#include <Graphyte/Graphics/Gpu/GpuCommandList.hxx>
#include <Graphyte/Bitwise.hxx>
#include <Graphyte/ByteAccess.hxx>
#include <Graphyte/Math/Color.hxx>

#if GRAPHYTE_PLATFORM_LINUX
#include <SDL2/SDL.h>
#include <glad/glad.h>
#endif

GX_DECLARE_TRACE_SWITCH(LogApplication, Verbose, Verbose);
GX_DEFINE_TRACE_SWITCH(LogApplication);

#if GRAPHYTE_PLATFORM_LINUX
#define CHECKING_OPENGL 1
#else
#define CHECKING_OPENGL 0
#endif
class AppMessageHandler final : public Graphyte::System::GenericMessageHandler
{
private:
    Graphyte::System::GenericApplication* m_Application;
    Graphyte::System::WindowRef m_Window;
    //Graphyte::Graphics::Render::RenderSystemRef m_RenderSystem;
    //Graphyte::Graphics::Render::ViewportRef m_Viewport;

    Graphyte::Graphics::GpuDeviceRef m_Device;
    Graphyte::Graphics::GpuViewportHandle m_Viewport;

    Graphyte::Graphics::GpuIndexBufferHandle m_IndexBuffer;
    Graphyte::Graphics::GpuVertexBufferHandle m_VertexBuffer;
    Graphyte::Graphics::GpuTexture2DHandle m_Texture;
    Graphyte::Graphics::GpuGraphicsPipelineStateHandle m_PipelineState;
    //Graphyte::Graphics::GpuGraphicsPipelineStateHandle m_PipelineState2;
    Graphyte::Graphics::GpuResourceSetHandle m_ResourceSet;
    Graphyte::Graphics::GpuSamplerHandle m_Sampler;
    Graphyte::Graphics::GpuUniformBufferHandle m_GlobalData;
    Graphyte::Graphics::GpuUniformBufferHandle m_ObjectData;

    Graphyte::Graphics::GpuTexture2DHandle m_RTColor;
    Graphyte::Graphics::GpuTexture2DHandle m_RTDepth;
    Graphyte::Graphics::GpuRenderTargetHandle m_RT;

    Graphyte::Graphics::GpuCommandListHandle m_IntermediateCommandList;

    struct GlobalData
    {
        Graphyte::Float4x4A View;
        Graphyte::Float4x4A Projection;
        Graphyte::Float4x4A ViewProjection;
    } Global{};
    float m_Counter{ 0.0F };
    struct ObjectData
    {
        Graphyte::Float4x4A World;
        Graphyte::Float4x4A InverseWorld;
    } Object{};


public:
    explicit AppMessageHandler(Graphyte::System::GenericApplication* application) noexcept
        : m_Application{ application }
        , m_Window{}
    {
        using namespace Graphyte::Graphics;

        application->SetMessageHandler(this);

        Graphyte::System::WindowDescriptor descriptor{};
        descriptor.Title = "Game Window";
        descriptor.Type = Graphyte::System::WindowType::Normal;
        descriptor.Taskbar = true;
        descriptor.CloseButton = true;
        descriptor.MinimizeButton = false;
        descriptor.MaximizeButton = false;
        descriptor.Resizable = true;
        descriptor.Regular = true;
        descriptor.Position = { 120, 120 };
        descriptor.Size = { 1280, 720 };
        descriptor.SystemBorder = true;
        descriptor.AcceptInput = true;
        descriptor.ActivationPolicy = Graphyte::System::WindowActivationPolicy::FirstShown;
        descriptor.DelayResize = true;
        //descriptor.SizeLimits = { 1280, 720, std::nullopt, std::nullopt };
        descriptor.Topmost = false;

        m_Window = application->MakeWindow(descriptor);
        m_Window->SetCaption("Graphyte Engine");
        m_Window->Show();

        m_Device = GpuDevice::Make(
#if CHECKING_OPENGL
            GpuRenderAPI::OpenGL
#else
            GpuRenderAPI::D3D11
#endif
        );

        if (m_Device != nullptr)
        {
            m_IntermediateCommandList = m_Device->GetIntermediateCommandList();

            m_Viewport = m_Device->CreateViewport(
                m_Window->GetNativeHandle(),
                static_cast<uint32_t>(descriptor.Size.Width),
                static_cast<uint32_t>(descriptor.Size.Height),
                false,
                Graphyte::Graphics::PixelFormat::R8G8B8A8_UNORM,
                Graphyte::Graphics::PixelFormat::D32_FLOAT,
                Graphyte::Graphics::GpuMsaaQuality::Disabled
            );

            {
                GpuSamplerCreateArgs desc{};
                desc.Filter = GpuFilter::MIN_MAG_MIP_LINEAR;
                desc.AddressU = GpuSamplerAddressMode::Wrap;
                desc.AddressV = GpuSamplerAddressMode::Wrap;
                desc.AddressW = GpuSamplerAddressMode::Wrap;
                desc.CompareOp = GpuCompareOperation::Less;
                desc.MinLod = 0.0F;
                desc.MaxLod = 10.0F;

                m_Sampler = m_Device->CreateSampler(desc);
                
                {
                    auto vbuffer = m_Device->CreateVertexBuffer(
                        16 * 1024,
                        GpuBufferUsage::Static,
                        nullptr
                    );
                    
                    auto lock = m_Device->LockVertexBuffer(
                        vbuffer,
                        0,
                        1024,
                        GpuResourceLockMode::WriteOnly
                    );
                    reinterpret_cast<uint64_t*>(lock)[0] = 0xdeadc0dedeadbeef;
                    m_Device->UnlockVertexBuffer(vbuffer);
                    m_Device->DestroyVertexBuffer(vbuffer);
                }
                {
                    [[maybe_unused]] auto ibuffer = m_Device->CreateIndexBuffer(
                        0,
                        16 * 1024,
                        GpuBufferUsage::Static |
                        GpuBufferUsage::ShaderResource,
                        nullptr
                    );

                    auto lock = m_Device->LockIndexBuffer(
                        ibuffer,
                        0,
                        16 * 1024,
                        GpuResourceLockMode::ReadOnly
                    );
                    reinterpret_cast<uint64_t*>(lock)[0] = 0xdeadbeefdeadc0de;
                    m_Device->UnlockIndexBuffer(ibuffer);
                    m_Device->DestroyIndexBuffer(ibuffer);
                }
            }
            {
                std::unique_ptr<Graphyte::Graphics::Image> image{};
                {
#if true
                    std::unique_ptr<Graphyte::Storage::Archive> writer{
                        Graphyte::Storage::FileManager::CreateReader(
                            Graphyte::Storage::FileManager::GetProjectContentDirectory() + "images/checker2.dds"
                        )
                    };

                    GX_ASSERT(writer != nullptr);
                    Graphyte::Graphics::ImageCodecDDS codec{};
#else
                    std::unique_ptr<Graphyte::Storage::Archive> writer{
                        Graphyte::Storage::FileManager::CreateReader(
                            Graphyte::Storage::FileManager::GetProjectContentDirectory() + "images/uv_checker.png"
                        )
                    };

                    GX_ASSERT(writer != nullptr);
                    Graphyte::Graphics::ImageCodecPNG codec{};
#endif
                    GX_VERIFY(codec.Decode(*writer, image));
                }

                GX_ASSERT(image != nullptr);

                std::vector<GpuSubresourceData> pixels{ image->GetSubresourcesCount() };
                for (size_t i = 0; i < image->GetSubresourcesCount(); ++i)
                {
                    auto const data = image->GetSubresource(i);
                    pixels[i].Memory = data->Buffer;
                    pixels[i].Pitch = static_cast<uint32_t>(data->LinePitch);
                    pixels[i].SlicePitch = static_cast<uint32_t>(data->SlicePitch);
                }

                GpuTextureCreateArgs args{};
                args.Type = GpuTextureType::Texture2D;
                args.Width = image->GetWidth();
                args.Height = image->GetHeight();
                args.Depth = 1;
                args.MipCount = image->GetMipmapCount();
                args.DataFormat = image->GetPixelFormat();
                args.ViewFormat = image->GetPixelFormat();
                args.Data = pixels.data();
                m_Texture = m_Device->CreateTexture2D(args);
            }
            {
                auto p0 = Graphyte::Float3(-0.5F, -0.5F,  0.5F);
                auto p1 = Graphyte::Float3( 0.5F, -0.5F,  0.5F);
                auto p2 = Graphyte::Float3( 0.5F, -0.5F, -0.5F);
                auto p3 = Graphyte::Float3(-0.5F, -0.5F, -0.5F);
                auto p4 = Graphyte::Float3(-0.5F,  0.5F,  0.5F);
                auto p5 = Graphyte::Float3( 0.5F,  0.5F,  0.5F);
                auto p6 = Graphyte::Float3( 0.5F,  0.5F, -0.5F);
                auto p7 = Graphyte::Float3(-0.5F,  0.5F, -0.5F);


                auto nu = Graphyte::SByte4(0, 127, 0, 0);
                auto nd = Graphyte::SByte4(0, -128, 0, 0);
                auto nf = Graphyte::SByte4(0, 0, 127, 0);
                auto nb = Graphyte::SByte4(0, 0, -128, 0);
                auto nl = Graphyte::SByte4(-128, 0, 0, 0);
                auto nr = Graphyte::SByte4(127, 0, 0, 0);


                auto _11 = Graphyte::Half2(Graphyte::Half{ Graphyte::ToHalf(1.0F) }, Graphyte::Half{ Graphyte::ToHalf(0.0F) });
                auto _01 = Graphyte::Half2(Graphyte::Half{ Graphyte::ToHalf(0.0F) }, Graphyte::Half{ Graphyte::ToHalf(0.0F) });
                auto _10 = Graphyte::Half2(Graphyte::Half{ Graphyte::ToHalf(1.0F) }, Graphyte::Half{ Graphyte::ToHalf(1.0F) });
                auto _00 = Graphyte::Half2(Graphyte::Half{ Graphyte::ToHalf(0.0F) }, Graphyte::Half{ Graphyte::ToHalf(1.0F) });

                auto c = Graphyte::ColorBGRA(1.0F, 1.0F, 1.0F, 1.0F);

                std::array<GpuVertexComplex, 24> vertices = { {
                    // Bottom
                    { p0, nd, nd, { _11, _11 }, c }, { p1, nd, nd, { _01, _01 }, c }, { p2, nd, nd, { _00, _00 }, c }, { p3, nd, nd, { _10, _10 }, c },

                    // Left
                    { p7, nl, nl, { _11, _11 }, c }, { p4, nl, nl, { _01, _01 }, c }, { p0, nl, nl, { _00, _00 }, c }, { p3, nl, nl, { _10, _10 }, c },

                    // Front
                    { p4, nf, nf, { _11, _11 }, c }, { p5, nf, nf, { _01, _01 }, c }, { p1, nf, nf, { _00, _00 }, c }, { p0, nf, nf, { _10, _10 }, c },

                    // Back
                    { p6, nb, nb, { _11, _11 }, c }, { p7, nb, nb, { _01, _01 }, c }, { p3, nb, nb, { _00, _00 }, c }, { p2, nb, nb, { _10, _10 }, c },

                    // Right
                    { p5, nr, nr, { _11, _11 }, c }, { p6, nr, nr, { _01, _01 }, c }, { p2, nr, nr, { _00, _00 }, c }, { p1, nr, nr, { _10, _10 }, c },

                    // Top
                    { p7, nu, nu, { _10, _10 }, c }, { p6, nu, nu, { _11, _11 }, c }, { p5, nu, nu, { _01, _01 }, c }, { p4, nu, nu, { _00, _00 }, c },
                } };

                std::array<uint16_t, 3 * 2 * 6> indices = { {

                    // Bottom
                    3 + 4 * 0, 1 + 4 * 0, 0 + 4 * 0,
                    3 + 4 * 0, 2 + 4 * 0, 1 + 4 * 0,

                    // Left
                    3 + 4 * 1, 1 + 4 * 1, 0 + 4 * 1,
                    3 + 4 * 1, 2 + 4 * 1, 1 + 4 * 1,

                    // Front
                    3 + 4 * 2, 1 + 4 * 2, 0 + 4 * 2,
                    3 + 4 * 2, 2 + 4 * 2, 1 + 4 * 2,

                    // Back
                    3 + 4 * 3, 1 + 4 * 3, 0 + 4 * 3,
                    3 + 4 * 3, 2 + 4 * 3, 1 + 4 * 3,

                    // Right
                    3 + 4 * 4, 1 + 4 * 4, 0 + 4 * 4,
                    3 + 4 * 4, 2 + 4 * 4, 1 + 4 * 4,

                    // Top
                    3 + 4 * 5, 1 + 4 * 5, 0 + 4 * 5,
                    3 + 4 * 5, 2 + 4 * 5, 1 + 4 * 5,
                } };


                GpuSubresourceData subresource{};
                subresource.Memory = reinterpret_cast<void*>(indices.data());
                subresource.Pitch = static_cast<uint32_t>(indices.size() * sizeof(indices[0]));
                subresource.SlicePitch = 0;

                m_IndexBuffer = m_Device->CreateIndexBuffer(sizeof(uint16_t), subresource.Pitch, GpuBufferUsage::Static, &subresource);

                subresource.Memory = reinterpret_cast<void*>(vertices.data());
                subresource.Pitch = static_cast<uint32_t>(vertices.size() * sizeof(vertices[0]));
                subresource.SlicePitch = 0;
                m_VertexBuffer = m_Device->CreateVertexBuffer(subresource.Pitch, GpuBufferUsage::Static, &subresource);
            }

            Graphyte::Float4x4A identity{};
            identity.M11 = identity.M22 = identity.M33 = identity.M44 = 1.0F;
            {
                Global.View = identity;
                Global.Projection = identity;
                Global.ViewProjection = identity;

                auto const distance = 1.5F;
                auto xform = Graphyte::Math::Matrix::LookAtLH(
                    Graphyte::Math::Vector3::Make(-distance, distance, -distance),
                    Graphyte::Math::Vector3::Zero(),
                    Graphyte::Math::Vector3::UnitY()
                );

                Graphyte::Math::Matrix::Store(&Global.View, xform);

                auto proj = Graphyte::Math::Matrix::PerspectiveFovLH(Graphyte::Math::DegreesToRadians(55.0F), 1920.0F / 1080.0F, 0.001F, 100.0F);
                Graphyte::Math::Matrix::Store(&Global.Projection, proj);
                
                auto xproj = Graphyte::Math::Matrix::Multiply(xform, proj);
                Graphyte::Math::Matrix::Store(&Global.ViewProjection, xproj);

                GpuSubresourceData data_desc{};
                data_desc.Memory = &Global;
                data_desc.Pitch = sizeof(Global);
                data_desc.SlicePitch = 0;
                m_GlobalData = m_Device->CreateUniformBuffer(sizeof(Global), GpuBufferUsage::Static, &data_desc);
            }
            {
                Object.World = identity;
                Object.InverseWorld = identity;

                GpuSubresourceData data_desc{};
                data_desc.Memory = &Object;
                data_desc.Pitch = sizeof(Object);
                data_desc.SlicePitch = 0;
                m_ObjectData = m_Device->CreateUniformBuffer(sizeof(Object), GpuBufferUsage::Static, &data_desc);
            }
            {
                // Render target textures:

                GpuTextureCreateArgs args{};
                args.DataFormat = args.ViewFormat = Graphyte::Graphics::PixelFormat::D32_FLOAT;
                args.Width = 1920;
                args.Height = 1080;
                args.Depth = 1;
                args.MipCount = 1;
                args.DebugName = "DepthStencil";
                args.Flags = GpuTextureFlags::DepthStencil;
                
                m_RTDepth = m_Device->CreateTexture2D(args);

                args.DataFormat = args.ViewFormat = Graphyte::Graphics::PixelFormat::B8G8R8A8_UNORM;
                args.Flags = GpuTextureFlags::RenderTarget;
                args.DebugName = "Color";

                m_RTColor = m_Device->CreateTexture2D(args);

                m_RT = m_Device->BeginCreateRenderTarget(1920, 1080, 1);
                m_Device->SetRenderTargetSurface(m_RT, -1, m_RTDepth, 0);
                m_Device->SetRenderTargetSurface(m_RT,  0, m_RTColor, 0);

                m_Device->EndCreateRenderTarget(m_RT);



            }
            {
                GpuGraphicsPipelineStateCreateArgs state{};
                state.InputLayout = GpuInputLayout::Complex;

                std::unique_ptr<uint8_t[]> vs_data{};
                size_t vs_size{};

                Graphyte::Storage::FileManager::ReadBinary(vs_data, vs_size,
                    Graphyte::Storage::FileManager::GetProjectContentDirectory() + "shaders/basic.vs"
#if CHECKING_OPENGL
                    + ".glsl"
#else
                    + "o"
#endif
                );

                std::unique_ptr<uint8_t[]> ps_data{};
                size_t ps_size{};

                Graphyte::Storage::FileManager::ReadBinary(ps_data, ps_size,
                    Graphyte::Storage::FileManager::GetProjectContentDirectory() + "shaders/basic.ps"
#if CHECKING_OPENGL
                    + ".glsl"
#else
                    + "o"
#endif
                );

                //state.VertexShader.Bytecode = vs_data.get();
                //state.VertexShader.Size = vs_size;
                //
                //state.PixelShader.Bytecode = ps_data.get();
                //state.PixelShader.Size = ps_size;
                
                state.Sample.Count = 1;
                state.Sample.Quality = 0;

                state.Topology = GpuPrimitiveTopology::Triangle;
                state.RenderTargetCount = 1;
                state.RenderTargetFormat[0] = Graphyte::Graphics::PixelFormat::B8G8R8A8_UNORM;
                state.DepthStencilFormat = Graphyte::Graphics::PixelFormat::D32_FLOAT;
                state.IndexBufferStripCut = GpuIndexBufferStripCut::UInt16; // 16 bit indices

                state.BlendState.AlphaToCoverage = false;
                state.BlendState.IndependentBlend = false;
                state.BlendState.RenderTarget[0].BlendEnable = true;
                state.BlendState.RenderTarget[0].SourceBlendColor = GpuBlendType::One;
                state.BlendState.RenderTarget[0].DestinationBlendColor = GpuBlendType::Zero;
                state.BlendState.RenderTarget[0].BlendOperationColor = GpuBlendOperation::Add;
                state.BlendState.RenderTarget[0].SourceBlendAlpha = GpuBlendType::One;
                state.BlendState.RenderTarget[0].DestinationBlendAlpha = GpuBlendType::Zero;
                state.BlendState.RenderTarget[0].BlendOperationAlpha = GpuBlendOperation::Add;
                state.BlendState.RenderTarget[0].RenderTargetWriteMask = GpuColorWriteEnable::All;
                state.BlendState.SampleMask = 0xffffffff;
                state.BlendState.BlendFactors = Graphyte::Float4{ 1.0F, 1.0F, 1.0F, 1.0F };

                state.RasterizerState.FillMode = GpuFillMode::Solid;
                state.RasterizerState.CullMode = GpuCullMode::Back;
                state.RasterizerState.DepthBias = 0;
                state.RasterizerState.DepthBiasClamp = 0.0F;
                state.RasterizerState.SlopeScaledDepthBias = 0.0F;
                state.RasterizerState.DepthClipEnable = false;
                state.RasterizerState.MultisampleEnable = false;
                state.RasterizerState.AntialiasedLineEnable = false;
                state.RasterizerState.FrontCounterClockwise = false;
                state.RasterizerState.ForcedSampleCount = 0;
                state.RasterizerState.ConservativeRasterizationMode = false;

                state.DepthStencilState.DepthWriteMask = GpuDepthWriteMask::All;
                state.DepthStencilState.DepthCompare = GpuCompareOperation::LessEqual;
                state.DepthStencilState.StencilEnable = false;
                state.DepthStencilState.StencilReadMask = 0xff;
                state.DepthStencilState.StencilWriteMask = 0xff;
                state.DepthStencilState.DepthEnable = true;
                state.DepthStencilState.FrontFace.Pass = GpuStencilOperation::Keep;
                state.DepthStencilState.FrontFace.Fail = GpuStencilOperation::Keep;
                state.DepthStencilState.FrontFace.DepthFail = GpuStencilOperation::Keep;
                state.DepthStencilState.FrontFace.Compare = GpuCompareOperation::Always;
                state.DepthStencilState.BackFace.Pass = GpuStencilOperation::Keep;
                state.DepthStencilState.BackFace.Fail = GpuStencilOperation::Keep;
                state.DepthStencilState.BackFace.DepthFail = GpuStencilOperation::Keep;
                state.DepthStencilState.BackFace.Compare = GpuCompareOperation::Always;
                state.DepthStencilState.DepthBoundsTestEnable = false;


                GpuResourceSetDesc layout{};
                layout.SetUniformBuffer(0, m_GlobalData, Graphyte::Graphics::GpuShaderVisibility::Pixel | Graphyte::Graphics::GpuShaderVisibility::Vertex);
                layout.SetUniformBuffer(1, m_ObjectData, Graphyte::Graphics::GpuShaderVisibility::Pixel | Graphyte::Graphics::GpuShaderVisibility::Vertex);
                layout.SetTexture(0, this->m_Texture, Graphyte::Graphics::GpuShaderVisibility::Pixel);
                layout.SetSampler(0, this->m_Sampler, Graphyte::Graphics::GpuShaderVisibility::Pixel);

                m_ResourceSet = m_Device->CreateResourceSet(layout);

                m_PipelineState = m_Device->CreateGraphicsPipelineState(state, layout);
            }
        }
    }

    ~AppMessageHandler() noexcept final
    {
        m_Device->DestroyResourceSet(m_ResourceSet);
        m_Device->DestroyGraphicsPipelineState(m_PipelineState);
        m_Device->DestroyTexture2D(m_RTDepth);
        m_Device->DestroyTexture2D(m_RTColor);
        m_Device->DestroyRenderTarget(m_RT);
        m_Device->DestroyUniformBuffer(m_GlobalData);
        m_Device->DestroyUniformBuffer(m_ObjectData);
        m_Device->DestroyIndexBuffer(m_IndexBuffer);
        m_Device->DestroyVertexBuffer(m_VertexBuffer);
        m_Device->DestroySampler(m_Sampler);
        m_Device->DestroyTexture2D(m_Texture);
        m_Device->DestroyViewport(m_Viewport);
        m_Device->Tick(0.016666666667F);
        m_Application->DestroyWindow(m_Window);
    }

    void Tick(
        [[maybe_unused]] float delta) noexcept
    {
        if (m_Device != nullptr && m_Viewport != nullptr)
        {
            m_Device->BeginDrawViewport(m_Viewport);
            {
                {
                    //m_IntermediateCommandList->BindRenderTarget(m_RT);
                    m_IntermediateCommandList->BindGraphicsPipelineState(m_PipelineState);

#if false
                    m_Counter = Graphyte::Math::UnwindRadians(m_Counter + delta);
#endif // false


                    float cs = Graphyte::Math::Cos(m_Counter);
                    float ss = Graphyte::Math::Sin(m_Counter);

                    auto const distance = m_Zoom;
                    auto xform = Graphyte::Math::Matrix::LookAtRH(
                        Graphyte::Math::Vector3::Make(distance * cs, distance, distance * ss),
                        Graphyte::Math::Vector3::Zero(),
                        Graphyte::Math::Vector3::UnitY()
                    );

                    Graphyte::Math::Matrix::Store(&Global.View, xform);

                    auto buffer = m_Device->LockUniformBuffer(m_GlobalData, 0, static_cast<uint32_t>(sizeof(Global)), Graphyte::Graphics::GpuResourceLockMode::WriteOnly);
                    memcpy(buffer, &Global, sizeof(Global));
                    m_Device->UnlockUniformBuffer(m_GlobalData);

                    m_IntermediateCommandList->BindResourceSet(m_ResourceSet);
                    m_IntermediateCommandList->BindVertexBuffer(m_VertexBuffer, 0, sizeof(Graphyte::Graphics::GpuVertexComplex), 0);
                    m_IntermediateCommandList->BindIndexBuffer(m_IndexBuffer, 0, true);

                    auto xform1 = Graphyte::Math::Matrix::Translation(-0.75F, 0.0F, 0.0F);
                    auto xform2 = Graphyte::Math::Matrix::Translation(0.75F, 0.0F, 0.0F);

                    Graphyte::Math::Matrix::Store(&Object.World, xform1);
                    buffer = m_Device->LockUniformBuffer(m_ObjectData, 0, static_cast<uint32_t>(sizeof(m_ObjectData)), Graphyte::Graphics::GpuResourceLockMode::WriteOnly);
                    memcpy(buffer, &Object, sizeof(Object));
                    m_Device->UnlockUniformBuffer(m_ObjectData);
                    m_IntermediateCommandList->DrawIndexed(3 * 2 * 6, 0, 0);

                    Graphyte::Math::Matrix::Store(&Object.World, xform2);
                    buffer = m_Device->LockUniformBuffer(m_ObjectData, 0, static_cast<uint32_t>(sizeof(m_ObjectData)), Graphyte::Graphics::GpuResourceLockMode::WriteOnly);
                    memcpy(buffer, &Object, sizeof(Object));
                    m_Device->UnlockUniformBuffer(m_ObjectData);
                    m_IntermediateCommandList->DrawIndexed(3 * 2 * 6, 0, 0);
                }
            }
            m_Device->EndDrawViewport(m_Viewport, true, 1);
        }
    }

public:
    bool CanHandleInput(
        [[maybe_unused]] Graphyte::System::WindowRef window) noexcept override
    {
        return true;
    }

    bool OnKeyDown(Graphyte::Input::KeyCode key, char32_t character, bool repeat) noexcept override
    {
        GX_LOG(LogApplication, Verbose, "OnKeyDown: key: {:x}, character: {:x} ({}), repeat: {}}\n", key, character, character, repeat);

        if (key == Graphyte::Input::KeyCode::Q)
        {
            Graphyte::System::ApplicationProperties::RequestExit(false);
        }
        else if (key == Graphyte::Input::KeyCode::F)
        {
            static bool fullscreen = false;
            fullscreen =! fullscreen;

            if (fullscreen)
            {
                Graphyte::System::DisplayMetrics metrics{};
                Graphyte::System::DisplayMetrics::Get(metrics);

                m_Window->SetWindowMode(Graphyte::System::WindowMode::WindowedFullscreen);
                m_Device->ResizeViewport(
                    m_Viewport,
                    static_cast<uint32_t>(metrics.PrimaryDisplaySize.Width),
                    static_cast<uint32_t>(metrics.PrimaryDisplaySize.Height),
                    false,
                    Graphyte::Graphics::PixelFormat::B8G8R8A8_UNORM
                );
            }
            else
            {
                m_Window->SetWindowMode(Graphyte::System::WindowMode::Windowed);
                m_Window->Resize(1280, 720);
                m_Device->ResizeViewport(
                    m_Viewport,
                    1280,
                    720,
                    false,
                    Graphyte::Graphics::PixelFormat::B8G8R8A8_UNORM
                );
            }

        }
        else if (key == Graphyte::Input::KeyCode::ArrowLeft)
        {
            m_Counter = Graphyte::Math::UnwindRadians(m_Counter + 0.1F);
        }
        else if (key == Graphyte::Input::KeyCode::ArrowRight)
        {
            m_Counter = Graphyte::Math::UnwindRadians(m_Counter - 0.1F);
        }
        return true;
    }

    bool OnControllerAnalog(
        Graphyte::Input::GamepadKey key,
        [[maybe_unused]] uint32_t controller,
        float value) noexcept override
    {
        switch (key)
        {
        case Graphyte::Input::GamepadKey::LeftAnalogX:
            {
                m_Zoom += value * 0.1F;
                break;
            }
        case Graphyte::Input::GamepadKey::LeftTriggerAnalog:
            {
                m_Counter = Graphyte::Math::UnwindRadians(m_Counter + value * 0.1F);
                break;
            }
        case Graphyte::Input::GamepadKey::RightTriggerAnalog:
            {
                m_Counter = Graphyte::Math::UnwindRadians(m_Counter - value * 0.1F);
                break;
            }
        case Graphyte::Input::GamepadKey::RightAnalogX:
            {
                m_Counter = Graphyte::Math::UnwindRadians(m_Counter + value * 0.1F);
                break;
            }
        default:
            {
                break;
            }
        }

        return true;
    }

    float m_Zoom = 1.0F;

    bool OnMouseWheel(
        float delta,
        [[maybe_unused]] Graphyte::Float2A position) noexcept override
    {
        m_Zoom = Graphyte::Math::Clamp(m_Zoom + delta, 0.01F, 10.0F);
        return true;
    }

    bool OnWindowActivated(
        [[maybe_unused]] Graphyte::System::WindowRef window,
        Graphyte::System::WindowActivation activation) noexcept override
    {
        if (m_Device != nullptr && m_Viewport != nullptr && activation != Graphyte::System::WindowActivation::Deactivate)
        {
            m_Device->ResizeViewport(m_Viewport, 1280, 720, m_Window->GetWindowMode() != Graphyte::System::WindowMode::Windowed, Graphyte::Graphics::PixelFormat::B8G8R8A8_UNORM);
        }

        return false;
    }

    bool OnApplicationActivated(bool active) noexcept override
    {
        if (m_Device != nullptr && m_Viewport != nullptr && active)
        {
            m_Device->ResizeViewport(m_Viewport, 1280, 720, m_Window->GetWindowMode() != Graphyte::System::WindowMode::Windowed, Graphyte::Graphics::PixelFormat::B8G8R8A8_UNORM);
        }

        return false;
    }

    bool OnKeyChar(
        char32_t character,
        [[maybe_unused]] bool repeat) noexcept override
    {
        std::array<char8_t, 8> str{};
        auto* start = str.data();
        auto* end = str.data() + 8;

        Graphyte::Text::ConvertUTF8Sequence(character, &start, end, Graphyte::Text::ConversionType::Strict);
        (*start) = '\0';

        GX_LOG(LogApplication, Verbose, "OnKeyChar: {}, Bits: {}\n", str.data(), Graphyte::CountBits<uint32_t>(character));
        return true;
    }

    void OnWindowClose(
        [[maybe_unused]] Graphyte::System::WindowRef window) noexcept override
    {
        GX_LOG(LogApplication, Verbose, "Closing window: {}\n", window.Get());
        Graphyte::System::ApplicationProperties::RequestExit(false);
    }

    bool OnWindowSizeChanged(
        [[maybe_unused]] Graphyte::System::WindowRef window,
        [[maybe_unused]] int32_t width,
        [[maybe_unused]] int32_t height,
        bool was_minimized) noexcept override
    {
        if (m_Device != nullptr && m_Viewport != nullptr && !was_minimized)
        {
            //m_Viewport->Resize();
            //auto windowed = window->GetWindowMode() == Graphyte::System::WindowMode::Windowed;
            //m_Device->ResizeViewport(m_Viewport, static_cast<uint32_t>(width), static_cast<uint32_t>(height), false, Graphyte::Graphics::PixelFormat::B8G8R8A8_UNORM);
            //m_RenderSystem->ResizeViewport(m_Viewport, 1920, 1080, !windowed);
        }

        return true;
    }
};

#include <Graphyte/Graphics/Image.hxx>

float turbulence(float x, float y, float size)
{
    float value = 0.0F, initialSize = size;

    while (size >= 1)
    {
        value += Graphyte::Math::SimplexNoise::Normal(x / size, y / size) * size;
        size /= 2.0F;
    }

    return(128.0F * value / initialSize);
}

#define GX_APP_ID               "assets.compiler"
#define GX_APP_NAME             "Graphyte Assets Compiler"
#define GX_APP_VERSION          Graphyte::Version{ 1, 0, 0, 0 }
#define GX_APP_COMPANY          "Graphyte"
#define GX_APP_TYPE             Graphyte::System::ApplicationType::Game

#include <Graphyte/Launch/Main.hxx>

int GraphyteMain(
    [[maybe_unused]] int argc,
    [[maybe_unused]] char** argv) noexcept
{
#if GRAPHYTE_PLATFORM_WINDOWS
    {
        std::string result{};
        Graphyte::System::Windows::QueryRegistry(HKEY_LOCAL_MACHINE, L"HARDWARE\\DESCRIPTION\\System", L"SystemBiosVersion", result);
        GX_LOG(LogApplication, Verbose, ": {}\n", result);

        Graphyte::System::Windows::QueryRegistry(HKEY_LOCAL_MACHINE, L"HARDWARE\\DESCRIPTION\\System\\BIOS", L"SystemManufacturer", result);
        GX_LOG(LogApplication, Verbose, ": {}\n", result);

        Graphyte::System::Windows::QueryRegistry(HKEY_LOCAL_MACHINE, L"HARDWARE\\DESCRIPTION\\System\\BIOS", L"SystemProductName", result);
        GX_LOG(LogApplication, Verbose, ": {}\n", result);

        Graphyte::System::Windows::QueryRegistry(HKEY_LOCAL_MACHINE, L"HARDWARE\\DESCRIPTION\\System\\BIOS", L"SystemVersion", result);
        GX_LOG(LogApplication, Verbose, ": {}\n", result);

        Graphyte::System::Windows::QueryRegistry(HKEY_LOCAL_MACHINE, L"Software\\Microsoft\\Cryptography", L"MachineGuid", result);
        GX_LOG(LogApplication, Verbose, ": {}\n", result);
    }
#endif
    {
        Graphyte::System::DisplayMetrics metrics{};
        Graphyte::System::DisplayMetrics::Get(metrics);

        GX_LOG(LogApplication, Verbose, "Display metrics\n");
        GX_LOG(LogApplication, Verbose, "- VirtualDisplayRect: ({}, {}, {}, {})\n",
            metrics.VirtualDisplayRect.Left,
            metrics.VirtualDisplayRect.Top,
            metrics.VirtualDisplayRect.Width,
            metrics.VirtualDisplayRect.Height
        );
        GX_LOG(LogApplication, Verbose, "- PrimaryDisplayWorkArea: ({}, {}, {}, {})\n",
            metrics.PrimaryDisplayWorkArea.Left,
            metrics.PrimaryDisplayWorkArea.Top,
            metrics.PrimaryDisplayWorkArea.Width,
            metrics.PrimaryDisplayWorkArea.Height
        );

        for (size_t i = 0; i < metrics.Displays.size(); ++i)
        {
            auto& display = metrics.Displays[i];

            GX_LOG(LogApplication, Verbose, "  - Display:\n");
            GX_LOG(LogApplication, Verbose, "    - Id: {}\n", display.Id);
            GX_LOG(LogApplication, Verbose, "    - Name: {}\n", display.Name);
            GX_LOG(LogApplication, Verbose, "    - Primary: {}\n", display.Primary);
            GX_LOG(LogApplication, Verbose, "    - WorkAreaRect: ({}, {}, {}, {})\n",
                display.WorkAreaRect.Left,
                display.WorkAreaRect.Top,
                display.WorkAreaRect.Width,
                display.WorkAreaRect.Height
            );
            GX_LOG(LogApplication, Verbose, "    - DisplayRect: ({}, {}, {}, {})\n",
                display.DisplayRect.Left,
                display.DisplayRect.Top,
                display.DisplayRect.Width,
                display.DisplayRect.Height
            );
        }
    }
    {
        Graphyte::System::ProcessorFeature features = Graphyte::System::SystemProperties::GetProcessorFeatures();
        GX_LOG(LogApplication, Verbose, "Features: {:x}\n", static_cast<uint64_t>(features));
    }

    {
        auto application = Graphyte::System::ApplicationProperties::MakeApplication();
        
        if (application != nullptr)
        {

            AppMessageHandler handler{ application };
            
            //frame_counter.Start();
            //uint32_t frames = 0;
            //double accumulated = 0.0;
            auto delta = 0.0015F;
            while (!Graphyte::System::ApplicationProperties::GIsRequestingExit)
            {
                //frame_counter.Restart();
                application->PollInputDevices(delta);
                application->PumpMessages(delta);
                application->Tick(delta);
                handler.Tick(delta);
                //Graphyte::Threading::Thread::Sleep(16);
                //frame_counter.Stop();

                //++frames;
                //accumulated += frame_counter.GetElapsedTime();
                /*if (accumulated >= 1.0)
                {
                    double count = (double)frames / 1.0;
                    std::array<char, 32> frames_str{};
                    Graphyte::ToString(frames_str, count);
                    window1->SetCaption(std::data(frames_str));
                    frames = 0;
                    accumulated = 0.0;
                }*/
            }
        }
    }

    return 0;
}

