#pragma once
#include <Graphyte/Base.module.hxx>
#include <concepts>

#if GRAPHYTE_HW_AVX
#   include <xmmintrin.h>
#   include <emmintrin.h>
#endif

#if GRAPHYTE_HW_FMA4
#   define fma_madd_ps(v1, v2, v3)          _mm_macc_ps(v1, v2, v3)
#   define fma_nmadd_ps(v1, v2, v3)         _mm_nmacc_ps(v1, v2, v3)
#elif GRAPHYTE_HW_FMA3
#   define fma_madd_ps(v1, v2, v3)          _mm_fmadd_ps(v1, v2, v3)
#   define fma_nmadd_ps(v1, v2, v3)         _mm_fnmadd_ps(v1, v2, v3)
#endif


#if GRAPHYTE_HW_NEON
#if defined(_MSC_VER)
#   if defined(_M_ARM)
#       include <arm_neon.h>
#   elif defined(_M_ARM64)
#       include <arm64_neon.h>
#   endif
#endif
#endif

#if GRAPHYTE_HAVE_VECTORCALL
#define mathcall __vectorcall
#elif GRAPHYTE_COMPILER_MSVC
#define mathcall __fastcall
#else
#define mathcall
#endif

#ifndef mathinline
#define mathinline inline
#endif

namespace Graphyte::NewMath::Impl
{
#if GRAPHYTE_MATH_NO_INTRINSICS
    struct SimdFloat4 final
    {
        union
        {
            float F[4];
            uint32_t U[4];
            int32_t I[4];
        };
    };
#elif GRAPHYTE_HW_NEON
    using SimdFloat4 = float32x4_t;
#elif GRAPHYTE_HW_AVX
    using SimdFloat4 = __m128;
#endif

#if GRAPHYTE_MATH_NO_INTRINSICS
    struct SimdFloat4x4 final
#else
    struct alignas(16) SimdFloat4x4 final
#endif
    {
#if GRAPHYTE_MATH_NO_INTRINSICS
        union
        {
            SimdFloat4 R[4];
            struct
            {
                float M11, M12, M13, M14;
                float M21, M22, M23, M24;
                float M31, M32, M33, M34;
                float M41, M42, M43, M44;
    };
            float M[4][4];
            float F[16];
        };
#else
        SimdFloat4 R[4];
#endif
    };

    static_assert(std::is_pod_v<SimdFloat4x4>);
}

//#define mathconst constexpr const

namespace Graphyte::NewMath::Impl
{
    struct alignas(16) Vec4F32 final
    {
        union
        {
            float F[4];
            SimdFloat4 V;
        };
    };
    static_assert(std::is_pod_v<Vec4F32>);

    struct alignas(16) Vec4I32 final
    {
        union
        {
            int32_t I[4];
            SimdFloat4 V;
        };
    };
    static_assert(std::is_pod_v<Vec4I32>);

    struct alignas(16) Vec4U32 final
    {
        union
        {
            uint32_t I[4];
            SimdFloat4 V;
        };
    };
    static_assert(std::is_pod_v<Vec4U32>);

    struct alignas(16) Vec16U8 final
    {
        union
        {
            uint8_t U[16];
            SimdFloat4 V;
        };
    };
    static_assert(std::is_pod_v<Vec4U32>);
}

namespace Graphyte::NewMath
{
    struct Vector4
    {
        Impl::SimdFloat4 V;
    };

    struct Vector3
    {
        Impl::SimdFloat4 V;
    };

    struct Vector2
    {
        Impl::SimdFloat4 V;
    };

    struct Quaternion
    {
        Impl::SimdFloat4 V;
    };

    struct Bool4
    {
        Impl::SimdFloat4 V;
    };

    struct Matrix
    {
        Impl::SimdFloat4 R[5];
    };

    struct Color
    {
        Impl::SimdFloat4 V;
    };

    struct Plane
    {
        Impl::SimdFloat4 V;
    };
}

namespace Graphyte::NewMath::Impl
{
    template <typename T>
    concept SimdVectorType = requires(T value)
    {
        { value.V } -> std::convertible_to<SimdFloat4>;
    };

    template <typename T>
    concept SimdMatrixType = requires(T value)
    {
        { value.R[0] } -> std::convertible_to<SimdFloat4>;
        { std::size(value.R) == 4 };
    };
}

namespace Graphyte::NewMath
{
    mathinline Bool4 mathcall CompareGreater(Vector4 v1, Vector4 v2) noexcept;
    mathinline Bool4 mathcall CompareGreaterEqual(Vector4 v1, Vector4 v2) noexcept;
    mathinline Bool4 mathcall CompareLess(Vector4 v1, Vector4 v2) noexcept;
    mathinline Bool4 mathcall CompareLessEqual(Vector4 v1, Vector4 v2) noexcept;
    mathinline Bool4 mathcall CompareEqual(Vector4 v1, Vector4 v2) noexcept;
    mathinline Bool4 mathcall CompareNotEqual(Vector4 v1, Vector4 v2) noexcept;

    mathinline bool mathcall IsGreater(Vector4 v1, Vector4 v2) noexcept;
    mathinline bool mathcall IsGreaterEqual(Vector4 v1, Vector4 v2) noexcept;
    mathinline bool mathcall IsLess(Vector4 v1, Vector4 v2) noexcept;
    mathinline bool mathcall IsLessEqual(Vector4 v1, Vector4 v2) noexcept;
    mathinline bool mathcall IsEqual(Vector4 v1, Vector4 v2) noexcept;
    mathinline bool mathcall IsNotEqual(Vector4 v1, Vector4 v2) noexcept;

    mathinline Bool4 mathcall CompareNaN(Vector4 v) noexcept;

    mathinline bool mathcall IsNaN(Vector4 v) noexcept;

    mathinline Vector4 mathcall SplatX(Vector4 v) noexcept;

    mathinline Vector4 mathcall Select(Vector4 v1, Vector4 v2, Bool4 mask) noexcept;

    mathinline Bool4 mathcall And(Bool4 v1, Bool4 v2) noexcept;
    mathinline Bool4 mathcall AndNot(Bool4 v1, Bool4 v2) noexcept;

    mathinline Vector4 mathcall ToVector4(Vector3 v) noexcept;
    mathinline Vector4 mathcall ToVector4(Vector3 v, float w) noexcept;
    mathinline Vector4 mathcall ToVector4(Vector2 v) noexcept;
    mathinline Vector4 mathcall ToVector4(Vector2 v, float z) noexcept;
    mathinline Vector4 mathcall ToVector4(Vector2 v, float z, float w) noexcept;

    mathinline Vector4 mathcall ToVector4(Quaternion v) noexcept;
    mathinline Vector4 mathcall ToVector4(Plane v) noexcept;
    mathinline Vector4 mathcall ToVector4(Color v) noexcept;

    mathinline Vector4 mathcall AsVector4(Quaternion q) noexcept;
    mathinline Vector4 mathcall AsVector4(Plane p) noexcept
    {
        return { p.V };
    }

    mathinline Vector4 mathcall AsVector4(Color c) noexcept
    {
        return { c.V };
    }

    mathinline Bool4 mathcall AsBool4(Vector4 v) noexcept;

    mathinline Bool4 mathcall MaskFalse() noexcept;
    mathinline Bool4 mathcall MaskTrue() noexcept;

    mathinline bool mathcall AnyTrue4(Bool4 value) noexcept;
    mathinline bool mathcall AnyTrue3(Bool4 value) noexcept;
    mathinline bool mathcall AnyTrue2(Bool4 value) noexcept;
    mathinline bool mathcall AnyTrue1(Bool4 value) noexcept;

    template <typename T> mathinline T mathcall Zero() noexcept
        requires Impl::SimdVectorType<T>
    {
        return { _mm_setzero_ps() };
    }

    template <typename T> mathinline T mathcall One() noexcept;



    mathinline Vector4 mathcall MakeVector4(float value) noexcept;
    mathinline Vector4 mathcall MakeVector4(float x, float y, float z, float w) noexcept;
    mathinline Vector3 mathcall MakeVector3(float value) noexcept;
    mathinline Vector3 mathcall MakeVector3(float x, float y, float z) noexcept;

    mathinline Vector4 mathcall Add(Vector4 v1, Vector4 v2) noexcept;
    mathinline Vector3 mathcall Add(Vector3 v1, Vector3 v2) noexcept;
    mathinline Vector2 mathcall Add(Vector2 v1, Vector2 v2) noexcept;

    mathinline Vector4 mathcall Min(Vector4 v1, Vector4 v2) noexcept;
    mathinline Vector3 mathcall Min(Vector3 v1, Vector3 v2) noexcept;
    mathinline Vector2 mathcall Min(Vector2 v1, Vector2 v2) noexcept;
}
