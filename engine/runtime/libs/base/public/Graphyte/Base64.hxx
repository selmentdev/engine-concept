#pragma once
#include <Graphyte/Base.module.hxx>
#include <Graphyte/Span.hxx>

namespace Graphyte
{
    struct Base64 final
    {
    public:
        BASE_API static std::string Encode(
            notstd::span<std::byte> buffer
        ) noexcept;

        BASE_API static std::vector<std::byte> Decode(
            std::string_view string
        ) noexcept;
    };
}
