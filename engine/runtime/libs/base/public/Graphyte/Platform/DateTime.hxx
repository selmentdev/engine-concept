#pragma once
#include <Graphyte/Base.module.hxx>

namespace Graphyte::Platform::Impl
{
    static constexpr const int64_t TicksInMicrosecond = INT64_C(10);
    static constexpr const int64_t TicksInMillisecond = TicksInMicrosecond * INT64_C(1000);
    static constexpr const int64_t TicksInSecond = TicksInMillisecond * INT64_C(1000);
    static constexpr const int64_t TicksInMinute = TicksInSecond * INT64_C(60);
    static constexpr const int64_t TicksInHour = TicksInMinute * INT64_C(60);
    static constexpr const int64_t TicksInDay = TicksInHour * INT64_C(24);

    static constexpr const int32_t DaysInYear = 365;
    static constexpr const int32_t DaysIn4Years = (4 * 365) + 1;
    static constexpr const int32_t DaysIn100Years = (100 * 365) + 24;
    static constexpr const int32_t DaysIn400Years = (400 * 365) + 97;

    static constexpr const int32_t DaysTo1601 = DaysIn400Years * 4;
    static constexpr const int32_t DaysTo1899 = DaysIn400Years * 4 + DaysIn100Years * 3 - 367;
    static constexpr const int32_t DaysTo10000 = DaysIn400Years * 25 - 366;

    static constexpr const int64_t TicksMinValue = INT64_C(0);
    static constexpr const int64_t TicksMaxValue = DaysTo10000 * TicksInDay - 1;
    static constexpr const int64_t DateAdjustOffset = DaysTo1601 * TicksInDay;
    static constexpr const int64_t UnixAdjustOffset = INT64_C(621355968000000000);

    static_assert(DaysTo1601 == 584388, "Invalid number of days from 0001-01-01 to 1601-12-31");
    static_assert(DaysTo1899 == 693593, "Invalid number of days from 0001-01-01 to 1899-12-31");
    static_assert(DaysTo10000 == 3652059, "Invalid number of days from 0001-01-01 to 9999-12-31");
    static_assert(TicksInSecond == 10000000, "Invalid number of ticks in second");
    static_assert(DateAdjustOffset == 504911232000000000, "Invalid number of ticks for time adjustment");
}

namespace Graphyte::Platform
{
    struct DateTime;
    struct TimeSpan;

    enum struct DateTimeKind
    {
        System,
        Local,
    };

    enum struct Month
    {
        First = 1,
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        June = 6,
        July = 7,
        August = 8,
        September = 9,
        October = 10,
        November = 11,
        December = 12,
        Count = 12,
        Last = 12
    };

    enum struct WeekDay
    {
        Sunday = 0,
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6,
        Count = 7
    };

    enum struct DateTimeFormat
    {
        Date,
        Time,
        DateTime,
        FileSafe,
        TimeStamp,
        DateIso8601 = Date,
        DateTimeIso8601 = DateTime,
    };

    struct CalendarTime final
    {
        uint16_t Year;
        uint16_t Month;
        uint16_t Day;
        uint16_t Hour;
        uint16_t Minute;
        uint16_t Second;
        uint16_t Millisecond;
        uint16_t DayOfWeek;
        uint16_t DayOfYear;

        BASE_API int64_t ToTicks() const noexcept;
        BASE_API bool IsValid() const noexcept;
    };

    struct TimeSpanMembers final
    {
        int32_t Days;
        int32_t Hours;
        int32_t Minutes;
        int32_t Seconds;
        int32_t Milliseconds;
    };

    struct DateTime final
    {
        int64_t Value;

        BASE_API static DateTime Create(int year, int month, int day) noexcept;
        BASE_API static DateTime Create(int year, int month, int day, int hour, int minute, int second) noexcept;
        BASE_API static DateTime Create(int year, int month, int day, int hour, int minute, int second, int millisecond) noexcept;

        BASE_API static DateTime Now() noexcept;
        BASE_API static DateTime Now(DateTimeKind kind) noexcept;

        BASE_API static DateTime Today(DateTimeKind kind) noexcept;
        BASE_API static DateTime Today() noexcept;

        BASE_API static DateTime FromUnixTimestamp(int64_t seconds) noexcept;
        BASE_API int64_t ToUnixTimestamp() noexcept;

        BASE_API TimeSpan GetTimeOfDay() const noexcept;
        BASE_API DateTime GetDate() const noexcept;
    };

    static_assert(sizeof(DateTime) == sizeof(int64_t));

    BASE_API DateTime FromCalendar(const CalendarTime& value) noexcept;
    BASE_API bool ToCalendar(CalendarTime& result, DateTime value) noexcept;

    BASE_API std::string ToString(DateTime value, DateTimeFormat format = DateTimeFormat::DateTime) noexcept;
    BASE_API std::string ToString(DateTime value, std::string_view format) noexcept;

    BASE_API bool FromString(DateTime& result, std::string_view format) noexcept;


    struct TimeSpan final
    {
        int64_t Value;

        BASE_API static TimeSpan Create(int hours, int minutes, int seconds) noexcept;
        BASE_API static TimeSpan Create(int days, int hours, int minutes, int seconds) noexcept;
        BASE_API static TimeSpan Create(int days, int hours, int minutes, int seconds, int milliseconds) noexcept;

        static TimeSpan FromSeconds(int64_t seconds) noexcept
        {
            return TimeSpan{ seconds * Impl::TicksInSecond };
        }

        static int64_t ToSeconds(TimeSpan value) noexcept
        {
            return value.Value / Impl::TicksInSecond;
        }

        TimeSpan Duration() const noexcept
        {
            return TimeSpan{ std::abs(this->Value) };
        }

        double GetTotalDays() const noexcept
        {
            return static_cast<double>(this->Value) / static_cast<double>(Impl::TicksInDay);
        }

        double GetTotalHours() const noexcept
        {
            return static_cast<double>(this->Value) / static_cast<double>(Impl::TicksInHour);
        }

        double GetTotalMinutes() const noexcept
        {
            return static_cast<double>(this->Value) / static_cast<double>(Impl::TicksInMinute);
        }

        double GetTotalSeconds() const noexcept
        {
            return static_cast<double>(this->Value) / static_cast<double>(Impl::TicksInSecond);
        }

        double GetTotalMilliseconds() const noexcept
        {
            return static_cast<double>(this->Value) / static_cast<double>(Impl::TicksInMillisecond);
        }

    };

    static_assert(sizeof(TimeSpan) == sizeof(int64_t));

    BASE_API void ToMembers(TimeSpanMembers& result, TimeSpan value) noexcept;
    BASE_API TimeSpan FromMembers(const TimeSpanMembers& value) noexcept;

    BASE_API std::string ToString(TimeSpan value) noexcept;
    BASE_API std::string ToString(TimeSpan value, std::string_view format) noexcept;
}

namespace Graphyte::Platform
{


    inline bool Equals(DateTime lhs, DateTime rhs) noexcept
    {
        return lhs.Value == rhs.Value;
    }

    inline int Compare(DateTime lhs, DateTime rhs) noexcept
    {
        if (lhs.Value < rhs.Value)
        {
            return -1;
        }
        else if (lhs.Value > rhs.Value)
        {
            return 1;
        }

        return 0;
    }

    inline bool operator == (DateTime lhs, DateTime rhs) noexcept
    {
        return lhs.Value == rhs.Value;
    }

    inline bool operator != (DateTime lhs, DateTime rhs) noexcept
    {
        return lhs.Value != rhs.Value;
    }

    inline bool operator <= (DateTime lhs, DateTime rhs) noexcept
    {
        return lhs.Value <= rhs.Value;
    }

    inline bool operator >= (DateTime lhs, DateTime rhs) noexcept
    {
        return lhs.Value >= rhs.Value;
    }

    inline bool operator < (DateTime lhs, DateTime rhs) noexcept
    {
        return lhs.Value < rhs.Value;
    }

    inline bool operator > (DateTime lhs, DateTime rhs) noexcept
    {
        return lhs.Value > rhs.Value;
    }

    inline TimeSpan operator - (DateTime lhs, DateTime rhs) noexcept
    {
        return TimeSpan{ lhs.Value - rhs.Value };
    }

    inline DateTime operator + (DateTime lhs, TimeSpan rhs) noexcept
    {
        return DateTime{ lhs.Value + rhs.Value };
    }

    inline DateTime operator - (DateTime lhs, TimeSpan rhs) noexcept
    {
        return DateTime{ lhs.Value - rhs.Value };
    }

    inline DateTime& operator += (DateTime& lhs, TimeSpan rhs) noexcept
    {
        lhs.Value += rhs.Value;
        return lhs;
    }

    inline DateTime& operator -= (DateTime& lhs, TimeSpan rhs) noexcept
    {
        lhs.Value += rhs.Value;
        return lhs;
    }
}

namespace Graphyte::Platform
{

    inline bool Equals(TimeSpan lhs, TimeSpan rhs) noexcept
    {
        return lhs.Value == rhs.Value;
    }

    inline int Compare(TimeSpan lhs, TimeSpan rhs) noexcept
    {
        if (lhs.Value < rhs.Value)
        {
            return -1;
        }
        else if (lhs.Value > rhs.Value)
        {
            return 1;
        }

        return 0;
    }

    inline bool operator == (TimeSpan lhs, TimeSpan rhs) noexcept
    {
        return lhs.Value == rhs.Value;
    }

    inline bool operator != (TimeSpan lhs, TimeSpan rhs) noexcept
    {
        return lhs.Value != rhs.Value;
    }

    inline bool operator <= (TimeSpan lhs, TimeSpan rhs) noexcept
    {
        return lhs.Value <= rhs.Value;
    }

    inline bool operator >= (TimeSpan lhs, TimeSpan rhs) noexcept
    {
        return lhs.Value >= rhs.Value;
    }

    inline bool operator < (TimeSpan lhs, TimeSpan rhs) noexcept
    {
        return lhs.Value < rhs.Value;
    }

    inline bool operator > (TimeSpan lhs, TimeSpan rhs) noexcept
    {
        return lhs.Value > rhs.Value;
    }

    inline TimeSpan operator - (TimeSpan value) noexcept
    {
        return TimeSpan{ -value.Value };
    }

    inline TimeSpan operator + (TimeSpan lhs, TimeSpan rhs) noexcept
    {
        return TimeSpan{ lhs.Value + rhs.Value };
    }

    inline TimeSpan operator - (TimeSpan lhs, TimeSpan rhs) noexcept
    {
        return TimeSpan{ lhs.Value - rhs.Value };
    }

    inline TimeSpan& operator += (TimeSpan& lhs, TimeSpan rhs) noexcept
    {
        lhs.Value += rhs.Value;
        return lhs;
    }

    inline TimeSpan& operator -= (TimeSpan& lhs, TimeSpan rhs) noexcept
    {
        lhs.Value -= rhs.Value;
        return lhs;
    }
}
