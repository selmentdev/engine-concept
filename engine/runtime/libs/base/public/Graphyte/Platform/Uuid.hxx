#pragma once
#include <Graphyte/Base.module.hxx>
#include <Graphyte/String.hxx>
#include <Graphyte/Diagnostics.hxx>
#include <Graphyte/Converter.hxx>

namespace Graphyte::Platform
{
    struct Uuid final
    {
    public:
        uint32_t A;
        uint32_t B;
        uint32_t C;
        uint32_t D;

    public:
        BASE_API static int Compare(
            Uuid lhs,
            Uuid rhs
        ) noexcept;

        BASE_API static bool Equals(
            Uuid lhs,
            Uuid rhs
        ) noexcept;
    };

    static_assert(sizeof(Uuid) == 16);


    inline bool operator == (Uuid lhs, Uuid rhs) noexcept
    {
        return Uuid::Equals(lhs, rhs);
    }

    inline bool operator != (Uuid lhs, Uuid rhs) noexcept
    {
        return !Uuid::Equals(lhs, rhs);
    }

    inline bool operator < (Uuid lhs, Uuid rhs) noexcept
    {
        return Uuid::Compare(lhs, rhs) < 0;
    }

    inline bool operator <= (Uuid lhs, Uuid rhs) noexcept
    {
        return Uuid::Compare(lhs, rhs) <= 0;
    }

    inline bool operator > (Uuid lhs, Uuid rhs) noexcept
    {
        return Uuid::Compare(lhs, rhs) > 0;
    }

    inline bool operator >= (Uuid lhs, Uuid rhs) noexcept
    {
        return Uuid::Compare(lhs, rhs) >= 0;
    }


    BASE_API Uuid CreateUuid() noexcept;

    BASE_API std::string ToString(
        Uuid value
    ) noexcept;

    BASE_API bool FromString(
        Uuid& out_result,
        std::string_view value
    ) noexcept;
}

namespace Graphyte
{
    template <>
    struct Converter<Platform::Uuid> final
    {
        static std::string ToString(
            Platform::Uuid value
        ) noexcept
        {
            return Graphyte::Platform::ToString(value);
        }

        static bool FromString(
            Platform::Uuid& out_result,
            std::string_view value
        ) noexcept
        {
            return Graphyte::Platform::FromString(
                out_result,
                value
            );
        }
    };
}
