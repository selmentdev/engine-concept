#pragma once
#include <Graphyte/Base.module.hxx>
#include <Graphyte/Storage/Archive.hxx>

namespace Graphyte::Serialization
{
    enum struct DataFormat : uint32_t
    {
        Json,
        MPack,
    };

    enum struct DataType : uint8_t
    {
        Invalid,
        Object,
        EndObject,
        Array,
        EndArray,
        Int32,
        UInt32,
        Int64,
        UInt64,
        Float,
        Double,
        String,
        Boolean,
        Null,
    };

    class DataReader
    {
    protected:
        BASE_API DataReader() noexcept;

    public:
        BASE_API virtual ~DataReader() noexcept;

    public:
        virtual bool StartObject(uint32_t& members) noexcept = 0;
        virtual bool EndObject() noexcept = 0;

        virtual bool StartArray(uint32_t& members) noexcept = 0;
        virtual bool EndArray() noexcept = 0;

        virtual bool ReadBoolean(bool& value) noexcept = 0;
        virtual bool ReadUInt(uint64_t& value) noexcept = 0;
        virtual bool ReadInt(int64_t& value) noexcept = 0;
        virtual bool ReadString(std::string& value) noexcept = 0;
        virtual bool ReadDouble(double& value) noexcept = 0;
        virtual bool ReadFloat(float& value) noexcept = 0;
        virtual bool Close() noexcept = 0;
    };

    class DataReader2
    {
    protected:
        DataReader2() noexcept;
    public:
        BASE_API virtual ~DataReader2() noexcept;

        virtual bool ReadNext() noexcept = 0;
        virtual bool Close() noexcept = 0;

        virtual bool GetInt32(int32_t& result) const noexcept = 0;
        virtual bool GetUInt32(uint32_t& result) const noexcept = 0;
        virtual bool GetInt64(int64_t& result) const noexcept = 0;
        virtual bool GetUInt64(uint64_t& result) const noexcept = 0;
        virtual bool GetFloat(float& result) const noexcept = 0;
        virtual bool GetDouble(double& result) const noexcept = 0;
        virtual bool GetString(std::string& result) const noexcept = 0;
        virtual bool GetBoolean(bool& result) const noexcept = 0;

    public:
        virtual bool IsValid() const noexcept = 0;
        virtual bool IsObject() const noexcept = 0;
        virtual bool IsEndObject() const noexcept = 0;
        virtual bool IsArray() const noexcept = 0;
        virtual bool IsEndArray() const noexcept = 0;
        virtual bool IsInt32() const noexcept = 0;
        virtual bool IsUInt32() const noexcept = 0;
        virtual bool IsInt64() const noexcept = 0;
        virtual bool IsUInt64() const noexcept = 0;
        virtual bool IsFloat() const noexcept = 0;
        virtual bool IsDouble() const noexcept = 0;
        virtual bool IsString() const noexcept = 0;
        virtual bool IsBoolean() const noexcept = 0;
        virtual bool IsNull() const noexcept = 0;
    };

    class DataWriter
    {
    protected:
        BASE_API DataWriter() noexcept;

    public:
        BASE_API virtual ~DataWriter() noexcept;

    public:
        virtual bool StartObject(uint32_t members) noexcept = 0;
        virtual bool EndObject() noexcept = 0;

        virtual bool StartArray(uint32_t members) noexcept = 0;
        virtual bool EndArray() noexcept = 0;

        virtual bool WriteBoolean(bool value) noexcept = 0;
        virtual bool WriteUInt(uint64_t value) noexcept = 0;
        virtual bool WriteInt(int64_t value) noexcept = 0;
        virtual bool WriteString(std::string_view value) noexcept = 0;
        virtual bool WriteDouble(double value) noexcept = 0;
        virtual bool WriteFloat(float value) noexcept = 0;
        virtual bool WriteNull() noexcept = 0;
        virtual bool Close() noexcept = 0;
    };


    BASE_API std::unique_ptr<DataReader> CreateReader(DataFormat format, Storage::Archive& archive) noexcept;
    BASE_API std::unique_ptr<DataReader2> CreateReader2(DataFormat format, Storage::Archive& archive) noexcept;

    BASE_API std::unique_ptr<DataWriter> CreateWriter(DataFormat format, Storage::Archive& archive) noexcept;
}
