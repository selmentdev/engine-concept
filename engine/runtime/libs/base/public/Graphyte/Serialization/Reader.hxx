#pragma once
#include <Graphyte/Base.module.hxx>

//
// Forward declarations for libyaml.
//

struct yaml_document_s;
struct yaml_node_s;
struct yaml_emitter_s;

namespace Graphyte::Serialization
{
    /**
     * @brief   This class implements serialization reader.
     */
    class BASE_API Reader final
    {
    private:
        std::unique_ptr<yaml_document_s> m_Document;

    public:
        /**
         * @brief This class represents value being read.
         */
        class BASE_API Value final
        {
            friend class Reader;
        private:
            yaml_document_s* m_Document;
            yaml_node_s* m_Node;

        public:
            Value() noexcept = default;
            ~Value() noexcept = default;

        public:
            /**
             * @brief   Checks whether value object is valid.
             */
            bool IsValid() const noexcept
            {
                return m_Document != nullptr && m_Node != nullptr;
            }

            /**
             * @brief   Checks whether value object represents scalar value.
             */
            bool IsValue() const noexcept;

            /**
             * @brief   Checks whether value object represents array.
             */
            bool IsArray() const noexcept;

            /**
             * @brief   Checks whether value object represents key-value mapping object.
             */
            bool IsObject() const noexcept;

            /**
             * @brief   Gets scalar value as string.
             *
             * @param   value   Returns string value.
             *
             * @return  The value indicating whether value was read correctly.
             */
            bool GetValue(std::string_view& value) const noexcept;

            /**
             * @brief   Gets number of elements in array.
             *
             * @return  The number of elements in array when successful, 0 otherwise.
             */
            size_t GetArraySize() const noexcept;

            /**
             * @brief   Gets value representing element of array at specified index.
             *
             * @param   index   Provides index of element in array.
             *
             * @return  The value representing element in array.
             */
            Value GetArrayElement(size_t index) const noexcept;

            /**
             * @brief   Gets number of members in mapping object.
             */
            size_t GetMemberCount() const noexcept;

            /**
             * @brief   Gets object mapping member by index.
             *
             * @param   index   Provides an index of member to retrieve.
             *
             * @return  The pair of key and value for given index.
             */
            std::pair<Value, Value> GetMember(size_t index) const noexcept;

            /**
             * @brief   Finds object mapping member by key.
             *
             * @param   key     Provides a name of member to find.
             *
             * @return  The pair of key and value for given key.
             */
            std::pair<Value, Value> FindMember(std::string_view key) const noexcept;
        };

    public:
        /**
         * @brief   Initializes reader for given content string.
         *
         * @param   content     Provides a string with content to parse.
         */
        Reader(std::string_view content) noexcept;
        ~Reader() noexcept;

    public:

        /**
         * @brief   Gets root element from parsed string.
         */
        Value GetRoot() const noexcept;

        /**
         * @brief   Returns value indicating whether string was parsed successfully.
         */
        bool IsValid() const noexcept
        {
            return m_Document != nullptr;
        }
    };
}
