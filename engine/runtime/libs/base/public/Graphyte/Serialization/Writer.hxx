#pragma once
#include <Graphyte/Base.module.hxx>

//
// Forward declarations for libyaml.
//

struct yaml_document_s;
struct yaml_node_s;
struct yaml_emitter_s;

namespace Graphyte::Serialization
{
    /**
     * @brief   This class implements serialization writer.
     */
    class BASE_API Writer
    {
    private:
        std::unique_ptr<yaml_emitter_s> m_Emitter;
        std::string m_Result;

    public:
        /**
         * @brief   This class represents value being written.
         */
        class BASE_API Value final
        {
            friend class Writer;

            enum struct ValueType
            {
                Unknown = 0,
                Stream,
                Document,
                Array,
                Object,
            };

        private:
            yaml_emitter_s* m_Emitter;
            ValueType m_ValueType;

        public:
            Value() noexcept = default;
            ~Value() noexcept = default;

        public:
            /**
             * @brief   Return value indicating whether current value is valid.
             */
            bool IsValid() const noexcept
            {
                return m_Emitter != nullptr && m_ValueType != ValueType::Unknown;
            }

        public:
            /**
             * @brief   Writes specified key as a child of current value node.
             *
             * @param   name    Provides a name of key to write.
             * @param   quote   Provides value indicating whether key should be quoted.
             *
             * @return  The value indicating whether operation was successful.
             */
            bool Key(std::string_view name, bool quote = false) noexcept;

            /**
             * @brief   Writes raw string as a child of current value node.
             *
             * @param   value   Provides a value to write.
             * @param   quote   Provides value indicating whether string should be quoted.
             *
             * @return  The value indicating whether operation was successful.
             */
            bool String(std::string_view value, bool quote = false) noexcept;

            /**
             * @brief   Writes key value pair as a child of current value node.
             *
             * @param   name        Provides a key to write.
             * @param   value       Provides a value to write.
             * @param   quote_name  Provides a value indicating whether key should be quoted.
             * @param   quote_value Provides a value indicating whether value should be quoted.
             *
             * @return  The value indicating whether operation was successful.
             */
            bool KeyValue(std::string_view name, std::string_view value, bool quote_name = false, bool quote_value = false) noexcept;

            /**
             * @brief   Starts new object as child of current object.
             *
             * @param   flow    Provides value indicating whether object elements should be written
             *                  in line.
             *
             * @return  The value node representing newly created object.
             */
            Value StartObject(bool flow = false) noexcept;

            /**
             * @brief   Starts new array as child of current object.
             *
             * @param   flow    Provides value indicating whether array elements should be written
             *                  in line
             *
             * @return  The value indicating whether operation was successful.
             */
            Value StartArray(bool flow = false) noexcept;

            /**
             * @brief   Ends current value scope.
             *
             * @note    This operation invalidates value - all other methods will fail.
             */
            void End() noexcept;
        };

    public:
        Writer() noexcept;
        ~Writer() noexcept;

    public:
        /**
         * @brief   Returns value indicating whether writer object is valid.
         */
        bool IsValid() const noexcept
        {
            return m_Emitter != nullptr;
        }

        /**
         * @brief   Starts new document within current writer.
         *
         * @return  The value node representing newly created document.
         *
         * @note    YAML files may contain multiple documents.
         */
        Value StartDocument() noexcept;

        /**
         * @brief   Converts emitted document to string.
         *
         * @param   result  Returns string representation of emitted document.
         *
         * @return  The value indicating whether operation was successful.
         */
        bool ToString(std::string& result) noexcept;
    };
}
