#pragma once
#include <Graphyte/Serialization/Reader.hxx>
#include <Graphyte/Serialization/Writer.hxx>

namespace Graphyte::Serialization
{
    struct ISerializable
    {
        virtual bool Serialize(Writer::Value& value) noexcept = 0;
        virtual bool Deserialize(Reader::Value& value) noexcept = 0;
    };
}
