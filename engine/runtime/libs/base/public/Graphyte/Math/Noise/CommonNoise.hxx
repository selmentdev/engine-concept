#pragma once
#include <Graphyte/Base.module.hxx>

namespace Graphyte::Math
{
    class BASE_API CommonNoise
    {
    public:
        static const uint8_t GPermutations[512];
    };
}
