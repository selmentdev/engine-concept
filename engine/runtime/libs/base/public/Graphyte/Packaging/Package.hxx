#pragma once
#include <Graphyte/Compression.hxx>
#include <Graphyte/Platform/DateTime.hxx>
#include <Graphyte/Version.hxx>
#include <Graphyte/Storage/IFileSystem.hxx>
#include <Graphyte/Storage/BinaryFormat.hxx>
#include <Graphyte/Storage/Archive.hxx>

namespace Graphyte::Packaging
{
    enum struct PackageAttributes : uint32_t
    {
        File        = 1 << 0,
        Directory   = 1 << 1,
        Readonly    = 1 << 2,
    };

    GX_ENUM_CLASS_FLAGS(PackageAttributes);

    struct PackageEntry final
    {
        std::string Name;
        Platform::DateTime CreationTime;
        uint64_t Checksum;
        uint64_t Offset;
        uint64_t CompressedSize;
        uint64_t UncompressedSize;
        CompressionMethod Compression;
        PackageAttributes Attributes;

        friend inline Storage::Archive& operator<< (Storage::Archive& archive, PackageEntry& value) noexcept
        {
            return archive
                << value.Name
                << value.Checksum
                << value.Offset
                << value.CompressedSize
                << value.UncompressedSize
                << value.Compression
                << value.Attributes;
        }
    };

    struct PackageHeader final
    {
        Version EngineVersion;
        Platform::DateTime CreationTime;
        uint64_t Checksum;

        friend inline Storage::Archive& operator<< (Storage::Archive& archive, PackageHeader& value) noexcept
        {
            return archive
                << value.EngineVersion
                << value.CreationTime
                << value.Checksum;
        }
    };

    class BASE_API PackageFileSystem : public Storage::IFileSystem
    {
    private:
        Storage::IStream* m_Stream;
        std::vector<PackageEntry> m_Entries;

    public:
        PackageFileSystem(Storage::IStream* handle) noexcept;
        virtual ~PackageFileSystem() noexcept;

    private:
        void ReadMetadata() noexcept;

    public:
        Status OpenRead(std::unique_ptr<Storage::IStream>& result, const std::string& path, bool share = false) noexcept override;
        Status OpenWrite(std::unique_ptr<Storage::IStream>& result, const std::string& path, bool append = false, bool share = false) noexcept override;

    public:
        Status IsReadonly(bool& result, const std::string& path) noexcept override;
        Status SetReadonly(const std::string& path, bool value) noexcept override;
        Status GetFileInfo(Storage::FileInfo& result, const std::string& path) noexcept override;
        Status GetFileSize(int64_t& result, const std::string& path) noexcept override;
        Status Exists(const std::string& path) noexcept override;

    public:
        Status FileMove(const std::string& destination, const std::string& source) noexcept override;
        Status FileDelete(const std::string& path) noexcept override;

    public:
        Status DirectoryCreate(const std::string& path) noexcept override;
        Status DirectoryDelete(const std::string& path) noexcept override;

    public:
        Status Enumerate(const std::string& path, Storage::IDirectoryVisitor& visitor) noexcept override;
        Status Enumerate(const std::string& path, Storage::IDirectoryInfoVisitor& visitor) noexcept override;
    };
}
