#pragma once
#include <Graphyte/Base.module.hxx>
#include <Graphyte/Storage/Archive.hxx>
#include <Graphyte/Converter.hxx>

namespace Graphyte
{
    struct Version
    {
    public:
        uint32_t Major;
        uint32_t Minor;
        uint32_t Patch;
        uint32_t Build;

        constexpr bool IsEmpty() const noexcept
        {
            return Major == 0
                && Minor == 0
                && Patch == 0;
        }

    public:
        friend inline Storage::Archive& operator<< (Storage::Archive& archive, Version& value) noexcept
        {
            return archive
                << value.Major
                << value.Minor
                << value.Patch
                << value.Build;
        }
    };
}

namespace Graphyte
{
    template <>
    struct Converter<Version> final
    {
        BASE_API static std::string ToString(const Version& value) noexcept;
        BASE_API static bool FromString(Version& out_result, std::string_view value) noexcept;
    };
}

template <>
struct fmt::formatter<Graphyte::Version>
{
    template <typename FormatContext>
    auto format(
        Graphyte::Version value,
        FormatContext& context
    )
    {
        return format_to(
            context.out(),
            "{:d}.{:d}.{:d}.{:d}",
            value.Major,
            value.Minor,
            value.Patch,
            value.Build
        );
    }
};
