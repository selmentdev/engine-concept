#pragma once
#include <Graphyte/Base.module.hxx>

namespace Graphyte
{
    struct ColorBGRA final
    {
    public:
        union
        {
            struct
            {
                uint8_t B;
                uint8_t G;
                uint8_t R;
                uint8_t A;
            };
            uint32_t Value;
        };
    public:
        ColorBGRA() = default;
        ColorBGRA(const ColorBGRA&) = default;
        ColorBGRA& operator= (const ColorBGRA&) = default;
        ColorBGRA(ColorBGRA&&) = default;
        ColorBGRA& operator= (ColorBGRA&&) = default;

        constexpr ColorBGRA(float b, float g, float r, float a) noexcept
            : B{ static_cast<uint8_t>(b * 255.0F) }
            , G{ static_cast<uint8_t>(g * 255.0F) }
            , R{ static_cast<uint8_t>(r * 255.0F) }
            , A{ static_cast<uint8_t>(a * 255.0F) }
        {
        }
        explicit ColorBGRA(float const* components) noexcept
            : ColorBGRA{ components[0], components[1], components[2], components[3] }
        {
        }
        constexpr explicit ColorBGRA(uint32_t value) noexcept
            : Value{ value }
        {
        }

    public:
        constexpr explicit operator uint32_t() const noexcept { return Value; }
        constexpr ColorBGRA& operator = (uint32_t color) noexcept
        {
            Value = color;
            return (*this);
        }

        constexpr bool operator == (ColorBGRA other) const noexcept
        {
            return this->Value == other.Value;
        }
    };
    static_assert(std::is_trivial_v<ColorBGRA>);
    static_assert(sizeof(ColorBGRA) == 4);

    struct Color64 final
    {
    public:
        union
        {
            struct
            {
                uint16_t B;
                uint16_t G;
                uint16_t R;
                uint16_t A;
            };
            uint64_t Value;
        };

    public:
        Color64() = default;
        Color64(const Color64&) = default;
        Color64& operator= (const Color64&) = default;
        Color64(Color64&&) = default;
        Color64& operator= (Color64&&) = default;

        constexpr Color64(float b, float g, float r, float a) noexcept
            : B{ static_cast<uint16_t>(b * 65535.0F) }
            , G{ static_cast<uint16_t>(g * 65535.0F) }
            , R{ static_cast<uint16_t>(r * 65535.0F) }
            , A{ static_cast<uint16_t>(a * 65535.0F) }
        {
        }
        constexpr explicit Color64(float const* components) noexcept
            : Color64{ components[0], components[1], components[2], components[3] }
        {
        }
        constexpr explicit Color64(uint64_t value) noexcept
            : Value{ value }
        {
        }

    public:
        constexpr explicit operator uint64_t() const noexcept
        {
            return Value;
        }

        constexpr Color64& operator = (uint64_t color) noexcept
        {
            Value = color;
            return (*this);
        }
    };
    static_assert(std::is_trivial_v<Color64>);
    static_assert(sizeof(Color64) == 8);

    struct alignas(16) Float4A final
    {
    public:
        float X;
        float Y;
        float Z;
        float W;
    public:
        Float4A() = default;
        Float4A(const Float4A&) = default;
        Float4A& operator= (const Float4A&) = default;
        Float4A(Float4A&&) = default;
        Float4A& operator= (Float4A&&) = default;

        constexpr Float4A(float x, float y, float z, float w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }
        explicit Float4A(float const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };
    static_assert(sizeof(Float4A) == 16);
    static_assert(alignof(Float4A) == 16);
    static_assert(std::is_nothrow_move_constructible_v<Float4A>);
    static_assert(std::is_nothrow_move_assignable_v<Float4A>);
    static_assert(std::is_trivial_v<Float4A>);

    struct alignas(16) Float3A final
    {
    public:
        float X;
        float Y;
        float Z;
    public:
        Float3A() = default;
        Float3A(const Float3A&) = default;
        Float3A& operator= (const Float3A&) = default;
        Float3A(Float3A&&) = default;
        Float3A& operator= (Float3A&&) = default;

        constexpr Float3A(float x, float y, float z) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
        {
        }
        explicit Float3A(float const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
        {
        }
    };
    static_assert(sizeof(Float3A) == 16);
    static_assert(alignof(Float3A) == 16);
    static_assert(std::is_nothrow_move_constructible_v<Float3A>);
    static_assert(std::is_nothrow_move_assignable_v<Float3A>);
    static_assert(std::is_trivial_v<Float3A>);

    struct alignas(16) Float2A final
    {
    public:
        float X;
        float Y;

    public:
        Float2A() = default;
        Float2A(const Float2A&) = default;
        Float2A& operator= (const Float2A&) = default;
        Float2A(Float2A&&) = default;
        Float2A& operator= (Float2A&&) = default;

        constexpr Float2A(float x, float y) noexcept
            : X{ x }
            , Y{ y }
        {
        }
        explicit Float2A(float const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
        {
        }
    };
    static_assert(sizeof(Float2A) == 16);
    static_assert(alignof(Float2A) == 16);
    static_assert(std::is_nothrow_move_constructible_v<Float2A>);
    static_assert(std::is_nothrow_move_assignable_v<Float2A>);
    static_assert(std::is_trivial_v<Float2A>);
    
    struct Float4 final
    {
    public:
        float X;
        float Y;
        float Z;
        float W;
    public:
        Float4() = default;
        Float4(const Float4&) = default;
        Float4& operator= (const Float4&) = default;
        Float4(Float4&&) = default;
        Float4& operator= (Float4&&) = default;

        constexpr Float4(float x, float y, float z, float w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }
        explicit Float4(float const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };
    static_assert(sizeof(Float4) == 16);
    static_assert(alignof(Float4) == 4);
    static_assert(std::is_nothrow_move_constructible_v<Float4>);
    static_assert(std::is_nothrow_move_assignable_v<Float4>);
    static_assert(std::is_trivial_v<Float4>);

    struct Float3 final
    {
    public:
        float X;
        float Y;
        float Z;
    public:
        Float3() = default;
        Float3(const Float3&) = default;
        Float3& operator= (const Float3&) = default;
        Float3(Float3&&) = default;
        Float3& operator= (Float3&&) = default;

        constexpr Float3(float x, float y, float z) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
        {
        }
        explicit Float3(float const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
        {
        }

        constexpr bool operator == (const Float3& other) const noexcept
        {
            return this->X == other.X
                && this->Y == other.Y
                && this->Z == other.Z;
        }
    };
    static_assert(sizeof(Float3) == 12);
    static_assert(alignof(Float3) == 4);
    static_assert(std::is_nothrow_move_constructible_v<Float3>);
    static_assert(std::is_nothrow_move_assignable_v<Float3>);
    static_assert(std::is_trivial_v<Float3>);

    struct Float2
    {
    public:
        float X;
        float Y;
    public:
        Float2() = default;
        Float2(const Float2&) = default;
        Float2& operator= (const Float2&) = default;
        Float2(Float2&&) = default;
        Float2& operator= (Float2&&) = default;

        constexpr Float2(float x, float y) noexcept
            : X{ x }
            , Y{ y }
        {
        }
        explicit Float2(float const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
        {
        }

        constexpr bool operator == (const Float2& other) const noexcept
        {
            return this->X == other.X
                && this->Y == other.Y;
        }
    };
    static_assert(sizeof(Float2) == 8);
    static_assert(alignof(Float2) == 4);
    static_assert(std::is_nothrow_move_constructible_v<Float2>);
    static_assert(std::is_nothrow_move_assignable_v<Float2>);
    static_assert(std::is_trivial_v<Float2>);

    struct alignas(16) Float4x4A final
    {
    public:
        union
        {
            struct
            {
                float M11, M12, M13, M14;
                float M21, M22, M23, M24;
                float M31, M32, M33, M34;
                float M41, M42, M43, M44;
            };
            float M[4][4];
            float F[16];
        };
    public:
        Float4x4A() = default;
        Float4x4A(const Float4x4A&) = default;
        Float4x4A& operator= (const Float4x4A&) = default;
        Float4x4A(Float4x4A&&) = default;
        Float4x4A& operator= (Float4x4A&&) = default;

        constexpr Float4x4A(
            float m11, float m12, float m13, float m14,
            float m21, float m22, float m23, float m24,
            float m31, float m32, float m33, float m34,
            float m41, float m42, float m43, float m44) noexcept
            : M11{ m11 }, M12{ m12 }, M13{ m13 }, M14{ m14 }
            , M21{ m21 }, M22{ m22 }, M23{ m23 }, M24{ m24 }
            , M31{ m31 }, M32{ m32 }, M33{ m33 }, M34{ m34 }
            , M41{ m41 }, M42{ m42 }, M43{ m43 }, M44{ m44 }
        {
        }
        explicit Float4x4A(float const* components) noexcept
            : M11{ components[0] }, M12{ components[1] }, M13{ components[2] }, M14{ components[3] }
            , M21{ components[4] }, M22{ components[5] }, M23{ components[6] }, M24{ components[7] }
            , M31{ components[8] }, M32{ components[9] }, M33{ components[10] }, M34{ components[11] }
            , M41{ components[12] }, M42{ components[13] }, M43{ components[14] }, M44{ components[15] }
        {
        }
    public:
        constexpr float operator() (size_t row, size_t column) const noexcept
        {
            return M[row][column];
        }
        constexpr float& operator() (size_t row, size_t column) noexcept
        {
            return M[row][column];
        }
    };
    static_assert(sizeof(Float4x4A) == 64);
    static_assert(alignof(Float4x4A) == 16);
    static_assert(std::is_nothrow_move_constructible_v<Float4x4A>);
    static_assert(std::is_nothrow_move_assignable_v<Float4x4A>);
    static_assert(std::is_trivial_v<Float4x4A>);

    struct alignas(16) Float4x3A final
    {
    public:
        union
        {
            struct
            {
                float M11, M12, M13;
                float M21, M22, M23;
                float M31, M32, M33;
                float M41, M42, M43;
            };
            float M[4][3];
            float F[12];
        };
    public:
        Float4x3A() = default;
        Float4x3A(const Float4x3A&) = default;
        Float4x3A& operator= (const Float4x3A&) = default;
        Float4x3A(Float4x3A&&) = default;
        Float4x3A& operator= (Float4x3A&&) = default;

        constexpr Float4x3A(
            float m11, float m12, float m13,
            float m21, float m22, float m23,
            float m31, float m32, float m33,
            float m41, float m42, float m43) noexcept
            : M11{ m11 }, M12{ m12 }, M13{ m13 }
            , M21{ m21 }, M22{ m22 }, M23{ m23 }
            , M31{ m31 }, M32{ m32 }, M33{ m33 }
            , M41{ m41 }, M42{ m42 }, M43{ m43 }
        {
        }
        explicit Float4x3A(float const* components) noexcept
            : M11{ components[0] }, M12{ components[1] }, M13{ components[2] }
            , M21{ components[3] }, M22{ components[4] }, M23{ components[5] }
            , M31{ components[6] }, M32{ components[7] }, M33{ components[8] }
            , M41{ components[9] }, M42{ components[10] }, M43{ components[11] }
        {
        }
    public:
        constexpr float operator() (size_t row, size_t column) const noexcept
        {
            return M[row][column];
        }
        constexpr float& operator() (size_t row, size_t column) noexcept
        {
            return M[row][column];
        }
    };
    static_assert(sizeof(Float4x3A) == 48);
    static_assert(alignof(Float4x3A) == 16);
    static_assert(std::is_nothrow_move_constructible_v<Float4x3A>);
    static_assert(std::is_nothrow_move_assignable_v<Float4x3A>);
    static_assert(std::is_trivial_v<Float4x3A>);

    struct alignas(16) Float3x4A final
    {
    public:
        union
        {
            struct
            {
                float M11, M12, M13, M14;
                float M21, M22, M23, M24;
                float M31, M32, M33, M34;
            };
            float M[3][4];
            float F[12];
        };

    public:
        Float3x4A() = default;
        Float3x4A(const Float3x4A&) = default;
        Float3x4A& operator= (const Float3x4A&) = default;
        Float3x4A(Float3x4A&&) = default;
        Float3x4A& operator= (Float3x4A&&) = default;

        constexpr Float3x4A(
            float m11, float m12, float m13, float m14,
            float m21, float m22, float m23, float m24,
            float m31, float m32, float m33, float m34) noexcept
            : M11{ m11 }, M12{ m12 }, M13{ m13 }, M14{ m14 }
            , M21{ m21 }, M22{ m22 }, M23{ m23 }, M24{ m24 }
            , M31{ m31 }, M32{ m32 }, M33{ m33 }, M34{ m34}
        {
        }
        explicit Float3x4A(const float* components) noexcept
            : M11{ components[0] }, M12{ components[1] }, M13{ components[2] }, M14{ components[3] }
            , M21{ components[4] }, M22{ components[5] }, M23{ components[6] }, M24{ components[7] }
            , M31{ components[8] }, M32{ components[9] }, M33{ components[10]}, M34{ components[11] }
        {
        }

    public:
        constexpr float operator() (size_t row, size_t column) const noexcept
        {
            return M[row][column];
        }
        constexpr float& operator() (size_t row, size_t column) noexcept
        {
            return M[row][column];
        }
    };
    static_assert(sizeof(Float3x4A) == 48);
    static_assert(alignof(Float3x4A) == 16);
    static_assert(std::is_nothrow_move_constructible_v<Float3x4A>);
    static_assert(std::is_nothrow_move_assignable_v<Float3x4A>);
    static_assert(std::is_trivial_v<Float3x4A>);

    struct alignas(16) Float3x3A final
    {
    public:
        union
        {
            struct
            {
                float M11, M12, M13;
                float M21, M22, M23;
                float M31, M32, M33;
            };
            float M[3][3];
            float F[9];
        };
    public:
        Float3x3A() = default;
        Float3x3A(const Float3x3A&) = default;
        Float3x3A& operator= (const Float3x3A&) = default;
        Float3x3A(Float3x3A&&) = default;
        Float3x3A& operator= (Float3x3A&&) = default;

        constexpr Float3x3A(
            float m11, float m12, float m13,
            float m21, float m22, float m23,
            float m31, float m32, float m33) noexcept
            : M11{ m11 }, M12{ m12 }, M13{ m13 }
            , M21{ m21 }, M22{ m22 }, M23{ m23 }
            , M31{ m31 }, M32{ m32 }, M33{ m33 }
        {
        }
        explicit Float3x3A(float const* components) noexcept
            : M11{ components[0] }, M12{ components[1] }, M13{ components[2] }
            , M21{ components[3] }, M22{ components[4] }, M23{ components[5] }
            , M31{ components[6] }, M32{ components[7] }, M33{ components[8] }
        {
        }
    public:
        constexpr float operator() (size_t row, size_t column) const noexcept
        {
            return M[row][column];
        }
        constexpr float& operator() (size_t row, size_t column) noexcept
        {
            return M[row][column];
        }
    };
    static_assert(sizeof(Float3x3A) == 48);
    static_assert(alignof(Float3x3A) == 16);
    static_assert(std::is_nothrow_move_constructible_v<Float3x3A>);
    static_assert(std::is_nothrow_move_assignable_v<Float3x3A>);
    static_assert(std::is_trivial_v<Float3x3A>);


    struct Float4x4 final
    {
    public:
        union
        {
            struct
            {
                float M11, M12, M13, M14;
                float M21, M22, M23, M24;
                float M31, M32, M33, M34;
                float M41, M42, M43, M44;
            };
            float M[4][4];
            float F[16];
        };
    public:
        Float4x4() = default;
        Float4x4(const Float4x4&) = default;
        Float4x4& operator= (const Float4x4&) = default;
        Float4x4(Float4x4&&) = default;
        Float4x4& operator= (Float4x4&&) = default;

        constexpr Float4x4(
            float m11, float m12, float m13, float m14,
            float m21, float m22, float m23, float m24,
            float m31, float m32, float m33, float m34,
            float m41, float m42, float m43, float m44) noexcept
            : M11{ m11 }, M12{ m12 }, M13{ m13 }, M14{ m14 }
            , M21{ m21 }, M22{ m22 }, M23{ m23 }, M24{ m24 }
            , M31{ m31 }, M32{ m32 }, M33{ m33 }, M34{ m34 }
            , M41{ m41 }, M42{ m42 }, M43{ m43 }, M44{ m44 }
        {
        }
        explicit Float4x4(float const* components) noexcept
            : M11{ components[0] }, M12{ components[1] }, M13{ components[2] }, M14{ components[3] }
            , M21{ components[4] }, M22{ components[5] }, M23{ components[6] }, M24{ components[7] }
            , M31{ components[8] }, M32{ components[9] }, M33{ components[10] }, M34{ components[11] }
            , M41{ components[12] }, M42{ components[13] }, M43{ components[14] }, M44{ components[15] }
        {
        }
    public:
        constexpr float operator() (size_t row, size_t column) const noexcept
        {
            return M[row][column];
        }
        constexpr float& operator() (size_t row, size_t column) noexcept
        {
            return M[row][column];
        }
    };
    static_assert(sizeof(Float4x4) == 64);
    static_assert(alignof(Float4x4) == 4);
    static_assert(std::is_nothrow_move_constructible_v<Float4x4>);
    static_assert(std::is_nothrow_move_assignable_v<Float4x4>);
    static_assert(std::is_trivial_v<Float4x4>);

    struct Float4x3 final
    {
    public:
        union
        {
            struct
            {
                float M11, M12, M13;
                float M21, M22, M23;
                float M31, M32, M33;
                float M41, M42, M43;
            };
            float M[4][3];
            float F[12];
        };
    public:
        Float4x3() = default;
        Float4x3(const Float4x3&) = default;
        Float4x3& operator= (const Float4x3&) = default;
        Float4x3(Float4x3&&) = default;
        Float4x3& operator= (Float4x3&&) = default;

        constexpr Float4x3(
            float m11, float m12, float m13,
            float m21, float m22, float m23,
            float m31, float m32, float m33,
            float m41, float m42, float m43) noexcept
            : M11{ m11 }, M12{ m12 }, M13{ m13 }
            , M21{ m21 }, M22{ m22 }, M23{ m23 }
            , M31{ m31 }, M32{ m32 }, M33{ m33 }
            , M41{ m41 }, M42{ m42 }, M43{ m43 }
        {
        }
        explicit Float4x3(float const* components) noexcept
            : M11{ components[0] }, M12{ components[1] }, M13{ components[2] }
            , M21{ components[3] }, M22{ components[4] }, M23{ components[5] }
            , M31{ components[6] }, M32{ components[7] }, M33{ components[8] }
            , M41{ components[9] }, M42{ components[10] }, M43{ components[11] }
        {
        }
    public:
        constexpr float operator() (size_t row, size_t column) const noexcept
        {
            return M[row][column];
        }
        constexpr float& operator() (size_t row, size_t column) noexcept
        {
            return M[row][column];
        }
    };
    static_assert(sizeof(Float4x3) == 48);
    static_assert(alignof(Float4x3) == 4);
    static_assert(std::is_nothrow_move_constructible_v<Float4x3>);
    static_assert(std::is_nothrow_move_assignable_v<Float4x3>);
    static_assert(std::is_trivial_v<Float4x3>);

    struct Float3x4 final
    {
    public:
        union
        {
            struct
            {
                float M11, M12, M13, M14;
                float M21, M22, M23, M24;
                float M31, M32, M33, M34;
            };
            float M[3][4];
            float F[12];
        };

    public:
        Float3x4() = default;
        Float3x4(const Float3x4&) = default;
        Float3x4& operator= (const Float3x4&) = default;
        Float3x4(Float3x4&&) = default;
        Float3x4& operator= (Float3x4&&) = default;

        constexpr Float3x4(
            float m11, float m12, float m13, float m14,
            float m21, float m22, float m23, float m24,
            float m31, float m32, float m33, float m34) noexcept
            : M11{ m11 }, M12{ m12 }, M13{ m13 }, M14{ m14 }
            , M21{ m21 }, M22{ m22 }, M23{ m23 }, M24{ m24 }
            , M31{ m31 }, M32{ m32 }, M33{ m33 }, M34{ m34}
        {
        }
        explicit Float3x4(const float* components) noexcept
            : M11{ components[0] }, M12{ components[1] }, M13{ components[2] }, M14{ components[3] }
            , M21{ components[4] }, M22{ components[5] }, M23{ components[6] }, M24{ components[7] }
            , M31{ components[8] }, M32{ components[9] }, M33{ components[10]}, M34{ components[11] }
        {
        }

    public:
        constexpr float operator() (size_t row, size_t column) const noexcept
        {
            return M[row][column];
        }
        constexpr float& operator() (size_t row, size_t column) noexcept
        {
            return M[row][column];
        }
    };
    static_assert(sizeof(Float3x4) == 48);
    static_assert(alignof(Float3x4) == 4);
    static_assert(std::is_nothrow_move_constructible_v<Float3x4>);
    static_assert(std::is_nothrow_move_assignable_v<Float3x4>);
    static_assert(std::is_trivial_v<Float3x4>);

    struct Float3x3 final
    {
    public:
        union
        {
            struct
            {
                float M11, M12, M13;
                float M21, M22, M23;
                float M31, M32, M33;
            };
            float M[3][3];
            float F[9];
        };
    public:
        Float3x3() = default;
        Float3x3(const Float3x3&) = default;
        Float3x3& operator= (const Float3x3&) = default;
        Float3x3(Float3x3&&) = default;
        Float3x3& operator= (Float3x3&&) = default;

        constexpr Float3x3(
            float m11, float m12, float m13,
            float m21, float m22, float m23,
            float m31, float m32, float m33) noexcept
            : M11{ m11 }, M12{ m12 }, M13{ m13 }
            , M21{ m21 }, M22{ m22 }, M23{ m23 }
            , M31{ m31 }, M32{ m32 }, M33{ m33 }
        {
        }
        explicit Float3x3(float const* components) noexcept
            : M11{ components[0] }, M12{ components[1] }, M13{ components[2] }
            , M21{ components[3] }, M22{ components[4] }, M23{ components[5] }
            , M31{ components[6] }, M32{ components[7] }, M33{ components[8] }
        {
        }
    public:
        constexpr float operator() (size_t row, size_t column) const noexcept
        {
            return M[row][column];
        }
        constexpr float& operator() (size_t row, size_t column) noexcept
        {
            return M[row][column];
        }
    };
    static_assert(sizeof(Float3x3) == 36);
    static_assert(alignof(Float3x3) == 4);
    static_assert(std::is_nothrow_move_constructible_v<Float3x3>);
    static_assert(std::is_nothrow_move_assignable_v<Float3x3>);
    static_assert(std::is_trivial_v<Float3x3>);
    
    struct UInt4 final
    {
    public:
        uint32_t X;
        uint32_t Y;
        uint32_t Z;
        uint32_t W;
    public:
        UInt4() = default;
        UInt4(const UInt4&) = default;
        UInt4& operator= (const UInt4&) = default;
        UInt4(UInt4&&) = default;
        UInt4& operator= (UInt4&&) = default;

        constexpr UInt4(uint32_t x, uint32_t y, uint32_t z, uint32_t w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }
        explicit UInt4(uint32_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };
    static_assert(sizeof(UInt4) == 16);
    static_assert(std::is_trivial_v<UInt4>);

    struct UInt3 final
    {
    public:
        uint32_t X;
        uint32_t Y;
        uint32_t Z;
    public:
        UInt3() = default;
        UInt3(const UInt3&) = default;
        UInt3& operator= (const UInt3&) = default;
        UInt3(UInt3&&) = default;
        UInt3& operator= (UInt3&&) = default;

        constexpr UInt3(uint32_t x, uint32_t y, uint32_t z) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
        {
        }
        explicit UInt3(uint32_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
        {
        }
    };
    static_assert(sizeof(UInt3) == 12);
    static_assert(std::is_trivial_v<UInt3>);

    struct UInt2 final
    {
    public:
        uint32_t X;
        uint32_t Y;
    public:
        UInt2() = default;
        UInt2(const UInt2&) = default;
        UInt2& operator= (const UInt2&) = default;
        UInt2(UInt2&&) = default;
        UInt2& operator= (UInt2&&) = default;

        constexpr UInt2(uint32_t x, uint32_t y) noexcept
            : X{ x }
            , Y{ y }
        {
        }
        explicit UInt2(uint32_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
        {
        }
    };
    static_assert(sizeof(UInt2) == 8);
    static_assert(std::is_trivial_v<UInt2>);

    struct Int4 final
    {
    public:
        int32_t X;
        int32_t Y;
        int32_t Z;
        int32_t W;
    public:
        Int4() = default;
        Int4(const Int4&) = default;
        Int4& operator= (const Int4&) = default;
        Int4(Int4&&) = default;
        Int4& operator= (Int4&&) = default;

        constexpr Int4(int32_t x, int32_t y, int32_t z, int32_t w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }
        explicit Int4(int32_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };
    static_assert(sizeof(Int4) == 16);
    static_assert(std::is_trivial_v<Int4>);

    struct Int3 final
    {
    public:
        int32_t X;
        int32_t Y;
        int32_t Z;
    public:
        Int3() = default;
        Int3(const Int3&) = default;
        Int3& operator= (const Int3&) = default;
        Int3(Int3&&) = default;
        Int3& operator= (Int3&&) = default;

        constexpr Int3(int32_t x, int32_t y, int32_t z) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
        {
        }
        explicit Int3(int32_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
        {
        }
    };
    static_assert(sizeof(Int3) == 12);
    static_assert(std::is_trivial_v<Int3>);

    struct Int2 final
    {
    public:
        int32_t X;
        int32_t Y;
    public:
        Int2() = default;
        Int2(const Int2&) = default;
        Int2& operator= (const Int2&) = default;
        Int2(Int2&&) = default;
        Int2& operator= (Int2&&) = default;

        constexpr Int2(int32_t x, int32_t y) noexcept
            : X{ x }
            , Y{ y }
        {
        }
        explicit Int2(int32_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
        {
        }
    };
    static_assert(sizeof(Int2) == 8);
    static_assert(std::is_trivial_v<Int2>);

    struct Half final
    {
        uint16_t Value;
    };
    static_assert(sizeof(Half) == sizeof(uint16_t));
    static_assert(alignof(Half) == alignof(uint16_t));
    static_assert(std::is_trivial_v<Half>);

    struct Half2 final
    {
    public:
        Half X;
        Half Y;
    public:
        Half2() = default;
        Half2(const Half2&) = default;
        Half2& operator= (const Half2&) = default;
        Half2(Half2&&) = default;
        Half2& operator= (Half2&&) = default;

        constexpr Half2(Half x, Half y) noexcept
            : X{ x }
            , Y{ y }
        {
        }
        explicit Half2(Half const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
        {
        }

        Half2(float x, float y) noexcept;
        explicit Half2(const float* components) noexcept;
    };
    static_assert(sizeof(Half2) == (sizeof(Half) * 2));
    static_assert(alignof(Half2) == alignof(Half));
    static_assert(std::is_trivial_v<Half2>);

    //inline Half2 ToHalf(float x, float y) noexcept
    //{
    //    return Half2{
    //        ToHalf(x),
    //        ToHalf(y)
    //    };
    //}

    struct Half3 final
    {
    public:
        Half X;
        Half Y;
        Half Z;
    public:
        Half3() = default;
        Half3(const Half3&) = default;
        Half3& operator= (const Half3&) = default;
        Half3(Half3&&) = default;
        Half3& operator= (Half3&&) = default;

    public:
        constexpr Half3(Half x, Half y, Half z) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
        {
        }
        explicit Half3(const Half* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
        {
        }

        Half3(float x, float y, float z) noexcept;
        explicit Half3(const float* components) noexcept;
    };
    static_assert(sizeof(Half3) == (sizeof(Half) * 3));
    static_assert(alignof(Half3) == alignof(Half));
    static_assert(std::is_trivial_v<Half3>);

    struct Half4 final
    {
    public:
        Half X;
        Half Y;
        Half Z;
        Half W;
    public:
        Half4() = default;
        Half4(const Half4&) = default;
        Half4& operator= (const Half4&) = default;
        Half4(Half4&&) = default;
        Half4& operator= (Half4&&) = default;

        constexpr Half4(Half x, Half y, Half z, Half w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }
        explicit Half4(Half const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }

        Half4(float x, float y, float z, float w) noexcept;
        explicit Half4(const float* components) noexcept;
    };
    static_assert(sizeof(Half4) == (sizeof(Half) * 4));
    static_assert(alignof(Half4) == alignof(Half));
    static_assert(std::is_trivial_v<Half4>);

    struct SByte4 final
    {
    public:
        int8_t X;
        int8_t Y;
        int8_t Z;
        int8_t W;
    public:
        SByte4() = default;
        SByte4(const SByte4&) = default;
        SByte4& operator= (const SByte4&) = default;
        SByte4(SByte4&&) = default;
        SByte4& operator= (SByte4&&) = default;

        constexpr SByte4(int8_t x, int8_t y, int8_t z, int8_t w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }

        explicit SByte4(int8_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };

    struct UByte4 final
    {
    public:
        uint8_t X;
        uint8_t Y;
        uint8_t Z;
        uint8_t W;
    public:
        UByte4() = default;
        UByte4(const UByte4&) = default;
        UByte4& operator= (const UByte4&) = default;
        UByte4(UByte4&&) = default;
        UByte4& operator= (UByte4&&) = default;

        constexpr UByte4(uint8_t x, uint8_t y, uint8_t z, uint8_t w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }

        explicit UByte4(uint8_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };

    struct SByteN4 final
    {
    public:
        int8_t X;
        int8_t Y;
        int8_t Z;
        int8_t W;
    public:
        SByteN4() = default;
        SByteN4(const SByteN4&) = default;
        SByteN4& operator= (const SByteN4&) = default;
        SByteN4(SByteN4&&) = default;
        SByteN4& operator= (SByteN4&&) = default;

        constexpr SByteN4(int8_t x, int8_t y, int8_t z, int8_t w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }

        explicit SByteN4(int8_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };

    struct UByteN4 final
    {
    public:
        uint8_t X;
        uint8_t Y;
        uint8_t Z;
        uint8_t W;
    public:
        UByteN4() = default;
        UByteN4(const UByteN4&) = default;
        UByteN4& operator= (const UByteN4&) = default;
        UByteN4(UByteN4&&) = default;
        UByteN4& operator= (UByteN4&&) = default;

        constexpr UByteN4(uint8_t x, uint8_t y, uint8_t z, uint8_t w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }

        explicit UByteN4(uint8_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };
}

namespace Graphyte
{
    struct Short4 final
    {
    public:
        int16_t X;
        int16_t Y;
        int16_t Z;
        int16_t W;
    public:
        Short4() = default;
        Short4(const Short4&) = default;
        Short4& operator= (const Short4&) = default;
        Short4(Short4&&) = default;
        Short4& operator= (Short4&&) = default;

        constexpr Short4(int16_t x, int16_t y, int16_t z, int16_t w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }

        explicit Short4(int16_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };

    struct UShort4 final
    {
    public:
        uint16_t X;
        uint16_t Y;
        uint16_t Z;
        uint16_t W;
    public:
        UShort4() = default;
        UShort4(const UShort4&) = default;
        UShort4& operator= (const UShort4&) = default;
        UShort4(UShort4&&) = default;
        UShort4& operator= (UShort4&&) = default;

        constexpr UShort4(uint16_t x, uint16_t y, uint16_t z, uint16_t w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }

        explicit UShort4(uint16_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };

    struct ShortN4 final
    {
    public:
        int16_t X;
        int16_t Y;
        int16_t Z;
        int16_t W;
    public:
        ShortN4() = default;
        ShortN4(const ShortN4&) = default;
        ShortN4& operator= (const ShortN4&) = default;
        ShortN4(ShortN4&&) = default;
        ShortN4& operator= (ShortN4&&) = default;

        constexpr ShortN4(int16_t x, int16_t y, int16_t z, int16_t w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }

        explicit ShortN4(int16_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };

    struct UShortN4 final
    {
    public:
        uint16_t X;
        uint16_t Y;
        uint16_t Z;
        uint16_t W;
    public:
        UShortN4() = default;
        UShortN4(const UShortN4&) = default;
        UShortN4& operator= (const UShortN4&) = default;
        UShortN4(UShortN4&&) = default;
        UShortN4& operator= (UShortN4&&) = default;

        constexpr UShortN4(uint16_t x, uint16_t y, uint16_t z, uint16_t w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }

        explicit UShortN4(uint16_t const* components) noexcept
            : X{ components[0] }
            , Y{ components[1] }
            , Z{ components[2] }
            , W{ components[3] }
        {
        }
    };
}

namespace Graphyte
{
    struct Float3PK final
    {
        union
        {
            struct
            {
                uint32_t XM : 6;
                uint32_t XE : 5;

                uint32_t YM : 6;
                uint32_t YE : 5;

                uint32_t ZM : 5;
                uint32_t ZE : 5;
            };

            uint32_t Value;
        };

        Float3PK() noexcept = default;
        Float3PK(const Float3PK&) noexcept = default;
        Float3PK& operator= (const Float3PK&) noexcept = default;
        Float3PK(Float3PK&&) noexcept = default;
        Float3PK& operator= (Float3PK&&) noexcept = default;

        explicit constexpr Float3PK(uint32_t value)
            : Value{ value }
        {
        }

        Float3PK(float x, float y, float z) noexcept;
        explicit Float3PK(const float* source) noexcept;

        operator uint32_t() const noexcept
        {
            return Value;
        }

        Float3PK& operator= (uint32_t value) noexcept
        {
            Value = value;
            return *this;
        }
    };

    struct Float3SE final
    {
        union
        {
            struct
            {
                uint32_t XM : 9;
                uint32_t YM : 9;
                uint32_t ZM : 9;
                uint32_t E : 5;
            };

            uint32_t Value;
        };

        Float3SE() noexcept = default;
        Float3SE(const Float3SE&) noexcept = default;
        Float3SE& operator= (const Float3SE&) noexcept = default;
        Float3SE(Float3SE&&) noexcept = default;
        Float3SE& operator= (Float3SE&&) noexcept = default;

        explicit constexpr Float3SE(uint32_t value)
            : Value{ value }
        {
        }

        Float3SE(float x, float y, float z) noexcept;
        explicit Float3SE(const float* source) noexcept;

        operator uint32_t() const noexcept
        {
            return Value;
        }

        Float3SE& operator= (uint32_t value) noexcept
        {
            Value = value;
            return *this;
        }
    };

    struct Int_10_10_10_2_SNorm
    {
        union
        {
            struct
            {
                int32_t X : 10;     // -511/511 -> 511/511
                int32_t Y : 10;     // -511/511 -> 511/511
                int32_t Z : 10;     // -511/511 -> 511/511
                uint32_t W : 2;     // 0/3 -> 3/3
            };

            uint32_t Value;
        };

        Int_10_10_10_2_SNorm() noexcept = default;
        Int_10_10_10_2_SNorm(const Int_10_10_10_2_SNorm&) noexcept = default;
        Int_10_10_10_2_SNorm& operator= (const Int_10_10_10_2_SNorm&) noexcept = default;
        Int_10_10_10_2_SNorm(Int_10_10_10_2_SNorm&&) noexcept = default;
        Int_10_10_10_2_SNorm& operator= (Int_10_10_10_2_SNorm&&) noexcept = default;

        explicit constexpr Int_10_10_10_2_SNorm(uint32_t value) noexcept
            : Value{ value }
        {
        }
        Int_10_10_10_2_SNorm(float x, float y, float z, float w) noexcept;
        explicit Int_10_10_10_2_SNorm(const float* source) noexcept;

        operator uint32_t() const noexcept
        {
            return Value;
        }

        Int_10_10_10_2_SNorm& operator= (uint32_t value) noexcept
        {
            Value = value;
            return *this;
        }
    };

    struct Int_10_10_10_2_UNorm final
    {
        union
        {
            struct
            {
                uint32_t X : 10;    // 0/1023 -> 1023/1023
                uint32_t Y : 10;    // 0/1023 -> 1023/1023
                uint32_t Z : 10;    // 0/1023 -> 1023/1023
                uint32_t W : 2;     // 0/3 -> 3/3
            };

            uint32_t Value;
        };

        Int_10_10_10_2_UNorm() noexcept = default;
        Int_10_10_10_2_UNorm(const Int_10_10_10_2_UNorm&) noexcept = default;
        Int_10_10_10_2_UNorm& operator= (const Int_10_10_10_2_UNorm&) noexcept = default;
        Int_10_10_10_2_UNorm(Int_10_10_10_2_UNorm&&) noexcept = default;
        Int_10_10_10_2_UNorm& operator= (Int_10_10_10_2_UNorm&&) noexcept = default;

        explicit constexpr Int_10_10_10_2_UNorm(uint32_t value) noexcept
            : Value{ value }
        {
        }
        Int_10_10_10_2_UNorm(float x, float y, float z, float w) noexcept;
        explicit Int_10_10_10_2_UNorm(const float* source) noexcept;

        operator uint32_t() const noexcept
        {
            return Value;
        }

        Int_10_10_10_2_UNorm& operator= (uint32_t value) noexcept
        {
            Value = value;
            return *this;
        }
    };

    struct UInt_10_10_10_2 final
    {
        union
        {
            struct
            {
                uint32_t X : 10;    // 0 -> 1023
                uint32_t Y : 10;    // 0 -> 1023
                uint32_t Z : 10;    // 0 -> 1023
                uint32_t W : 2;     // 0 -> 3
            };

            uint32_t Value;
        };

        UInt_10_10_10_2() noexcept = default;
        UInt_10_10_10_2(const UInt_10_10_10_2&) noexcept = default;
        UInt_10_10_10_2& operator= (const UInt_10_10_10_2&) noexcept = default;
        UInt_10_10_10_2(UInt_10_10_10_2&&) noexcept = default;
        UInt_10_10_10_2& operator= (UInt_10_10_10_2&&) noexcept = default;

        explicit constexpr UInt_10_10_10_2(uint32_t value) noexcept
            : Value{ value }
        {
        }

        UInt_10_10_10_2(float x, float y, float z, float w) noexcept;
        explicit UInt_10_10_10_2(const float* source) noexcept;

        operator uint32_t() const noexcept
        {
            return Value;
        }
        UInt_10_10_10_2& operator= (uint32_t value) noexcept
        {
            Value = value;
            return *this;
        }
    };

    struct UShort_4_4_4_4 final
    {
        union
        {
            struct
            {
                uint16_t X : 4; // 0 -> 15
                uint16_t Y : 4; // 0 -> 15
                uint16_t Z : 4; // 0 -> 15
                uint16_t W : 4; // 0 -> 15
            };

            uint16_t Value;
        };

        UShort_4_4_4_4() noexcept = default;
        UShort_4_4_4_4(const UShort_4_4_4_4&) noexcept = default;
        UShort_4_4_4_4& operator= (const UShort_4_4_4_4&) noexcept = default;
        UShort_4_4_4_4(UShort_4_4_4_4&&) noexcept = default;
        UShort_4_4_4_4& operator= (UShort_4_4_4_4&&) noexcept = default;

        explicit constexpr UShort_4_4_4_4(uint16_t value) noexcept
            : Value{ value }
        {
        }
        constexpr UShort_4_4_4_4(uint8_t x, uint8_t y, uint8_t z, uint8_t w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ w }
        {
        }
        explicit UShort_4_4_4_4(const uint8_t* source) noexcept
            : X{ source[0] }
            , Y{ source[1] }
            , Z{ source[2] }
            , W{ source[3] }
        {
        }
        UShort_4_4_4_4(float x, float y, float z, float w) noexcept;
        explicit UShort_4_4_4_4(const float* source) noexcept;

        operator uint16_t () const noexcept
        {
            return Value;
        }
        UShort_4_4_4_4& operator= (uint16_t value) noexcept
        {
            Value = value;
            return *this;
        }
    };

    struct UShort_5_5_5_1 final
    {
        union
        {
            struct
            {
                uint16_t X : 5; // 0 -> 31
                uint16_t Y : 5; // 0 -> 31
                uint16_t Z : 5; // 0 -> 31
                uint16_t W : 1; // 0 -> 1
            };

            uint16_t Value;
        };

        UShort_5_5_5_1() noexcept = default;
        UShort_5_5_5_1(const UShort_5_5_5_1&) noexcept = default;
        UShort_5_5_5_1& operator= (const UShort_5_5_5_1&) noexcept = default;
        UShort_5_5_5_1(UShort_5_5_5_1&&) noexcept = default;
        UShort_5_5_5_1& operator= (UShort_5_5_5_1&&) noexcept = default;

        explicit UShort_5_5_5_1(uint16_t value) noexcept
            : Value{ value }
        {
        }
        constexpr UShort_5_5_5_1(uint8_t x, uint8_t y, uint8_t z, bool w) noexcept
            : X{ x }
            , Y{ y }
            , Z{ z }
            , W{ static_cast<uint16_t>(w ? 1U : 0U) }
        {
        }

        UShort_5_5_5_1(const uint8_t* source, bool w) noexcept
            : X{ source[0] }
            , Y{ source[1] }
            , Z{ source[2] }
            , W{ static_cast<uint16_t>(w ? 1U : 0U) }
        {
        }

        UShort_5_5_5_1(float x, float y, float z, bool w) noexcept;
        UShort_5_5_5_1(const float* source, bool value) noexcept;

        operator uint16_t() const noexcept
        {
            return Value;
        }

        UShort_5_5_5_1& operator= (uint16_t value) noexcept
        {
            Value = value;
            return *this;
        }
    };

    struct UShort_5_6_5 final
    {
        union
        {
            struct
            {
                uint16_t X : 5; // 0 -> 31
                uint16_t Y : 6; // 0 -> 63
                uint16_t Z : 5; // 0 -> 31
            };

            uint16_t Value;
        };

        UShort_5_6_5() noexcept = default;
        UShort_5_6_5(const UShort_5_6_5&) noexcept = default;
        UShort_5_6_5& operator= (const UShort_5_6_5&) noexcept = default;
        UShort_5_6_5(UShort_5_6_5&&) noexcept = default;
        UShort_5_6_5& operator= (UShort_5_6_5&&) noexcept = default;

        explicit UShort_5_6_5(uint16_t value) noexcept
            : Value{ value }
        {
        }
        constexpr UShort_5_6_5(uint8_t x, uint8_t y, uint8_t z)
            : X{ x }
            , Y{ y }
            , Z{ z }
        {
        }
        explicit UShort_5_6_5(const uint8_t* source) noexcept
            : X{ source[0] }
            , Y{ source[1] }
            , Z{ source[2] }
        {
        }
        UShort_5_6_5(float x, float y, float z) noexcept;
        explicit UShort_5_6_5(const float* source) noexcept;

        operator uint16_t() const noexcept
        {
            return Value;
        }

        UShort_5_6_5& operator= (uint16_t value) noexcept
        {
            Value = value;
            return *this;
        }
    };
}

#include <Graphyte/Types.impl.hxx>
