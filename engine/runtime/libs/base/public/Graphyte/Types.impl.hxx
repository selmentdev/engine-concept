#pragma once
#include <Graphyte/Types.hxx>
#include <Graphyte/Math/Scalar.hxx>
#include <Graphyte/Half.hxx>

namespace Graphyte
{
    inline Half2::Half2(float x, float y) noexcept
        : X{ ToHalf(x) }
        , Y{ ToHalf(y) }
    {
    }

    inline Half2::Half2(const float* components) noexcept
        : X{ ToHalf(components[0]) }
        , Y{ ToHalf(components[1]) }
    {
    }

    inline Half3::Half3(float x, float y, float z) noexcept
        : X{ ToHalf(x) }
        , Y{ ToHalf(y) }
        , Z{ ToHalf(z) }
    {
    }

    inline Half3::Half3(const float* components) noexcept
        : X{ ToHalf(components[0]) }
        , Y{ ToHalf(components[1]) }
        , Z{ ToHalf(components[2]) }
    {
    }

    inline Half4::Half4(float x, float y, float z, float w) noexcept
        : X{ ToHalf(x) }
        , Y{ ToHalf(y) }
        , Z{ ToHalf(z) }
        , W{ ToHalf(w) }
    {
    }

    inline Half4::Half4(const float* components) noexcept
        : X{ ToHalf(components[0]) }
        , Y{ ToHalf(components[1]) }
        , Z{ ToHalf(components[2]) }
        , W{ ToHalf(components[3]) }
    {
    }
}
