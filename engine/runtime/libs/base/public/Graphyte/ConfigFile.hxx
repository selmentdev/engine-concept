#pragma once
#include <Graphyte/Base.module.hxx>
#include <Graphyte/Storage/Archive.hxx>
#include <Graphyte/Delegate.hxx>

namespace Graphyte
{
    class BASE_API ConfigFileParser
    {
    public:
        /**
         * \brief       Parse configuration file content using provided callback.
         *
         * \param[in]   content     Provides config file content.
         * \param[in]   callback    Provides callback to call for each parsed name-value pair.
         *
         * \return      true when file was parsed successfully, \c false otherwise.
         */
        static bool Parse(
            std::string_view content,
            const Delegate<bool(std::string_view section, std::string_view name, std::string_view value)>& callback
        ) noexcept;
    };

    class BASE_API ConfigFile final
    {
    public:
        using NameValueContainer = std::map<std::string, std::string, std::less<>>;
        using SectionsContainer = std::map<std::string, NameValueContainer, std::less<>>;

    private:
        SectionsContainer m_Sections;

    public:
        bool Read(std::string_view content) noexcept;
        bool Write(std::string& content) const noexcept;

        bool Read(Storage::Archive& archive) noexcept;
        bool Write(Storage::Archive& archive) const noexcept;

        SectionsContainer& GetSections() noexcept
        {
            return m_Sections;
        }

        const SectionsContainer& GetSections() const noexcept
        {
            return m_Sections;
        }

    public:
        bool GetString(std::string_view section, std::string_view name, std::string& value) const noexcept;
        bool GetInt32(std::string_view section, std::string_view name, int32_t& value) const noexcept;
        bool GetInt64(std::string_view section, std::string_view name, int64_t& value) const noexcept;
        bool GetFloat(std::string_view section, std::string_view name, float& value) const noexcept;
        bool GetBool(std::string_view section, std::string_view name, bool& value) const noexcept;

        void SetString(std::string_view section, std::string_view name, std::string_view value) noexcept;
        void SetInt32(std::string_view section, std::string_view name, int32_t value) noexcept;
        void SetInt64(std::string_view section, std::string_view name, int64_t value) noexcept;
        void SetFloat(std::string_view section, std::string_view name, float value) noexcept;
        void SetBool(std::string_view section, std::string_view name, bool value) noexcept;
    };
}
