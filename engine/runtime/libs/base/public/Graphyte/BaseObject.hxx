#pragma once
#include <Graphyte/Base.module.hxx>
#include <Graphyte/Threading/Interlocked.hxx>
#include <Graphyte/Flags.hxx>
#include <Graphyte/Diagnostics.hxx>

namespace Graphyte
{
    struct TypeInfo;
    class Object;

    enum class TypeFlags
    {
        None = 0,
        IsInterface = 1 << 0,
        IsAbstract = 1 << 1,
        IsDeprecated = 1 << 2,
        IsStatic = 1 << 3,
        IsEnum = 1 << 4,
    };
    GX_ENUM_CLASS_FLAGS(TypeFlags);

    using TypeInfoRegisterCallback = void();

    using TypeCode = uint64_t;

    struct BASE_API TypeInfo
    {
    private:
        TypeInfo* m_BaseType;
        TypeInfo* m_FirstChild;
        TypeInfo* m_NextType;
        const char* m_Name;
        size_t m_SizeOf;
        size_t m_AlignOf;
        TypeFlags m_Flags;
        TypeCode m_TypeCode;

    public:
        TypeInfo(
            const char* name,
            TypeInfoRegisterCallback register_callback,
            TypeInfo* base_type,
            size_t sizeof_type,
            size_t alignof_type,
            TypeFlags flags,
            TypeCode type_uuid) noexcept
            : m_BaseType{ base_type }
            , m_FirstChild{ nullptr }
            , m_NextType{ nullptr }
            , m_Name{ name }
            , m_SizeOf{ sizeof_type }
            , m_AlignOf{ alignof_type }
            , m_Flags{ flags }
            , m_TypeCode{ type_uuid }
        {
            if (base_type != nullptr)
            {
                if (base_type->m_FirstChild != nullptr)
                {
                    m_NextType = base_type->m_FirstChild;
                }
                base_type->m_FirstChild = this;
            }

            if (register_callback != nullptr)
            {
                register_callback();
            }
        }

    public:
        const char* GetTypeName() const noexcept
        {
            return m_Name;
        }

        const TypeInfo* GetBaseType() const noexcept
        {
            return m_BaseType;
        }
        TypeCode GetTypeCode() const noexcept
        {
            return m_TypeCode;
        }
        size_t GetSizeOf() const noexcept
        {
            return m_SizeOf;
        }
        size_t GetAlignOf() const noexcept
        {
            return m_AlignOf;
        }
        TypeFlags GetFlags() const noexcept
        {
            return m_Flags;
        }
        bool IsAbstract() const noexcept
        {
            return Flags::Has(m_Flags, TypeFlags::IsAbstract);
        }
        bool IsInterface() const noexcept
        {
            return Flags::Has(m_Flags, TypeFlags::IsInterface);
        }
        bool IsDeprecated() const noexcept
        {
            return Flags::Has(m_Flags, TypeFlags::IsDeprecated);
        }
        bool IsEnum() const noexcept
        {
            return Flags::Has(m_Flags, TypeFlags::IsEnum);
        }

    public:
        virtual Object* ActivateInstance() const noexcept
        {
            return nullptr;
        }

    public:
        bool IsExact(const TypeInfo* type) const noexcept
        {
            return this == type;
        }
        bool IsExact(std::string_view name) const noexcept
        {
            return m_Name == name;
        }
        bool IsExact(TypeCode type_uuid) const noexcept
        {
            return m_TypeCode == type_uuid;
        }

    public:
        bool IsDerived(const TypeInfo* type) const noexcept;
        bool IsDerived(std::string_view name) const noexcept;
        bool IsDerived(TypeCode type_uuid) const noexcept;
    };

    template <typename T>
    struct TypeInfoT : public TypeInfo
    {
    public:
        TypeInfoT(
            const char* name,
            TypeInfoRegisterCallback register_callback,
            TypeInfo* base_class,
            size_t sizeof_type,
            size_t alignof_type,
            TypeFlags flags,
            TypeCode type_uuid) noexcept
            : TypeInfo{ name, register_callback, base_class, sizeof_type, alignof_type, flags, type_uuid }
        {
        }

        virtual Object* ActivateInstance() const noexcept override
        {
            return new T();
        }
    };

    template <typename T>
    __forceinline const TypeInfo* TypeOf() noexcept
    {
        return &T::RTTI_TypeInfo;
    }

    template <typename T>
    __forceinline TypeCode TypeCodeOf() noexcept
    {
        return T::RTTI_TypeCode();
    }

    template <typename T>
    __forceinline const char* TypeNameOf() noexcept
    {
        return T::RTTI_TypeInfo.GetTypeName();
    }

    template <typename T, typename U>
    __forceinline T* TypeCastImpl(U* original) noexcept
    {
        if (original->RTTI_GetTypeInfo()->IsDerived(TypeOf<T>()))
        {
              return static_cast<T*>(original);
        }

        return nullptr;
    }

    class TypeFactory final
    {
    private:
        std::map<TypeCode, const TypeInfo*> m_Types;

    public:
        template <typename T>
        bool Register() noexcept
        {
            return Register(TypeOf<T>());
        }
        bool Register(const TypeInfo* type) noexcept
        {
            auto const& result = m_Types.insert(std::make_pair(type->GetTypeCode(), type));
            return result.second;
        }
        template <typename T>
        bool Unregister() noexcept
        {
            return Unregister(TypeCodeOf<T>());
        }
        bool Unregister(TypeCode type_code) noexcept
        {
            return m_Types.erase(type_code) != 0;
        }
        bool Unregister(const TypeInfo* type) noexcept
        {
            return Unregister(type->GetTypeCode());
        }
        Object* ActivateInstance(TypeCode type_code) noexcept
        {
            auto const& iter = m_Types.find(type_code);
            if (iter != m_Types.end())
            {
                return iter->second->ActivateInstance();
            }

            return nullptr;
        }

    };
}

#define GX_DECLARE_TYPEINFO_ROOT(type, uuid) \
    public: static ::Graphyte::TypeInfo RTTI_TypeInfo; \
    public: static ::Graphyte::TypeCode RTTI_TypeCode() noexcept { return ::Graphyte::TypeCode{ uuid }; } \
    public: static void RTTI_RegisterType() noexcept; \
    public: virtual const ::Graphyte::TypeInfo* RTTI_GetTypeInfo() const noexcept { return &RTTI_TypeInfo; }

#define GX_DECLARE_TYPEINFO(type, uuid) \
    public: static ::Graphyte::TypeInfoT<type> RTTI_TypeInfo; \
    public: static ::Graphyte::TypeCode RTTI_TypeCode() noexcept { return ::Graphyte::TypeCode{ uuid }; } \
    public: static void RTTI_RegisterType() noexcept; \
    public: virtual const ::Graphyte::TypeInfo* RTTI_GetTypeInfo() const noexcept override { return &RTTI_TypeInfo; }

#define GX_DECLARE_TYPEINFO_ABSTRACT(type, uuid) \
    public: static ::Graphyte::TypeInfo RTTI_TypeInfo; \
    public: static ::Graphyte::TypeCode RTTI_TypeCode() noexcept { return ::Graphyte::TypeCode{ uuid }; } \
    public: static void RTTI_RegisterType() noexcept; \
    public: virtual const ::Graphyte::TypeInfo* RTTI_GetTypeInfo() const noexcept override { return &RTTI_TypeInfo; }

#define GX_DEFINE_TYPEINFO_ROOT(type) \
    void type::RTTI_RegisterType() noexcept { }; \
    ::Graphyte::TypeInfo type::RTTI_TypeInfo { \
        #type, \
        type::RTTI_RegisterType, \
        nullptr, \
        sizeof(type), \
        alignof(type), \
        ::Graphyte::TypeFlags::IsAbstract, \
        ::Graphyte::TypeCodeOf<type>() \
    }

#define GX_DEFINE_TYPEINFO(type, base) \
    void type::RTTI_RegisterType() noexcept { }; \
    ::Graphyte::TypeInfoT<type> type::RTTI_TypeInfo { \
        #type, \
        type::RTTI_RegisterType, \
        &base::RTTI_TypeInfo, \
        sizeof(type), \
        alignof(type), \
        ::Graphyte::TypeFlags::None, \
        ::Graphyte::TypeCodeOf<type>() \
    }

#define GX_DEFINE_TYPEINFO_ABSTRACT(type, base) \
    void type::RTTI_RegisterType() noexcept { }; \
    ::Graphyte::TypeInfo type::RTTI_TypeInfo { \
        #type, \
        type::RTTI_RegisterType, \
        &base::RTTI_TypeInfo, \
        sizeof(type), \
        alignof(type), \
        ::Graphyte::TypeFlags::IsAbstract, \
        ::Graphyte::TypeCodeOf<type>() \
    }

#define GX_DEFINE_TYPECODE(type, uuid) \
namespace Graphyte { \
    template <> __forceinline ::Graphyte::TypeCode TypeCodeOf<type>() noexcept { \
        return ::Graphyte::TypeCode{ uuid }; \
    } \
    template <> __forceinline const char* TypeNameOf<type>() noexcept { \
        return #type; \
    } \
}


GX_DEFINE_TYPECODE(float, 0xa00a62a942b20165);
GX_DEFINE_TYPECODE(double, 0xa0880a9ce131dea8);

GX_DEFINE_TYPECODE(std::uint8_t, 0x44bfbb392bb2b61d);
GX_DEFINE_TYPECODE(std::uint16_t, 0xe9938e6de34a7c0e);
GX_DEFINE_TYPECODE(std::uint32_t, 0xa197f65ee4adee28);
GX_DEFINE_TYPECODE(std::uint64_t, 0x47d46876a53138eb);
GX_DEFINE_TYPECODE(std::int8_t, 0x9ae377d680bf4106);
GX_DEFINE_TYPECODE(std::int16_t, 0xfdee9033d2037037);
GX_DEFINE_TYPECODE(std::int32_t, 0xaaf6c8228a70f741);
GX_DEFINE_TYPECODE(std::int64_t, 0xef8b360a2cee43de);

GX_DEFINE_TYPECODE(std::string, 0x2767bd747119cc57);


namespace Graphyte::Impl
{
    class ReferenceCounterTrait final
    {
    public:
        using Type = int32_t;

        static Type Increment(Type& value) noexcept
        {
            return ++value;
        }

        static Type Decrement(Type& value) noexcept
        {
            return --value;
        }
    };

    class ThreadsafeCounterTrait final
    {
    public:
        using Type = std::atomic<int32_t>;

        static Type Increment(Type& value) noexcept
        {
            return ++value;
        }

        static Type Decrement(Type& value) noexcept
        {
            return --value;
        }
    };
}

namespace Graphyte
{
    using DefaultReferenceCountingTrait = Impl::ReferenceCounterTrait;

    class BASE_API Object
    {
        GX_DECLARE_TYPEINFO_ROOT(Object, 0xdeadc0de00000000);
    private:
        DefaultReferenceCountingTrait::Type m_ReferenceCount = DefaultReferenceCountingTrait::Type{ 0 };

    public:
        Object() noexcept = default;
        virtual ~Object() noexcept {};

    public:
        __forceinline DefaultReferenceCountingTrait::Type AddRef() noexcept
        {
            return DefaultReferenceCountingTrait::Increment(this->m_ReferenceCount);
        }

        inline DefaultReferenceCountingTrait::Type ReleaseRef() noexcept
        {
            if (DefaultReferenceCountingTrait::Decrement(this->m_ReferenceCount) == 0)
            {
                delete this;
                return 0;
            }

            return this->m_ReferenceCount;
        }

        __forceinline DefaultReferenceCountingTrait::Type GetReferenceCount() noexcept
        {
            return this->m_ReferenceCount;
        }
    };
}


namespace Graphyte
{
    template <typename T>
    class Reference final
    {
    public:
        using ReferencedType = T;

        // Be friend with other type references.
        template <typename U> friend class Reference;

    private:
        ReferencedType* m_Reference = nullptr;

    private:
        void InternalAddRef() const noexcept
        {
            if (this->m_Reference != nullptr)
            {
                this->m_Reference->AddRef();
            }
        }

        DefaultReferenceCountingTrait::Type InternalReleaseRef() noexcept
        {
            DefaultReferenceCountingTrait::Type ref_count{ 0 };

            auto* temp = this->m_Reference;

            if (temp != nullptr)
            {
                this->m_Reference = nullptr;
                ref_count = temp->ReleaseRef();
            }

            return ref_count;
        }

        void Swap(Reference<T>&& other) noexcept
        {
            auto* temp = this->m_Reference;
            this->m_Reference = other.m_Reference;
            other.m_Reference = temp;
        }

        void Swap(Reference<T>& other) noexcept
        {
            auto* temp = this->m_Reference;
            this->m_Reference = other.m_Reference;
            other.m_Reference = temp;
        }

    public:
        Reference() noexcept
            : m_Reference{ nullptr }
        {
        }
        Reference(std::nullptr_t) noexcept
            : m_Reference{ nullptr }
        {
        }

        template <typename U>
        Reference(U* other) noexcept
            : m_Reference{ other }
        {
            this->InternalAddRef();
        }

        Reference(const Reference<T>& other) noexcept
            : m_Reference{ other.m_Reference }
        {
            this->InternalAddRef();
        }

        template <typename U>
        Reference(const Reference<U>& other, typename std::enable_if<std::is_convertible<U*, T*>::value, void*>::type* = nullptr) noexcept
            : m_Reference{ other.m_Reference }
        {
            this->InternalAddRef();
        }

        Reference(Reference<T>&& other) noexcept
            : m_Reference{ nullptr }
        {
            if (this != reinterpret_cast<Reference*>(&reinterpret_cast<uint8_t&>(other)))
            {
                this->Swap(other);
            }
        }

        template <typename U>
        Reference(Reference<U>&& other, typename std::enable_if<std::is_convertible<U*, T*>::value, void*>::type* = nullptr) noexcept
            : m_Reference{ other.m_Reference }
        {
            other.m_Reference = nullptr;
        }

    public:
        ~Reference() noexcept
        {
            this->InternalReleaseRef();
        }

    public:
        Reference & operator = (std::nullptr_t) noexcept
        {
            this->InternalReleaseRef();
            return (*this);
        }

        Reference& operator = (T* other) noexcept
        {
            if (this->m_Reference != other)
            {
                Reference(other).Swap(*this);
            }

            return (*this);
        }

        template <typename U>
        Reference& operator = (U* other) noexcept
        {
            Reference(other).Swap(*this);
            return (*this);
        }

        Reference& operator = (const Reference<T>& other) noexcept
        {
            if (this->m_Reference != other.m_Reference)
            {
                Reference(other).Swap(*this);
            }

            return (*this);
        }

        template <typename U>
        Reference& operator = (const Reference<U>& other) noexcept
        {
            Reference(other).Swap(*this);
            return (*this);
        }

        Reference& operator = (Reference<T>&& other) noexcept
        {
            Reference(static_cast<Reference&&>(other)).Swap(*this);
            return (*this);
        }

        template <typename U>
        Reference& operator = (Reference<U>&& other) noexcept
        {
            Reference(static_cast<Reference<U>&&>(other)).Swap(*this);
            return (*this);
        }

    public:
        explicit operator bool() const noexcept
        {
            return this->m_Reference != nullptr;
        }

    public:
        operator T* () const noexcept
        {
            return this->m_Reference;
        }

        operator const T* () const noexcept
        {
            return this->m_Reference;
        }

        T& operator * () const noexcept
        {
            return *this->m_Reference;
        }

        T** operator & () noexcept
        {
            return &this->m_Reference;
        }

        T* operator -> () const noexcept
        {
            return this->m_Reference;
        }

    public:
        T * Get() const noexcept
        {
            return this->m_Reference;
        }

        T* const* GetAddressOf() const noexcept
        {
            return &this->m_Reference;
        }

        T** GetAddressOf() noexcept
        {
            return &this->m_Reference;
        }

        T** ReleaseAndGetAddressOf() noexcept
        {
            this->InternalReleaseRef();
            return &this->m_Reference;
        }

        T* Detach() noexcept
        {
            auto* reference = this->m_Reference;
            this->m_Reference = nullptr;
            return reference;
        }

        void Attach(T* other) noexcept
        {
            if (this->m_Reference != nullptr)
            {
                [[maybe_unused]] auto refcount = this->m_Reference->ReleaseRef();
                GX_ASSERT(refcount != 0 || this->m_Reference != other);
            }

            this->m_Reference = other;
        }

        DefaultReferenceCountingTrait::Type reset() noexcept
        {
            return this->InternalReleaseRef();
        }

        bool CopyTo(T** reference) const noexcept
        {
            if (reference != nullptr)
            {
                this->InternalAddRef();
                (*reference) = this->m_Reference;
                return true;
            }

            return false;
        }

        bool IsValid() const noexcept
        {
            return this->m_Reference != nullptr;
        }

        template <typename U>
        bool As(Reference<U>& result) noexcept
        {
            result = static_cast<U*>(this->m_Reference);
            return true;
        }
    };

    template <typename T, typename U>
    inline bool operator == (const Reference<T>& l, const Reference<U>& r) noexcept
    {
        static_assert(std::is_base_of<T, U>::value || std::is_base_of<U, T>::value, "T and U pointers must be compatible");
        return l.Get() == r.Get();
    }

    template <typename T>
    inline bool operator == (const Reference<T>& l, std::nullptr_t) noexcept
    {
        return l.Get() == nullptr;
    }

    template <typename T>
    inline bool operator == (std::nullptr_t, const Reference<T>& r) noexcept
    {
        return nullptr == r.Get();
    }

    template <typename T, typename U>
    inline bool operator != (const Reference<T>& l, const Reference<U>& r) noexcept
    {
        static_assert(std::is_base_of<T, U>::value || std::is_base_of<U, T>::value, "T and U pointers must be compatible");
        return l.Get() != r.Get();
    }

    template <typename T>
    inline bool operator != (const Reference<T>& l, std::nullptr_t) noexcept
    {
        return l.Get() != nullptr;
    }

    template <typename T>
    inline bool operator != (std::nullptr_t, const Reference<T>& r) noexcept
    {
        return nullptr != r.Get();
    }

    template <typename T, typename U>
    inline bool operator > (const Reference<T>& l, const Reference<U>& r) noexcept
    {
    static_assert(std::is_base_of<T, U>::value || std::is_base_of<U, T>::value, "T and U pointers must be compatible");
    return l.Get() > r.Get();
    }

    template <typename T>
    inline bool operator > (const Reference<T>& l, std::nullptr_t) noexcept
    {
        return l.Get() > nullptr;
    }

    template <typename T>
    inline bool operator > (std::nullptr_t, const Reference<T>& r) noexcept
    {
        return nullptr > r.Get();
    }

    template <typename T, typename U>
    inline bool operator >= (const Reference<T>& l, const Reference<U>& r) noexcept
    {
        static_assert(std::is_base_of<T, U>::value || std::is_base_of<U, T>::value, "T and U pointers must be compatible");
        return l.Get() >= r.Get();
    }

    template <typename T>
    inline bool operator >= (const Reference<T>& l, std::nullptr_t) noexcept
    {
        return l.Get() >= nullptr;
    }

    template <typename T>
    inline bool operator >= (std::nullptr_t, const Reference<T>& r) noexcept
    {
        return nullptr >= r.Get();
    }

    template <typename T, typename U>
    inline bool operator < (const Reference<T>& l, const Reference<U>& r) noexcept
    {
        static_assert(std::is_base_of<T, U>::value || std::is_base_of<U, T>::value, "T and U pointers must be compatible");
        return l.Get() < r.Get();
    }

    template <typename T>
    inline bool operator < (const Reference<T>& l, std::nullptr_t) noexcept
    {
        return l.Get() < nullptr;
    }

    template <typename T>
    inline bool operator < (std::nullptr_t, const Reference<T>& r) noexcept
    {
        return nullptr < r.Get();
    }

    template <typename T, typename U>
    inline bool operator <= (const Reference<T>& l, const Reference<U>& r) noexcept
    {
        static_assert(std::is_base_of<T, U>::value || std::is_base_of<U, T>::value, "T and U pointers must be compatible");
        return l.Get() <= r.Get();
    }

    template <typename T>
    inline bool operator <= (const Reference<T>& l, std::nullptr_t) noexcept
    {
        return l.Get() <= nullptr;
    }

    template <typename T>
    inline bool operator <= (std::nullptr_t, const Reference<T>& r) noexcept
    {
        return nullptr <= r.Get();
    }

    template <typename T, typename ...TArgs>
    inline Reference<T> MakeRef(TArgs&&... args) noexcept
    {
        return Reference<T>{ new T(std::forward<TArgs>(args)...) };
    }
}


namespace Graphyte
{
    template <typename T, typename S>
    __forceinline T* TypeCast(S* pointer) noexcept
    {
         return TypeCastImpl<T, S>(pointer);
    }

    template <typename T, typename S>
    __forceinline Reference<T> TypeCast(const Reference<S>& pointer) noexcept
    {
        return TypeCastImpl<T, S>(pointer.Get());
    }
}
