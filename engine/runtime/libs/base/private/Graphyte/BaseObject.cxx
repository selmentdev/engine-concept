#include "Base.pch.hxx"
#include <Graphyte/BaseObject.hxx>

GX_DEFINE_TYPEINFO_ROOT(Graphyte::Object);

namespace Graphyte
{
    bool TypeInfo::IsDerived(
        const TypeInfo* type
    ) const noexcept
    {
        auto const* iter = this;

        while (iter != nullptr)
        {
            if (iter->IsExact(type))
            {
                return true;
            }

            iter = iter->GetBaseType();
        }

        return false;
    }

    bool TypeInfo::IsDerived(
        std::string_view name
    ) const noexcept
    {
        auto const* iter = this;

        while (iter != nullptr)
        {
            if (iter->IsExact(name))
            {
                return true;
            }

            iter = iter->GetBaseType();
        }

        return false;
    }

    bool TypeInfo::IsDerived(
        TypeCode type_uuid
    ) const noexcept
    {
        auto const* iter = this;

        while (iter != nullptr)
        {
            if (iter->IsExact(type_uuid))
            {
                return true;
            }

            iter = iter->GetBaseType();
        }

        return false;
    }
}
