#include "Base.pch.hxx"
#include "Json.DataSerializer.hxx"

namespace Graphyte::Serialization
{
    JsonDataWriter::JsonDataWriter(Storage::Archive& archive) noexcept
        : m_Archive{ archive }
        , m_Buffer{}
        , m_Writer{ m_Buffer }
    {
    }

    JsonDataWriter::~JsonDataWriter() noexcept = default;

    bool JsonDataWriter::StartObject(uint32_t members) noexcept
    {
        (void)members;
        return m_Writer.StartObject();
    }

    bool JsonDataWriter::EndObject() noexcept
    {
        return m_Writer.EndObject();
    }

    bool JsonDataWriter::StartArray(uint32_t members) noexcept
    {
        (void)members;
        return m_Writer.StartArray();
    }

    bool JsonDataWriter::EndArray() noexcept
    {
        return m_Writer.EndArray();
    }

    bool JsonDataWriter::WriteBoolean(bool value) noexcept
    {
        return m_Writer.Bool(value);
    }

    bool JsonDataWriter::WriteUInt(uint64_t value) noexcept
    {
        return m_Writer.Uint64(value);
    }

    bool JsonDataWriter::WriteInt(int64_t value) noexcept
    {
        return m_Writer.Int64(value);
    }

    bool JsonDataWriter::WriteString(std::string_view value) noexcept
    {
        return m_Writer.String(
            value.data(),
            static_cast<rapidjson::SizeType>(value.size())
        );
    }

    bool JsonDataWriter::WriteDouble(double value) noexcept
    {
        return m_Writer.Double(value);
    }

    bool JsonDataWriter::WriteFloat(float value) noexcept
    {
        return m_Writer.Double(value);
    }

    bool JsonDataWriter::WriteNull() noexcept
    {
        return m_Writer.Null();
    }

    bool JsonDataWriter::Close() noexcept
    {
        m_Archive.Serialize(
            const_cast<char*>(m_Buffer.GetString()), m_Buffer.GetSize()
        );

        return !m_Archive.IsError();
    }
}

namespace Graphyte::Serialization
{
    JsonDataReader::JsonDataReader(Storage::Archive& archive) noexcept
        : m_Handler{}
        , m_Reader{}
        , m_Buffer{ archive }
    {
        m_Reader.IterativeParseInit();
    }

    JsonDataReader::~JsonDataReader() noexcept
    {
    }

    bool JsonDataReader::ReadNext() noexcept
    {
        return m_Reader.IterativeParseNext<rapidjson::kParseDefaultFlags | rapidjson::kParseCommentsFlag>(
            m_Buffer,
            m_Handler
        );
    }

    bool JsonDataReader::StartObject(uint32_t& members) noexcept
    {
        members = 0;

        if (ReadNext())
        {
            return this->m_Handler.Type == DataType::Object;
        }

        return false;
    }

    bool JsonDataReader::EndObject() noexcept
    {
        if (ReadNext())
        {
            return this->m_Handler.Type == DataType::EndObject;
        }

        return false;
    }

    bool JsonDataReader::StartArray(uint32_t& members) noexcept
    {
        members = 0;

        if (ReadNext())
        {
            return this->m_Handler.Type == DataType::Array;
        }

        return false;
    }

    bool JsonDataReader::EndArray() noexcept
    {
        if (ReadNext())
        {
            return this->m_Handler.Type == DataType::EndArray;
        }

        return false;
    }

    bool JsonDataReader::ReadBoolean(bool& value) noexcept
    {
        if (ReadNext())
        {
            if (this->m_Handler.Type == DataType::Boolean)
            {
                value = this->m_Handler.AsBoolean;
                return true;
            }
        }

        return false;
    }

    bool JsonDataReader::ReadUInt(uint64_t& value) noexcept
    {
        if (ReadNext())
        {
            switch (this->m_Handler.Type)
            {
                case DataType::Int64:
                    {
                        value = static_cast<uint64_t>(this->m_Handler.AsInt64);
                        break;
                    }
                case DataType::UInt64:
                    {
                        value = static_cast<uint64_t>(this->m_Handler.AsUInt64);
                        break;
                    }
                case DataType::Double:
                    {
                        value = static_cast<uint64_t>(this->m_Handler.AsDouble);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        return false;
    }

    bool JsonDataReader::ReadInt(int64_t& value) noexcept
    {
        if (ReadNext())
        {
            switch (this->m_Handler.Type)
            {
                case DataType::Int64:
                    {
                        value = static_cast<int64_t>(this->m_Handler.AsInt64);
                        break;
                    }
                case DataType::UInt64:
                    {
                        value = static_cast<int64_t>(this->m_Handler.AsUInt64);
                        break;
                    }
                case DataType::Double:
                    {
                        value = static_cast<int64_t>(this->m_Handler.AsDouble);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        return false;
    }

    bool JsonDataReader::ReadString(std::string& value) noexcept
    {
        if (ReadNext())
        {
            if (this->m_Handler.Type == DataType::String)
            {
                value = this->m_Handler.AsString;
                return true;
            }
        }

        return false;
    }

    bool JsonDataReader::ReadDouble(double& value) noexcept
    {
        if (ReadNext())
        {
            switch (this->m_Handler.Type)
            {
                case DataType::Int64:
                    {
                        value = static_cast<double>(this->m_Handler.AsInt64);
                        break;
                    }
                case DataType::UInt64:
                    {
                        value = static_cast<double>(this->m_Handler.AsUInt64);
                        break;
                    }
                case DataType::Double:
                    {
                        value = static_cast<double>(this->m_Handler.AsDouble);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        return false;
    }

    bool JsonDataReader::ReadFloat(float& value) noexcept
    {
        if (ReadNext())
        {
            switch (this->m_Handler.Type)
            {
                case DataType::Int64:
                    {
                        value = static_cast<float>(this->m_Handler.AsInt64);
                        break;
                    }
                case DataType::UInt64:
                    {
                        value = static_cast<float>(this->m_Handler.AsUInt64);
                        break;
                    }
                case DataType::Double:
                    {
                        value = static_cast<float>(this->m_Handler.AsDouble);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        return false;
    }

    bool JsonDataReader::Close() noexcept
    {
        return m_Reader.IterativeParseComplete();
    }
}

namespace Graphyte::Serialization
{
    JsonDataReader2::JsonDataReader2(Storage::Archive& archive) noexcept
        : m_Handler{}
        , m_Reader{}
        , m_Buffer{ archive }
    {
        m_Reader.IterativeParseInit();
    }

    JsonDataReader2::~JsonDataReader2() noexcept
    {
    }

    bool JsonDataReader2::ReadNext() noexcept
    {
        if (!m_Reader.IterativeParseComplete())
        {
            return m_Reader.IterativeParseNext<rapidjson::kParseDefaultFlags | rapidjson::kParseCommentsFlag>(
                m_Buffer,
                m_Handler
                );
        }

        //
        // Json file has been fully parsed.
        //

        return false;
    }

    bool JsonDataReader2::Close() noexcept
    {
        if (m_Reader.IterativeParseComplete())
        {
            GX_ASSERTF(this->m_Handler.m_Stack.empty(), "Parsing stack is not empty");
            return true;
        }

        return false;
    }

    bool JsonDataReader2::GetInt32(int32_t& result) const noexcept
    {
        if (this->m_Handler.Type == DataType::Int32)
        {
            result = this->m_Handler.AsInt32;
            return true;
        }

        return false;
    }

    bool JsonDataReader2::GetUInt32(uint32_t& result) const noexcept
    {
        if (this->m_Handler.Type == DataType::UInt32)
        {
            result = this->m_Handler.AsUInt32;
            return true;
        }

        return false;
    }

    bool JsonDataReader2::GetInt64(int64_t& result) const noexcept
    {
        if (this->m_Handler.Type == DataType::Int64)
        {
            result = this->m_Handler.AsInt64;
            return true;
        }

        return false;
    }

    bool JsonDataReader2::GetUInt64(uint64_t& result) const noexcept
    {
        if (this->m_Handler.Type == DataType::UInt64)
        {
            result = this->m_Handler.AsUInt64;
            return true;
        }

        return false;
    }

    bool JsonDataReader2::GetFloat(float& result) const noexcept
    {
        if (this->m_Handler.Type == DataType::Double)
        {
            result = static_cast<float>(this->m_Handler.AsDouble);
            return true;
        }

        return false;
    }

    bool JsonDataReader2::GetDouble(double& result) const noexcept
    {
        if (this->m_Handler.Type == DataType::Double)
        {
            result = this->m_Handler.AsDouble;
            return true;
        }

        return false;
    }

    bool JsonDataReader2::GetString(std::string& result) const noexcept
    {
        if (this->m_Handler.Type == DataType::String)
        {
            result = this->m_Handler.AsString;
            return true;
        }

        return false;
    }

    bool JsonDataReader2::GetBoolean(bool& result) const noexcept
    {
        if (this->m_Handler.Type == DataType::Boolean)
        {
            result = this->m_Handler.AsBoolean;
            return true;
        }

        return false;
    }

    bool JsonDataReader2::IsValid() const noexcept
    {
        return this->m_Handler.Type != DataType::Invalid;
    }

    bool JsonDataReader2::IsObject() const noexcept
    {
        return this->m_Handler.Type == DataType::Object;
    }

    bool JsonDataReader2::IsEndObject() const noexcept
    {
        return this->m_Handler.Type == DataType::EndObject;
    }

    bool JsonDataReader2::IsArray() const noexcept
    {
        return this->m_Handler.Type == DataType::Array;
    }

    bool JsonDataReader2::IsEndArray() const noexcept
    {
        return this->m_Handler.Type == DataType::EndArray;
    }

    bool JsonDataReader2::IsInt32() const noexcept
    {
        return this->m_Handler.Type == DataType::Int32;
    }

    bool JsonDataReader2::IsUInt32() const noexcept
    {
        return this->m_Handler.Type == DataType::UInt32;
    }

    bool JsonDataReader2::IsInt64() const noexcept
    {
        return this->m_Handler.Type == DataType::Int64;
    }

    bool JsonDataReader2::IsUInt64() const noexcept
    {
        return this->m_Handler.Type == DataType::UInt64;
    }

    bool JsonDataReader2::IsFloat() const noexcept
    {
        return this->m_Handler.Type == DataType::Float;
    }

    bool JsonDataReader2::IsDouble() const noexcept
    {
        return this->m_Handler.Type == DataType::Double;
    }

    bool JsonDataReader2::IsString() const noexcept
    {
        return this->m_Handler.Type == DataType::String;
    }

    bool JsonDataReader2::IsBoolean() const noexcept
    {
        return this->m_Handler.Type == DataType::Boolean;
    }

    bool JsonDataReader2::IsNull() const noexcept
    {
        return this->m_Handler.Type == DataType::Null;
    }
}
