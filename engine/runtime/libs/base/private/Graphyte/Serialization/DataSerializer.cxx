#include "Base.pch.hxx"
#include <Graphyte/Serialization/DataSerializer.hxx>

#include "Json.DataSerializer.hxx"
#include "MPack.DataSerializer.hxx"

namespace Graphyte::Serialization
{
    BASE_API DataReader::DataReader() noexcept = default;
    BASE_API DataReader::~DataReader() noexcept = default;

    DataReader2::DataReader2() noexcept = default;
    DataReader2::~DataReader2() noexcept = default;

    BASE_API DataWriter::DataWriter() noexcept = default;
    BASE_API DataWriter::~DataWriter() noexcept = default;
}

namespace Graphyte::Serialization
{
    BASE_API std::unique_ptr<DataReader> CreateReader(DataFormat format, Storage::Archive& archive) noexcept
    {
        switch (format)
        {
            case DataFormat::Json:
                {
                    return std::make_unique<JsonDataReader>(archive);
                }
            default:
                {
                    GX_ASSERTF(false, "Unknown data format: {}\n", static_cast<uint32_t>(format));
                    break;
                }
        }

        return nullptr;
    }

    BASE_API std::unique_ptr<DataReader2> CreateReader2(DataFormat format, Storage::Archive& archive) noexcept
    {
        switch (format)
        {
        case DataFormat::Json:
            {
                return std::make_unique<JsonDataReader2>(archive);
            }
        default:
            {
                GX_ASSERTF(false, "Unknown data format: {}\n", static_cast<uint32_t>(format));
                break;
            }
        }

        return nullptr;
    }

    BASE_API std::unique_ptr<DataWriter> CreateWriter(DataFormat format, Storage::Archive& archive) noexcept
    {
        switch (format)
        {
            case DataFormat::Json:
                {
                    return std::make_unique<JsonDataWriter>(archive);
                }
            default:
                {
                    GX_ASSERTF(false, "Unknown data format: {}\n", static_cast<uint32_t>(format));
                    break;
                }
        }

        return nullptr;
    }
}
