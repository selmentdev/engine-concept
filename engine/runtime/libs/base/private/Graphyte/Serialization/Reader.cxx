#include "Base.pch.hxx"
#include <Graphyte/Serialization/Reader.hxx>
#include <Graphyte/Diagnostics.hxx>

#include <yaml.h>

namespace Graphyte::Serialization
{
    bool Reader::Value::IsValue() const noexcept
    {
        return IsValid() && m_Node->type == YAML_SCALAR_NODE;
    }

    bool Reader::Value::IsArray() const noexcept
    {
        return IsValid() && m_Node->type == YAML_SEQUENCE_NODE;
    }

    bool Reader::Value::IsObject() const noexcept
    {
        return IsValid() && m_Node->type == YAML_MAPPING_NODE;
    }

    bool Reader::Value::GetValue(
        std::string_view& value
    ) const noexcept
    {
        bool valid = IsValue();
        GX_ASSERT(valid);

        if (valid)
        {
            value = std::string_view{
                reinterpret_cast<const char*>(m_Node->data.scalar.value),
                m_Node->data.scalar.length
            };
        }

        return valid;
    }

    size_t Reader::Value::GetArraySize() const noexcept
    {
        bool valid = IsArray();
        GX_ASSERT(valid);

        if (valid)
        {
            size_t count = static_cast<size_t>(m_Node->data.sequence.items.top - m_Node->data.sequence.items.start);
            return count;
        }

        return 0;
    }

    Reader::Value Reader::Value::GetArrayElement(
        size_t index
    ) const noexcept
    {
        bool valid = IsArray();
        GX_ASSERT(valid);

        Reader::Value result{};

        if (valid)
        {
            auto const* item = m_Node->data.sequence.items.start + index;
            if (item < m_Node->data.sequence.items.top)
            {
                result.m_Document = m_Document;
                result.m_Node = yaml_document_get_node(m_Document, *item);
            }
        }

        return result;
    }

    size_t Reader::Value::GetMemberCount() const noexcept
    {
        bool valid = IsObject();
        GX_ASSERT(valid);

        if (valid)
        {
            size_t count = static_cast<size_t>(m_Node->data.mapping.pairs.top - m_Node->data.mapping.pairs.start);
            return count;
        }

        return 0;
    }

    std::pair<Reader::Value, Reader::Value> Reader::Value::GetMember(
        size_t index
    ) const noexcept
    {
        bool valid = IsObject();
        GX_ASSERT(valid);

        std::pair<Reader::Value, Reader::Value> result{};

        if (valid)
        {
            auto const* pair = m_Node->data.mapping.pairs.start + index;
            if (pair < m_Node->data.mapping.pairs.top)
            {
                result.first.m_Document = m_Document;
                result.first.m_Node = yaml_document_get_node(m_Document, pair->key);
                result.second.m_Document = m_Document;
                result.second.m_Node = yaml_document_get_node(m_Document, pair->value);
            }
        }

        return result;
    }

    std::pair<Reader::Value, Reader::Value> Reader::Value::FindMember(
        std::string_view key
    ) const noexcept
    {
        bool valid = IsObject();
        GX_ASSERT(valid);

        std::pair<Reader::Value, Reader::Value> result{};

        if (valid)
        {
            auto const& collection = m_Node->data.mapping.pairs;

            for (auto const* item = collection.start; item < collection.top; ++item)
            {
                auto* key_node = yaml_document_get_node(m_Document, item->key);

                std::string_view item_key{
                    reinterpret_cast<const char*>(key_node->data.scalar.value),
                    key_node->data.scalar.length
                };

                if (item_key == key)
                {
                    auto* value_node = yaml_document_get_node(m_Document, item->value);

                    result.first.m_Document = m_Document;
                    result.first.m_Node = key_node;
                    result.second.m_Document = m_Document;
                    result.second.m_Node = value_node;
                }
            }
        }

        return result;
    }

    Reader::Reader(
        std::string_view content
    ) noexcept
        : m_Document{}
    {
        yaml_parser_t parser;
        if (yaml_parser_initialize(&parser) == 1)
        {
            yaml_parser_set_encoding(&parser, YAML_UTF8_ENCODING);
            yaml_parser_set_input_string(&parser, reinterpret_cast<const yaml_char_t*>(content.data()), content.size());

            auto document = std::make_unique<yaml_document_s>();
            if (yaml_parser_load(&parser, document.get()) == 1)
            {
                m_Document = std::move(document);
            }

            yaml_parser_delete(&parser);
        }
    }

    Reader::~Reader() noexcept
    {
        if (m_Document != nullptr)
        {
            yaml_document_delete(m_Document.get());
        }
    }

    Reader::Value Reader::GetRoot() const noexcept
    {
        Reader::Value result{};
        if (m_Document != nullptr)
        {
            result.m_Document = m_Document.get();
            result.m_Node = yaml_document_get_root_node(result.m_Document);
        }

        return result;
    }
}
