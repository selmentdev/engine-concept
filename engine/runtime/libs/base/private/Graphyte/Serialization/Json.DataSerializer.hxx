#pragma once
#include <Graphyte/Serialization/DataSerializer.hxx>

#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/reader.h>

namespace Graphyte::Serialization
{
    class JsonDataWriter : public DataWriter
    {
    private:
        Storage::Archive& m_Archive;
        rapidjson::StringBuffer m_Buffer;
        rapidjson::PrettyWriter<rapidjson::StringBuffer> m_Writer;

    public:
        JsonDataWriter(Storage::Archive& archive) noexcept;
        virtual ~JsonDataWriter() noexcept;

    public:
        virtual bool StartObject(uint32_t members) noexcept final override;
        virtual bool EndObject() noexcept final override;

        virtual bool StartArray(uint32_t members) noexcept final override;
        virtual bool EndArray() noexcept final override;

        virtual bool WriteBoolean(bool value) noexcept final override;
        virtual bool WriteUInt(uint64_t value) noexcept final override;
        virtual bool WriteInt(int64_t value) noexcept final override;
        virtual bool WriteString(std::string_view value) noexcept final override;
        virtual bool WriteDouble(double value) noexcept final override;
        virtual bool WriteFloat(float value) noexcept final override;
        virtual bool WriteNull() noexcept final override;
        virtual bool Close() noexcept final override;
    };
}

namespace Graphyte::Serialization::Impl
{
    class RapidjsonArchiveWrapper
    {
    private:
        Graphyte::Storage::Archive& m_Archive;
        char m_Peek;
        bool m_HasPeek;

    public:
        RapidjsonArchiveWrapper(Graphyte::Storage::Archive& archive)
            : m_Archive{ archive }
            , m_Peek{ '\0' }
            , m_HasPeek{ false }
        {
        }

        using Ch = char;

        //! Read the current character from stream without moving the read cursor.
        Ch Peek()
        {
            if (!m_HasPeek)
            {
                m_Archive.Serialize(&m_Peek, sizeof(m_Peek));
                m_HasPeek = true;

                if (m_Archive.IsError())
                {
                    m_Peek = '\0';
                }
            }

            return m_Peek;
        }
        //! Read the current character from stream and moving the read cursor to next character.
        Ch Take()
        {
            if (m_HasPeek)
            {
                m_HasPeek = false;
            }
            else
            {
                m_Archive.Serialize(&m_Peek, sizeof(m_Peek));

                if (m_Archive.IsError())
                {
                    m_Peek = '\0';
                }
            }

            return m_Peek;
        }

        //! Get the current read cursor.
        //! \return Number of characters read from start.
        size_t Tell()
        {
            return static_cast<size_t>(m_Archive.GetPosition());
        }
        //! Begin writing operation at the current read pointer.
        //! \return The begin writer pointer.
        Ch* PutBegin()
        {
            GX_ASSERT(false);
            return nullptr;
        }
        //! Write a character.
        void Put(Ch c)
        {
            GX_ASSERT(false);
            (void)c;
        }

        //! Flush the buffer.
        void Flush()
        {
            GX_ASSERT(false);
        }
        //! End the writing operation.
        //! \param begin The begin write pointer returned by PutBegin().
        //! \return Number of characters written.
        size_t PutEnd(Ch* begin)
        {
            (void)begin;
            GX_ASSERT(false);
            return 0;
        }
    };

    struct JsonSaxHandler
    {
        std::string AsString{};
        double AsDouble{};
        uint64_t AsUInt64{};
        int64_t AsInt64{};
        uint32_t AsUInt32{};
        int32_t AsInt32{};
        bool AsBoolean{};
        DataType Type{ DataType::Invalid };
        std::vector<DataType> m_Stack{};

        bool Null()
        {
            Type = DataType::Null;
            return true;
        }

        bool Bool(bool value)
        {
            Type = DataType::Boolean;
            AsBoolean = value;
            return true;
        }

        bool Uint(unsigned value)
        {
            Type = DataType::UInt32;
            AsUInt32 = value;
            return true;
        }

        bool Uint64(uint64_t value)
        {
            Type = DataType::UInt64;
            AsUInt64 = value;
            return true;
        }

        bool Int(int value)
        {
            Type = DataType::Int32;
            AsInt32 = value;
            return true;
        }

        bool Int64(int64_t value)
        {
            Type = DataType::Int64;
            AsInt64 = value;
            return true;
        }

        bool Double(double value)
        {
            Type = DataType::Double;
            AsDouble = value;
            return true;
        }

        bool RawNumber(const char*, rapidjson::SizeType, bool)
        {
            GX_ASSERT(false);
            return false;
        }

        bool String(const char* str, rapidjson::SizeType size, bool)
        {
            Type = DataType::String;
            AsString.assign(str, static_cast<size_t>(size));
            return true;
        }

        bool StartObject()
        {
            Type = DataType::Object;
            m_Stack.push_back(this->Type);
            return true;
        }

        bool Key(const char* str, rapidjson::SizeType size, bool)
        {
            Type = DataType::String;
            AsString.assign(str, static_cast<size_t>(size));
            return true;
        }

        bool EndObject(rapidjson::SizeType)
        {
            Type = DataType::EndObject;
            m_Stack.pop_back();
            return true;
        }

        bool StartArray()
        {
            Type = DataType::Array;
            m_Stack.push_back(this->Type);
            return true;
        }

        bool EndArray(rapidjson::SizeType)
        {
            Type = DataType::EndArray;
            m_Stack.pop_back();
            return true;
        }
    };
}

namespace Graphyte::Serialization
{
    class JsonDataReader : public DataReader
    {
    private:
        Impl::JsonSaxHandler m_Handler;
        rapidjson::Reader m_Reader;
        Impl::RapidjsonArchiveWrapper m_Buffer;

    private:
        bool ReadNext() noexcept;

    public:
        JsonDataReader(Storage::Archive& archive) noexcept;
        virtual ~JsonDataReader() noexcept;

    public:
        virtual bool StartObject(uint32_t& members) noexcept final override;
        virtual bool EndObject() noexcept final override;

        virtual bool StartArray(uint32_t& members) noexcept final override;
        virtual bool EndArray() noexcept final override;

        virtual bool ReadBoolean(bool& value) noexcept final override;
        virtual bool ReadUInt(uint64_t& value) noexcept final override;
        virtual bool ReadInt(int64_t& value) noexcept final override;
        virtual bool ReadString(std::string& value) noexcept final override;
        virtual bool ReadDouble(double& value) noexcept final override;
        virtual bool ReadFloat(float& value) noexcept final override;
        virtual bool Close() noexcept final override;
    };
}

namespace Graphyte::Serialization
{
    class JsonDataReader2 : public DataReader2
    {
    private:
        Impl::JsonSaxHandler m_Handler;
        rapidjson::Reader m_Reader;
        Impl::RapidjsonArchiveWrapper m_Buffer;

    public:
        JsonDataReader2(Storage::Archive& archive) noexcept;
        virtual ~JsonDataReader2() noexcept;

    public:
        virtual bool ReadNext() noexcept final override;
        virtual bool Close() noexcept final override;

    public:
        virtual bool GetInt32(int32_t& result) const noexcept final override;
        virtual bool GetUInt32(uint32_t& result) const noexcept final override;
        virtual bool GetInt64(int64_t& result) const noexcept final override;
        virtual bool GetUInt64(uint64_t& result) const noexcept final override;
        virtual bool GetFloat(float& result) const noexcept final override;
        virtual bool GetDouble(double& result) const noexcept final override;
        virtual bool GetString(std::string& result) const noexcept final override;
        virtual bool GetBoolean(bool& result) const noexcept final override;

    public:
        virtual bool IsValid() const noexcept final override;
        virtual bool IsObject() const noexcept final override;
        virtual bool IsEndObject() const noexcept final override;
        virtual bool IsArray() const noexcept final override;
        virtual bool IsEndArray() const noexcept final override;
        virtual bool IsInt32() const noexcept final override;
        virtual bool IsUInt32() const noexcept final override;
        virtual bool IsInt64() const noexcept final override;
        virtual bool IsUInt64() const noexcept final override;
        virtual bool IsFloat() const noexcept final override;
        virtual bool IsDouble() const noexcept final override;
        virtual bool IsString() const noexcept final override;
        virtual bool IsBoolean() const noexcept final override;
        virtual bool IsNull() const noexcept final override;
    };
}
