#include "Base.pch.hxx"
#include <Graphyte/Serialization/Writer.hxx>
#include <Graphyte/Diagnostics.hxx>

#include <yaml.h>

namespace Graphyte::Serialization
{
    bool Writer::Value::Key(
        std::string_view name,
        bool quote
    ) noexcept
    {
        bool valid = IsValid();
        GX_ASSERT(valid);

        if (valid)
        {
            yaml_event_t e;
            valid = (yaml_scalar_event_initialize(
                &e,
                nullptr,
                nullptr,
                reinterpret_cast<yaml_char_t*>(const_cast<char*>(name.data())),
                static_cast<int>(name.length()),
                1,
                0,
                quote ? YAML_DOUBLE_QUOTED_SCALAR_STYLE : YAML_ANY_SCALAR_STYLE
            ) == 1);

            GX_ASSERT(valid);

            if (valid)
            {
                valid = (yaml_emitter_emit(m_Emitter, &e) == 1);
            }
        }

        return valid;
    }

    bool Writer::Value::String(
        std::string_view value,
        bool quote
    ) noexcept
    {
        bool valid = IsValid();
        GX_ASSERT(valid);

        if (valid)
        {
            yaml_event_t e;
            valid = (yaml_scalar_event_initialize(
                &e,
                nullptr,
                nullptr,
                reinterpret_cast<yaml_char_t*>(const_cast<char*>(value.data())),
                static_cast<int>(value.length()),
                1,
                0,
                quote ? YAML_DOUBLE_QUOTED_SCALAR_STYLE : YAML_ANY_SCALAR_STYLE
            ) == 1);

            if (valid)
            {
                valid = (yaml_emitter_emit(m_Emitter, &e) == 1);
            }
        }

        return valid;
    }

    bool Writer::Value::KeyValue(
        std::string_view name,
        std::string_view value,
        bool quote_name,
        bool quote_value
    ) noexcept
    {
        bool valid = IsValid();
        GX_ASSERT(valid);

        if (valid)
        {
            yaml_event_t key_event;
            valid = (yaml_scalar_event_initialize(
                &key_event,
                nullptr,
                nullptr,
                reinterpret_cast<yaml_char_t*>(const_cast<char*>(name.data())),
                static_cast<int>(name.length()),
                1,
                0,
                quote_name ? YAML_DOUBLE_QUOTED_SCALAR_STYLE : YAML_ANY_SCALAR_STYLE
            ) == 1);

            if (valid)
            {
                yaml_event_t value_event;
                valid = (yaml_scalar_event_initialize(
                    &value_event,
                    nullptr,
                    nullptr,
                    reinterpret_cast<yaml_char_t*>(const_cast<char*>(value.data())),
                    static_cast<int>(value.length()),
                    1,
                    0,
                    quote_value ? YAML_DOUBLE_QUOTED_SCALAR_STYLE : YAML_ANY_SCALAR_STYLE
                ) == 1);

                if (valid)
                {
                    valid = (yaml_emitter_emit(m_Emitter, &key_event) == 1) && (yaml_emitter_emit(m_Emitter, &value_event) == 1);
                }
            }
        }

        return valid;
    }

    Writer::Value Writer::Value::StartObject(
        bool flow
    ) noexcept
    {
        bool valid = IsValid();
        GX_ASSERT(valid);

        Writer::Value result{};

        if (valid)
        {
            yaml_event_t e;
            valid = (yaml_mapping_start_event_initialize(&e, nullptr, nullptr, 0, flow ? YAML_FLOW_MAPPING_STYLE : YAML_ANY_MAPPING_STYLE) == 1);

            if (valid)
            {
                valid = (yaml_emitter_emit(m_Emitter, &e) == 1);

                if (valid)
                {
                    result.m_Emitter = m_Emitter;
                    result.m_ValueType = Writer::Value::ValueType::Object;
                }
            }
        }

        return result;
    }

    Writer::Value Writer::Value::StartArray(
        bool flow
    ) noexcept
    {
        bool valid = IsValid();
        GX_ASSERT(valid);

        Writer::Value result{};

        if (valid)
        {
            yaml_event_t e;
            valid = (yaml_sequence_start_event_initialize(&e, nullptr, nullptr, 0, flow ? YAML_FLOW_SEQUENCE_STYLE : YAML_ANY_SEQUENCE_STYLE) == 1);

            if (valid)
            {
                valid = (yaml_emitter_emit(m_Emitter, &e) == 1);

                if (valid)
                {
                    result.m_Emitter = m_Emitter;
                    result.m_ValueType = Writer::Value::ValueType::Array;
                }
            }
        }

        return result;
    }

    void Writer::Value::End() noexcept
    {
        yaml_event_t e;

        int status = 0;

        switch (m_ValueType)
        {
        case Writer::Value::ValueType::Document:
        {
            status = yaml_document_end_event_initialize(&e, 0);
            break;
        }
        case Writer::Value::ValueType::Object:
        {
            status = yaml_mapping_end_event_initialize(&e);
            break;
        }
        case Writer::Value::ValueType::Array:
        {
            status = yaml_sequence_end_event_initialize(&e);
            break;
        }
        case Writer::Value::ValueType::Stream:
        {
            status = yaml_stream_end_event_initialize(&e);
            break;
        }
        default:
        case Writer::Value::ValueType::Unknown:
        {
            break;
        }
        }

        GX_ASSERT(status == 1);

        if (status == 1)
        {
            status = yaml_emitter_emit(m_Emitter, &e);
            GX_ASSERT(status == 1);
        }

        m_Emitter = nullptr;
        m_ValueType = Writer::Value::ValueType::Unknown;
    }



    static int WriterStringWriteHandler(
        void *data,
        yaml_char_t* buffer,
        size_t size
    ) noexcept
    {
        std::string* s = reinterpret_cast<std::string*>(data);
        s->append(
            reinterpret_cast<const char*>(buffer),
            size
        );

        return 1;
    }

    Writer::Writer() noexcept
        : m_Emitter{}
        , m_Result{}
    {
        auto emitter = std::make_unique<yaml_emitter_t>();
        if (yaml_emitter_initialize(emitter.get()) == 1)
        {
            yaml_emitter_set_output(emitter.get(), WriterStringWriteHandler, &m_Result);
            yaml_emitter_set_break(emitter.get(), YAML_LN_BREAK);
            yaml_emitter_set_encoding(emitter.get(), YAML_UTF8_ENCODING);
            yaml_emitter_set_indent(emitter.get(), 2);
            yaml_emitter_open(emitter.get());

            m_Emitter = std::move(emitter);
        }
    }

    Writer::~Writer() noexcept
    {
        if (m_Emitter != nullptr)
        {
            yaml_emitter_close(m_Emitter.get());
            yaml_emitter_delete(m_Emitter.get());
        }
    }

    Writer::Value Writer::StartDocument() noexcept
    {
        bool valid = IsValid();
        GX_ASSERT(valid);

        Writer::Value result{};

        if (valid)
        {
            yaml_version_directive_t version{};
            version.major = 1;
            version.minor = 1;

            yaml_event_t e;
            valid = (yaml_document_start_event_initialize(&e, &version, nullptr, nullptr, 0) == 1);

            if (valid)
            {
                valid = (yaml_emitter_emit(m_Emitter.get(), &e) == 1);

                if (valid)
                {
                    result.m_Emitter = m_Emitter.get();
                    result.m_ValueType = Writer::Value::ValueType::Document;
                }
            }
        }

        return result;
    }

    bool Writer::ToString(
        std::string& result
    ) noexcept
    {
        bool valid = (yaml_emitter_flush(m_Emitter.get()) == 1);

        if (valid)
        {
            result = m_Result;
        }

        m_Result.clear();

        return valid;
    }
}
