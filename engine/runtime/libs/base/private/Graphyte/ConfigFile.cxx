#include "Base.pch.hxx"
#include <Graphyte/ConfigFile.hxx>
#include <Graphyte/String.hxx>
#include <Graphyte/Converter.hxx>

namespace Graphyte
{
    bool ConfigFileParser::Parse(
        std::string_view content,
        const Delegate<bool(std::string_view section, std::string_view name, std::string_view value)>& callback
    ) noexcept
    {
        std::string_view section{};

        size_t start{};

        while (true)
        {
            //
            // Match line.
            //
            start = content.find_first_not_of("\r\n", start);

            if (start == std::string_view::npos)
            {
                //
                // End of parsing.
                //
                break;
            }

            size_t end = content.find_first_of("\r\n", start);

            //
            // Get next line.
            //
            std::string_view line = content.substr(start, end - start);

            if (line.empty())
            {
                GX_ASSERT(start == std::string_view::npos);

                //
                // End of line.
                //
                break;
            }

            //
            // Move past end.
            //
            start = end;

            //
            // Remove comment from line.
            //
            auto comment = line.find(';');
            line = line.substr(0, comment);

            //
            // Trim whitespaces
            //
            line = Trim(line);

            if (!line.empty())
            {
                if (line.front() == '[' && line.back() == ']')
                {
                    //
                    // Got section.
                    //
                    line.remove_prefix(1);
                    line.remove_suffix(1);

                    section = line;
                }
                else
                {
                    //
                    // Find '=' separator.
                    //
                    auto separator = line.find('=');

                    if (separator != std::string_view::npos)
                    {
                        //
                        // Parse name = value
                        //
                        auto name = line.substr(0, separator);
                        auto value = line.substr(separator + 1);

                        //
                        // Because line is already trimmed, we trim-right name and trim-left value separately.
                        //
                        name = TrimRight(name);
                        value = TrimLeft(value);

                        //
                        // Set this up as string value.
                        //
                        if (!callback(section, name, value))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        //
                        // Line doesn't match `name=value` pattern.
                        //
                        return false;
                    }
                }
            }
        }

        return true;
    }

    bool ConfigFile::Read(
        std::string_view content
    ) noexcept
    {
#if true
        return ConfigFileParser::Parse(
            content,
            [&](std::string_view section, std::string_view name, std::string_view value) -> bool
            {
                this->SetString(section, name, value);
                return true;
            }
        );
#else
        auto section = m_Sections.end();

        size_t start{ 0 };

        while (true)
        {
            //
            // Match line.
            //
            start = content.find_first_not_of("\r\n", start);

            if (start == std::string_view::npos)
            {
                //
                // Not found anything
                //
                break;
            }

            auto end = content.find_first_of("\r\n", start);

            //
            // Get next line.
            //
            std::string_view line = content.substr(start, end - start);

            if (line.empty())
            {
                GX_ASSERT(start == std::string_view::npos);

                //
                // End of file.
                //
                break;
            }

            //
            // Move past end.
            //
            start = end;

            //
            // Remove comment from line.
            //
            auto comment = line.find(';');
            line = line.substr(0, comment);

            //
            // Trim whitespaces
            //
            line = Trim(line);

            if (!line.empty())
            {
                if (line.front() == '[' && line.back() == ']')
                {
                    //
                    // Got section.
                    //
                    line.remove_prefix(1);
                    line.remove_suffix(1);

                    section = m_Sections.insert(std::make_pair(std::string{ line }, ConfigSection{})).first;
                }
                else
                {
                    //
                    // Find '=' separator.
                    //
                    auto separator = line.find('=');

                    if (separator != std::string_view::npos)
                    {
                        //
                        // Parse name = value
                        //
                        auto name = line.substr(0, separator);
                        auto value = line.substr(separator + 1);

                        //
                        // Because line is already trimmed, we trim-right name and trim-left value separately.
                        //
                        name = TrimRight(name);
                        value = TrimLeft(value);

                        //
                        // Set this up as string value.
                        //
                        SetString(section->first, name, value);
                    }
                    else
                    {
                        //
                        // Line doesn't match `name=value` pattern.
                        //
                        return false;
                    }
                }
            }
        }

        return true;
#endif
    }

    bool ConfigFile::Write(
        std::string& content
    ) const noexcept
    {
        content.clear();

        for (auto&& section : m_Sections)
        {
            content += '[';
            content += section.first;
            content += ']';
            content += '\n';

            for (auto&& keyvalue : section.second)
            {
                content += keyvalue.first;
                content += '=';
                content += keyvalue.second;
                content += '\n';
            }

            content += '\n';
        }

        return true;
    }

    bool ConfigFile::Read(
        Storage::Archive& archive
    ) noexcept
    {
        GX_ASSERT(archive.IsLoading());

        auto const size = static_cast<size_t>(archive.GetSize());
        std::string content(size, '\0');
        archive.Serialize(std::data(content), std::size(content));

        return Read(content);
    }

    bool ConfigFile::Write(
        Storage::Archive& archive
    ) const noexcept
    {
        GX_ASSERT(archive.IsSaving());

        std::string content{};

        if (Write(content))
        {
            archive.Serialize(std::data(content), std::size(content));
            return true;
        }

        return false;
    }

    bool ConfigFile::GetString(
        std::string_view section,
        std::string_view name,
        std::string& value
    ) const noexcept
    {
        auto it_section = m_Sections.find(section);
        if (it_section != m_Sections.end())
        {
            auto&& data = it_section->second;
            auto it_name = data.find(name);
            if (it_name != data.end())
            {
                value = it_name->second;
                return true;
            }
        }

        return false;
    }

    bool ConfigFile::GetInt32(
        std::string_view section,
        std::string_view name,
        int32_t& value
    ) const noexcept
    {
        std::string string_value{};

        if (GetString(section, name, string_value))
        {
            return Converter<int32_t>::FromString(value, string_value);
        }

        return false;
    }

    bool ConfigFile::GetInt64(
        std::string_view section,
        std::string_view name,
        int64_t& value
    ) const noexcept
    {
        std::string string_value{};

        if (GetString(section, name, string_value))
        {
            return Converter<int64_t>::FromString(value, string_value);
        }

        return false;
    }

    bool ConfigFile::GetFloat(
        std::string_view section,
        std::string_view name,
        float& value
    ) const noexcept
    {
        std::string string_value{};

        if (GetString(section, name, string_value))
        {
            return Converter<float>::FromString(value, string_value);
        }

        return false;
    }

    bool ConfigFile::GetBool(
        std::string_view section,
        std::string_view name,
        bool& value
    ) const noexcept
    {
        std::string string_value{};

        if (GetString(section, name, string_value))
        {
            return Converter<bool>::FromString(value, string_value);
        }

        return false;
    }

    void ConfigFile::SetString(
        std::string_view section,
        std::string_view name,
        std::string_view value
    ) noexcept
    {
        //
        // Insert or get section with specific name.
        //
        auto&& data = m_Sections.insert(std::make_pair(std::string{ section }, NameValueContainer{})).first->second;

        //
        // Find value store for this name.
        //
        auto&& result = data.insert(std::make_pair(std::string{ name }, std::string_view{}));

        //
        // Set value for found entry.
        //
        result.first->second = value;
    }

    void ConfigFile::SetInt32(
        std::string_view section,
        std::string_view name,
        int32_t value
    ) noexcept
    {
        SetString(section, name, std::move(Converter<int32_t>::ToString(value)));
    }

    void ConfigFile::SetInt64(
        std::string_view section,
        std::string_view name,
        int64_t value
    ) noexcept
    {
        SetString(section, name, std::move(Converter<int64_t>::ToString(value)));
    }

    void ConfigFile::SetFloat(
        std::string_view section,
        std::string_view name,
        float value
    ) noexcept
    {
        SetString(section, name, std::move(Converter<float>::ToString(value)));
    }

    void ConfigFile::SetBool(
        std::string_view section,
        std::string_view name,
        bool value
    ) noexcept
    {
        SetString(section, name, std::move(Converter<bool>::ToString(value)));
    }
}
