#include "Base.pch.hxx"
#include <Graphyte/Version.hxx>
#include <Graphyte/String.hxx>

using namespace Graphyte;

BASE_API std::string Converter<Version>::ToString(
    const Version& value
) noexcept
{
    return fmt::format("{}.{}.{}.{}",
        value.Major,
        value.Minor,
        value.Patch,
        value.Build
    );
}

BASE_API bool Converter<Version>::FromString(
    Version& out_result,
    std::string_view value
) noexcept
{
    if (!value.empty())
    {
        auto* ptr_begin = std::data(value);
        auto* ptr_end = std::data(value) + std::size(value);

        uint32_t major{};

        auto[dot_1, ecc_1] = std::from_chars(ptr_begin, ptr_end, major, 10);

        if ((dot_1 == ptr_end) || (dot_1 != ptr_end && *dot_1 != '.') || ecc_1 != std::errc{})
        {
            return false;
        }

        uint32_t minor{};

        auto[dot_2, ecc_2] = std::from_chars(dot_1 + 1, ptr_end, minor, 10);

        if ((dot_2 == ptr_end) || (dot_2 != ptr_end && *dot_2 != '.') || ecc_2 != std::errc{})
        {
            return false;
        }

        uint32_t patch{};

        auto[dot_3, ecc_3] = std::from_chars(dot_2 + 1, ptr_end, patch, 10);

        if ((dot_3 == ptr_end) || (dot_3 != ptr_end && *dot_3 != '.') || ecc_3 != std::errc{})
        {
            return false;
        }

        uint32_t build{};

        auto[end_4, ecc_4] = std::from_chars(dot_3 + 1, ptr_end, build, 10);

        if (end_4 != ptr_end || ecc_4 != std::errc{})
        {
            return false;
        }

        out_result = {
            major,
            minor,
            patch,
            build
        };

        return true;
    }

    return false;
}

