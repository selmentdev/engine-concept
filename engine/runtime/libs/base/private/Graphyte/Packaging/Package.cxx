#include "Base.pch.hxx"
#include <Graphyte/Packaging/Package.hxx>
#include <Graphyte/Diagnostics.hxx>
#include <Graphyte/Flags.hxx>

namespace Graphyte::Packaging
{
    PackageFileSystem::PackageFileSystem(
        Storage::IStream* stream
    ) noexcept
        : m_Stream{ stream }
        , m_Entries{}
    {
    }

    PackageFileSystem::~PackageFileSystem() noexcept = default;

    Status PackageFileSystem::OpenRead(
        [[maybe_unused]] std::unique_ptr<Storage::IStream>& result,
        [[maybe_unused]] const std::string& path,
        [[maybe_unused]] bool share
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::OpenWrite(
        [[maybe_unused]] std::unique_ptr<Storage::IStream>& result,
        [[maybe_unused]] const std::string& path,
        [[maybe_unused]] bool append,
        [[maybe_unused]] bool share
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::IsReadonly(
        [[maybe_unused]] bool& result,
        [[maybe_unused]] const std::string& path
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::SetReadonly(
        [[maybe_unused]] const std::string& path,
        [[maybe_unused]] bool value
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::GetFileInfo(
        [[maybe_unused]] Storage::FileInfo& result,
        [[maybe_unused]] const std::string& path
    ) noexcept
    {
        PackageEntry entry{};

        result.AccessTime = result.CreationTime = result.ModificationTime = entry.CreationTime;
        result.FileSize = static_cast<int64_t>(entry.UncompressedSize);
        result.IsDirectory = Flags::Has(entry.Attributes, PackageAttributes::Directory);
        result.IsReadonly = Flags::Has(entry.Attributes, PackageAttributes::Readonly);
        result.IsValid = false;

        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::GetFileSize(
        [[maybe_unused]] int64_t& result,
        [[maybe_unused]] const std::string& path
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::FileMove(
        [[maybe_unused]] const std::string& destination,
        [[maybe_unused]] const std::string& source
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::FileDelete(
        [[maybe_unused]] const std::string& path
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::Exists(
        [[maybe_unused]] const std::string& path
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::DirectoryCreate(
        [[maybe_unused]] const std::string& path
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::DirectoryDelete(
        [[maybe_unused]] const std::string& path
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::Enumerate(
        [[maybe_unused]] const std::string& path,
        [[maybe_unused]] Storage::IDirectoryVisitor& visitor
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }

    Status PackageFileSystem::Enumerate(
        [[maybe_unused]] const std::string& path,
        [[maybe_unused]] Storage::IDirectoryInfoVisitor& visitor
    ) noexcept
    {
        GX_ASSERT_NOT_IMPLEMENTED();
        return Status::NotImplemented;
    }
}
