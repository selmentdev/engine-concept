#include "Base.pch.hxx"
#include <Graphyte/Bitwise.hxx>

namespace Graphyte
{
    static_assert(ByteSwap<uint8_t>(0x1F) == 0x1F);
    static_assert(ByteSwap<uint16_t>(0x10F0) == 0xF010);
    static_assert(ByteSwap<uint32_t>(0x1020F0E0) == 0xE0F02010);
    static_assert(ByteSwap<uint64_t>(0x10203040F0E0D0C0) == 0xC0D0E0F040302010);

    static_assert(IsAligned<uint32_t>(0x1F, 0x4) == false);
    static_assert(IsAligned<uint32_t>(0x20, 0x4) == true);
    static_assert(IsAligned<uint32_t>(0, 0x4) == true);

    static_assert(AlignUp<uint32_t>(0x1F, 0x4) == 0x20);

    static_assert(AlignDown<uint32_t>(0x1F, 0x4) == 0x1C);

    static_assert(IsPowerOf2<uint32_t>(0x1001) == false);
    static_assert(IsPowerOf2<uint32_t>(0x1000) == true);
    static_assert(IsPowerOf2<uint32_t>(0) == false);

    static_assert(CeilPowerOf2<uint32_t>(0x101) == 0x200);
    static_assert(CeilPowerOf2<uint32_t>(0x1F) == 0x20);

    static_assert(FloorPowerOf2<uint32_t>(0x101) == 0x100);
    static_assert(FloorPowerOf2<uint32_t>(0x1F) == 0x10);

    static_assert(RotateLeft<uint64_t>(0x1000, 8) == 0x100000);
    static_assert(RotateLeft<uint64_t>(0x8000000000000000, 1) == 0x1);

    static_assert(RotateRight<uint64_t>(0x1000, 56) == 0x100000);
    static_assert(RotateRight<uint64_t>(0x8000000000000000, 63) == 0x1);

    //static_assert(CountBits<uint64_t>(0x1000'0000'0000'0000) == 1);
    //static_assert(CountBits<uint32_t>(0xffff'ffff) == 32);
    //static_assert(CountBits<uint16_t>(0) == 0);
    //static_assert(CountBits<uint8_t>(0xff) == 8);
}
