#include "Base.pch.hxx"
#include <Graphyte/Diagnostics.hxx>
#include <Graphyte/Platform.hxx>
#include <Graphyte/Application.hxx>
#include <Graphyte/CommandLine.hxx>
#include <Graphyte/Storage/IFileSystem.hxx>
#include <Graphyte/Storage/Path.hxx>
#include <Graphyte/Storage/FileManager.hxx>
#include "Diagnostics.Impl.hxx"

#if GRAPHYTE_PLATFORM_POSIX
#   include <syslog.h>
#endif

#if GRAPHYTE_PLATFORM_LINUX
#   include <sys/types.h>
#   include <signal.h>
#endif

#if GRAPHYTE_PLATFORM_WINDOWS
#   include <io.h>
#endif

#if GRAPHYTE_PLATFORM_WINDOWS && GRAPHYTE_ENABLE_STACKTRACE_SYMBOLS
#include <DbgHelp.h>
#endif

namespace Graphyte::Diagnostics::Impl
{
    ErrorReporting GErrorReporting{ ErrorReporting::Interactive };

    bool GLogOutputTerminal{ false };
    bool GLogOutputDebugger{ false };
    std::unique_ptr<Storage::IStream> GLogOutputFile{};

#if GRAPHYTE_PLATFORM_WINDOWS

    HANDLE GSystemEventLog{};

#endif

    void FinishLogOutput() noexcept
    {
        //
        // Flush file log.
        //

        if (Impl::GLogOutputFile != nullptr)
        {
            [[maybe_unused]] auto status = Impl::GLogOutputFile->Flush();
            Impl::GLogOutputFile.reset();
        }


        //
        // Flush terminal output.
        //

        if (Impl::GLogOutputTerminal)
        {
            fflush(stdout);
            fflush(stderr);
        }
    }
}

namespace Graphyte::Diagnostics
{
    BASE_API void Initialize() noexcept
    {
        //
        // Check whether application has debugger attached.
        //

        Impl::GLogOutputDebugger = Diagnostics::IsDebuggerAttached();



        //
        // Enable debug symbols access for stacktrace.
        //

#if GRAPHYTE_PLATFORM_WINDOWS && GRAPHYTE_ENABLE_STACKTRACE_SYMBOLS


        if (SymInitialize(GetCurrentProcess(), nullptr, TRUE) != FALSE)
        {
            SymSetOptions(SYMOPT_LOAD_LINES | SYMOPT_FAIL_CRITICAL_ERRORS);
        }
        else
        {
            GX_ABORT("Failed to initialize symbols handler");
        }

#endif


        //
        // Check for syslog output.
        //

#if GRAPHYTE_PLATFORM_WINDOWS

        DWORD process_id = GetCurrentProcessId();

        std::wstring event_source{ L"pid-" };
        event_source += std::to_wstring(process_id);

        Impl::GSystemEventLog = RegisterEventSourceW(nullptr, event_source.c_str());

        if (Impl::GSystemEventLog == nullptr)
        {
            GX_LOG(LogPlatform, Error, "Cannot open event log: {}\n", Diagnostics::GetMessageFromSystemError());
        }

#elif GRAPHYTE_PLATFORM_POSIX

        openlog(nullptr, LOG_ODELAY, LOG_USER);

#endif


        //
        // Check if we want to output log to terminal.
        //

        if (Application::GetDescriptor().Type == Application::ApplicationType::ConsoleTool)
        {
#if GRAPHYTE_PLATFORM_WINDOWS
            if (_isatty(_fileno(stdout)))
#endif
            {
                Impl::GLogOutputTerminal = true;
            }
        }


        //
        // Check if application wants to output data to log file.
        //

        if (!CommandLine::Has("--force-no-log"))
        {
            std::string log_path = Storage::FileManager::GetLogsDirectory();

            if (Storage::IFileSystem::GetPlatformNative().DirectoryTreeCreate(log_path) == Status::Success)
            {
                std::string const& executable = Platform::GetExecutableName();
                std::string const& timestamp = ToString(Platform::DateTime::Now(), Platform::DateTimeFormat::FileSafe);

                Storage::Path::Append(log_path, fmt::format("{}-{}.txt", executable, timestamp));

                Status status = Storage::IFileSystem::GetPlatformNative().OpenWrite(
                    Impl::GLogOutputFile,
                    log_path,
                    false,
                    true
                );

                GX_ABORT_UNLESS(status == Status::Success, "Failed to initialize output log {}", log_path);
            }
        }
    }

    BASE_API void Finalize() noexcept
    {
        //
        // This is regular way of terminating application. Flush any log outputs.
        //

        Impl::FinishLogOutput();


        //
        // Cleanup syslog.
        //

#if GRAPHYTE_PLATFORM_WINDOWS

        GX_ASSERT(Impl::GSystemEventLog != nullptr);

        if (Impl::GSystemEventLog != nullptr)
        {
            DeregisterEventSource(Impl::GSystemEventLog);
        }
        
#elif GRAPHYTE_PLATFORM_POSIX

        closelog();

#endif


#if GRAPHYTE_PLATFORM_WINDOWS && GRAPHYTE_ENABLE_STACKTRACE_SYMBOLS

        if (SymCleanup(GetCurrentProcess()) == FALSE)
        {
            GX_ABORT("Failed to cleanup symbol handler");
        }

#endif
    }

    BASE_API bool IsDebuggerAttached() noexcept
    {
#if GRAPHYTE_PLATFORM_WINDOWS

        return IsDebuggerPresent() != FALSE;

#elif GRAPHYTE_PLATFORM_LINUX

        int fd = open("/proc/self/status", O_RDONLY);

        if (fd != -1)
        {
            std::array<char, 1024> buffer{};

            ssize_t length = read(fd, buffer.data(), buffer.size());

            close(fd);

            if (length > 0)
            {
                std::string_view buffer_view{ buffer.data(), static_cast<size_t>(length) };
                std::string_view match{ "TracerPid:\t" };

                if (auto position = buffer_view.find(match); position != std::string_view::npos)
                {
                    auto value = buffer_view.substr(position + match.length(), 1);

                    return value != "0";
                }
            }
        }

        return false;

#else

#   error "Unimplemented"

#endif
    }

    [[noreturn]]
    BASE_API void Exit(int32_t exitCode) noexcept
    {
        //
        // Exiting from application in unusual but expected way.
        //

        GX_LOG(LogPlatform, Warn, "Exiting from application in unusual way: exit = {}\n", exitCode);


        //
        // There will be no way of flushing log outputs anytime.
        //

        Impl::FinishLogOutput();

#if GRAPHYTE_PLATFORM_WINDOWS

        ExitProcess(static_cast<UINT>(exitCode));

#elif GRAPHYTE_PLATFORM_LINUX

        exit(exitCode);

#else
#   error "Not implemented"
#endif
    }

    [[noreturn]]
    BASE_API void FailFast() noexcept
    {
        //
        // Exiting from application due to fast-fail is unexpected.
        //

        GX_LOG(LogPlatform, Error, "Exiting from application due to fast-fail\n");


        //
        // Flush any log outputs.
        //

        Impl::FinishLogOutput();


        //
        // And fail application.
        //

#if GRAPHYTE_PLATFORM_WINDOWS

        RaiseFailFastException(nullptr, nullptr, 0);
        TerminateProcess(GetCurrentProcess(), 1);

#elif GRAPHYTE_PLATFORM_LINUX

        kill(getpid(), SIGKILL);


        //
        // Well, this is noreturn...
        //

        for (;;)
        {
            ;
        }

#else
#   error "Not implemented"
#endif
    }

    BASE_API ErrorReporting GetErrorReporting() noexcept
    {
        return Impl::GErrorReporting;
    }

    BASE_API void SetErrorReporting(
        ErrorReporting value
    ) noexcept
    {
        Impl::GErrorReporting = value;
    }
}
