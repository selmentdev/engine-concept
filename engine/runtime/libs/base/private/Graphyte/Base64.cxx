#include "Base.pch.hxx"
#include <Graphyte/Base64.hxx>

namespace Graphyte
{
    static const std::string_view base64_chars =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz"
        "0123456789+/";

    static inline bool is_base64(
        std::uint8_t c
    ) noexcept
    {
        return (isalnum(c) != 0) || (c == '+') || (c == '/');
    }

    BASE_API std::string Base64::Encode(
        notstd::span<std::byte> buffer
    ) noexcept
    {
        std::string result;

        size_t i = 0;
        size_t j = 0;

        std::array<std::uint8_t, 3> char_array_3{};
        std::array<std::uint8_t, 4> char_array_4{};

        while (!buffer.empty())//(buffer_size--)
        {
            char_array_3[i++] = static_cast<uint8_t>(buffer[0]);
            buffer.remove_prefix(1);

            if (i == 3)
            {
                char_array_4[0] = static_cast<uint8_t>((char_array_3[0] & 0xfc) >> 2);
                char_array_4[1] = static_cast<uint8_t>(((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4));
                char_array_4[2] = static_cast<uint8_t>(((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6));
                char_array_4[3] = static_cast<uint8_t>(char_array_3[2] & 0x3f);

                for (i = 0; (i < 4); i++)
                {
                    result += base64_chars[char_array_4[i]];
                }

                i = 0;
            }
        }

        if (i)
        {
            for (j = i; j < 3; j++)
            {
                char_array_3[j] = '\0';
            }

            char_array_4[0] = static_cast<uint8_t>((char_array_3[0] & 0xfc) >> 2);
            char_array_4[1] = static_cast<uint8_t>(((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4));
            char_array_4[2] = static_cast<uint8_t>(((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6));
            char_array_4[3] = static_cast<uint8_t>(char_array_3[2] & 0x3f);

            for (j = 0; (j < i + 1); j++)
            {
                result += base64_chars[char_array_4[j]];
            }

            while ((i++ < 3))
            {
                result += '=';
            }
        }

        return result;
    }

    BASE_API std::vector<std::byte> Base64::Decode(
        std::string_view string
    ) noexcept
    {
        size_t in_len = string.size();
        size_t i = 0;
        size_t j = 0;
        size_t in_ = 0;

        std::array<uint8_t, 4> char_array_4;
        std::array<uint8_t, 3> char_array_3;

        std::vector<std::byte> result;

        while (in_len-- && (string[in_] != '=') && is_base64(static_cast<uint8_t>(string[in_])))
        {
            char_array_4[i++] = static_cast<uint8_t>(string[in_]);
            in_++;
            if (i == 4)
            {
                for (i = 0; i < 4; i++)
                {
                    char_array_4[i] = static_cast<uint8_t>(base64_chars.find(static_cast<char>(char_array_4[i])));
                }

                char_array_3[0] = static_cast<uint8_t>((char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4));
                char_array_3[1] = static_cast<uint8_t>(((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2));
                char_array_3[2] = static_cast<uint8_t>(((char_array_4[2] & 0x3) << 6) + char_array_4[3]);

                for (i = 0; (i < 3); i++)
                {
                    result.push_back(static_cast<std::byte>(char_array_3[i]));
                }

                i = 0;
            }
        }

        if (i)
        {
            for (j = i; j < 4; j++)
            {
                char_array_4[j] = 0;
            }

            for (j = 0; j < 4; j++)
            {
                char_array_4[j] = static_cast<uint8_t>(base64_chars.find(static_cast<char>(char_array_4[j])));
            }

            char_array_3[0] = static_cast<uint8_t>((char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4));
            char_array_3[1] = static_cast<uint8_t>(((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2));
            char_array_3[2] = static_cast<uint8_t>(((char_array_4[2] & 0x3) << 6) + char_array_4[3]);

            for (j = 0; (j < i - 1); j++)
            {
                result.push_back(static_cast<std::byte>(char_array_3[j]));
            }
        }

        return result;
    }
}
