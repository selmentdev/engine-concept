#include "Base.pch.hxx"
#include <Graphyte/ByteAccess.hxx>
#include <Graphyte/Platform.hxx>
#include <Graphyte/Platform/Uuid.hxx>

#if GRAPHYTE_PLATFORM_LINUX
#   include <uuid/uuid.h>
#endif

namespace Graphyte::Platform::Impl
{
    static auto UuidParsePart(
        std::string_view value
    ) noexcept
        -> std::pair<bool, uint32_t>
    {
        auto get_digit_value = [](char digit) noexcept -> int32_t
        {
            if (digit >= '0' && digit <= '9')
            {
                return static_cast<int32_t>(digit - '0');
            }

            if (digit >= 'a' && digit <= 'f')
            {
                return static_cast<int32_t>(digit + 10 - 'a');
            }
            
            if (digit >= 'A' && digit <= 'F')
            {
                return static_cast<int32_t>(digit + 10 - 'A');
            }

            return -1;
        };

        uint32_t parsed{};
        bool valid{ true };

        for (char const ch : value)
        {
            if (int32_t const digit = get_digit_value(ch); digit >= 0)
            {
                parsed <<= 4;
                parsed += static_cast<uint32_t>(digit);
            }
            else
            {
                valid = false;
                break;
            }
        }

        return std::make_pair(valid, parsed);
    }
}

namespace Graphyte::Platform
{
    BASE_API int Uuid::Compare(
        Uuid lhs,
        Uuid rhs
    ) noexcept
    {
        auto comparePart = [](uint32_t l, uint32_t r) noexcept
        {
            return (l < r)
                ? -1
                : +1;
        };

        if (lhs.A != rhs.A)
        {
            return comparePart(lhs.A, rhs.A);
        }

        if (lhs.B != rhs.B)
        {
            return comparePart(lhs.B, rhs.B);
        }

        if (lhs.C != rhs.C)
        {
            return comparePart(lhs.C, rhs.C);
        }

        if (lhs.D != rhs.D)
        {
            return comparePart(lhs.D, rhs.D);
        }

        return 0;
    }

    BASE_API bool Uuid::Equals(
        Uuid lhs,
        Uuid rhs
    ) noexcept
    {
        return lhs.A == rhs.A
            && lhs.B == rhs.B
            && lhs.C == rhs.C
            && lhs.D == rhs.D;
    }

    BASE_API std::string ToString(
        Uuid value
    ) noexcept
    {
        return fmt::format(
            "{:08x}{:08x}{:08x}{:08x}",
            value.A,
            value.B,
            value.C,
            value.D
        );
    }

    BASE_API bool FromString(Uuid& out_result, std::string_view value) noexcept
    {
        if (value.length() == 32)
        {
            auto const partA = value.substr(0, 8);
            auto const partB = value.substr(8, 8);
            auto const partC = value.substr(16, 8);
            auto const partD = value.substr(24, 8);

            auto[validA, parsedA] = Impl::UuidParsePart(partA);
            auto[validB, parsedB] = Impl::UuidParsePart(partB);
            auto[validC, parsedC] = Impl::UuidParsePart(partC);
            auto[validD, parsedD] = Impl::UuidParsePart(partD);

            out_result = {
                parsedA,
                parsedB,
                parsedC,
                parsedD
            };

            return validA
                && validB
                && validC
                && validD;
        }

        out_result = {};
        return false;
    }

    BASE_API Uuid CreateUuid() noexcept
    {
        Uuid result{};

#if GRAPHYTE_PLATFORM_WINDOWS

        [[maybe_unused]] HRESULT hr = CoCreateGuid(reinterpret_cast<GUID*>(&result));
        GX_ASSERT(hr == S_OK);

#elif GRAPHYTE_PLATFORM_LINUX

        uuid_generate(reinterpret_cast<unsigned char*>(&result));

#else
#   error "Not implemented"
#endif

        return result;
    }
}
