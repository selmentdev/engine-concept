#include "Base.pch.hxx"
#include <Graphyte/Platform.hxx>
#include <Graphyte/Platform/Impl.Windows/Windows.Types.hxx>

namespace Graphyte::Platform
{
    BASE_API void GetSystemTime(CalendarTime& time) noexcept
    {
        SYSTEMTIME st{};
        GetSystemTime(&st);
        Platform::TypeConverter<SYSTEMTIME>::Convert(time, st);
    }

    BASE_API void GetLocalTime(CalendarTime& time) noexcept
    {
        SYSTEMTIME st{};
        GetLocalTime(&st);
        Platform::TypeConverter<SYSTEMTIME>::Convert(time, st);
    }

    BASE_API uint64_t GetSystemTime() noexcept
    {
        FILETIME ft{};
        GetSystemTimeAsFileTime(&ft);
        return Platform::TypeConverter<FILETIME>::ConvertUInt64(ft);
    }

    BASE_API uint64_t GetLocalTime() noexcept
    {
        SYSTEMTIME st{};
        GetLocalTime(&st);

        FILETIME ft{};
        SystemTimeToFileTime(&st, &ft);
        return Platform::TypeConverter<FILETIME>::ConvertUInt64(ft);
    }

    BASE_API uint64_t GetTimestampResolution() noexcept
    {
        LARGE_INTEGER li{};
        QueryPerformanceFrequency(&li);
        return Platform::TypeConverter<LARGE_INTEGER>::ConvertUInt64(li);
    }

    BASE_API uint64_t GetTimestamp() noexcept
    {
        LARGE_INTEGER li{};
        QueryPerformanceCounter(&li);
        return Platform::TypeConverter<LARGE_INTEGER>::ConvertUInt64(li);
    }

    BASE_API uint64_t GetMonotonic() noexcept
    {
        return static_cast<uint64_t>(GetTickCount64());
    }

    BASE_API double GetSeconds() noexcept
    {
        LARGE_INTEGER li{};
        QueryPerformanceCounter(&li);

        return li.QuadPart * GSecondsPerCycle;
    }
}
