#include "Base.pch.hxx"
#include <Graphyte/Platform.hxx>
#include <Graphyte/Platform/DateTime.hxx>
#include <Graphyte/Diagnostics.hxx>

namespace Graphyte::Platform::Impl
{
    static const int32_t DaysPerMonth[12] =
    {
        31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };

    static const int32_t DaysBeforeMonth[12] =
    {
        0,
        31,
        31 + 28,
        31 + 28 + 31,
        31 + 28 + 31 + 30,
        31 + 28 + 31 + 30 + 31,
        31 + 28 + 31 + 30 + 31 + 30,
        31 + 28 + 31 + 30 + 31 + 30 + 31,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31,
        31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30
    };

    static bool IsLeapYear(int year) noexcept
    {
        return ((year % 4) == 0) && (((year % 100) != 0) || ((year % 400) == 0));
    }

    static int DaysInMonth(int year, int month) noexcept
    {
        if (IsLeapYear(year) && (month == 2))
        {
            return 29;
        }

        return Impl::DaysPerMonth[month - 1];
    }

    static int64_t YearToTicks(int year) noexcept
    {
        --year;

        return (
            static_cast<int64_t>(year) * 365 +
            static_cast<int64_t>(year) / 4 -
            static_cast<int64_t>(year) / 100 +
            static_cast<int64_t>(year) / 400
        ) * Impl::TicksInDay;
    }

    static int64_t DateToTicks(int year, int month, int day) noexcept
    {
        GX_ASSERT((year >= 1) && (year <= 9999));
        GX_ASSERT((month >= 1) && (month <= 12));
        GX_ASSERT((day >= 1) && (day <= 31));

        bool is_leap_year = Impl::IsLeapYear(year);
        int days_in_month = DaysPerMonth[month - 1];

        if ((month == 2) && is_leap_year)
        {
            ++days_in_month;
        }

        GX_ASSERT((day >= 1) && (day <= days_in_month));

        int64_t result = YearToTicks(year) / Impl::TicksInDay;
        result += static_cast<int64_t>(Impl::DaysBeforeMonth[month - 1]);

        if ((month > 2) && is_leap_year)
        {
            ++result;
        }

        return (result + static_cast<int64_t>(day) - 1) * Impl::TicksInDay;
    }

    static int64_t TimeToTicks(int hour, int minute, int second) noexcept
    {
        GX_ASSERT((hour >= 0) && (hour <= 23));
        GX_ASSERT((minute >= 0) && (minute <= 59));
        GX_ASSERT((second >= 0) && (second <= 59));

        return (static_cast<int64_t>(hour) * 3600
            + static_cast<int64_t>(minute) * 60
            + static_cast<int64_t>(second)) * Impl::TicksInSecond;

    }

    static int32_t GetYear(DateTime value) noexcept
    {
        int days = static_cast<int>(value.Value / Impl::TicksInDay);
        int year_base = ((days / Impl::DaysIn400Years) * 400) + 1;
        int year_offset = days % Impl::DaysIn400Years;

        if (year_offset >= Impl::DaysIn100Years * 3)
        {
            year_base += 300;
            year_offset -= Impl::DaysIn100Years * 3;

            if (year_offset >= Impl::DaysIn100Years)
            {
                year_base += 399;
            }
        }
        else if (year_offset >= Impl::DaysIn100Years * 2)
        {
            year_base += 200;
            year_offset -= Impl::DaysIn100Years * 2;
        }
        else if (year_offset >= Impl::DaysIn100Years)
        {
            year_base += 100;
            year_offset -= Impl::DaysIn100Years;
        }

        int t = year_offset / Impl::DaysIn4Years;
        year_base += t * 4;
        year_offset -= t * Impl::DaysIn4Years;

        if (year_offset >= Impl::DaysInYear * 3)
        {
            year_base += 3;
        }
        else if (year_offset >= Impl::DaysInYear * 2)
        {
            year_base += 2;
        }
        else if (year_offset >= Impl::DaysInYear)
        {
            year_base += 1;
        }

        return year_base;
    }

    static int32_t GetMonth(
        DateTime value,
        int32_t year
    ) noexcept
    {
        int64_t ticks = value.Value - YearToTicks(year);

        int days = static_cast<int>(ticks / Impl::TicksInDay);

        if (IsLeapYear(year))
        {
            if (days < 31)
            {
                return 1;
            }
            else if (days < (31 + 29))
            {
                return 2;
            }

            --days;
        }

        int month = static_cast<int>(Month::First);

        while ((month < static_cast<int>(Month::Last)) && (days >= Impl::DaysBeforeMonth[month]))
        {
            ++month;
        }

        return month;
    }

#if false
    static int32_t GetMonth(DateTime value) noexcept
    {
        return GetMonth(value, GetYear(value));
    }
#endif

    static int32_t GetDay(
        DateTime value,
        int32_t year
    ) noexcept
    {
        int64_t ticks = value.Value - YearToTicks(year);

        int days = static_cast<int>(ticks / Impl::TicksInDay);

        if (IsLeapYear(year))
        {
            if (days < 31)
            {
                return days + 1;
            }
            else if (days < (31 + 29))
            {
                return days - 30;
            }

            --days;
        }

        int month = static_cast<int>(Month::First);

        while ((month < static_cast<int>(Month::Last)) && (days >= Impl::DaysBeforeMonth[month]))
        {
            ++month;
        }

        return days - Impl::DaysBeforeMonth[month - 1] + 1;
    }

#if false
    static int32_t GetDay(DateTime value) noexcept
    {
        return GetDay(value, GetYear(value));
    }
#endif

    static int32_t GetHour(DateTime value) noexcept
    {
        return static_cast<int>((value.Value / Impl::TicksInHour) % 24);
    }

    static int32_t GetMinute(DateTime value) noexcept
    {
        return static_cast<int>((value.Value / Impl::TicksInMinute) % 60);
    }

    static int32_t GetSecond(DateTime value) noexcept
    {
        return static_cast<int>((value.Value / Impl::TicksInSecond) % 60);
    }

    static int32_t GetMillisecond(DateTime value) noexcept
    {
        return static_cast<int>((value.Value / Impl::TicksInMillisecond) % 1000);
    }

    static int32_t GetDayOfWeek(DateTime value) noexcept
    {
        return static_cast<int32_t>(((value.Value / Impl::TicksInDay) + 1) % static_cast<int>(WeekDay::Count));
    }

    static int32_t GetDayOfYear(DateTime value) noexcept
    {
        int64_t ticks = value.Value - YearToTicks(GetYear(value));
        return static_cast<int>(ticks / Impl::TicksInDay) + 1;
    }

}

namespace Graphyte::Platform
{
    BASE_API int64_t CalendarTime::ToTicks() const noexcept
    {
        int64_t const ticksDate = Impl::DateToTicks(this->Year, this->Month, this->Day);
        int64_t const ticksTime = Impl::TimeToTicks(this->Hour, this->Minute, this->Second);
        int64_t const ticksMillisecond = this->Millisecond * Impl::TicksInMillisecond;

        return ticksDate + ticksTime + ticksMillisecond;
    }

    BASE_API bool CalendarTime::IsValid() const noexcept
    {
        return (this->Year >= 1)
            && (this->Year <= 9999)
            && (this->Month >= 1)
            && (this->Month <= 12)
            && (this->Day >= 1)
            && (this->Day <= Impl::DaysInMonth(this->Year, this->Month))
            && (this->Hour <= 23)
            && (this->Minute <= 59)
            && (this->Second <= 59)
            && (this->Millisecond <= 999);
    }
}

namespace Graphyte::Platform
{
    BASE_API DateTime DateTime::Create(int year, int month, int day) noexcept
    {
        return DateTime
        {
            Impl::DateToTicks(year, month, day)
        };
    }

    BASE_API DateTime DateTime::Create(int year, int month, int day, int hour, int minute, int second) noexcept
    {
        return DateTime
        {
            Impl::DateToTicks(year, month, day) +
            Impl::TimeToTicks(hour, minute, second)
        };
    }

    BASE_API DateTime DateTime::Create(int year, int month, int day, int hour, int minute, int second, int millisecond) noexcept
    {
        return DateTime
        {
            Impl::DateToTicks(year, month, day) +
            Impl::TimeToTicks(hour, minute, second) +
            static_cast<int64_t>(millisecond) * Impl::TicksInMillisecond
        };
    }

    BASE_API DateTime DateTime::Now() noexcept
    {
        int64_t ticks = Impl::DateAdjustOffset;
        ticks += Platform::GetLocalTime();
        return DateTime{ ticks };
    }

    BASE_API DateTime DateTime::Now(DateTimeKind kind) noexcept
    {
        int64_t ticks = Impl::DateAdjustOffset;

        if (kind == DateTimeKind::Local)
        {
            ticks += Platform::GetLocalTime();
        }
        else
        {
            ticks += Platform::GetSystemTime();
        }

        return DateTime{ ticks };
    }

    BASE_API DateTime DateTime::Today(DateTimeKind kind) noexcept
    {
        return DateTime::Now(kind).GetDate();
    }

    BASE_API DateTime DateTime::Today() noexcept
    {
        return DateTime::Now().GetDate();
    }

    BASE_API DateTime DateTime::FromUnixTimestamp(int64_t seconds) noexcept
    {
        return DateTime::Create(1970, 1, 1) + TimeSpan::FromSeconds(seconds);
    }

    BASE_API int64_t DateTime::ToUnixTimestamp() noexcept
    {
        return (this->Value - DateTime::Create(1970, 1, 1).Value) / Impl::TicksInSecond;
    }

    BASE_API TimeSpan DateTime::GetTimeOfDay() const noexcept
    {
        return TimeSpan
        {
            this->Value % Impl::TicksInDay
        };
    }

    BASE_API DateTime DateTime::GetDate() const noexcept
    {
        return DateTime
        {
            this->Value - (this->Value % Impl::TicksInDay)
        };
    }

    BASE_API DateTime FromCalendar(const CalendarTime& value) noexcept
    {
        return DateTime::Create(
            value.Year,
            value.Month,
            value.Day,
            value.Hour,
            value.Minute,
            value.Second,
            value.Millisecond
        );
    }

    BASE_API bool ToCalendar(CalendarTime& result, DateTime value) noexcept
    {
        int32_t const year = Impl::GetYear(value);

        result.Year = static_cast<uint16_t>(year);
        result.Month = static_cast<uint16_t>(Impl::GetMonth(value, year));
        result.Day = static_cast<uint16_t>(Impl::GetDay(value, year));
        result.Hour = static_cast<uint16_t>(Impl::GetHour(value));
        result.Minute = static_cast<uint16_t>(Impl::GetMinute(value));
        result.Second = static_cast<uint16_t>(Impl::GetSecond(value));
        result.Millisecond = static_cast<uint16_t>(Impl::GetMillisecond(value));
        result.DayOfWeek = static_cast<uint16_t>(Impl::GetDayOfWeek(value));
        result.DayOfYear = static_cast<uint16_t>(Impl::GetDayOfYear(value));
        return true;
    }

    BASE_API std::string ToString(DateTime value, DateTimeFormat format) noexcept
    {
        std::string result{};

        CalendarTime time;
        ToCalendar(time, value);

        switch (format)
        {
        case DateTimeFormat::Date:
            {
                result = fmt::format(
                    "{:04d}-{:02d}-{:02d}",
                    time.Year,
                    time.Month,
                    time.Day
                );
                break;
            }

        case DateTimeFormat::Time:
            {
                result = fmt::format(
                    "{:02d}:{:02d}:{:02d}",
                    time.Hour,
                    time.Minute,
                    time.Second
                );
                break;
            }

        case DateTimeFormat::FileSafe:
            {
                result = fmt::format(
                    "{:04d}.{:02d}.{:02d}-{:02d}.{:02d}.{:02d}",
                    time.Year,
                    time.Month,
                    time.Day,
                    time.Hour,
                    time.Minute,
                    time.Second
                );
                break;
            }

        case DateTimeFormat::DateTime:
            {
                result = fmt::format(
                    "{:04d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}",
                    time.Year,
                    time.Month,
                    time.Day,
                    time.Hour,
                    time.Minute,
                    time.Second
                );
                break;
            }

        case DateTimeFormat::TimeStamp:
            {
                result = fmt::format(
                    "{:04d}.{:02d}.{:02d}-{:02d}.{:02d}.{:02d}.{:03d}",
                    time.Year,
                    time.Month,
                    time.Day,
                    time.Hour,
                    time.Minute,
                    time.Second,
                    time.Millisecond
                );
                break;
            }

        default:
            {
                GX_ASSERTF(false, "Unknown date time format `{}`", static_cast<uint32_t>(format));
                break;
            }
        }

        return result;
    }

    BASE_API std::string ToString(DateTime value, std::string_view format) noexcept
    {
        CalendarTime time{};
        ToCalendar(time, value);

        const bool isMorning = time.Hour < 12;

        bool saw_percent = false;

        fmt::memory_buffer buffer{};

        for (const char ch : format)
        {
            if (ch == '%')
            {
                if (saw_percent)
                {
                    //
                    // Output percent sign as we saw `%` twice.
                    //
                    buffer.push_back('%');
                    saw_percent = false;
                }
                else
                {
                    //
                    // It's first time we saw this sign.
                    //
                    saw_percent = true;
                    continue;
                }
            }

            if (saw_percent)
            {
                saw_percent = false;

                //
                // If we saw `%` sign. `ch` contains formatter character.
                //

                switch (ch)
                {
                case 'a':
                {
                    const char* ampm = isMorning ? "am" : "pm";
                    buffer.append(ampm, ampm + 2);
                    break;
                }
                case 'A':
                {
                    const char* ampm = isMorning ? "AM" : "PM";
                    buffer.append(ampm, ampm + 2);
                    break;
                }
                case 'd':
                {
                    fmt::format_to(buffer, "{:02}", time.Day);
                    break;
                }
                case 'D':
                {
                    fmt::format_to(buffer, "{:03}", time.DayOfYear);
                    break;
                }
                case 'm':
                {
                    fmt::format_to(buffer, "{:02}", time.Month);
                    break;
                }
                case 'y':
                {
                    fmt::format_to(buffer, "{:02}", time.Year % 100);
                    break;
                }
                case 'Y':
                {
                    fmt::format_to(buffer, "{}", time.Year);
                    break;
                }
                case 'h':
                {
                    fmt::format_to(buffer, "{:02}", time.Hour % 12);
                    break;
                }
                case 'H':
                {
                    fmt::format_to(buffer, "{:02}", time.Hour);
                    break;
                }
                case 'M':
                {
                    fmt::format_to(buffer, "{:02}", time.Minute);
                    break;
                }
                case 'S':
                {
                    fmt::format_to(buffer, "{:02}", time.Second);
                    break;
                }
                case 's':
                {
                    fmt::format_to(buffer, "{:03}", time.Millisecond);
                    break;
                }
                default:
                {
                    GX_ASSERTF(false, "Unknown format specifier: {:c}", ch);
                    buffer.push_back(ch);
                    break;
                }
                }
            }
            else
            {
                //
                // Put character to buffer.
                //
                buffer.push_back(ch);
            }
        }

        GX_ASSERTF(saw_percent == false, "Unterminated string. Saw percent sign without specifier");

        return std::string{
            std::data(buffer),
            std::size(buffer)
        };
    }

    BASE_API bool FromString(DateTime& result, std::string_view format) noexcept
    {
        bool valid = true;

        CalendarTime time{};
        int32_t timezone_hours{};
        int32_t timezone_minutes{};
        int32_t timezone_sign = 0;

        auto* ptr_begin = std::data(format);
        auto* ptr_end = std::data(format) + std::size(format);

        // Parse year.
        auto [sep1, ecc1] = std::from_chars(ptr_begin, ptr_end, time.Year, 10);

        if (std::distance(ptr_begin, sep1) != 4 || ecc1 != std::errc{} || *sep1 != '-')
        {
            return false;
        }


        // Parse month.
        auto [sep2, ecc2] = std::from_chars(sep1 + 1, ptr_end, time.Month, 10);

        if (std::distance(sep1 + 1, sep2) != 2 || ecc2 != std::errc{} || *sep2 != '-')
        {
            return false;
        }

        // Parse day.
        auto [sep3, ecc3] = std::from_chars(sep2 + 1, ptr_end, time.Day, 10);

        if (std::distance(sep2 + 1, sep3) != 2 || ecc3 != std::errc{})
        {
            // Cannot parse day.
            return false;
        }

        //
        // Check if we have time specifier.
        //
        if (sep3 != ptr_end && *sep3 == 'T')
        {
            // Parse hour
            auto [sep4, ecc4] = std::from_chars(sep3 + 1, ptr_end, time.Hour, 10);

            if (std::distance(sep3 + 1, sep4) != 2 || ecc4 != std::errc{} || *sep4 != ':')
            {
                return false;
            }

            // Parse minute.
            auto [sep5, ecc5] = std::from_chars(sep4 + 1, ptr_end, time.Minute, 10);

            if (std::distance(sep4 + 1, sep5) != 2 || ecc5 != std::errc{} || *sep5 != ':')
            {
                return false;
            }

            // Parse second.
            auto [sep6, ecc6] = std::from_chars(sep5 + 1, ptr_end, time.Second, 10);

            if (std::distance(sep5 + 1, sep6) != 2 || ecc6 != std::errc{})
            {
                return false;
            }

            // Parse optional milliseconds
            if (sep6 != ptr_end && *sep6 == '.')
            {
                auto [sep7, ecc7] = std::from_chars(sep6 + 1, ptr_end, time.Millisecond, 10);

                if (std::distance(sep6 + 1, sep7) != 3 || ecc7 != std::errc{})
                {
                    return false;
                }

                //
                // Copy over last location.
                //
                sep6 = sep7;
                ecc6 = ecc7;
            }

            if (sep6 != ptr_end)
            {
                //
                // Parse timezone sign.
                if (*sep6 == '+')
                {
                    timezone_sign = 1;
                }
                else if (*sep6 == '-')
                {
                    timezone_sign = -1;
                }

                if (timezone_sign != 0)
                {
                    auto [sep7, ecc7] = std::from_chars(sep6 + 1, ptr_end, timezone_hours, 10);
                    if (std::distance(sep6 + 1, sep7) != 2 || ecc7 != std::errc{} || *sep7 != ':')
                    {
                        return false;
                    }

                    auto [sep8, ecc8] = std::from_chars(sep7 + 1, ptr_end, timezone_minutes, 10);
                    if (std::distance(sep7 + 1, sep8) != 2 || ecc8 != std::errc{})
                    {
                        return false;
                    }

                    valid = (sep8 == ptr_end);
                }
            }
        }
        else
        {
            valid = (sep3 == ptr_end);
        }

        if (valid)
        {
            result = FromCalendar(time);

            int32_t timezone_offset = timezone_sign * (timezone_hours * 60 + timezone_minutes);
            result.Value -= Impl::TicksInMinute * timezone_offset;
        }

        return valid;
    }
}

namespace Graphyte::Platform::Impl
{
    static bool ComputeTimeSpanTicks(int64_t& result, int days, int hours, int minutes, int seconds, int milliseconds) noexcept
    {
        int64_t const hours_as_seconds = static_cast<int64_t>(hours) * 3600;
        int64_t const minutes_as_seconds = static_cast<int64_t>(minutes) * 60;
        int64_t const as_seconds = hours_as_seconds + minutes_as_seconds + seconds;
        int64_t const as_milliseconds = as_seconds * INT64_C(1000);
        int64_t const total_milliseconds = as_milliseconds + static_cast<int64_t>(milliseconds);

        int64_t const ticks = total_milliseconds * Impl::TicksInMillisecond;

        bool overflow = false;

        int64_t const ticks_per_day = Impl::TicksInDay * days;
        int64_t const biased_ticks = ticks + ticks_per_day;

        if (days > 0)
        {
            if (ticks < 0)
            {
                overflow = (ticks > biased_ticks);
            }
            else
            {
                overflow = (biased_ticks < 0);
            }
        }
        else if (days < 0)
        {
            if (ticks <= 0)
            {
                overflow = (biased_ticks > 0);
            }
            else
            {
                overflow = (ticks > biased_ticks);
            }
        }

        if (overflow != false)
        {
            return false;
        }

        result = biased_ticks;
        return true;
    }
}

namespace Graphyte::Platform
{
    BASE_API TimeSpan TimeSpan::Create(int hours, int minutes, int seconds) noexcept
    {
        int64_t ticks{};
        Impl::ComputeTimeSpanTicks(ticks, 0, hours, minutes, seconds, 0);
        return TimeSpan{ ticks };
    }

    BASE_API TimeSpan TimeSpan::Create(int days, int hours, int minutes, int seconds) noexcept
    {
        int64_t ticks{};
        Impl::ComputeTimeSpanTicks(ticks, days, hours, minutes, seconds, 0);
        return TimeSpan{ ticks };
    }

    BASE_API TimeSpan TimeSpan::Create(int days, int hours, int minutes, int seconds, int milliseconds) noexcept
    {
        int64_t ticks{};
        Impl::ComputeTimeSpanTicks(ticks, days, hours, minutes, seconds, milliseconds);
        return TimeSpan{ ticks };
    }

    BASE_API void ToMembers(TimeSpanMembers& result, TimeSpan value) noexcept
    {
        result.Days = static_cast<int32_t>(value.Value / Impl::TicksInDay);
        result.Hours = static_cast<int32_t>((value.Value % Impl::TicksInDay) / Impl::TicksInHour);
        result.Minutes = static_cast<int32_t>((value.Value % Impl::TicksInHour) / Impl::TicksInMinute);
        result.Seconds = static_cast<int32_t>((value.Value % Impl::TicksInMinute) / Impl::TicksInSecond);
        result.Milliseconds = static_cast<int32_t>((value.Value % Impl::TicksInSecond) / Impl::TicksInMillisecond);
    }

    BASE_API TimeSpan FromMembers(const TimeSpanMembers& value) noexcept
    {
        int64_t ticks = value.Days * Impl::TicksInDay;
        ticks += value.Hours * Impl::TicksInHour;
        ticks += value.Minutes * Impl::TicksInMinute;
        ticks += value.Seconds * Impl::TicksInSecond;
        ticks += value.Milliseconds * Impl::TicksInMillisecond;
        return TimeSpan{ ticks };
    }

    BASE_API std::string ToString(TimeSpan value) noexcept
    {
        const char* format = (value.Value < Impl::TicksInDay)
            ? "%n%h:%m:%s.%f"
            : "%n%d.%h:%m:%s.%f";

        return ToString(value, format);
    }

    BASE_API std::string ToString(TimeSpan value, std::string_view format) noexcept
    {
        TimeSpanMembers members{};
        ToMembers(members, value);

        fmt::memory_buffer buffer{};

        bool percentSign = false;

        for (const char ch : format)
        {
            if (ch == '%')
            {
                percentSign = true;
                continue;
            }

            if (percentSign)
            {
                switch (ch)
                {
                case 'n':
                {
                    if (value.Value < 0)
                    {
                        buffer.push_back('-');
                    }
                    break;
                }
                case 'N':
                {
                    buffer.push_back((value.Value < 0) ? '-' : '+');
                    break;
                }
                case 'd':
                {
                    fmt::format_to(buffer, "{}", std::abs(members.Days));
                    break;
                }
                case 'h':
                {
                    fmt::format_to(buffer, "{:02d}", std::abs(members.Hours));
                    break;
                }
                case 'm':
                {
                    fmt::format_to(buffer, "{:02d}", std::abs(members.Minutes));
                    break;
                }
                case 's':
                {
                    fmt::format_to(buffer, "{:02d}", std::abs(members.Seconds));
                    break;
                }
                case 'f':
                {
                    fmt::format_to(buffer, "{:03d}", std::abs(members.Milliseconds));
                    break;
                }
                case 'D':
                {
                    fmt::format_to(buffer, "{}", std::abs(value.GetTotalDays()));
                    break;
                }
                case 'H':
                {
                    fmt::format_to(buffer, "{:02d}", std::abs(value.GetTotalHours()));
                    break;
                }
                case 'M':
                {
                    fmt::format_to(buffer, "{:02d}", std::abs(value.GetTotalMinutes()));
                    break;
                }
                case 'S':
                {
                    fmt::format_to(buffer, "{:02d}", std::abs(value.GetTotalSeconds()));
                    break;
                }
                case 'F':
                {
                    fmt::format_to(buffer, "{:03d}", std::abs(value.GetTotalMilliseconds()));
                    break;
                }
                default:
                {
                    buffer.push_back(ch);
                    break;
                }
                }

                percentSign = false;
            }
            else
            {
                buffer.push_back(ch);
            }
        }

        GX_ASSERT(percentSign == false);

        return std::string
        {
            buffer.data(),
            buffer.size()
        };
    }
}
