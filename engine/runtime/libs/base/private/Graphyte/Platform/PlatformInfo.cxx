#include "Base.pch.hxx"
#include <Graphyte/Platform.hxx>
#include "Platform.impl.hxx"

namespace Graphyte::Platform
{
    BASE_API PlatformKind GetPlatformKind() noexcept
    {
        return Platform::CurrentPlatformKind;
    }

    BASE_API PlatformType GetPlatformType() noexcept
    {
        return Platform::CurrentPlatformType;
    }

    BASE_API std::string_view GetPlatformName() noexcept
    {
        return GRAPHYTE_PLATFORM_NAME;
    }

    BASE_API bool HasPlatformFeature(
        PlatformFeature feature
    ) noexcept
    {
        return Impl::GPlatformFeatureSet.Has(feature);
    }

    BASE_API std::string_view GetFileManagerName() noexcept
    {
#if GRAPHYTE_PLATFORM_WINDOWS

        return "explorer.exe";

#elif GRAPHYTE_PLATFORM_LINUX

        return {};

#else
#   error "Unknown file manager name"
#endif
    }

    BASE_API std::string_view GetModuleExtension() noexcept
    {
#if GRAPHYTE_PLATFORM_WINDOWS

        return "dll";

#elif GRAPHYTE_PLATFORM_LINUX

        return "so";

#else
#   error "Unknown module extension"
#endif
    }

    BASE_API std::string_view GetModulePrefix() noexcept
    {
#if GRAPHYTE_PLATFORM_WINDOWS

        return {};

#elif GRAPHYTE_PLATFORM_LINUX

        return "lib";

#else
#   error "Unknown module prefix"
#endif
    }

    BASE_API std::string_view GetExecutableExtension() noexcept
    {
#if GRAPHYTE_PLATFORM_WINDOWS

        return "exe";

#elif GRAPHYTE_PLATFORM_LINUX

        return {};

#else
#   error "Unknown executable extension"
#endif
    }
}
