#include "Geometry.pch.hxx"
#include <Graphyte/Geometry/MeshVertexComparator.hxx>
#include <Graphyte/Math/Vector4.hxx>
#include <Graphyte/Math/Vector3.hxx>
#include <Graphyte/Math/Vector2.hxx>
#include <Graphyte/Math/Color.hxx>

namespace Graphyte::Geometry
{
    int MeshVertexComparator::Compare(
        const Mesh& mesh,
        uint32_t lhs_index,
        uint32_t rhs_index,
        float tolerance
    ) const noexcept
    {
        Math::Vector4 const vtolerance = Math::Vector4::Make(tolerance);

        //
        // Compare vertex positions.
        //
        {
            Float3 const& lhs_vertex = mesh.VertexPositions[mesh.WedgeIndices[lhs_index]];
            Float3 const& rhs_vertex = mesh.VertexPositions[mesh.WedgeIndices[rhs_index]];

            Math::Vector3 const lhs = Math::Vector3::LoadPacked(&lhs_vertex);
            Math::Vector3 const rhs = Math::Vector3::LoadPacked(&rhs_vertex);

            if (!Math::Vector3::IsEqual(lhs, rhs, vtolerance))
            {
                if (Math::Vector3::IsLess(lhs, rhs))
                {
                    return -1;
                }
                else if (Math::Vector3::IsGreater(lhs, rhs))
                {
                    return 1;
                }
            }
        }

        //
        // Compare vertex normals.
        //
        if (this->CompareNormals)
        {
            Math::Vector3 const lhs = Math::Vector3::LoadPacked(&mesh.WedgeTangentZ[lhs_index]);
            Math::Vector3 const rhs = Math::Vector3::LoadPacked(&mesh.WedgeTangentZ[rhs_index]);

            if (!Math::Vector3::IsEqual(lhs, rhs, vtolerance))
            {
                if (Math::Vector3::IsLess(lhs, rhs))
                {
                    return -1;
                }
                else if (Math::Vector3::IsGreater(lhs, rhs))
                {
                    return 1;
                }
            }
        }

        //
        // Compare tangent X.
        //
        if (this->CompareTangentX)
        {
            Math::Vector3 const lhs = Math::Vector3::LoadPacked(&mesh.WedgeTangentX[lhs_index]);
            Math::Vector3 const rhs = Math::Vector3::LoadPacked(&mesh.WedgeTangentX[rhs_index]);

            if (!Math::Vector3::IsEqual(lhs, rhs, vtolerance))
            {
                if (Math::Vector3::IsLess(lhs, rhs))
                {
                    return -1;
                }
                else if (Math::Vector3::IsGreater(lhs, rhs))
                {
                    return 1;
                }
            }
        }

        //
        // Compare tangent Y.
        //
        if (this->CompareTangentY)
        {
            Math::Vector3 const lhs = Math::Vector3::LoadPacked(&mesh.WedgeTangentY[lhs_index]);
            Math::Vector3 const rhs = Math::Vector3::LoadPacked(&mesh.WedgeTangentY[rhs_index]);

            if (!Math::Vector3::IsEqual(lhs, rhs, vtolerance))
            {
                if (Math::Vector3::IsLess(lhs, rhs))
                {
                    return -1;
                }
                else if (Math::Vector3::IsGreater(lhs, rhs))
                {
                    return 1;
                }
            }
        }

        //
        // Compare texcoords.
        //
        for (size_t i = 0; i < Mesh::MaxTextureCoords; ++i)
        {
            if (this->CompareTexcoords[i])
            {
                Math::Vector2 const lhs = Math::Vector2::LoadPacked(&mesh.WedgeTextureCoords[i][lhs_index]);
                Math::Vector2 const rhs = Math::Vector2::LoadPacked(&mesh.WedgeTextureCoords[i][rhs_index]);

                if (!Math::Vector2::IsEqual(lhs, rhs, Math::Vector2{ vtolerance.V }))
                {
                    if (Math::Vector2::IsLess(lhs, rhs))
                    {
                        return -1;
                    }
                    else if (Math::Vector2::IsGreater(lhs, rhs))
                    {
                        return 1;
                    }
                }
            }
        }

        //
        // Compare colors.
        //
        if (this->CompareTangentY)
        {
            Math::Color const lhs = Math::Color::Load(&mesh.WedgeColors[lhs_index]);
            Math::Color const rhs = Math::Color::Load(&mesh.WedgeColors[rhs_index]);

            if (!Math::Vector4::IsEqual(Math::Vector4{ lhs.V }, Math::Vector4{ rhs.V }, vtolerance))
            {
                if (Math::Color::IsLess(lhs, rhs))
                {
                    return -1;
                }
                else if (Math::Color::IsGreater(lhs, rhs))
                {
                    return 1;
                }
            }
        }

        return 0;
    }
}
