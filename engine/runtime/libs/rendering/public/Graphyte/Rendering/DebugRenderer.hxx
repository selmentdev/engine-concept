#pragma once
#include <Graphyte/Rendering.module.hxx>

#include <Graphyte/Math/Vector3.hxx>
#include <Graphyte/Math/Color.hxx>
#include <Graphyte/Math/Matrix.hxx>

#include <Graphyte/Graphics/Gpu/GpuDefinitions.hxx>

namespace Graphyte::Rendering
{
    static constexpr bool IsDebugRenderEnabled = (GRAPHYTE_ENABLE_DEBUG_RENDER != 0);

    class IDebugRenderer
    {
    public:
        virtual ~IDebugRenderer() noexcept = default;

    public:
        virtual void Update(
            float deltaTime
        ) noexcept = 0;

        virtual void Enable(
            bool value
        ) noexcept = 0;

        virtual void Render(
            Graphics::GpuCommandListHandle commandList
        ) noexcept = 0;

    public:
        virtual void Line(
            Math::Vector3 start,
            Math::Vector3 end,
            Math::Color color,
            float width,
            bool ztest,
            float timeout = 0.0F
        ) noexcept = 0;
    };

    extern RENDERING_API std::unique_ptr<IDebugRenderer> CreateDebugRender(
        Graphics::GpuDeviceHandle device
    ) noexcept;
}
