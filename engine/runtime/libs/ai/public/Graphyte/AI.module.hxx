#pragma once
#include <Graphyte/Platform/Impl/Detect.hxx>

#if GRAPHYTE_STATIC_BUILD
#define AI_API
#else
#if defined(module_ai_EXPORTS)
#define AI_API      GX_LIB_EXPORT
#else
#define AI_API      GX_LIB_IMPORT
#endif
#endif
