#pragma once
#include <Graphyte/AI.module.hxx>
#include <Graphyte/BaseObject.hxx>

namespace Graphyte::AI
{
    class AI_API IContext
    {
    public:
        virtual ~IContext() noexcept = default;
    };
}
