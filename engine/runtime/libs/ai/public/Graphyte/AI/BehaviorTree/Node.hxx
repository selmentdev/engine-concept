#pragma once
#include <Graphyte/AI.module.hxx>
#include <Graphyte/BaseObject.hxx>
#include <Graphyte/AI/Context.hxx>

namespace Graphyte::AI::BehaviorTrees
{
    enum class NodeResult
    {
        Success,
        Failure,
        Running,
    };

    class AI_API Node
    {
    public:
        Node() noexcept;
        virtual ~Node() noexcept;

    public:
        virtual NodeResult Evaluate(
            IContext* context
        ) noexcept = 0;
    };

    class AI_API Decorator : public Node
    {
    protected:
        Node* m_Child = nullptr;

    public:
        Decorator() noexcept;

        virtual ~Decorator() noexcept;

    public:
        void SetChild(
            Node* value
        ) noexcept
        {
            m_Child = value;
        }

        Node* GetChild() const noexcept
        {
            return m_Child;
        }
    };

    class AI_API Failer final : public Decorator
    {
    public:
        Failer() noexcept;

        virtual ~Failer() noexcept;

    public:
        virtual NodeResult Evaluate(
            IContext* context
        ) noexcept override
        {
            (void)m_Child->Evaluate(
                context
            );

            return NodeResult::Failure;
        }
    };

    class AI_API Succeeder final : public Decorator
    {
    public:
        Succeeder() noexcept;

        virtual ~Succeeder() noexcept;

    public:
        virtual NodeResult Evaluate(
            IContext* context
        ) noexcept override
        {
            (void)m_Child->Evaluate(
                context
            );

            return NodeResult::Success;
        }
    };

    class AI_API Inverter final : public Decorator
    {
    public:
        Inverter() noexcept;

        virtual ~Inverter() noexcept;

    public:
        virtual NodeResult Evaluate(
            IContext* context
        ) noexcept override
        {
            auto result = m_Child->Evaluate(
                context
            );

            switch (result)
            {
            case NodeResult::Success:
                {
                    return NodeResult::Failure;
                }
            case NodeResult::Failure:
                {
                    return NodeResult::Success;
                }
            default:
                {
                    return result;
                }
            }
        }
    };

    class AI_API Repeater final : public Decorator
    {
    private:
        size_t m_Count;
        size_t m_Limit;

    public:
        explicit Repeater(
            size_t count = 0
        ) noexcept;

        virtual ~Repeater() noexcept;

    public:
        virtual NodeResult Evaluate(
            IContext* context
        ) noexcept override
        {
            for (;;)
            {
                auto result = m_Child->Evaluate(
                    context
                );

                if (result == NodeResult::Running || result == NodeResult::Failure)
                {
                    return result;
                }

                if (m_Limit != 0 && ++m_Count == m_Limit)
                {
                    m_Count = 0;
                    return NodeResult::Success;
                }
            }
        }
    };

    class AI_API Composite : public Node
    {
    protected:
        std::vector<Node*> m_Children;
        size_t m_Index;

    public:
        Composite() noexcept;

        virtual ~Composite() noexcept;

    public:
        void SetChildren(
            const std::vector<Node*>& value
        ) noexcept
        {
            m_Children = value;
        }

        const std::vector<Node*>& GetChildren() noexcept
        {
            return m_Children;
        }

        void SetIndex(
            size_t value
        ) noexcept
        {
            m_Index = value;
        }

        size_t GetIndex() const noexcept
        {
            return m_Index;
        }
    };

    class AI_API Sequence final : public Composite
    {
    public:
        Sequence() noexcept;
        virtual ~Sequence() noexcept;

    public:
        virtual NodeResult Evaluate(
            IContext* context
        ) noexcept override
        {
            for (;;)
            {
                auto node = m_Children[m_Index];

                if (auto status = node->Evaluate(context); status != NodeResult::Success)
                {
                    return status;
                }

                if (++m_Index == m_Children.size())
                {
                    m_Index = 0;
                    return NodeResult::Success;
                }
            }
        }
    };

    class AI_API Selector final : public Composite
    {
    public:
        Selector() noexcept;

        virtual ~Selector() noexcept;

    public:
        virtual NodeResult Evaluate(
            IContext* context
        ) noexcept override
        {
            for (;;)
            {
                auto node = m_Children[m_Index];
                
                if (auto status = node->Evaluate(context); status != NodeResult::Failure)
                {
                    return status;
                }

                if (++m_Index == m_Children.size())
                {
                    m_Index = 0;
                    return NodeResult::Failure;
                }
            }
        }
    };

    class AI_API ParallelSequence final : public Composite
    {
    private:
        size_t m_MinFailure;
        size_t m_MinSuccess;
    public:
        ParallelSequence() noexcept;

        ParallelSequence(
            size_t min_failure,
            size_t min_success
        ) noexcept;

        virtual ~ParallelSequence() noexcept;

    public:
        virtual NodeResult Evaluate(
            IContext* context
        ) noexcept override
        {
            size_t failure_count{ 0 };
            size_t success_count{ 0 };

            for (auto&& child : m_Children)
            {
                auto result = child->Evaluate(context);

                if (result == NodeResult::Success)
                {
                    ++success_count;
                }

                if (result == NodeResult::Failure)
                {
                    ++failure_count;
                }
            }

            if (success_count >= m_MinSuccess)
            {
                return NodeResult::Success;
            }

            if (failure_count >= m_MinFailure)
            {
                return NodeResult::Failure;
            }

            return NodeResult::Running;
        }
    };

        //class Decorator;
            //class Repeater;
            //class UntilFail;
            //class UntilSuccess;
        //class Leaf;
            
}
