#pragma once
#include <Graphyte/AI.module.hxx>
#include <Graphyte/BaseObject.hxx>
#include <Graphyte/AI/Context.hxx>
#include <Graphyte/AI/UtilityAI/Action.hxx>
#include <Graphyte/Span.hxx>
//#include <Graphyte/Entities/Component.hxx>

namespace Graphyte::AI::UtilityAI
{
    using AgentRef = Reference<class Agent>;
    class AI_API Agent// : public Entities::Component
    {
        //GX_DECLARE_TYPEINFO_ABSTRACT(Agent, 0x100000a1a5e65);
    public:
        float m_EvaluationTimeout = 0.0F;
        float m_CurrentActionEvaluationTimeout = 0.0F;
        ActionRef m_CurrentAction = nullptr;
        std::vector<ActionRef> m_Actions;

    public:
        float EvaluationInterval = 1.0F;

    public:
        Agent() noexcept;

        virtual ~Agent() noexcept;

    public:
        ActionRef CurrentAction() const noexcept
        {
            return m_CurrentAction;
        }

    protected:
        void InitializeActions(
            notstd::span<ActionRef> actions
        ) noexcept
        {
            m_Actions.assign(
                std::begin(actions),
                std::end(actions)
            );
        }

        template <typename TIterator>
        void InitializeAgent(
            TIterator first,
            TIterator last
        ) noexcept
        {
            m_Actions.assign(first, last);
        }

        virtual IContext* ProvideContext() noexcept
        {
            return nullptr;
        }

    private:
        ActionRef ChooseBestAction() noexcept;

    protected:
        //virtual void OnStart() noexcept override;
        //virtual void OnUpdate(float delta_time) noexcept override;
    };

}
