#pragma once
#include <Graphyte/AI.module.hxx>
#include <Graphyte/BaseObject.hxx>
#include <Graphyte/AI/Context.hxx>
#include <Graphyte/AI/UtilityAI/Scorer.hxx>

namespace Graphyte::AI::UtilityAI
{
    using ActionRef = Reference<class Action>;
    class AI_API Action : public Object
    {
    public:
        std::vector<ScorerRef> Scorers;
        float Interval;

    public:
        virtual ~Action() noexcept;

    public:
        virtual float Score(
            IContext* context
        ) noexcept;

        virtual void Execute(
            IContext* context
        ) noexcept = 0;

    public:
        virtual void OnEnter(
            IContext* context
        ) noexcept;

        virtual void OnLeave(
            IContext* context
        ) noexcept;
    };

}
