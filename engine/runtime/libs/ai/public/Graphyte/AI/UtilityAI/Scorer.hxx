#pragma once
#include <Graphyte/AI.module.hxx>
#include <Graphyte/BaseObject.hxx>
#include <Graphyte/AI/Context.hxx>

namespace Graphyte::AI::UtilityAI
{
    using ScorerRef = Reference<class Scorer>;
    class AI_API Scorer : public Object
    {
    public:
        virtual ~Scorer() noexcept;

    protected:
        Scorer(const Scorer&) = delete;

        Scorer& operator = (const Scorer&) = delete;

    public:
        virtual float Score(
            IContext* context
        ) noexcept = 0;
    };
}
