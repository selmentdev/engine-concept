project "com.graphyte.ai"
    targetname "com.graphyte.ai"

    language "c++"

    graphyte_module {}

    files {
        "public/**.hxx",
        "private/**.cxx",
        "*.lua",
    }

    filter { "toolset:msc*" }
        pchheader "AI.pch.hxx"
        pchsource "private/AI.pch.cxx"
    
    filter { "kind:SharedLib" }
        defines {
            "module_ai_EXPORTS=1"
        }

    use_com_graphyte_base()
