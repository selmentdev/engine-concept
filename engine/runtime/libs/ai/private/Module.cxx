#include "AI.pch.hxx"
#include <Graphyte/AI.module.hxx>
#include <Graphyte/Modules.hxx>

GX_DECLARE_LOG_CATEGORY(LogEntities, Trace, Trace);
GX_DEFINE_LOG_CATEGORY(LogEntities);

class AIModule final : public Graphyte::IModule
{
public:
    virtual void OnInitialize() noexcept final override
    {
    }

    virtual void OnFinalize() noexcept final override
    {
    }
};

GX_IMPLEMENT_MODULE(AIModule);
