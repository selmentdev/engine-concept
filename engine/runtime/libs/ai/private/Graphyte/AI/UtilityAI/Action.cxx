#include "AI.pch.hxx"
#include <Graphyte/AI/UtilityAI/Action.hxx>

namespace Graphyte::AI::UtilityAI
{
    Action::~Action() noexcept = default;

    float Action::Score(
        IContext* context
    ) noexcept
    {
        float result = 0.0F;

        for (auto const& scorer : this->Scorers)
        {
            result += scorer->Score(
                context
            );
        }

        return result;
    }

    void Action::OnEnter(
        [[maybe_unused]] IContext* context
    ) noexcept
    {
    }

    void Action::OnLeave(
        [[maybe_unused]] IContext* context
    ) noexcept
    {
        (void)context;
    }
}
