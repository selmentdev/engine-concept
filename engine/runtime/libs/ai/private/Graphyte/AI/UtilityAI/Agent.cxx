#include "AI.pch.hxx"
#include <Graphyte/AI/UtilityAI/Agent.hxx>

namespace Graphyte::AI::UtilityAI
{
    Agent::Agent() noexcept = default;
    Agent::~Agent() noexcept = default;

    ActionRef Agent::ChooseBestAction() noexcept
    {
        GX_ASSERTF(!m_Actions.empty(), "Agent must have actions!");

        ActionRef best_action{ nullptr };

        if (!m_Actions.empty())
        {
            IContext* context = this->ProvideContext();
            float best_score = 0.0F;

            for (auto const& action : m_Actions)
            {
                float const action_score = action->Score(context);

                if (action_score > best_score)
                {
                    best_action = action;
                    best_score = action_score;
                }
            }
        }

        return best_action;
    }

    //void Agent::OnStart() noexcept
    //{
    //    m_CurrentAction = this->ChooseBestAction();
    //
    //    if (m_CurrentAction != nullptr)
    //    {
    //        m_CurrentAction->OnEnter(this->ProvideContext());
    //    }
    //}

    //void Agent::OnUpdate(float delta_time) noexcept
    //{
    //    m_EvaluationTimeout -= delta_time;
    //    m_CurrentActionEvaluationTimeout -= delta_time;
    //
    //    if (m_EvaluationTimeout <= 0.0F)
    //    {
    //        m_EvaluationTimeout = EvaluationInterval;
    //
    //        auto best_action = this->ChooseBestAction();
    //
    //        if (best_action != nullptr)
    //        {
    //            if (m_CurrentAction != best_action)
    //            {
    //                auto context = this->ProvideContext();
    //                if (m_CurrentAction != nullptr)
    //                {
    //                    m_CurrentAction->OnLeave(context);
    //                }
    //
    //                m_CurrentAction = best_action;
    //                m_CurrentActionEvaluationTimeout = 0.0F;
    //                m_CurrentAction->OnEnter(context);
    //            }
    //        }
    //    }
    //
    //    if (m_CurrentAction != nullptr && m_CurrentActionEvaluationTimeout <= 0.0F)
    //    {
    //        m_CurrentActionEvaluationTimeout = m_CurrentAction->Interval;
    //        m_CurrentAction->Execute(this->ProvideContext());
    //    }
    //}
}

//GX_DEFINE_TYPEINFO_ABSTRACT(Graphyte::AI::UtilityAI::Agent, Graphyte::Entities::Component);
