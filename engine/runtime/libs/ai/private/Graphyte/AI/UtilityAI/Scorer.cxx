#include "AI.pch.hxx"
#include <Graphyte/AI/UtilityAI/Scorer.hxx>

namespace Graphyte::AI::UtilityAI
{
    Scorer::~Scorer() noexcept = default;
}
