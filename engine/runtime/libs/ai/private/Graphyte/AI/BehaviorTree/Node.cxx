#include "AI.pch.hxx"
#include <Graphyte/AI/BehaviorTree/Node.hxx>

namespace Graphyte::AI::BehaviorTrees
{
    Node::Node() noexcept = default;
    Node::~Node() noexcept = default;

    Decorator::Decorator() noexcept = default;
    Decorator::~Decorator() noexcept = default;

    Failer::Failer() noexcept = default;
    Failer::~Failer() noexcept = default;

    Succeeder::Succeeder() noexcept = default;
    Succeeder::~Succeeder() noexcept = default;

    Inverter::Inverter() noexcept = default;
    Inverter::~Inverter() noexcept = default;

    Repeater::Repeater(size_t count) noexcept
        : m_Count{ count }
        , m_Limit{ 0 }
    {
    }
    Repeater::~Repeater() noexcept = default;

    Composite::Composite() noexcept = default;
    Composite::~Composite() noexcept = default;

    Sequence::Sequence() noexcept = default;
    Sequence::~Sequence() noexcept = default;

    Selector::Selector() noexcept = default;
    Selector::~Selector() noexcept = default;

    ParallelSequence::ParallelSequence() noexcept = default;
    ParallelSequence::~ParallelSequence() noexcept = default;
}
