#include "Graphics.pch.hxx"
#include <Graphyte/Graphics/ImageCodec.hxx>

namespace Graphyte::Graphics
{
    ImageCodec::ImageCodec() noexcept = default;

    ImageCodec::~ImageCodec() noexcept = default;
}
