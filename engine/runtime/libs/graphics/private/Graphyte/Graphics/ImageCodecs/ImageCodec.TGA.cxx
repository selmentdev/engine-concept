#include "Graphics.pch.hxx"
#include <Graphyte/Graphics/ImageCodecs/ImageCodec.TGA.hxx>

namespace Graphyte::Graphics::Impl::TGA
{
    static constexpr const uint8_t TGA_COLOR_MAP_TYPE_NONE = 0;
    static constexpr const uint8_t TGA_COLOR_MAP_TYPE_INCLUDED = 1;

    static constexpr const uint8_t TGA_IMAGE_TYPE_NONE = 0;
    static constexpr const uint8_t TGA_IMAGE_TYPE_COLOR_MAPPED = 1;
    static constexpr const uint8_t TGA_IMAGE_TYPE_TRUE_COLOR = 2;
    static constexpr const uint8_t TGA_IMAGE_TYPE_BLACK_AND_WHITE = 3;
    static constexpr const uint8_t TGA_IMAGE_TYPE_COMPRESSED_COLOR_MAPPED = 9;
    static constexpr const uint8_t TGA_IMAGE_TYPE_COMPRESSED_TRUE_COLOR = 10;
    static constexpr const uint8_t TGA_IMAGE_TYPE_COMPRESSED_BLACK_AND_WHITE = 11;

    static constexpr const uint8_t TGA_IMAGE_DESCIPTOR_ORIGIN_MASK = 0b00110000;
    static constexpr const uint8_t TGA_IMAGE_DESCIPTOR_ORIGIN_BOTTOM_LEFT = 0b00000000;
    static constexpr const uint8_t TGA_IMAGE_DESCIPTOR_ORIGIN_BOTTOM_RIGHT = 0b00010000;
    static constexpr const uint8_t TGA_IMAGE_DESCIPTOR_ORIGIN_TOP_LEFT = 0b00100000;
    static constexpr const uint8_t TGA_IMAGE_DESCIPTOR_ORIGIN_TOP_RIGHT = 0b00110000;

    static constexpr const uint8_t TGA_FOOTER_SIGNATURE[] = { 'T', 'R', 'U', 'E', 'V', 'I', 'S', 'I', 'O', 'N', '-', 'X', 'F', 'I', 'L', 'E' };

#if defined(_MSC_VER)
#pragma pack(push)
#pragma pack(1)
#endif

    struct GX_PACKED TGA_HEADER final
    {
        uint8_t IDLength;
        uint8_t ColorMapType;
        uint8_t ImageType;
        uint16_t ColorMapOrigin;
        uint16_t ColorMapLength;
        uint8_t ColorMapEntrySize;
        uint16_t ImageXOrigin;
        uint16_t ImageYOrigin;
        uint16_t ImageWidth;
        uint16_t ImageHeight;
        uint8_t PixelDepth;
        uint8_t ImageDescriptor;
    };

    static_assert(sizeof(TGA_HEADER) == 18);

    struct GX_PACKED TGA_FOOTER final
    {
        uint32_t ExtensionAreaOffset;
        uint32_t DeveloperDirectoryOffset;
        uint8_t Signature[16];
        uint8_t Dot;
        uint8_t StringTerminator;
    };

    static_assert(sizeof(TGA_FOOTER) == 26);

#if defined(_MSC_VER)
#pragma pack(pop)
#endif
}


namespace Graphyte::Graphics
{
    ImageCodecTGA::ImageCodecTGA() noexcept = default;

    ImageCodecTGA::~ImageCodecTGA() noexcept = default;

    Status ImageCodecTGA::Decode(
        [[maybe_unused]] Storage::Archive& archive,
        [[maybe_unused]] std::unique_ptr<Image>& out_image
    ) noexcept
    {
        return Status::NotImplemented;
    }

    Status ImageCodecTGA::Encode(
        const std::unique_ptr<Image>& image,
        Storage::Archive& archive
    ) noexcept
    {
        if (image != nullptr)
        {
            auto const image_width = image->GetWidth();
            auto const image_height = image->GetHeight();
            auto const image_type = image->GetDimension();
            auto const image_pixel_format = image->GetPixelFormat();

            if (image_width > std::numeric_limits<uint16_t>::max())
            {
                return Status::InvalidFormat;
            }

            if (image_height > std::numeric_limits<uint16_t>::max())
            {
                return Status::InvalidFormat;
            }

            if (image_type != ImageDimension::Texture2D)
            {
                return Status::InvalidFormat;
            }

            uint8_t pixelformat_pixel_depth = 0;
            switch (image_pixel_format)
            {
            case PixelFormat::B8G8R8A8_UNORM:
                pixelformat_pixel_depth = 32;
                break;

            case PixelFormat::B5G6R5_UNORM:
                pixelformat_pixel_depth = 16;
                break;

            default:
                return Status::InvalidFormat;
            }

            if (image_pixel_format != PixelFormat::B8G8R8A8_UNORM)
            {
                return Status::InvalidFormat;
            }

            Impl::TGA::TGA_HEADER header{};
            header.IDLength = 0;
            header.ColorMapType = Impl::TGA::TGA_COLOR_MAP_TYPE_NONE;
            header.ImageType = Impl::TGA::TGA_IMAGE_TYPE_TRUE_COLOR;
            header.ColorMapOrigin = 0;
            header.ColorMapLength = 0;
            header.ColorMapEntrySize = 0;
            header.ImageXOrigin = 0;
            header.ImageYOrigin = 0;
            header.ImageWidth = static_cast<uint16_t>(image->GetWidth());
            header.ImageHeight = static_cast<uint16_t>(image->GetHeight());
            header.PixelDepth = pixelformat_pixel_depth;
            header.ImageDescriptor = Impl::TGA::TGA_IMAGE_DESCIPTOR_ORIGIN_TOP_LEFT | 8;

            archive.Serialize(&header, sizeof(header));

            auto const pixels = image->GetSubresource(0);
            GX_ASSERT(pixels != nullptr);

            archive.Serialize(pixels->Buffer, pixels->Size);

            Impl::TGA::TGA_FOOTER footer{};
            footer.ExtensionAreaOffset = 0;
            footer.DeveloperDirectoryOffset = 0;
            std::memcpy(footer.Signature, Impl::TGA::TGA_FOOTER_SIGNATURE, sizeof(Impl::TGA::TGA_FOOTER_SIGNATURE));
            footer.Dot = '.';
            footer.StringTerminator = '\0';
            archive.Serialize(&footer, sizeof(footer));


            return Status::Success;
        }

        return Status::InvalidArgument;
    }
}
