#include "Graphics.pch.hxx"
#include <Graphyte/Graphics/Image.hxx>
#include <Graphyte/Diagnostics.hxx>
#include <Graphyte/Graphics/ImageCodecs/ImageCodec.PNG.hxx>

#include <png.h>

namespace Graphyte::Graphics
{
    ImageCodecPNG::ImageCodecPNG() noexcept = default;
    ImageCodecPNG::~ImageCodecPNG() noexcept = default;

    Status ImageCodecPNG::Decode(
        Storage::Archive& archive,
        std::unique_ptr<Image>& out_image
    ) noexcept
    {
        GX_ASSERT(archive.IsLoading());
        if (archive.IsLoading())
        {
            png_struct* png = png_create_read_struct_2(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);
            if (png == nullptr)
            {
                return Status::InvalidFormat;
            }

            png_info* info = png_create_info_struct(png);
            if (info == nullptr)
            {
                png_destroy_read_struct(&png, nullptr, nullptr);
                return Status::InvalidFormat;
            }

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4611)
#endif

            // stupid Flanders... I mean libpng
            if (setjmp(png_jmpbuf(png)))
            {
                png_destroy_read_struct(&png, &info, nullptr);
                return Status::InvalidFormat;
            }

#ifdef _MSC_VER
#pragma warning(pop)
#endif

            png_set_read_fn(png, &archive, [](png_struct* context, png_byte* data, size_t size) noexcept -> void
            {
                auto data_archive = reinterpret_cast<Storage::Archive*>(png_get_io_ptr(context));
                data_archive->Serialize(data, size);
            });


            png_read_info(png, info);

            uint32_t image_width = png_get_image_width(png, info);
            uint32_t image_height = png_get_image_height(png, info);
            uint8_t color_type = png_get_color_type(png, info);
            uint8_t bit_depth = png_get_bit_depth(png, info);

            if (bit_depth == 16)
            {
                png_set_strip_16(png);
            }

            if (color_type == PNG_COLOR_TYPE_PALETTE)
            {
                png_set_palette_to_rgb(png);
            }

            if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
            {
                png_set_expand_gray_1_2_4_to_8(png);
            }

            if (png_get_valid(png, info, PNG_INFO_tRNS))
            {
                png_set_tRNS_to_alpha(png);
            }


            if (color_type == PNG_COLOR_TYPE_RGB ||
                color_type == PNG_COLOR_TYPE_GRAY ||
                color_type == PNG_COLOR_TYPE_PALETTE)
            {
                png_set_filler(png, 0xFF, PNG_FILLER_AFTER);
            }

            if (color_type == PNG_COLOR_TYPE_GRAY ||
                color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
            {
                png_set_gray_to_rgb(png);
            }

            png_read_update_info(png, info);

            size_t image_pitch = png_get_rowbytes(png, info);

            out_image = Image::Create2D(Graphyte::Graphics::PixelFormat::R8G8B8A8_UNORM, image_width, image_height);

            auto subresource = out_image->GetSubresource(0);

            std::vector<std::unique_ptr<png_byte[]>> row_pointers{};
            std::vector<png_byte*> row_data{};

            if (image_pitch != subresource->LinePitch)
            {
                //
                // PNG pitch is somehow different from actual image.
                //

                for (uint32_t y = 0; y < image_height; ++y)
                {
                    auto* data = row_pointers.emplace_back(std::make_unique<png_byte[]>(image_pitch)).get();
                    row_data.emplace_back(data);
                }
            }
            else
            {
                //
                // Line pitch matches. Don't allocate any backing memory
                //

                for (uint32_t y = 0; y < image_height; ++y)
                {
                    row_data.emplace_back(subresource->GetScanline<png_byte>(y));
                }
            }

            png_read_image(png, row_data.data());

            if (image_pitch != subresource->LinePitch)
            {
                //
                // Copy scanlines from backing memory back to image.
                //

                for (uint32_t y = 0; y < image_height; ++y)
                {
                    auto scanline = subresource->GetScanline<uint8_t>(y);

                    std::memcpy(scanline, row_data[y], image_pitch);
                }
            }

            png_destroy_read_struct(&png, &info, nullptr);

            return Status::Success;
        }

        return Status::InvalidArgument;
    }

    Status ImageCodecPNG::Encode(const std::unique_ptr<Image>& image, Storage::Archive& archive) noexcept
    {
        GX_ASSERT(archive.IsSaving());
        if (archive.IsSaving())
        {
            GX_ASSERT(image->GetPixelFormat() == PixelFormat::R8G8B8A8_UNORM);

            png_struct* png = png_create_write_struct_2(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr);
            if (png == nullptr)
            {
                return Status::InvalidFormat;
            }

            png_info* info = png_create_info_struct(png);
            if (info == nullptr)
            {
                png_destroy_write_struct(&png, nullptr);
            }

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4611)
#endif

            // stupid Flanders... I mean libpng
            if (setjmp(png_jmpbuf(png)))
            {
                png_destroy_read_struct(&png, &info, nullptr);
                return Status::InvalidFormat;
            }

#ifdef _MSC_VER
#pragma warning(pop)
#endif

            png_set_write_fn(
                png,
                &archive,
                [](png_struct* context, png_byte* data, size_t size) noexcept -> void
                {
                    auto data_archive = reinterpret_cast<Storage::Archive*>(png_get_io_ptr(context));
                    data_archive->Serialize(data, size);
                },
                [](png_struct* context) noexcept -> void
                {
                    auto data_archive = reinterpret_cast<Storage::Archive*>(png_get_io_ptr(context));
                    data_archive->Flush();
                }
            );

            png_set_IHDR(
                png,
                info,
                image->GetWidth(),
                image->GetHeight(),
                8,
                PNG_COLOR_TYPE_RGBA,
                PNG_INTERLACE_NONE,
                PNG_COMPRESSION_TYPE_DEFAULT,
                PNG_FILTER_TYPE_DEFAULT
            );

            png_write_info(png, info);

            auto subresource = image->GetSubresource(0);
            GX_ASSERT(subresource->LinePitch == png_get_rowbytes(png, info));

            auto row_pointers = new png_byte*[subresource->Height];
            for (uint32_t y = 0; y < subresource->Height; ++y)
            {
                row_pointers[y] = subresource->GetScanline<png_byte>(y);
            }

            png_write_image(png, row_pointers);
            png_write_end(png, info);

            delete[] row_pointers;

            png_destroy_write_struct(&png, &info);

            return Status::Success;
        }

        return Status::InvalidArgument;
    }
}
