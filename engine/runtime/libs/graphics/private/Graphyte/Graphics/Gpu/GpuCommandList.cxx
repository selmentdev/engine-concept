#include "Graphics.pch.hxx"
#include <Graphyte/Graphics/Gpu/GpuCommandList.hxx>

namespace Graphyte::Graphics
{
    GpuCommandList::GpuCommandList() noexcept = default;

    GpuCommandList::~GpuCommandList() noexcept = default;
}
