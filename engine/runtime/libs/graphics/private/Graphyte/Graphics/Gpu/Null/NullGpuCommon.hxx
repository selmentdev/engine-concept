#pragma once
#include <Graphyte/Graphics.module.hxx>
#include <Graphyte/Diagnostics.hxx>
#include <Graphyte/String.hxx>

namespace Graphyte::Graphics
{
    GX_DECLARE_LOG_CATEGORY(LogNullRender, Trace, Trace);
}
