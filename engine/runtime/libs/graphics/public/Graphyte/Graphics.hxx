#pragma once
#include <Graphyte/Graphics.module.hxx>

namespace Graphyte::Graphics
{
    GRAPHICS_API void Initialize() noexcept;
    GRAPHICS_API void Finalize() noexcept;
}
