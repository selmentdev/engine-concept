#pragma once
#include <Graphyte/Graphics/ImageCodec.hxx>

namespace Graphyte::Graphics
{
    class GRAPHICS_API ImageCodecPNG : public ImageCodec
    {
    public:
        ImageCodecPNG() noexcept;
        virtual ~ImageCodecPNG() noexcept;

    public:
        virtual Status Decode(Storage::Archive& archive, std::unique_ptr<Image>& out_image) noexcept override;
        virtual Status Encode(const std::unique_ptr<Image>& image, Storage::Archive& archive) noexcept override;
    };
}
