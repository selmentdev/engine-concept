-- core modules
group "engine/modules"

include "base"
include "ai"
include "geometry"
include "rendering"
include "graphics"
include "entities"
include "framework"
include "launch"
include "ui"

-- graphics backend
group "engine/modules/graphics"

include "graphics-d3d11"
include "graphics-d3d12"
include "graphics-opengl"
include "graphics-vulkan"

-- plugins
group "engine/plugins"
