project "rapidjson"
    kind "none"
    language "C"
    files {
        "include/**.h",
        "**.lua"
    }
