project "freetype"
    language "C"

    files {
        "include/**.h",
        "**.lua",
        "source/ftdebug.c",
        "source/winfonts/winfnt.c",
        "source/type42/type42.c",
        "source/cid/type1cid.c",
        "source/type1/type1.c",
        "source/truetype/truetype.c",
        "source/smooth/smooth.c",
        "source/sfnt/sfnt.c",
        "source/raster/raster.c",
        "source/psnames/psmodule.c",
        "source/pshinter/pshinter.c",
        "source/psaux/psaux.c",
        "source/pfr/pfr.c",
        "source/pcf/pcf.c",
        "source/base/ftsystem.c",
        "source/base/ftver.rc",
        "source/lzw/ftlzw.c",
        "source/base/ftinit.c",
        "source/gzip/ftgzip.c",
        "source/cache/ftcache.c",
        "source/base/ftbase.c",
        "source/cff/cff.c",
        "source/bdf/bdf.c",
        "source/autofit/autofit.c",
        "source/base/ftwinfnt.c",
        "source/base/fttype1.c",
        "source/base/ftsynth.c",
        "source/base/ftstroke.c",
        "source/base/ftpfr.c",
        "source/base/ftpatent.c",
        "source/base/ftotval.c",
        "source/base/ftmm.c",
        "source/base/ftgxval.c",
        "source/base/ftglyph.c",
        "source/base/ftgasp.c",
        "source/base/ftfstype.c",
        "source/base/ftcid.c",
        "source/base/ftbitmap.c",
        "source/base/ftbdf.c",
        "source/base/ftbbox.c",
    }
    includedirs {
        "include"
    }
    defines {
        "FT2_BUILD_LIBRARY=1",
        "ZLIB_DLL=1",
        "WIN32",
    }
    removedefines {
        "DEBUG"
    }

    filter { "system:not windows" }
        removefiles {
            "**/*.rc",
        }

    filter { "toolset:msc*" }
        disablewarnings {
            "4100",
            "4013",
            "4018",
            "4996",
            "4267",
            "4244",
            "4312",
            "4431",
            "4131",
            "4477",
            "4701",
            --"2129",
            --"2373",
        }
        
    filter { "toolset:gcc*" }
        disablewarnings {
            "implicit-function-declaration",
            "cast-function-type",
            "implicit-fallthrough",
            "maybe-uninitialized",
        }

    filter { "kind:SharedLib" }
        defines {
        }

    filter { "toolset:msc*" }
        ufinclude "false"
