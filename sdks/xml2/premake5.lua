project "libxml2"
    targetname "xml2"

    language "C"
    files {
        "include/**.h",
        "source/**.c",
        "source/**.h",
        "**.lua"
    }
    includedirs {
        "include",
    }

    filter { "kind:SharedLib", "toolset:msc*" }
        defines {
        }

    filter { "toolset:msc*" }
        defines {
        }
        disablewarnings {
            "4132",
            "4047",
            "4090",
            "4100",
            "4127",
            "4133",
            "4244",
            "4245",
            "4267",
            "4057",
            "4152",
            "4295",
            "4311",
            "4456",
            "4459",
            "4700",
            "4701",
            "4702",
            "4706",
            "4996",
            "4232", -- serious one, may result in weird crashes
        }
        
    filter { "toolset:gcc*" }
        disablewarnings {
            "unused-but-set-variable",
            "format",
            "unused-const-variable",
            "implicit-fallthrough",
            "sign-compare",
            "unused-parameter",
            "implicit-function-declaration",
            "format-extra-args",
            "cast-function-type",
            "unused-function",
        }

    filter { "toolset:clang*" }
        warnings "off"

        
    filter { "system:linux" }
        removefiles {
            "source/trio.c",
        }
        defines {
        }
        links {
        }

    filter { "toolset:msc*" }
        ufinclude "false"
