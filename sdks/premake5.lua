group 'sdks'

include 'fmt'
--include 'lua'
include 'lz4'
include 'png'
--include 'sqlite'
include 'libvulkanvma'
--include 'yaml'
include 'zlib'
include 'glad'
--include 'freetype'
include 'curl'
include 'mbedtls'
--include 'xml2'
--include 'pugixml'
--include 'mpack'

if os.target() == "windows" then
    -- GMake will try to build them :/
    include 'catch2'
    --include 'sol2'
    --include 'nlohmann_json'
    include 'RapidJSON'
end
