project "nlohmann_json"
    kind "none"
    language "C"
    files {
        "include/**.hpp",
        "**.lua"
    }
