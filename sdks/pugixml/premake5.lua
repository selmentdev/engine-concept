project "pugixml"
    language "C++"

    files {
        "include/**.hpp",
        "source/**.cpp",
        "**.lua"
    }
    
    includedirs {
        "include"
    }

    filter { "kind:SharedLib", "toolset:msc*" }
        defines {
            "PUGIXML_API=__declspec(dllexport)",
            "PUGIXML_CLASS=__declspec(dllexport)"
        }

    filter{}