project "mbedtls"
    language "C"

    kind "StaticLib"

    files {
        "include/**.h",
        "source/**.c",
        "**.lua"
    }

    includedirs {
        "include",
    }

    filter { "toolset:msc*" }
        disablewarnings {
            "4310",
            "4127",
            "4132",
            "4244",
            "4245",
            "4701",
            "4389",
        }

    filter { "toolset:msc*" }
        ufinclude "false"
