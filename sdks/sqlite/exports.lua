filter {}
    links {
        "sqlite",
    }
    
    includedirs {
        "%{wks.location}/sdks/sqlite/include"
    }

filter { "toolset:msc*" }
    defines {
        "SQLITE_API=__declspec(dllimport)"
    }
