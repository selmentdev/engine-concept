project "sqlite"
    language "C"
    files {
        "include/**.h",
        "source/**.c",
        "**.lua"
    }
    includedirs {
        "include",
    }

    floatingpoint "default"

    filter { "toolset:clang*" }
        disablewarnings {
            "implicit-function-declaration",
            "cast-function-type",
            "implicit-fallthrough",
        }
        
    filter { "toolset:gcc*" }
        disablewarnings {
            "implicit-function-declaration",
            "cast-function-type",
            "implicit-fallthrough",
        }
    
    filter { "toolset:msc*" }
        defines {
            "SQLITE_API=__declspec(dllexport)"
        }

    filter { "toolset:clang*" }
        warnings "off"
