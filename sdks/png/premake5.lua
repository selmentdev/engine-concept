project "png"
    language "C"
    kind "StaticLib"
    files {
        "include/*.h",
        "source/*.c",
        "source/*.h",
        "**.lua"
    }

--    removefiles {
--        "source/arm/*.c",
--        "source/arm/*.S",
--    }

    includedirs {
        "include"
    }
    
    dofile "/sdks/zlib/exports.lua"

    filter { "kind:SharedLib", "toolset:msc*" }
        defines {
            "LUA_BUILD_AS_DLL=1",
        }

    filter { "toolset:msc*" }
        disablewarnings {
            "4100",
            "4127",
            "4131",
            "4244",
            "4245",
            "4267",
            "4456",
            "4457",
            "4701",
            "4702",
            "4706",
            "4996",
        }
        
    filter { "toolset:gcc*" }
        disablewarnings {
            "unused-parameter",
            "sign-compare",
        }

    filter { "platforms:arm*" }
        files {
            "source/arm/*.c",
        }

    filter { "platforms:arm*", "toolset:gcc*" }
        files {
            "source/arm/*.S",
        }

    filter { "platforms:x86 or x64" }
        files {
            "source/intel/*.c",
            "source/intel/*.S",
        }

    filter { "toolset:clang*" }
        warnings "off"

    filter {}
