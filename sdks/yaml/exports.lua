filter {}
    links {
        "yaml",
    }

    includedirs {
        "%{wks.location}/sdks/yaml/include"
    }

    defines {
        "YAML_DECLARE_STATIC=1",
    }
