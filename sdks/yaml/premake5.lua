project "yaml"
    language "C"
    kind "StaticLib"
    
    files {
        "include/**.h",
        "source/**.c",
        "**.lua"
    }

    defines {
        "YAML_DECLARE_STATIC=1",
    }

    includedirs {
        "include",
    }

    filter { "toolset:msc*" }
        disablewarnings {
            "4100",
            "4244",
            "4245",
            "4267",
            "4456",
            "4457",
            "4701",
            "4702",
            "4706",
            "4996",
        }
    filter { "toolset:clang*" }
        disablewarnings {
            "implicit-function-declaration",
            "int-to-pointer-cast",
        }
    filter { "toolset:gcc*" }
        disablewarnings {
            "implicit-function-declaration",
            "int-to-pointer-cast",
            "unused-value",
        }

    filter { "toolset:msc*" }
        ufinclude "false"
