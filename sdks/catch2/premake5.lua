project "catch2"
    kind "none"

    language "C++"

    files {
        "include/**.hpp",
        "**.lua"
    }
