project "curl"
    language "C"
    files {
        "include/**",
        "source/**",
        "**.lua"
    }
    includedirs {
        "include",
        "source",
    }

    defines {
        "BUILDING_LIBCURL",
        "USE_IPV6",
    }
    
    tags {
        "sdks-mbedtls",
        "sdks-zlib",
    }

    filter { "configurations:debug" }
        defines {
            "DEBUGBUILD",
        }
    

    filter { "system:windows" }
        defines {
            "USE_WINDOWS_SSPI",
            "USE_WIN32_IDN",
            "USE_SCHANNEL",
            "WANT_IDN_PROTOTYPES",
        }

        links {
            "ws2_32",
            "wldap32",
            "crypt32",
            "normaliz",
            "advapi32",
        }

    filter { "system:not windows" }
        removefiles {
            "**/*.rc",
        }

    filter { "system:linux" }
        cdialect "gnu11"
        defines {
            "HAVE_CONFIG_H",
            "USE_MBEDTLS",
        }

    filter { "kind:StaticLib" }
        defines {
            "CURL_STATICLIB",
        }
        
    filter { "toolset:msc*" }
        disablewarnings {
            "4090",
            "4127",
        }

    filter { "toolset:clang*" }
        warnings "off"

    filter { "toolset:msc*" }
        ufinclude "false"
