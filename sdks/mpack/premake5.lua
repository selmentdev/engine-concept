project "mpack"
    language "C"

    kind "StaticLib"

    files {
        "include/**.h",
        "include/**.c",
        "**.lua"
    }

    includedirs {
        "include",
    }

    filter { "toolset:msc*" }
        disablewarnings {
            "4127",
            "4996",
            "4310",
            "4702",
        }
