project "zlib"
    language "C"
    files {
        "include/**.h",
        "source/**.c",
        "**.lua"
    }

    includedirs {
        "include",
    }

    defines {
        "ZLIB_CONST=1",
        "STDC=1",
    }

    filter { "toolset:msc*" } -- "platforms:shared"
        defines {
            "ZLIB_DLL=1",
        }

    filter { "system:linux or system:android" }
        defines {
            "Z_HAVE_UNISTD_H=1",
        }

    filter { "toolset:msc*" }
        disablewarnings {
            "4100",
            "4127",
            "4131",
            "4244",
            "4245",
            "4267",
            "4456",
            "4457",
            "4701",
            "4702",
            "4706",
            "4996",
        }
        
    filter { "toolset:gcc*" }
        disablewarnings {
            "implicit-fallthrough",
        }

    filter { "toolset:msc*" }
        ufinclude "false"
