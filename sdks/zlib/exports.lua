filter {}
    links {
        "zlib",
    }
    
    includedirs {
        "%{wks.location}/sdks/zlib/include"
    }

    defines {
        "ZLIB_CONST=1",
        "STDC=1",
    }

    filter { "toolset:msc*" } -- "platforms:shared", 
        defines {
            "ZLIB_DLL=1",
        }

    filter { "system:linux or system:android" }
        defines {
            "Z_HAVE_UNISTD_H=1",
        }
