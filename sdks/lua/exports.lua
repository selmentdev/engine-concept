filter { "kind:SharedLib", "toolset:msc*" }
    defines {
        "LUA_BUILD_AS_DLL=1",
    }

filter { "system:linux" }
    defines {
        "LUA_USE_LINUX=1"
    }
    links {
        "dl",
        "m",
    }

filter {}
    links {
        "lua",
    }
    includedirs {
        "%{wks.location}/sdks/lua/include"
    }
