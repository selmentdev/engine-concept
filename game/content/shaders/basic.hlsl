struct INPUT_GPU_VERTEX_UI
{
    float3 Position : POSITION;
    float2 UV_0     : TEXCOORD0;
    float2 UV_1     : TEXCOORD1;
    float2 UV_2     : TEXCOORD2;
    float2 UV_3     : TEXCOORD3;
    float4 Color    : COLOR;
};

struct INPUT_GPU_VERTEX_COMPACT
{
    float3 Position : POSITION;
    float3 Normal   : NORMAL;
    float2 UV       : TEXCOORD;
    float3 Tangent  : TANGENT;
};

struct INPUT_GPU_VERTEX_COMPLEX
{
    float3 Position : POSITION;
    float3 Normal   : NORMAL;
    float3 Tangent  : TANGENT;
    float2 UV_0     : TEXCOORD0;
    float2 UV_1     : TEXCOORD1;
    float4 Color    : COLOR;
};

struct OUTPUT_GPU_VERTEX_COMPACT
{
    float4 Position : SV_POSITION;
    float2 UV_0     : TEXCOORD0;
    float3 Normal   : NORMAL;
};


//
//
//

Texture2D DiffuseTexture : register(t0);
SamplerState LinearSampler : register(s0);

cbuffer GlobalData : register(b0)
{
    float4x4 Camera_View;
    float4x4 Camera_Projection;
    float4x4 Camera_ViewProjection;
};

cbuffer ObjectData : register(b1)
{
    float4x4 Object_World;
    float4x4 Object_InverseWorld;
};

OUTPUT_GPU_VERTEX_COMPACT vs_main(INPUT_GPU_VERTEX_COMPLEX input)
{
    float4 position = float4(input.Position, 1.0F);

    float4x4 world_view = mul(Camera_View, Object_World);
    float4x4 world_view_projection = mul(Camera_Projection, world_view);

    OUTPUT_GPU_VERTEX_COMPACT output;
    output.Position = mul(world_view_projection, position);//position - float4(0.0F, 0.0F, -2.0F, 0.0F);// 
    output.UV_0 = input.UV_0;
    output.Normal = mul(input.Normal, (float3x3)Object_InverseWorld);

    return output;
}

struct OUTPUT_OM
{
    float4 Color : SV_TARGET0;
};

OUTPUT_OM ps_main(OUTPUT_GPU_VERTEX_COMPACT input)
{
    OUTPUT_OM result;
    result.Color = DiffuseTexture.Sample(LinearSampler, input.UV_0);
    if (result.Color.a < 0.7F)
    {
        discard;
    }

    return result;
}
