#version 450 core

layout(location = 0) in vec3 POSITION;
layout(location = 1) in vec3 NORMAL;
layout(location = 2) in vec3 TANGENT;
layout(location = 3) in vec2 TEXCOORD0;
layout(location = 4) in vec2 TEXCOORD1;
layout(location = 5) in vec4 COLOR;

layout(binding = 0) uniform global_data
{
    mat4 Camera_View;
    mat4 Camera_Projection;
    mat4 Camera_ViewProjection;
} GlobalData;

layout(binding = 1) uniform object_data
{
    mat4 Object_World;
    mat4 Object_InverseWorld;
} ObjectData;

layout(location = 0) out vec4 common_POSITION;
layout(location = 1) out vec2 common_TEXCOORD0;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    mat4 wv = GlobalData.Camera_View * ObjectData.Object_World;
    mat4 wvp = GlobalData.Camera_Projection * wv;
    gl_Position = wvp * vec4(POSITION, 1.0F);
    common_TEXCOORD0 = TEXCOORD0;
        
    //common_POSITION = vec4(POSITION, 1.0F) + vec4(0.0F, 0.0F, 2.0F, 0.0F);
}