#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec4 common_POSITION;
layout(location = 1) in vec2 common_TEXCOORD0;

layout(location = 0) out vec4 OUT_COLOR;

layout(binding = 0)

uniform sampler2D DiffuseTextureSampler;


void main()
{
    vec4 texColor = texture(DiffuseTextureSampler, common_TEXCOORD0);
    OUT_COLOR = texColor;
    //OUT_COLOR = vec4(1, 1, 1, 1);
}
