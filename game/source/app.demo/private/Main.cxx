#include <Graphyte/Base.module.hxx>
#include <Graphyte/Diagnostics/Stopwatch.hxx>
#include <Graphyte/Base64.hxx>
#include <Graphyte/Storage/IFileSystem.hxx>
#include <Graphyte/Storage/Path.hxx>
#include <Graphyte/Threading/Thread.hxx>
#include <Graphyte/Storage/ArchiveFileWriter.hxx>
#include <Graphyte/Storage/ArchiveFileReader.hxx>
#include <Graphyte/Storage/ArchiveMemoryReader.hxx>
#include <Graphyte/Storage/FileManager.hxx>
#include <Graphyte/Network.hxx>
#include <Graphyte/Network/IpEndPoint.hxx>
#include <Graphyte/Diagnostics.hxx>
#include <Graphyte/AI/UtilityAI/Agent.hxx>
#include <Graphyte/Application.hxx>
#include <Graphyte/Platform.hxx>
#include <Graphyte/String.hxx>
#include <Graphyte/Unicode.hxx>
#include <Graphyte/Console.hxx>

#include <Graphyte/Graphics/Image.hxx>
#include <Graphyte/Graphics/ImageCodecs/ImageCodec.DDS.hxx>
#include <Graphyte/Graphics/ImageCodecs/ImageCodec.PNG.hxx>
#include <Graphyte/Graphics/ImageCodecs/ImageCodec.TGA.hxx>
#include <Graphyte/Math/Noise/SimplexNoise.hxx>
#include <Graphyte/Math/Noise/PerlinNoise.hxx>
#include <Graphyte/Math/Vector3.hxx>
#include <Graphyte/Math/Vector4.hxx>
#include <Graphyte/Math/Matrix.hxx>
#include <Graphyte/Math/Quaternion.hxx>

#include <Graphyte/Graphics/ImageHistogram.hxx>
#include <Graphyte/Bitwise.hxx>
#include <Graphyte/Platform.hxx>
#include <Graphyte/Math/Scalar.hxx>

#include <Graphyte/Graphics/Gpu/GpuDevice.hxx>
#include <Graphyte/Bitwise.hxx>
#include <Graphyte/ByteAccess.hxx>
#include <Graphyte/Math/Color.hxx>

Graphyte::Application::ApplicationDescriptor GraphyteApp
{
    "Train Simulator",
    "train.simulator",
    "Graphyte",
    Graphyte::Application::ApplicationType::Game,
    Graphyte::Version{ 1, 0, 0, 0 }
};

#include <Graphyte/Launch/Main.hxx>

#include <Graphyte/Framework/GameEngine.hxx>
#include <Graphyte/Framework/GameInstance.hxx>
#include <Graphyte/Framework/Level.hxx>
#include <Graphyte/Framework/Actors/CameraActor.hxx>
#include <Graphyte/Framework/Actors/StaticMeshActor.hxx>

GX_DECLARE_LOG_CATEGORY(LogGame, Trace, Trace);
GX_DEFINE_LOG_CATEGORY(LogGame);

#if GRAPHYTE_PLATFORM_WINDOWS

#include <Graphyte/Platform/Impl.Windows/Windows.Helpers.hxx>

namespace Graphyte::Diagnostic
{
    class DebugHid final
    {
    private:
        static Float2 g_CursorPosition;
        static std::array<std::byte, 256> g_KeyboardState;

    public:
        static void Initialize() noexcept;
        static void Finalize() noexcept;

        static void Update() noexcept;

        static Float2 GetCursorPosition() noexcept
        {
            return g_CursorPosition;
        }

        static bool IsKeyDown(Input::KeyCode key) noexcept
        {
            return g_KeyboardState[static_cast<size_t>(key)] != std::byte{};
        }
    };

    void DebugHid::Initialize() noexcept
    {
    }

    void DebugHid::Finalize() noexcept
    {
    }

    void DebugHid::Update() noexcept
    {
        POINT position;
        if (::GetCursorPos(&position) != FALSE)
        {
            g_CursorPosition.X = static_cast<float>(position.x);
            g_CursorPosition.Y = static_cast<float>(position.y);
        }

        //::GetKeyboardState(
        //    reinterpret_cast<BYTE*>(std::data(g_KeyboardState))
        //);
    }

    Float2 DebugHid::g_CursorPosition{};
    std::array<std::byte, 256> DebugHid::g_KeyboardState{};
}

#endif

namespace Simulator
{
    class Instance : public Graphyte::Framework::GameInstance
    {
    public:
        Instance() noexcept = default;
        ~Instance() noexcept = default;
    public:
        void Initialize() noexcept final
        {
            m_World = std::make_unique<Graphyte::Framework::World>();

            auto level = new Graphyte::Framework::Level();
            level->Initialize();

            auto camera = new Graphyte::Framework::CameraActor();
            camera->Initialize();

            auto mesh = new Graphyte::Framework::StaticMeshActor();
            mesh->Initialize();

            level->Actors.push_back(camera);
            level->Actors.push_back(mesh);

            m_World->Levels.push_back(level);
        }

        void Finalize() noexcept final
        {
            m_World->Finalize();
            m_World = nullptr;
        }

        void Tick(float deltaTime) noexcept final
        {
            GameInstance::Tick(deltaTime);
        }
    };
}

namespace Simulator
{
    struct SomeComponent
    {
        static constexpr uint32_t TypeHandle = 0xdeadf00f;
    };
}

int GraphyteMain(
    [[maybe_unused]] int argc,
    [[maybe_unused]] char** argv
) noexcept
{
    auto engine = std::make_unique<Graphyte::Framework::GameEngine>(std::make_unique<Simulator::Instance>());

    Graphyte::Application::SetEventHandler(engine.get());

    GX_LOG(LogGame, Trace, "DEV_LOAD_GAME: {}",
        Graphyte::Console::Execute(
            "dev_LoadGame",
            notstd::span<std::string_view>{}
        )
    );

    engine->Initialize();

    Graphyte::Diagnostics::Stopwatch stopwatch{};

    float elapsed = 1.0F / 30.0F;

    while (!Graphyte::Application::IsRequestingExit())
    {
        stopwatch.Restart();

        bool idle = true;
        if (Graphyte::Application::IsForeground())
        {
            idle = false;
        }

        if (idle)
        {
            Graphyte::Threading::Thread::Sleep(100);
        }

        Graphyte::Application::PollInputDevices(elapsed);
        Graphyte::Application::PumpMessages(elapsed);
        Graphyte::Application::Tick(elapsed);

        engine->Tick(elapsed, idle);

        stopwatch.Stop();
        elapsed = static_cast<float>(stopwatch.GetElapsedTime());
    }

    engine->Finalize();
    engine = nullptr;

    return 0;
}

